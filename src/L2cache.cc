#include "noc-sim.hh"


L2cache::L2cache(int x, int y)
{
    conf = Conf_ReRAM::get();
    stats = Stats::get();

    output = nullptr;
    mem_out = nullptr;
    dram_out = nullptr;

    x_coord = x;
    y_coord = y;
    x_extent = conf->x_max;
    y_extent = conf->y_max;

    if (conf->ram_mode == l2_cpu)
        active = !conf->is_ram_tile(x, y);
    else if (conf->ram_mode == l2_reram || conf->ram_mode == het_l2_reram)
        active = conf->is_ram_tile(x, y, reram_tile);
    else
        active = true;

    out_routed = -1;
    in_routed = -1;
    mem_out_routed = -1;
    mem_in_routed = -1;
    dram_out_routed = -1;
    dram_in_routed = -1;

    last_out_routed = 0;
    last_mem_out_routed = 0;
    last_dram_out_routed = 0;

    if (active)
    {
        if (conf->l2_settings.banks > 1)
        {
            multibank_setup();
        }
        else
        {
            singlebank_setup();
        }
    }
    else
    {
        input = nullptr;
        multibank = false;
        banks = 0;
        cache_banks = nullptr;
        cache_inputs = nullptr;
        cache_outputs = nullptr;
        cache_mem_in = nullptr;
        cache_mem_out = nullptr;
        cache_dram_in = nullptr;
        cache_dram_out = nullptr;
    }

}

void L2cache::multibank_setup()
{
    multibank = true;
    banks = conf->l2_settings.banks;
    input = new buffer(conf->buffer_size);

    cache_banks = new L2cache_bank*[banks];

    if (conf->integrate_cache_rram)
    {
        mem_in = nullptr;
        
        if (conf->ram_mode != het_l2_reram)
        {
            if (conf->l2_settings.pipelined)
            {
                for(int i = 0; i < banks; i++)
                    cache_banks[i] = new L2cache_reram_pipeline(x_coord, y_coord);
            }
            else
            {
                for (int i = 0; i < banks; i++)
                    cache_banks[i] = new L2cache_reram(x_coord, y_coord);
            }
        }
        else
        {
            //TODO: make hetereogenous cache
        }

        cache_mem_in = nullptr;
        cache_mem_out = nullptr;
    }
    else
    {
        mem_in = new buffer(conf->buffer_size);

        if (conf->ram_mode != het_l2_reram)
        {
            if (conf->l2_settings.pipelined)
            {
                for (int i = 0; i < banks; i++)
                    cache_banks[i] = new L2cache_standalone_pipeline(x_coord, y_coord);
            }
            else
            {
                for (int i = 0; i < banks; i++)
                    cache_banks[i] = new L2cache_standalone(x_coord, y_coord);
            }
        }
        else
        {
            //make hetereogenous cache
            for (int i = 0; i < banks; i++)
                cache_banks[i] = new L2cache_het_pipeline(x_coord, y_coord);

        }

        //set up memory request network
        cache_mem_in = new buffer * [banks];
        cache_mem_out = new buffer * [banks];
        for (int i = 0; i < banks; i++)
        {
            cache_mem_in[i] = cache_banks[i]->get_mem_in_buffer();
            cache_mem_out[i] = new buffer(conf->buffer_size);
            cache_banks[i]->set_mem_out_buffer(cache_mem_out[i]);
        }
    }

    //set up CPU request network
    cache_inputs = new buffer * [banks];
    cache_outputs = new buffer * [banks];
    for (int i = 0; i < banks; i++)
    {
        cache_inputs[i] = cache_banks[i]->get_input_buffer();
        cache_outputs[i] = new buffer(conf->buffer_size);
        cache_banks[i]->set_output_buffer(cache_outputs[i]);
    }

    //set up dram request network
    if (conf->ram_mode == het_l2_reram)
    {
        dram_in = new buffer(conf->buffer_size);
        cache_dram_in = new buffer * [banks];
        cache_dram_out = new buffer * [banks];

        for (int i = 0; i < banks; i++)
        {
            cache_dram_in[i] = cache_banks[i]->get_dram_in_buffer();
            cache_dram_out[i] = new buffer(conf->buffer_size);
            cache_banks[i]->set_dram_out_buffer(cache_dram_out[i]);
        }
    }
    else
    {
        dram_in = nullptr;
        cache_dram_in = nullptr;
        cache_dram_out = nullptr;
    }
}

void L2cache::singlebank_setup()
{
    multibank = false;
    banks = 1;

    cache_banks = new L2cache_bank * [banks];

    if (conf->integrate_cache_rram)
    {
        if (conf->ram_mode != het_l2_reram)
        {
            if (conf->l2_settings.pipelined)
            {
                cache_banks[0] = new L2cache_reram_pipeline(x_coord, y_coord);
            }
            else
            {
                cache_banks[0] = new L2cache_reram(x_coord, y_coord);
            }
        }
        else
        {
            //TODO: make hetereogenous cache
        }
    }
    else
    {
        if (conf->ram_mode != het_l2_reram)
        {
            if (conf->l2_settings.pipelined)
            {
                cache_banks[0] = new L2cache_standalone_pipeline(x_coord, y_coord);
            }
            else
            {
                cache_banks[0] = new L2cache_standalone(x_coord, y_coord);
            }
        }
        else
        {
            //make hetereogenous cache
            cache_banks[0] = new L2cache_het_pipeline(x_coord, y_coord);
        }
    }

    input = cache_banks[0]->get_input_buffer();
    mem_in = cache_banks[0]->get_mem_in_buffer();
    dram_in = cache_banks[0]->get_dram_in_buffer();

    cache_inputs = nullptr;
    cache_outputs = nullptr;
    cache_mem_in = nullptr;
    cache_mem_out = nullptr;
    cache_dram_in = nullptr;
    cache_dram_out = nullptr;
}

void L2cache::set_output_buffer(buffer* out)
{
    if (!multibank && active)
        cache_banks[0]->set_output_buffer(out);

    output = out;
}

void L2cache::set_mem_out_buffer(buffer* out)
{
    if (!multibank && active)
        cache_banks[0]->set_mem_out_buffer(out);

    mem_out = out;
}

void L2cache::set_dram_out_buffer(buffer* out)
{
    if (!multibank && active)
        cache_banks[0]->set_dram_out_buffer(out);

    dram_out = out;
}

void L2cache::cycle(long long cur_cycle)
{
    if (active)
    {
        if (multibank)
        {
            //do routing
            route_in(cur_cycle);
            route_out(cur_cycle);
            if (!conf->integrate_cache_rram)
            {
                route_mem_in(cur_cycle);
                route_mem_out(cur_cycle);
            }

            if (conf->ram_mode == het_l2_reram)
            {
                route_dram_in(cur_cycle);
                route_dram_out(cur_cycle);
            }

            //cycle banks
            for (int i = 0; i < banks; i++)
                cache_banks[i]->cycle(cur_cycle);
        }
        else
        {
            cache_banks[0]->cycle(cur_cycle);
        }
    }
}

void L2cache::route_in(long long cur_cycle)
{
    uint64 flit;

    if (input->is_empty()) return;

    while (!input->is_empty())
    {
        if (in_routed < 0)  //not currently routed
        {
            if (input->entries() > 1)
            {
                flit = input->peak_at_second();
                in_routed = CACHE_BANK_ID(flit);
            }
            else  //don't have address yet
            {
                return;
            }
        }
        else
        {
            if (cache_inputs[in_routed]->is_full()) return;

            flit = input->remove_from_head();
            cache_inputs[in_routed]->insert_at_tail(flit);

            if (flit & TAIL_FLIT_MASK)
                in_routed = -1;  //finished packet
        }
    }
}


void L2cache::route_mem_in(long long cur_cycle)
{
    uint64 flit;

    if (mem_in->is_empty()) return;

    while (!mem_in->is_empty())
    {
        if (mem_in_routed < 0)  //not currently routed
        {
            if (mem_in->entries() > 1)
            {
                flit = mem_in->peak_at_second();
                mem_in_routed = CACHE_BANK_ID(flit);
            }
            else  //don't have address yet
            {
                return;
            }
        }
        else
        {
            if (cache_mem_in[mem_in_routed]->is_full()) return;

            flit = mem_in->remove_from_head();
            cache_mem_in[mem_in_routed]->insert_at_tail(flit);

            if (flit & TAIL_FLIT_MASK)
                mem_in_routed = -1;  //finished packet
        }
    }
}

void L2cache::route_dram_in(long long cur_cycle)
{
    uint64 flit;

    if (dram_in->is_empty()) return;

    while (!dram_in->is_empty())
    {
        if (dram_in_routed < 0)  //not currently routed
        {
            if (dram_in->entries() > 1)
            {
                flit = dram_in->peak_at_second();
                dram_in_routed = CACHE_BANK_ID(flit);
            }
            else  //don't have address yet
            {
                return;
            }
        }
        else
        {
            if (cache_dram_in[dram_in_routed]->is_full()) return;

            flit = dram_in->remove_from_head();
            cache_dram_in[dram_in_routed]->insert_at_tail(flit);

            if (flit & TAIL_FLIT_MASK)
                dram_in_routed = -1;  //finished packet
        }
    }
}

void L2cache::route_out(long long cur_cycle)
{
    uint64 flit;

    if (output->is_full()) return;

    while (!output->is_full())
    {
        if (out_routed < 0)
        {
            //find next packet
            for (int i = 0; i < banks; i++)
            {
                int bank = (last_out_routed + i) % banks;
                if (!cache_outputs[bank]->is_empty())
                {
                    out_routed = bank;
                    last_out_routed = bank + 1;
                    break;
                }
            }

            //no outstanding requests
            if (out_routed < 0) return;
        }
        else
        {
            if (cache_outputs[out_routed]->is_empty()) return;

            flit = cache_outputs[out_routed]->remove_from_head();
            output->insert_at_tail(flit);

            if (flit & TAIL_FLIT_MASK)
                out_routed = -1;
        }
    }
}

void L2cache::route_mem_out(long long cur_cycle)
{
    uint64 flit;

    if (mem_out->is_full()) return;

    while (!mem_out->is_full())
    {
        if (mem_out_routed < 0)
        {
            //find next packet
            for (int i = 0; i < banks; i++)
            {
                int bank = (last_mem_out_routed + i) % banks;
                if (!cache_mem_out[bank]->is_empty())
                {
                    mem_out_routed = bank;
                    last_mem_out_routed = bank + 1;
                    break;
                }
            }

            //no outstanding requests
            if (mem_out_routed < 0) return;
        }
        else
        {
            if (cache_mem_out[mem_out_routed]->is_empty()) return;

            flit = cache_mem_out[mem_out_routed]->remove_from_head();
            mem_out->insert_at_tail(flit);

            if (flit & TAIL_FLIT_MASK)
                mem_out_routed = -1;
        }
    }
}

void L2cache::route_dram_out(long long cur_cycle)
{
    uint64 flit;

    if (dram_out->is_full()) return;

    while (!dram_out->is_full())
    {
        if (dram_out_routed < 0)
        {
            //find next packet
            for (int i = 0; i < banks; i++)
            {
                int bank = (last_dram_out_routed + i) % banks;
                if (!cache_dram_out[bank]->is_empty())
                {
                    dram_out_routed = bank;
                    last_dram_out_routed = bank + 1;
                    break;
                }
            }

            //no outstanding requests
            if (dram_out_routed < 0) return;
        }
        else
        {
            if (cache_dram_out[dram_out_routed]->is_empty()) return;

            flit = cache_dram_out[dram_out_routed]->remove_from_head();
            dram_out->insert_at_tail(flit);

            if (flit & TAIL_FLIT_MASK)
                dram_out_routed = -1;
        }
    }
}
