/*
stats.cc

Candace Walden
30 August 2017

Statistics and latency table for all packets
*/

#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"


//sort such that smallest cycle has largest value then
// break any ties by address of bank
bool operator<(const schedule_item& a, const schedule_item& b)
{
    if (a.cycle == b.cycle)
        return a.bank < b.bank;

    return a.cycle > b.cycle;
}

Stats* Stats::instance = NULL;

Stats* Stats::get()
{
  if (!instance)
    instance = new Stats();

  return instance;
}

Stats::Stats()
{
  //Latency
  //map = new std::unordered_map<unsigned, packets>;
  map = new std::map<unsigned, packets>;
  packet_id = 1;

  conf = Conf_ReRAM::get();

  sent_packets = 0;

  total_latency = latency();
  latest_latency = latency();
  per_tile_latency = new latency[conf->x_max * conf->y_max];
  for (int i = 0; i < conf->x_max * conf->y_max; i++)
  {
          per_tile_latency[i] = latency();
  }

  //Queue Fill
  int nthreads = conf->threads;
  threads = new bool[nthreads];
  for (int i = 0; i < nthreads; i++)
  {
    threads[i] = false;
  }
  trace_done = false;

#if NOC_SIM_SNIPER
  threads_running = new bool[nthreads];
  for (int i = 0; i < nthreads; i++)
  {
	  /* DY:  assume threads begin in INITIALIZING state */
	  threads_running[i] = false;
  }
  full_queues = false;
  empty_queues = true;
  /* DY: initially, the back-end has nothing to do */
  fastfwd_mode = true;
  
  num_frontend_barriers = 0;
#else
  watermark = new bool[nthreads];
  for (int i = 0; i < nthreads; i++)
  {
	  watermark[i] = false;
  }
  any_under_watemark = true;
  any_empty = true;
#endif

  //Traffic Gen
  latest_cpu_stats = cpu_stats();
  total_cpu_stats = cpu_stats();

  //L1
  latest_l1_stats = l1_stats();
  total_l1_stats = l1_stats();

  //L2
  latest_l2_stats = l2_stats();
  total_l2_stats = l2_stats();

  //RAM
  latest_ram_stats = ram_stats();
  total_ram_stats = ram_stats();
  
  //Router
  latest_flit_jumps = 0;
  total_flit_jumps = 0;
  
  tile_cpu_stats = new cpu_stats[conf->x_max * conf->y_max];
  tile_ram_stats = new ram_stats[conf->x_max * conf->y_max];
  tile_l1_stats  = new l1_stats[conf->x_max * conf->y_max];
  tile_l2_stats  = new l2_stats[conf->x_max * conf->y_max];
  tile_flit_jumps = new long long[conf->x_max * conf->y_max];
  for (int i = 0; i < conf->x_max * conf->y_max; i++)
  {
      tile_cpu_stats[i] = cpu_stats();
      tile_ram_stats[i] = ram_stats();
      tile_l1_stats[i] = l1_stats();
      tile_l2_stats[i] = l2_stats();
      tile_flit_jumps[i] = 0;
  } 

  latest_dram_stats = dramsim3::noc_sim_dram_stats();
  new_total_dram_stats = dramsim3::noc_sim_dram_stats();
  total_dram_stats = dramsim3::noc_sim_dram_stats();

  total_ins_stats = ins_stats();

  max_banks = (conf->ram_mode == dram) ? conf->dram_settings.banks :
	  (conf->dram_settings.banks > conf->reram_settings.banks) ? conf->dram_settings.banks : conf->reram_settings.banks;

  bank_profile = new long long**[conf->x_max];
  for (int i = 0; i < conf->x_max; i++) {
	  bank_profile[i] = new long long*[conf->y_max];
	  for (int j = 0; j < conf->y_max; j++) {
		  bank_profile[i][j] = new long long[max_banks];
		  for (int k = 0; k < max_banks; k++)
			  bank_profile[i][j][k] = 0;
	  }
  }

#if OUTPUT_LOG
  log.open("log.txt", std::ofstream::out);
  inLog.open("inputLog.txt", std::ofstream::out);
#endif
}

void Stats::bank_profile_incr(int x_coord, int y_coord, int bank)
{
    if (x_coord < conf->x_max && y_coord < conf->y_max && bank < max_banks)
    {
        bank_profile[x_coord][y_coord][bank]++;
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: " << x_coord << ", " << y_coord << ", " << bank << "out of bank profile bounds";
        throw std::invalid_argument(errMsg.str());
    }
}

int Stats::get_x(unsigned id)
{
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);

  if (packet == map->end()) {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  //return x coord
  return packet->second.x_coord;
}

int Stats::get_y(unsigned id)
{
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);

  if (packet == map->end()) {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  //return x coord
  return packet->second.y_coord;
}

int Stats::get_l2_x(unsigned id)
{
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);

  if (packet == map->end()) {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  //return x coord
  return packet->second.l2_x_coord;
}

int Stats::get_l2_y(unsigned id)
{
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);

  if (packet == map->end()) {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  //return x coord
  return packet->second.l2_y_coord;
}
unsigned Stats::set_start(packet_type type, long long cycle, int x, int y, uint64 addr, int vector, uint64 pc)
{
    packets packet = packets(type, cycle, x, y, addr, pc);
  
  if (vector)
    packet.recv = (conf->ram_mode == dram)? conf->dram_settings.vector_size : conf->reram_settings.vector_size;
  else
    packet.recv = 1;

  std::lock_guard<std::mutex> lock(map_barrier);

  if (packet_id == 0) packet_id++;
  map->emplace(packet_id, packet);

  if (type == write_packet)
    sent_packets++;
  else
    sent_packets += 2;

  return packet_id++;
}

//Set L2 x and y coordinates
void Stats::set_l2_xy(unsigned id, int x, int y){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);

  if (packet == map->end()) {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  packet->second.l2_x_coord = x;
  packet->second.l2_y_coord = y;
}


//Set inbound network latency
void Stats::set_to_L2(unsigned id, long long cycle, int x, int y){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);
  double latency;

  if (packet == map->end()) {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  packet->second.to_l2 = cycle;

  //find the latency from entering the network to getting to the bank
  latency = packet->second.to_l2 - packet->second.from_cpu;

  per_tile_latency[IND(x,y)].in_noc_latency += latency;
  per_tile_latency[IND(x,y)].in_noc_packets++;
}

void Stats::set_to_controller(unsigned id, long long cycle, int x, int y) {
    //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
    std::map<unsigned, packets>::iterator packet = map->find(id);

    if (packet == map->end()) {
        throw std::invalid_argument("Error: Unknown packet id " + id);
    }

    packet->second.to_controller = cycle;
    per_tile_latency[IND(x, y)].controller_packets++;

    if (conf->ram_mode == dram || conf->ram_mode == l2_cpu)
    {
        //find the latency from entering the network to getting to the bank
        double latency = packet->second.to_controller - packet->second.to_l2;

        per_tile_latency[IND(x, y)].to_dram_latency += latency;
    }
}

void Stats::set_to_bank(unsigned id, long long cycle, int x, int y){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);
  double latency;

  if (packet == map->end())  {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  packet->second.to_bank = cycle;

  //find the latency from getting to the controller to getting to the bank
  if(conf->ram_mode == dram || conf->ram_mode == l2_cpu)
    latency = packet->second.to_bank - packet->second.to_controller;
  else
      latency = packet->second.to_bank - packet->second.to_l2;
  per_tile_latency[IND(x,y)].in_queue_latency += latency;
  per_tile_latency[IND(x,y)].in_queue_packets++;
}

//Sets the ram latency and the total latency for write packets
void Stats::set_from_bank(unsigned id, long long cycle, int x, int y){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);
  double latency;

  if (packet == map->end())  {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  packet->second.from_bank = cycle;

  //find the latency from getting to the bank to leaving the bank
  latency = packet->second.from_bank - packet->second.to_bank;
  per_tile_latency[IND(x,y)].ram_latency += latency;
  per_tile_latency[IND(x,y)].ram_packets++;

  //find queue + bank latency for histogram
  if (conf->ram_mode == dram || conf->ram_mode == l2_cpu)
    latency = packet->second.from_bank - packet->second.to_controller;
  else
    latency = packet->second.from_bank - packet->second.to_l2;
  add_ram_latency((long)latency);
  
  if (packet->second.type == write_packet)  //if write there is no outbound latency
  {
    //total latency
    latency = packet->second.from_bank - packet->second.from_cpu;
    per_tile_latency[IND(x,y)].tot_latency += latency;
    per_tile_latency[IND(x,y)].tot_packets++;
    add_tot_latency(long(latency));
    packet->second.recv--;
    if (packet->second.recv == 0)
    {
        //delete
        std::lock_guard<std::mutex> lock(map_barrier);
        map->erase(id);
    }
  }
  else
  {
    packet->second.recv--;
  }
}

//Sets the out queue latency
void Stats::set_from_controller(unsigned id, long long cycle, int x, int y){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);
  double latency;

  if (packet == map->end())  {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  packet->second.from_controller = cycle;

  //find the latency from leaving the bank to leaving the controller
  latency = packet->second.from_controller - packet->second.from_bank;
  per_tile_latency[IND(x,y)].out_queue_latency += latency;
  per_tile_latency[IND(x,y)].out_queue_packets++;
}

//Sets the L2 latency
void Stats::set_from_L2(unsigned id, long long cycle, int hit, int x, int y){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);
  double latency;

  if (packet == map->end()) {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  packet->second.from_l2 = cycle;
  per_tile_latency[IND(x,y)].from_L2_packets++;

  if (hit)
  {
    if (packet->second.type == write_packet)
    {
      //total latency
      latency = packet->second.from_l2 - packet->second.from_cpu;
      per_tile_latency[IND(x,y)].tot_latency += latency;
      per_tile_latency[IND(x,y)].tot_packets++;
      add_tot_latency(long(latency));
      //delete
      std::lock_guard<std::mutex> lock(map_barrier);
      map->erase(id);
    }
  }
  else
  {
    //L2 latency
    latency = packet->second.from_l2 - packet->second.to_l2;
    per_tile_latency[IND(x,y)].l2_latency += latency;

    if ((conf->ram_mode == dram || conf->ram_mode == l2_cpu))
    {
        if (packet->second.type == read_packet)
        {
            //find the latency from entering the network to getting to the cpu
            latency = packet->second.from_l2 - packet->second.from_controller;
            per_tile_latency[IND(x,y)].from_dram_latency += latency;
            per_tile_latency[IND(x,y)].from_dram_packets++;
        }
    }
    else
    {
        latency = packet->second.from_l2 - packet->second.from_bank;
        per_tile_latency[IND(x,y)].out_queue_latency += latency;
        per_tile_latency[IND(x,y)].out_queue_packets++;
    }
    //Time from getting to L2 to leave L2 minus time spent in memory
    latency = (packet->second.from_l2 - packet->second.to_l2) - (packet->second.from_controller - packet->second.to_controller);
    per_tile_latency[IND(x,y)].l2_latency += latency;

  }
}

//Sets the out bound latency and the total latency for read packets
void Stats::set_to_cpu(unsigned id, long long cycle, int x, int y){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);
  double latency;

  if (packet == map->end())  {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  packet->second.to_cpu = cycle;

  //find the latency from entering the network to getting to the cpu
  latency = packet->second.to_cpu - packet->second.from_l2;
  per_tile_latency[IND(x,y)].out_noc_latency += latency;
  per_tile_latency[IND(x,y)].out_noc_packets++;

  //total latency
  latency = packet->second.to_cpu - packet->second.from_cpu;
  per_tile_latency[IND(x,y)].tot_latency += latency;
  per_tile_latency[IND(x,y)].tot_packets++;
  add_tot_latency(long(latency));

  //delete
  std::lock_guard<std::mutex> lock(map_barrier);
  map->erase(id);
}


void Stats::add_ram_latency(long long latency)
{
  int bucket = conf->bucket_size;
  int place = latency / bucket;

  if ((int)ram_latency_distrib.size() <= place)
  {
    ram_latency_distrib.resize(place + 2, 0);
  }

  ram_latency_distrib[place]++;
}

void Stats::add_tot_latency(long long latency)
{
  int bucket = conf->bucket_size;
  int place = latency / bucket;

  if ((int)tot_latency_distrib.size() <= place)
  {
    tot_latency_distrib.resize(place + 2, 0);
  }

  tot_latency_distrib[place]++;
}

//Gets the address
uint64 Stats::get_addr(unsigned id){
  //std::unordered_map<unsigned, packets>::iterator packet = map->find(id);
  std::map<unsigned, packets>::iterator packet = map->find(id);

  if (packet == map->end())  {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  return packet->second.addr;
}

uint64 Stats::get_PC(unsigned id){

  std::map<unsigned, packets>::iterator packet = map->find(id);

  if (packet == map->end())  {
    throw std::invalid_argument("Error: Unknown packet id " + id);
  }

  return packet->second.PC;

}


void Stats::print_distrib()
{
  int tot_size, ram_size, i, bucket;
  ofstream hist;
  hist.open("latency_hist.csv", std::ofstream::out);

  hist << "Latency," << "RAM Count," << "Total Count," << "\n";
  tot_size = tot_latency_distrib.size();
  ram_size = ram_latency_distrib.size();
  bucket = conf->bucket_size;

  for (i = 0; i < ram_size; i++)
  {
    hist << (i*bucket) << "," << ram_latency_distrib[i] << "," << tot_latency_distrib[i] << "," << "\n";
  }

  //total latency should have more buckets than ram because it should be longer than ram
  for (; i < tot_size; i++)
  {
    hist << (i*bucket) << "," << "0" << "," << tot_latency_distrib[i] << "," << "\n";
  }

  hist.close();
}

void Stats::collect_stats()
{
    latest_latency = latency();
    latest_cpu_stats = cpu_stats();
    latest_l1_stats = l1_stats();
    latest_l2_stats = l2_stats();
    latest_ram_stats = ram_stats();
    for (int i = 0; i < conf->x_max * conf->y_max; i++)
    {
        //Latency numbers
        latest_latency += per_tile_latency[i];
        per_tile_latency[i] = latency();

        //Traffic Gen
        latest_cpu_stats += tile_cpu_stats[i];
        tile_cpu_stats[i] = cpu_stats();

        //L1
        latest_l1_stats += tile_l1_stats[i];
        tile_l1_stats[i] = l1_stats();

        //L2
        latest_l2_stats += tile_l2_stats[i];
        tile_l2_stats[i] = l2_stats();

        //RAM
        latest_ram_stats += tile_ram_stats[i];
        unsigned extra = tile_ram_stats[i].extra_cycles_from_next_epoch;
        tile_ram_stats[i] = ram_stats();
        tile_ram_stats[i].busy_bank_cycles = extra;
    }
    total_latency += latest_latency;
    total_cpu_stats += latest_cpu_stats;
    total_l1_stats += latest_l1_stats;
    total_l2_stats += latest_l2_stats;
    total_ram_stats += latest_ram_stats;

    //Router
    latest_flit_jumps = 0;
    for (int i = 0; i < conf->x_max * conf->y_max; i++)
    {
        latest_flit_jumps += tile_flit_jumps[i];
        tile_flit_jumps[i] = 0;
    }
    total_flit_jumps += latest_flit_jumps;

    //DRAMSim - eventually integrate into RAM stats, etc
    latest_dram_stats = new_total_dram_stats - total_dram_stats;
    total_dram_stats = new_total_dram_stats;
    new_total_dram_stats = dramsim3::noc_sim_dram_stats();
    latest_dram_stats.average_read_lat /= conf->num_dram_tiles;
}

#if NOC_SIM_SNIPER

//Sets a particular thread to have instructions or not
void Stats::set_queue(bool full, int threadID) {
  std::lock_guard<std::mutex> lock(barrier);
  threads[threadID] = full;

  if (full)
  {
    full_queues = check_full_queues();
    empty_queues = false;
  }
  else
  {
    full_queues = false;
    empty_queues = check_empty_queues();
  }
}

//Sets if all *runnable* threads have instructions or not
bool Stats::check_full_queues() {
	int nthreads = Conf_ReRAM::threads;
	bool queues = true;
	for (int i = (Conf_ReRAM::thread_zero_empty) ? 1 : 0; i < nthreads; i++)
	{
		queues &= (threads[i] || !(threads_running[i]));
	}

	return queues;
}

//Sets if any threads have instructions
bool Stats::check_empty_queues() {
  int nthreads = Conf_ReRAM::threads;
  bool queues = false;
  for (int i = (Conf_ReRAM::thread_zero_empty) ? 1 : 0; i < nthreads; i++)
  {
    queues |= threads[i];
  }

  return !queues;
}

//Returns true if all threads have instructions
bool Stats::queues_full() {

	return full_queues || trace_done;
}

//Returns true if there are no more instructions
bool Stats::queues_empty() {
  return empty_queues && trace_done;
}

// DY:  Just checks empty queues, doesn't worry about end-of-simulation
bool Stats::queues_empty_for_bar() {
  return empty_queues;
}

/* DY:  Need to track scheduling status from front-end */
void Stats::set_running(bool running, int threadID)
{
	threads_running[threadID] = running;

	full_queues = check_full_queues();
}

void Stats::set_detailed_mode() {
	fastfwd_mode = false;
}


bool Stats::in_fastfwd() {
	return (fastfwd_mode);
}
#else

//Sets a particular thread to have instructions or not
void Stats::set_queue(bool full, int threadID) {
  std::lock_guard<std::mutex> lock(barrier);
  threads[threadID] = full;

  if (full)
  {
    any_empty = check_any_empty();
  }
  else
  {
    if (!conf->thread_zero_empty || threadID != 0)
      any_empty = true;
  }
}

//Sets if all threads have instructions or not
bool Stats::check_any_empty() {
  int nthreads = conf->threads;
  bool queues = true;
  for (int i = (conf->thread_zero_empty) ? 1 : 0; i < nthreads && queues; i++)
  {
    queues &= threads[i];
  }

  return !queues;
}

//Returns false if all threads have instructions or trace is done
//Returns true some threads don't have instructions
bool Stats::any_empty_threads() {
  return any_empty && !trace_done;
}

//Sets a particular thread to have enough instructions or not
void Stats::set_queue_watermark(bool full, int threadID){
  std::lock_guard<std::mutex> lock(barrier);
  watermark[threadID] = full;

  if (full)
    any_under_watemark = check_any_under_watemark();
  else if(!conf->thread_zero_empty || threadID != 0)
    any_under_watemark = true;
}

//Sets if any threads have instructions
bool Stats::check_any_under_watemark(){
  int nthreads = conf->threads;
  bool queues = true;
  for (int i = (conf->thread_zero_empty) ? 1 : 0; i < nthreads && queues; i++)
  {
    queues &= watermark[i];
  }

  return !queues;
}

//Returns false if all threads have enough instructions
//Returns true if some threads do not have enough instructions
bool Stats::any_under_watemark_threads(){
  return any_under_watemark;
}
#endif


void Stats::set_trace_done(){
  trace_done = true;
}

#if OUTPUT_LOG
void Stats::printLog(string s, bool t2)
{
  std::lock_guard<std::mutex> lock(logMutex);
  if(t2)
    inLog << s;
  else
    log << s;
}
#endif



latency& latency::operator+=(const latency& a)
{
    tot_packets += a.tot_packets;
    in_noc_packets += a.in_noc_packets;
    out_noc_packets += a.out_noc_packets;
    controller_packets += a.controller_packets;
    from_dram_packets += a.from_dram_packets;
    ram_packets += a.ram_packets;
    out_queue_packets += a.out_queue_packets;
    in_queue_packets += a.in_queue_packets;
    from_L2_packets += a.from_L2_packets;

    tot_latency += a.tot_latency;
    in_noc_latency += a.in_noc_latency;
    out_noc_latency += a.out_noc_latency;
    to_dram_latency += a.to_dram_latency;
    from_dram_latency += a.from_dram_latency;
    ram_latency += a.ram_latency;
    out_queue_latency += a.out_queue_latency;
    in_queue_latency += a.in_queue_latency;
    l2_latency += a.l2_latency;

    return *this;
}

cpu_stats& cpu_stats::operator+=(const cpu_stats& a)
{
    te_count += a.te_count;
    ins_executed += a.ins_executed;
    packets_prefetched += a.packets_prefetched;
    prefetchq_hits += a.prefetchq_hits;
    cpu_idle_cycles += a.cpu_idle_cycles;

    return *this;
}

l1_stats& l1_stats::operator+=(const l1_stats& a) 
{
    rd_exec += a.rd_exec;
    wr_exec += a.wr_exec;
    wb_from_l1 += a.wb_from_l1;
    l1_hit += a.l1_hit;
    l1_miss += a.l1_miss;

    return *this;
}

l2_stats& l2_stats::operator+=(const l2_stats& a)
{
    wb_from_l2 += a.wb_from_l2;
    vec_wb_from_l2 += a.vec_wb_from_l2;
    l2_hit += a.l2_hit;
    l2_miss += a.l2_miss;
    l2_gather_hit += a.l2_gather_hit;
    l2_gather_miss += a.l2_gather_miss;
    l2_write += a.l2_write;
    l2_vec_write += a.l2_vec_write;
    l2_read += a.l2_read;
    l2_vec_read += a.l2_vec_read;
    cache_fill += a.cache_fill;

    return *this;
}

ram_stats& ram_stats::operator+=(const ram_stats& a)
{
    dram_read += a.dram_read;
    dram_write += a.dram_write;
    reram_read += a.reram_read;
    reram_write += a.reram_write;
    row_buf_misses += a.row_buf_misses;
    row_buf_hits += a.row_buf_hits;
#if BANK_UTILIZATION_TRACKING
    busy_bank_cycles += a.busy_bank_cycles;
    extra_cycles_from_next_epoch += a.extra_cycles_from_next_epoch;
#endif

    return *this;
}

ins_stats& ins_stats::operator+=(const ins_stats& a)
{
    this->read_single += a.read_single;
    this->gather += a.gather;
    this->read_vector += a.read_vector;
    this->write_single += a.write_single;
    this->scatter += a.scatter;
    this->write_vector += a.write_vector;
    this->other += a.other;

    return *this;
}