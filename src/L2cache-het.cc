#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_het::L2cache_het(int x, int y) : L2cache_bank(x, y)
{
    dram_out = nullptr;

    dram_read_state = sHeader;
    dram_write_state = sHeader;

    dram_req = nullptr;
    out_dram_req = nullptr;
    out_wb = nullptr;
    dram_req_queue = std::queue<mem_request>();

    //cycle_complete = 0;
    //cur_req = nullptr;


    dram_in = new buffer(conf->buffer_size);
}

void L2cache_het::set_dram_out_buffer(buffer* out)
{
    dram_out = out;
}

/*
void L2cache_het::cycle(long long cur_cycle)
{

    //route out and only proceed if output is cleared
    route_out(cur_cycle);
    send_dram_request(cur_cycle);
    if (output_queue.size() < conf->buffer_size)  //TODO: better size limit?
    {
        read_dram_reply(cur_cycle);
    }

    //if (cycle_complete <= cur_cycle)
    //{
    //    handle_request(cur_cycle);
    //}

    //route and transfer packets from NOC
    if (request_rob.size() < conf->l2_settings.rob_entries)
    {
        route_in(cur_cycle);
    }
}
*/
/*
void L2cache_het::handle_request(long long cur_cycle)
{
    if (cur_req != nullptr)
    {
        if (cur_req->next_stage == rr_r)  //receiving ReRAM reply
        {
            //check if mshr complete
            if (check_mshr_complete(cur_req))
            {
                L2_cache_line* evicted = cache_array->tag_update(*cur_req);

                if (evicted != nullptr)
                {
                    writeback_req(evicted);
                }

                //send back any read replies
                mshr_entry* mshr = cur_req->mshr;
                while (cur_req->mshr->read_subentries > 0)
                {
                    output_queue.push(*mshr->read_sub[--mshr->read_subentries].req);
                    delete mshr->read_sub[mshr->read_subentries].req;
                }

                //done with the read subentries, delete them
                delete_mshr(mshr, cur_req->subbank);
            }

            delete cur_req;
        }
        else if (cur_req->type == read_block || cur_req->type == read_dw)
        {
            //done with the read
            if (cur_req->valid_data)
            {
                output_queue.push(*cur_req);
                delete cur_req;
            }
            else
            {
                //send reram requests
                if (cur_req->type == read_dw)
                {
                    //send to reram output queue if hasn't already been sent
                    if (cur_req->mshr->sectors_recv[SECTOR_ID(cur_req->bank)] == pending_req)
                    {
                        cur_req->mshr->sectors_recv[SECTOR_ID(cur_req->bank)] = sent_req;
                        dram_req_queue.push(*cur_req);
                    }
                }
                else
                {
                    bool pending = false;
                    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                    {
                        pending |= (cur_req->mshr->sectors_recv[i] == pending_req);
                    }

                    if (pending)
                    {
                        dram_req_queue.push(*cur_req);
                        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                        {
                            if(cur_req->mshr->sectors_recv[i] == pending_req)
                                cur_req->mshr->sectors_recv[i] = sent_req;
                        }
                    }
                }
            }
        }
        else  //write request
        {
            //update cache tags if its not in the MSHR file or in the cache already
            if (cur_req->valid_data)
            {
                L2_cache_line* evicted = cache_array->tag_update(*cur_req);

                if (evicted != nullptr)
                {
                    writeback_req(evicted);
                }
            }

            stats->set_from_L2(cur_req->packet_id, cur_cycle, 1);
            delete cur_req;
        }
    }

    //get next request and find time to do it
    cur_req = next_request();
    if (cur_req != nullptr)
    {
        if (cur_req->next_stage == rr_r)
        {
            cycle_complete = cur_cycle + 2;

            update_mshr(cur_req);
            if (check_mshr_complete(cur_req))
            {
                cycle_complete += 4;

                //cache fill
                tag_res tag = cache_array->tag_look_up(cur_req->addr);

                if(tag.evict_dirty)
                    cycle_complete += 2;
            }
        }
        else
        {
            cycle_complete = cur_cycle + 4;

            bool valid = tag_lookup(cur_req);
            bool dirty = cur_req->dirty_data;
            cur_req->valid_data = valid;

            if (!valid)
            {
                //do mshr stuff
                mshr_entry* hit = check_mshrs(cur_req);
                if (cur_req->type == write_dw || cur_req->type == write_block)
                {
                    if (!hit)
                    {
                        if (!valid && dirty)
                            cycle_complete += 3;
                        else
                            cycle_complete += 1;

                        cur_req->valid_data = true;
                    }
                    else
                    {
                        //write data into MSHR
                        write_mshr(cur_req);
                    }
                }
                else  //read
                {
                    if (!hit)
                    {
                        //read without hit, make new mshr
                        cur_req->mshr = handle_mshr_miss(cur_req);
                        //create Read Subentry
                        add_read_subentry(cur_req);
                    }
                    else  //hit
                    {
                        valid = check_read_valid(cur_req);
                        if (valid)
                        {
                            cur_req->valid_data = true;
                        }
                        else
                        {
                            //create Read Subentry
                            add_read_subentry(cur_req);
                        }
                    }
                }
            }
        }
    }
}
*/

bool L2cache_het::read_dram_reply(long long cur_cycle)
{
    uint64 address;
    int vector;

    //take as many packets as channel is wide 
    //(this isn't perfect, but should be reasonable)
    for (int i = 0; i < conf->phits; i++)
    {
        if (dram_in->is_empty()) return true;

        uint64 flit = dram_in->remove_from_head();

        if (dram_read_state == sHeader)
        {
            dram_req = new mem_request(PACKET_ID(flit));
            vector = flit & BYTES_MASK;

            if (flit & REPLY_FLIT_MASK)
            {
                dram_req->type = (vector == 1) ? read_block : read_dw;
                dram_req->dw_count = 0;
                dram_read_state = sReadAddr;
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Unknown packet " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (dram_read_state == sReadAddr)
        {
            address = flit & (~TAIL_FLIT_MASK);
            dram_req->addr = address;
            dram_req->bank = BANK_ID(address);
            dram_req->subbank = SUBBANK_ID(address);
            dram_read_state = sData;
            dram_req->valid_data = true;

            if (dram_req->type == read_block)
            {
                for (unsigned j = 0; j < conf->l2_settings.n_sectors; j++)
                {
                    dram_req->valid[j] = true;
                }
            }
            else
            {
                dram_req->valid[SECTOR_ID(dram_req->bank)] = true;
            }

            //find mshr
            if (find_orignal_mshr(dram_req) == nullptr)
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot find MSHR.";
                throw std::invalid_argument(errMsg.str());
            }
        }
        else  //sData
        {
            unsigned fetch_width;
            if (dram_req->type == read_block)
            {
                fetch_width = conf->dram_settings.vector_size * conf->dram_settings.fetch_width;
            }
            else
            {
                fetch_width = conf->dram_settings.fetch_width;
            }

            dram_req->dw_count++;

            if (dram_req->dw_count > fetch_width)
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Too many data flits for reply packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }

        if (flit & TAIL_FLIT_MASK)
        {
            //send to request queue
            dram_req->next_stage = rr_r;
            request_rob.push_front(*dram_req);

            //start over
            dram_read_state = sHeader;
            delete dram_req;

#if L2_OUT
            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): L2 received reply packet : " << incomplete_req->packet_id << "\n";
            Stats::get()->printLog(msg.str(), false);
#endif

            return true;
        }

    }

    return false;
}

void L2cache_het::writeback_req(L2_cache_line * evicted)
{
    wb_entry wb;
    bool wb_vec_check = true;

    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
    {
        wb_vec_check &= evicted->valid[i];
    }

    unsigned fetch_width;
    if (wb_vec_check && conf->vector_cache_block_fetch)
    {
        fetch_width = conf->dram_settings.vector_size * conf->dram_settings.fetch_width;
    }
    else
    {
        fetch_width = conf->dram_settings.fetch_width;
    }

    if (wb_vec_check && conf->vector_cache_block_fetch)
    {
        // wb tag is just addr without sector shifted out, so shift back to get block addr
        wb = { (evicted->tag) << conf->l2_settings.index_mask_offset, fetch_width };
        dram_wb_queue.push_back(wb);

    }
    else
    {
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            if (evicted->dirty[i])
            {
                //wb tag is just addr without sector, so add back to get word address
                wb = { (evicted->tag) << conf->l2_settings.index_mask_offset | ((uint64)i << conf->l2_settings.sector_mask_offset) , fetch_width };
                stats->tile_l2_stats[IND(x_coord, y_coord)].wb_from_l2++;
                dram_wb_queue.push_back(wb);
            }
        }
    }

    delete evicted;
}


//send any needed ram requests
bool L2cache_het::send_dram_request(long long cur_cycle)
{
    uint64 flit;

    while (!dram_out->is_full() &&
           (!dram_req_queue.empty() || !dram_wb_queue.empty()))
    {
        //start with the header
        if (dram_write_state == sHeader)
        {
            int delta_x, delta_y;
            if (dram_req_queue.empty())
            {
                //do writeback
                out_wb = &dram_wb_queue.front();

                bool vector = (conf->ram_mode == dram) ? out_wb->data_flits > conf->dram_settings.fetch_width : out_wb->data_flits > conf->reram_settings.fetch_width;
                unsigned packet_id = stats->set_start(write_packet, cur_cycle, x_coord, y_coord, out_wb->addr, vector, -1);
                stats->set_to_L2(packet_id, cur_cycle, x_coord, y_coord);
                address_translation(out_wb->addr, &delta_x, &delta_y);

                //send the header flit for the last wb request
                flit = get_delta_flit(packet_id, delta_x, delta_y) | WRITE_FLIT_MASK;
                unsigned fetch_width = (conf->ram_mode == dram)? conf->dram_settings.fetch_width : conf->reram_settings.fetch_width;
                if(out_wb->data_flits > fetch_width)
                    flit |= 1;  //mark as vector

                dram_write_state = sWriteAddr;
            }
            else
            {
                //do read request
                out_dram_req = &dram_req_queue.front();
                address_translation(out_dram_req->addr, &delta_x, &delta_y);

                //send the header flit for the last read request
                flit = get_delta_flit(out_dram_req->packet_id, delta_x, delta_y) | READ_FLIT_MASK;
                if(out_dram_req->type == read_block)
                    flit |= 1;  //mark as vector

                dram_write_state = sReadAddr;
            }

            dram_out->insert_at_tail(flit);
        }
        //read request
        else if (dram_write_state == sReadAddr)
        {
            flit = out_dram_req->addr | TAIL_FLIT_MASK;
            dram_out->insert_at_tail(flit);

            dram_write_state = sHeader;

            dram_req_queue.pop();
            return true;
        }
        //writeback
        else if (dram_write_state == sWriteAddr)
        {
            flit = out_wb->addr;
            dram_out->insert_at_tail(flit);

            dram_write_state = sData;
        }
        //output the data
        else if (dram_write_state == sData)
        {
            if (out_wb->data_flits > 1)
            {
                flit = 0;                      //64 bits of data
                dram_out->insert_at_tail(flit);
                out_wb->data_flits--;
            }
            else //tail packet
            {
                flit = 0 | TAIL_FLIT_MASK;    //64 bits of data
                dram_out->insert_at_tail(flit);

                dram_write_state = sHeader;

                //finished this output
                dram_wb_queue.pop_front();
                return true;
            }
        }
    }

    return false;
}
