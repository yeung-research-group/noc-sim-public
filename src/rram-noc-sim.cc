
/*
  rram-noc-sim.cc

  Donald Yeung
  24 July 2017

  Simulator of a MESH NOC connected to RRAM banks
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#if !NOC_SIM_SIMULATOR
#include <sys/stat.h>
#endif
#include "noc-sim.hh"
#if NOC_SIM_SNIPER
#include "simulator.h"
#endif
#if NOC_SIM_PARA
#include "../ext/ctpl/ctpl.h"
#endif

/* simulation configuration parameters */
int NUMBER_CORES;
int X_DIMENSION;
int Y_DIMENSION;
int BANKS;
int NUM_PACKETS;
int THREADS_PER_CORE;
Conf_ReRAM *conf;
Stats* stats;
ofstream epochStatsFile;
mem_trace *tracer;
//FILE * epochStatsFile;

#if NOC_SIM_PARA
std::atomic<int> cores_finished_trace;
#endif

#if NOC_SIM_SNIPER
// Constructor for the entire ReRAM back-end timing simulator.
// Constains much of the initialization code from main().
ReRAM_Backend::ReRAM_Backend()
{
  int i, j;// , k;
  long long cycle;

  //mcheck(&abort);

  parse_config();

  x_extent = X_DIMENSION;
  y_extent = Y_DIMENSION;

  epochStatsFile.open(conf->out_file.c_str(), std::ofstream::app);

  /* create the routers */
  my_NOC = new router ***[Y_DIMENSION];
  for (i = 0; i < Y_DIMENSION; i++) {
    my_NOC[i] = new router *[X_DIMENSION];
    for (j = 0; j < X_DIMENSION; j++) {
      my_NOC[i][j] = new router(j, i, conf->buffer_size);
    }
  }

  /* create the caches and reram*/
  my_CACHE = new network_cache**[Y_DIMENSION];
  for (i = 0; i < Y_DIMENSION; i++) {
      my_CACHE[i] = new network_cache*[X_DIMENSION];
      for (j = 0; j < X_DIMENSION; j++) {
          my_CACHE[i][j] = new L2cacher(j, i);
      }
  }
}

  if (!conf->integrate_cache_rram)
  {
	  /* create the RRAM banks */
	  my_RRAM = new ram_controller **[Y_DIMENSION];
	  for (i = 0; i < Y_DIMENSION; i++) {
		  my_RRAM[i] = new ram_controller*[X_DIMENSION];
		  for (j = 0; j < X_DIMENSION; j++) {
              if(conf->get_tile_type(j, i) == dram_tile)
                my_RRAM[i][j] = new dram_controller(j, i);
              else
			    my_RRAM[i][j] = new rram_controller(j, i);
		  }
	  }
  }


  /* create the traffic generators */
  my_CORE = new traffic_generator **[Y_DIMENSION];
  for (i = 0; i < Y_DIMENSION; i++) {
    my_CORE[i] = new traffic_generator*[X_DIMENSION];
    for (j = 0; j < X_DIMENSION; j++) {
      my_CORE[i][j] = new traffic_generator(j, i, conf->packets);
    }
  }
  /*
  if (conf->packets == 0)
  {
    my_CORE[2][2]->set_npackets(1);
  }
  */

  /* link up routers and RRAM banks */
  init_NOC_channels(my_NOC);
  init_CORE_channels(my_NOC, my_CORE);
  if (!conf->integrate_cache_rram)
  {
	  init_RRAM_channels(my_CACHE, my_RRAM, my_NOC);
  }
  init_CACHE_channels(my_NOC, my_CACHE);

  //  DY:  We now enter from run()
  return;

  /* main sim loop */
  cycle = simulate(my_NOC, my_CORE, my_RRAM, my_CACHE);
  print_final_stats(cycle, my_RRAM);

#if BANK_PROF_OUT
  dump_bank_profile();
#endif

#ifdef _MSC_VER   //if visual studio
  getchar();
#endif
}
#endif

#if NOC_SIM_SNIPER
void ReRAM_Backend::parse_config()
#else
void parse_config(int argc, char *argv[])
#endif
{
  string config_file_name;

#if NOC_SIM_SNIPER
  config_file_name = "config/config-reram.ini";
  // DY: Need a different way to provide config file; just use default for now
#else
  // if no input config file use default config.ini
  if (argc < 3){
    config_file_name = "config.ini";
  } else {
    config_file_name = argv[1];
  }
#endif

  conf = Conf_ReRAM::get(config_file_name);
  
  X_DIMENSION = conf->x_max;
  Y_DIMENSION = conf->y_max;
  BANKS = (conf->dram_settings.banks * conf->num_dram_tiles) + (conf->reram_settings.banks * conf->num_reram_tiles);
  NUMBER_CORES = conf->cpu_tiles;
  NUM_PACKETS = conf->packets;
  THREADS_PER_CORE = conf->max_threads_per_core;

#if !NOC_SIM_SNIPER
  if (conf->exec){

    if (argc < 2 ){
      throw std::invalid_argument("Requires trace input file\n");
    }

    conf->trace_file = argv[argc-1];  // use the last input arg to maintain backward compatibility
    
  }
#endif

  printf("\n***NOC Simulation Configuration***\n");
  printf("%d x %d NOC\n", X_DIMENSION, Y_DIMENSION);
  printf("%d Banks Total\n", BANKS);
  printf("%d Cores %d Threads\n", NUMBER_CORES, conf->threads);
  printf("Buffer size = %u\n", conf->buffer_size);
  printf("\n");
}

#if NOC_SIM_SNIPER
void ReRAM_Backend::init_NOC_channels(router ***my_NOC)
#else
void init_NOC_channels(router ***my_NOC)
#endif
{
  int i, j;

  /* row 0 */
  my_NOC[0][0]->set_positive_x_link(my_NOC[0][1]);
  my_NOC[0][0]->set_negative_x_link(NULL);
  my_NOC[0][0]->set_positive_y_link(my_NOC[1][0]);
  my_NOC[0][0]->set_negative_y_link(NULL);
  for (i = 1; i < X_DIMENSION-1; i++) {
    my_NOC[0][i]->set_positive_x_link(my_NOC[0][i+1]);
    my_NOC[0][i]->set_negative_x_link(my_NOC[0][i-1]);
    my_NOC[0][i]->set_positive_y_link(my_NOC[1][i]);
    my_NOC[0][i]->set_negative_y_link(NULL);
  }
  my_NOC[0][X_DIMENSION-1]->set_positive_x_link(NULL);
  my_NOC[0][X_DIMENSION-1]->set_negative_x_link(my_NOC[0][X_DIMENSION-2]);
  my_NOC[0][X_DIMENSION-1]->set_positive_y_link(my_NOC[1][X_DIMENSION-1]);
  my_NOC[0][X_DIMENSION-1]->set_negative_y_link(NULL);

  /* rows 1 - Y_DIMENSION-2 */
  for (i = 1; i < Y_DIMENSION-1; i++) {
    my_NOC[i][0]->set_positive_x_link(my_NOC[i][1]);
    my_NOC[i][0]->set_negative_x_link(NULL);
    my_NOC[i][0]->set_positive_y_link(my_NOC[i+1][0]);
    my_NOC[i][0]->set_negative_y_link(my_NOC[i-1][0]);
    for (j = 1; j < X_DIMENSION-1; j++) {
      my_NOC[i][j]->set_positive_x_link(my_NOC[i][j+1]);
      my_NOC[i][j]->set_negative_x_link(my_NOC[i][j-1]);
      my_NOC[i][j]->set_positive_y_link(my_NOC[i+1][j]);
      my_NOC[i][j]->set_negative_y_link(my_NOC[i-1][j]);
    }
    my_NOC[i][X_DIMENSION-1]->set_positive_x_link(NULL);
    my_NOC[i][X_DIMENSION-1]->set_negative_x_link(my_NOC[i][X_DIMENSION-2]);
    my_NOC[i][X_DIMENSION-1]->set_positive_y_link(my_NOC[i+1][X_DIMENSION-1]);
    my_NOC[i][X_DIMENSION-1]->set_negative_y_link(my_NOC[i-1][X_DIMENSION-1]);
  }

  /* row Y_DIMENSION-1 */
  my_NOC[Y_DIMENSION-1][0]->set_positive_x_link(my_NOC[Y_DIMENSION-1][1]);
  my_NOC[Y_DIMENSION-1][0]->set_negative_x_link(NULL);
  my_NOC[Y_DIMENSION-1][0]->set_positive_y_link(NULL);
  my_NOC[Y_DIMENSION-1][0]->set_negative_y_link(my_NOC[Y_DIMENSION-2][0]);
  for (i = 1; i < X_DIMENSION-1; i++) {
    my_NOC[Y_DIMENSION-1][i]->set_positive_x_link(my_NOC[Y_DIMENSION-1][i+1]);
    my_NOC[Y_DIMENSION-1][i]->set_negative_x_link(my_NOC[Y_DIMENSION-1][i-1]);
    my_NOC[Y_DIMENSION-1][i]->set_positive_y_link(NULL);
    my_NOC[Y_DIMENSION-1][i]->set_negative_y_link(my_NOC[Y_DIMENSION-2][i]);
  }
  my_NOC[Y_DIMENSION-1][X_DIMENSION-1]->set_positive_x_link(NULL);
  my_NOC[Y_DIMENSION-1][X_DIMENSION-1]->set_negative_x_link(my_NOC[Y_DIMENSION-1][X_DIMENSION-2]);
  my_NOC[Y_DIMENSION-1][X_DIMENSION-1]->set_positive_y_link(NULL);
  my_NOC[Y_DIMENSION-1][X_DIMENSION-1]->set_negative_y_link(my_NOC[Y_DIMENSION-2][X_DIMENSION-1]);
}


#if NOC_SIM_SNIPER
void ReRAM_Backend::init_RRAM_channels(network_cache ***my_CACHE, ram_controller ***my_RRAM, router ***my_NOC)
#else
void init_RRAM_channels(L2cache ***my_CACHE, ram_controller ***my_RRAM, router ***my_NOC)
#endif
{
  int i, j;

  //RAM channels are connected to L2
  if (conf->ram_mode == reram_cpu || conf->ram_mode == l2_reram)
  {
    for (i = 0; i < Y_DIMENSION; i++)
      for (j = 0; j < X_DIMENSION; j++){
        my_CACHE[i][j]->set_mem_out_buffer(my_RRAM[i][j]->get_input_buffer());
        my_RRAM[i][j]->set_output_buffer(my_CACHE[i][j]->get_mem_in_buffer());
      }
  }
  //DRAM channels are connected to network, ReRAM channels are connected to L2
  else if (conf->ram_mode == het_l2_reram)
  {
    for (i = 0; i < Y_DIMENSION; i++)
      for (j = 0; j < X_DIMENSION; j++) 
      {
        if (my_RRAM[i][j]->active_type == dram_tile)
        {
          my_RRAM[i][j]->set_output_buffer(my_NOC[i][j]->get_p_in4_buffer());
          my_NOC[i][j]->set_p_out_link3(my_RRAM[i][j]);
        }
        else if (my_RRAM[i][j]->active_type == reram_tile)
        {
            my_CACHE[i][j]->set_mem_out_buffer(my_RRAM[i][j]->get_input_buffer());
            my_RRAM[i][j]->set_output_buffer(my_CACHE[i][j]->get_mem_in_buffer());
        }
      }
  }
  //RAM Channels are connected to network
  else
  {
    for (i = 0; i < Y_DIMENSION; i++)
      for (j = 0; j < X_DIMENSION; j++){
        if (my_RRAM[i][j]->active_type != compute)
        {
          my_RRAM[i][j]->set_output_buffer(my_NOC[i][j]->get_p_in4_buffer());
          my_NOC[i][j]->set_p_out_link3(my_RRAM[i][j]);
        }
      }
  }
}

#if NOC_SIM_SNIPER
void ReRAM_Backend::init_CORE_channels(router ***my_NOC, traffic_generator ***my_CORE)
#else
void init_CORE_channels(router ***my_NOC, traffic_generator ***my_CORE)
#endif
{
  int i, j;

  //All tiles have cores
  if (conf->ram_mode == reram_cpu || conf->ram_mode == dram)
  {
    for (i = 0; i < Y_DIMENSION; i++)
      for (j = 0; j < X_DIMENSION; j++)
      {
        my_CORE[i][j]->set_buffer_link(my_NOC[i][j]->get_p_in1_buffer());
        my_NOC[i][j]->set_p_out_link2(my_CORE[i][j]);
      }
  }
  //Some tiles do not have cores
  else
  {   
    for (i = 0; i < Y_DIMENSION; i++)
      for (j = 0; j < X_DIMENSION; j++)  
        if(my_CORE[i][j]->active)
        {
          my_CORE[i][j]->set_buffer_link(my_NOC[i][j]->get_p_in1_buffer());
          my_NOC[i][j]->set_p_out_link2(my_CORE[i][j]);
        }
  }
}

#if NOC_SIM_SNIPER
void ReRAM_Backend::init_CACHE_channels(router ***my_NOC, network_cache***my_CACHE)
#else
void init_CACHE_channels(router ***my_NOC, L2cache*** my_CACHE)
#endif
{
  int i, j;

  //TODO: need to seriously think about this.
  for (i = 0; i < Y_DIMENSION; i++)
    for (j = 0; j < X_DIMENSION; j++){
      my_NOC[i][j]->set_p_out_link1(my_CACHE[i][j]);
      my_CACHE[i][j]->set_output_buffer(my_NOC[i][j]->get_p_in2_buffer());

      //Cache Memory Request Channels are connected to network
      if (conf->ram_mode == dram || conf->ram_mode == l2_cpu)
      {
        my_CACHE[i][j]->set_mem_out_buffer(my_NOC[i][j]->get_p_in3_buffer());
        my_NOC[i][j]->set_p_out_link4(my_CACHE[i][j]);
      }
      //Cache DRAM Request Channels are connected to network
      else if (conf->ram_mode == het_l2_reram)
      {
          my_CACHE[i][j]->set_dram_out_buffer(my_NOC[i][j]->get_p_in3_buffer());
          my_NOC[i][j]->set_p_out_link4_dram(my_CACHE[i][j]);
      }
    }
}

#if NOC_SIM_SNIPER
traffic_generator* ReRAM_Backend::getCoreForThread(int tid)
{
  int x, y, t;

  threads_tile(tid, &x, &y, &t);

  return (my_CORE[y][x]);
}
#endif

#if NOC_SIM_SNIPER
void ReRAM_Backend::run()
#else
long long simulate(router*** my_NOC, traffic_generator*** my_CORE, ram_controller*** my_RRAM, L2cache*** my_CACHE)
#endif
{
    int i, j;
    long long cycle, num_packets_sent;
    long long min_packets;
    stats = Stats::get();
#if NOC_SIM_PARA
    cores_finished_trace = 0;
    ctpl::thread_pool workers(NUMBER_CORES, 100);
    if(NUMBER_CORES)
        printf("Core error");
#else
    int cores_finished_trace = 0;
#endif
#if NOC_SIM_SNIPER
    /* DY:  barrier handle that allows synchronization with front-end */
    ClockSkewMinimizationServer* barrierServer = Sim()->getClockSkewMinimizationServer();
#endif

    cycle = 0;
    num_packets_sent = 0;
    min_packets = conf->packets * NUMBER_CORES;
    if (!min_packets) min_packets = 1;

#if NOC_SIM_SNIPER
    /* DY:  Don't enter NOC-SIM until we've passed fastfwd region.
            Right now, just spinning; would be better to yield and be woken up.  */
    while (stats->in_fastfwd());

    printf("NOC-SIM started.  Will simulate %d minimum packets\n", min_packets);
#elif STATS_OUT
    print_epoch_stats_header();
    long long prev_num_packets_sent = 0;

    if (conf->ram_mode == dram || conf->ram_mode == het_l2_reram)
    {
        std::ofstream epoch_out(conf->dram_conf->json_epoch_name, std::ofstream::out);
        epoch_out << "[";
    }
#endif




    while ((num_packets_sent < min_packets) ||
        stats->map_size() > 0 ||
        (cores_finished_trace < conf->cpu_tiles)) {

#if NOC_SIM_SNIPER
        if (!stats->queues_full())  //not everyone has elements
        {
            /*DY:  for now, just spin*/
            if (barrierServer->isBarrierReached()) {
                if (stats->queues_empty_for_bar()) {  //nobody has elements
                    barrierServer->barrierRelease();
                    stats->incr_frontend_barriers();
                } // otherwise keep cranking, i.e. don't "continue"
                //cycles_queues_not_full++;
            }
            else {
                continue;
            }
        }
#else
        if (stats->any_empty_threads())  //not everyone has elements
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            continue;
        }
#endif


#if BANK_IN_QUEUE_HIST
        if ((cycle + 1) % 1 == 0 && cycle >= 1000) {
            int num_controllers = (conf->dram_mode) ? conf->num_dram_tiles : (conf->x_max * conf->y_max);
            int input_queue_entrees[num_controllers][conf->banks];
            int controller_coords[num_controllers][2];
            int cont_count = 0, min, max, avg, min_cont, max_cont;
            int avg_entrees[conf->banks], min_entree[conf->banks][2], max_entree[conf->banks][2]; // [2] - {entree, controller_id}
            for (int i = 0; i < X_DIMENSION; i++) {
                for (int j = 0; j < Y_DIMENSION; j++) {
                    if (conf->dram_mode) {
                        if (!my_RRAM[i][j]->active)
                            continue;
                    }
                    my_RRAM[i][j]->bank_queue_traffic(input_queue_entrees[cont_count]);
                    controller_coords[cont_count][0] = i;
                    controller_coords[cont_count++][1] = j;
                }
            }
            for (int i = 0; i < conf->banks; i++) {
                min = 0x0fffffff;
                max = 0;
                avg = 0;
                for (int j = 0; j < num_controllers; j++) {
                    if (input_queue_entrees[j][i] < min) {
                        min = input_queue_entrees[j][i];
                        min_cont = j;
                    }

                    if (input_queue_entrees[j][i] > max) {
                        max = input_queue_entrees[j][i];
                        max_cont = j;
                    }

                    avg += input_queue_entrees[j][i];
                }
                avg_entrees[i] = avg / num_controllers;
                min_entree[i][0] = min;
                min_entree[i][1] = min_cont;
                max_entree[i][0] = max;
                max_entree[i][1] = max_cont;

            }

            system("clear");
            printf("-------Cycle - %d--------\n", cycle);
            fflush(stdout);
            int hist_len = 25;

            for (int i = 0; i < conf->banks; i++) {
                int in_q_size = 0, idx, cont_id;
                in_q_size = avg_entrees[i];
                idx = in_q_size * hist_len / conf->queue_depth;
                printf("Bank [%3d] - ", i);
                //printf("%4d ",idx);
                for (int j = 0; j < hist_len; j++) {
                    if (j < idx)
                        printf("*");
                    else
                        printf(" ");
                }
                in_q_size = min_entree[i][0];
                idx = in_q_size * hist_len / conf->queue_depth;
                cont_id = min_entree[i][1];
                printf(" Min [%2d,%2d] - ", controller_coords[cont_id][0], controller_coords[cont_id][1]);
                //printf("%4d ",idx);
                for (int j = 0; j < hist_len; j++) {
                    if (j < idx)
                        printf("*");
                    else
                        printf(" ");
                }
                in_q_size = max_entree[i][0];
                idx = in_q_size * hist_len / conf->queue_depth;
                cont_id = max_entree[i][1];
                printf(" Max [%2d,%2d] - ", controller_coords[cont_id][0], controller_coords[cont_id][1]);
                //printf("%4d ",idx);
                for (int j = 0; j < hist_len; j++) {
                    if (j < idx)
                        printf("*");
                    else
                        printf(" ");
                }
                printf("\n");
            }
        }
#endif

#if !NOC_SIM_SNIPER
#if STATS_OUT
        if (((cycle + 1) % EPOCH_PERIOD) == 0) {
            print_epoch_stats(cycle, my_CORE, prev_num_packets_sent, cores_finished_trace, my_RRAM);
            prev_num_packets_sent = num_packets_sent;
        }
#endif
#endif

#if NOC_SIM_PARA
        cores_finished_trace = 0;

        for (i = 0; i < Y_DIMENSION; i++) {
            for (j = 0; j < X_DIMENSION; j++) {
                if (conf->integrate_cache_rram)
                {
                    workers.push(run_tile_int, cycle, my_NOC[i][j], my_CORE[i][j], my_CACHE[i][j]);
                }
                else
                {
                    workers.push(run_tile, cycle, my_NOC[i][j], my_CORE[i][j], my_RRAM[i][j], my_CACHE[i][j]);
                }
            }
        }

        //workers.stop(true);

        while(workers.n_idle() < workers.size())
            //wait
            continue;

        //printf("\tNetwork Perform Route\n");
        for (i = 0; i < Y_DIMENSION; i++) {
            for (j = 0; j < X_DIMENSION; j++) {
                my_NOC[i][j]->perform_route_actions();
            }
        }
#else
        /* Crank the L2 Caches */
        //printf("\tCache\n");
        for (i = 0; i < Y_DIMENSION; i++) {
            for (j = 0; j < X_DIMENSION; j++) {
                my_CACHE[i][j]->cycle(cycle);
            }
        }

        if (!conf->integrate_cache_rram)
        {
            /* Crank the RRAM controllers */
            //printf("\tRRAM\n");
            for (i = 0; i < Y_DIMENSION; i++) {
                for (j = 0; j < X_DIMENSION; j++) {
                    my_RRAM[i][j]->cycle(cycle);
                }
            }
        }

        /* Crank the traffic generators */
        //printf("\t Traffic Generators.\n");
        cores_finished_trace = 0;
        for (i = 0; i < Y_DIMENSION; i++) {
            for (j = 0; j < X_DIMENSION; j++) {
                my_CORE[i][j]->cycle(cycle);
                cores_finished_trace += my_CORE[i][j]->core_finished_trace();
            }
        }

        /* Crank the network */
        //printf("\tNetwork Determine Route\n");
        for (i = 0; i < Y_DIMENSION; i++) {
            for (j = 0; j < X_DIMENSION; j++) {
                my_NOC[i][j]->decide_route_actions();
            }
        }
        //printf("\tNetwork Perform Route\n");
        for (i = 0; i < Y_DIMENSION; i++) {
            for (j = 0; j < X_DIMENSION; j++) {
                my_NOC[i][j]->perform_route_actions();
            }
        }
#endif

        num_packets_sent = stats->sent_packets;
        cycle++;

#if NOC_SIM_SNIPER
        if (!(cycle % 100000)) {
            printf("NOC-SIM simulated %d cycles, %d packets sent\n", cycle, num_packets_sent);
        }
#endif
    }

    printf("NOC-SIM exiting\n");

#if NOC_SIM_SNIPER
    print_final_stats(cycle, my_RRAM);
    dump_bank_profile();
#else
    return cycle;
#endif
}

#if NOC_SIM_PARA
void run_tile_int(int id, long long cycle, router* noc, traffic_generator* core, L2cache* cache)
{
    /* Crank the L2 Caches */
    //printf("\tCache\n");
    cache->cycle(cycle);

    /* Crank the traffic generators */
    //printf("\t Traffic Generators.\n");
    core->cycle(cycle);
    if(core->core_finished_trace())
        cores_finished_trace++;

    /* Crank the network */
    //printf("\tNetwork Determine Route\n");
    noc->decide_route_actions();
}

void run_tile(int id, long long cycle, router* noc, traffic_generator* core, ram_controller* ram, L2cache* cache)
{
    /* Crank the L2 Caches */
    //printf("\tCache\n");
    cache->cycle(cycle);

    /* Crank the RAM */
    //printf("\tRAM\n");
    ram->cycle(cycle);
    
    /* Crank the traffic generators */
    //printf("\t Traffic Generators.\n");
    core->cycle(cycle);
    if(core->core_finished_trace())
        cores_finished_trace++;

    /* Crank the network */
    //printf("\tNetwork Determine Route\n");
    noc->decide_route_actions();
}
#endif

void print_epoch_stats(long long cycle, traffic_generator*** my_CORE, long long prev_num_packets_sent, long long cores_finished_trace, ram_controller*** my_RRAM)
{
    //Tabulate Stats
    for (int i = 0; i < conf->num_dram_tiles; i++)
    {
        int x, y;
        conf->nth_tile_type(i, dram_tile, &x, &y);
        my_RRAM[y][x]->PrintEpochStats();
        std::ofstream epoch_out(conf->dram_conf->json_epoch_name, std::ofstream::app);
        epoch_out << "," << std::endl;
        my_RRAM[y][x]->collect_stats();
    }
    stats->collect_stats();

    //Do needed calculations
    double mpki = (double)(stats->latest_l2_stats.l2_miss) / ((double)(stats->latest_cpu_stats.ins_executed) / 1000.0);
    double bw = (((stats->latest_ram_stats.reram_read + stats->latest_ram_stats.reram_write) / (EPOCH_PERIOD * conf->cycle_time * 1e-9) / (1e9)) * 8 * conf->reram_settings.fetch_width) +
                (((stats->latest_ram_stats.dram_read + stats->latest_ram_stats.dram_write) / (EPOCH_PERIOD * conf->cycle_time * 1e-9) / (1e9)) * 8 * conf->dram_settings.fetch_width);
    double refresh_energy = (conf->dram_settings.refresh_power + conf->reram_settings.refresh_power) * cycle * 1000; //J/s * ns (e-9) = pJ (e-12)
    double trans_energy = stats->latest_flit_jumps * conf->transport_energy; //pJ (e-12)
    double reram_energy = (stats->latest_ram_stats.reram_read * conf->reram_settings.read_energy) + (stats->latest_ram_stats.reram_write * conf->reram_settings.write_energy);
    double dram_energy = stats->latest_dram_stats.tot_energy;
    double trans_power = (trans_energy / EPOCH_PERIOD) / 1000.0; //pJ (e-12) / ns (e-9) / (e3) = W
    double reram_power = (reram_energy / EPOCH_PERIOD) / 1000.0;
    double dram_power = (dram_energy / EPOCH_PERIOD) / 1000.0;
    double gteps_per_watt = (stats->latest_cpu_stats.te_count / (trans_energy + reram_energy + dram_energy + refresh_energy)) * 1000; //TE (e9) / pJ (e-12) * e(3) = GTEPS/W
    double row_buffer_miss_rate = (1.0 - ((double)(stats->latest_dram_stats.row_hits) / (double)(stats->latest_dram_stats.reads + stats->latest_dram_stats.writes))) * 100;
    double percent_busy = (double)(stats->latest_ram_stats.busy_bank_cycles + stats->latest_dram_stats.busy_cycles - stats->latest_ram_stats.extra_cycles_from_next_epoch);
    percent_busy = percent_busy / (double)(EPOCH_PERIOD);
    percent_busy = percent_busy / (double)(BANKS);
    percent_busy = percent_busy * 100.0;

#if INSTRUCTION_QUEUE_STATS
    int x, y, thread;
    unsigned long long avg_entrees, max_entrees, min_entrees;
    int max_entrees_thread, min_entrees_thread;
    int entree_hist[10] = { 0,0,0,0,0,0,0,0,0,0 };

    avg_entrees = 0;
    min_entrees = 0x7fffffffffffff;
    max_entrees = 0;

    if (conf->trace) {
        for (int t = 0; t < conf->threads; t++) {
            conf->threads_tile(t, &x, &y, &thread);

            unsigned long long temp_entrees = my_CORE[y][x]->my_threads[thread]->queue->traffic_queue_size();
            avg_entrees += temp_entrees;
            if (temp_entrees > max_entrees) {
                max_entrees = temp_entrees;
                max_entrees_thread = t;
            }
            if (temp_entrees < min_entrees) {
                min_entrees = temp_entrees;
                min_entrees_thread = t;
            }

            for (int i = 0; i < 9; i++)
            {
                temp_entrees /= 10;
                if (temp_entrees == 0)
                {
                    entree_hist[i]++;
                    break;
                }
            }
            if (temp_entrees > 0)
                entree_hist[9]++;
        }
    }
#endif

    //Print to console
    printf("Cycle %lld: %.1f %% utilization. EPOCH_STATS : ", cycle, percent_busy);
    printf("packets sent-[%lld], ", (stats->sent_packets - prev_num_packets_sent));
    printf("traversed edges-[%lld], ", stats->latest_cpu_stats.te_count);
    printf("\n");

    printf("\t");
    printf("Instructions Executed - [%lld], ", stats->latest_cpu_stats.ins_executed);
    printf("Bandwidth - [%.3f], ", bw);
    printf("MPKI - [%.3f], ", mpki);
    printf("cores_finished_trace - [%lld], ", cores_finished_trace);
    printf("\n");

    printf("\t");
    printf("L1 misses - [%lld], ", stats->latest_l1_stats.l1_miss);
    printf("L1 hits - [%lld], ", stats->latest_l1_stats.l1_hit);
    printf("L1 wb - [%lld], ", stats->latest_l1_stats.wb_from_l1);
    printf("L2 misses - [%lld], ", stats->latest_l2_stats.l2_miss);
    printf("L2 hits - [%lld], ", stats->latest_l2_stats.l2_hit);
    printf("L2 wb - [%lld], ", stats->latest_l2_stats.wb_from_l2);
    printf("vec L2 wb - [%lld], ", stats->latest_l2_stats.vec_wb_from_l2);
    printf("\n");

    if (conf->prefetch || conf->ram_mode == het_l2_reram || conf->ram_mode == dram)
    {
        printf("\t");
        if (conf->ram_mode == het_l2_reram || conf->ram_mode == dram)
        {
            printf("Row buffer Misses - [%lld], ", stats->latest_dram_stats.row_hits);
            printf("Row Buffer miss rate - [%lf], ", row_buffer_miss_rate);
        }
        if (conf->prefetch)
        {
            printf("packets_pref - [%lld], ", stats->latest_cpu_stats.packets_prefetched);
            printf("pref_q_hits - [%lld], ", stats->latest_cpu_stats.prefetchq_hits);
        }
        printf("\n");
    }

    printf("\t");
    printf("Cache Fill - [%lld], ", stats->latest_l2_stats.cache_fill);
    printf("L2 reads - [%lld], ", stats->latest_l2_stats.l2_read);
    printf("L2 vec reads - [%lld], ", stats->latest_l2_stats.l2_vec_read);
    printf("L2 writes - [%lld], ", stats->latest_l2_stats.l2_write);
    printf("L2 vec writes - [%lld], ", stats->latest_l2_stats.l2_vec_write);
    printf("\n");

    printf("\t");
    printf("In queue - [%.0f], ", stats->cur_avg_in_queue_latency());
    printf("RAM - [%.0f], ", stats->cur_avg_ram_latency());
    printf("Out queue - [%.1f], ", stats->cur_avg_out_queue_latency());
    printf("In NOC - [%.1f], ", stats->cur_avg_in_noc_latency());
    printf("Out NOC - [%.1f], ", stats->cur_avg_out_noc_latency());
    if (conf->ram_mode == dram || conf->ram_mode == l2_cpu || conf->ram_mode == het_l2_reram)
    {
        printf("In DRAM NOC - [%.1f], ", stats->cur_avg_to_dram_noc_latency());
        printf("Out DRAM NOC - [%.1f], ", stats->cur_avg_from_dram_noc_latency());
    }
    printf("\n");


    //TODO: integrate better
    if (conf->ram_mode == het_l2_reram || conf->ram_mode == dram)
    {
        printf("\t");
        printf("DRAM read - [%lld], ", stats->latest_dram_stats.reads);
        printf("DRAM write - [%lld], ", stats->latest_dram_stats.writes);
        printf("DRAM BW - [%.1f], ", stats->latest_dram_stats.average_bw);
        printf("DRAM Energy - [%.1f], ", stats->latest_dram_stats.tot_energy);
        printf("DRAM read lat - [%.1f], ", stats->latest_dram_stats.average_read_lat);
        printf("\n");
    }


    printf("\t");
    printf("Flit Jumps - [%lld], ", stats->latest_flit_jumps);
    printf("RAM Reads - [%lld], ", stats->latest_ram_stats.reram_read + stats->latest_ram_stats.dram_read);
    printf("RAM Writes - [%lld], ", stats->latest_ram_stats.reram_write + stats->latest_ram_stats.dram_write);
    printf("Transport Power - [%.2f], ", trans_power);
    if (conf->ram_mode != dram)
        printf("ReRAM Power - [%.2f], ", reram_power);
    if (conf->ram_mode == dram || conf->ram_mode == het_l2_reram)
        printf("DRAM Power - [%.2f], ", dram_power);
    //printf("GTEPS/W - [%.2f], ", gteps_per_watt);
    printf("\n");

    printf("\t");
    printf("Read - [%lld], ", stats->total_ins_stats.read_single);
    printf("Vec Read - [%lld], ", stats->total_ins_stats.read_vector);
    printf("Gather - [%lld], ", stats->total_ins_stats.gather);
    printf("Write - [%lld], ", stats->total_ins_stats.write_single);
    printf("Vec Write - [%lld], ", stats->total_ins_stats.write_vector);
    printf("Scatter - [%lld], ", stats->total_ins_stats.scatter);
    printf("Other - [%lld], ", stats->total_ins_stats.other);
    printf("\n");

#if INSTRUCTION_QUEUE_STATS
    if (conf->trace) {
        printf("\t");
        printf("Avg_queue_size - [%llu], ", avg_entrees / conf->threads);
        printf("Max_queue_size_and_thread - [%llu, %d],  ", max_entrees, max_entrees_thread);
        printf("Min_queue_size_and_thread_ID - [%llu, %d], ", min_entrees, min_entrees_thread);
        printf("Map Size in Stats - [%d], ", stats->map_size());
        printf("\n");
        
        printf("\tQueue Size Distrib [(10^x), #] - ");
        for (int i = 0; i < 10; i++)
            printf("[%d, %d] ", i, entree_hist[i]);
        printf("\n");
    }
#endif

    //Print to file
    epochStatsFile << cycle << ",";
    epochStatsFile << percent_busy << ",";
    epochStatsFile << (stats->sent_packets - prev_num_packets_sent) << ",";
    epochStatsFile << (stats->latest_cpu_stats.te_count) << ",";
    epochStatsFile << stats->latest_cpu_stats.ins_executed << ",";
    epochStatsFile << bw << ",";
    epochStatsFile << mpki << ",";
    epochStatsFile << cores_finished_trace << ",";
    epochStatsFile << stats->latest_l1_stats.l1_miss << ",";
    epochStatsFile << stats->latest_l1_stats.l1_hit << ",";
    epochStatsFile << stats->latest_l1_stats.wb_from_l1 << ",";
    epochStatsFile << stats->latest_l2_stats.l2_miss << ",";
    epochStatsFile << stats->latest_l2_stats.l2_hit << ",";
    epochStatsFile << stats->latest_l2_stats.wb_from_l2 << ",";
    if (conf->ram_mode == het_l2_reram || conf->ram_mode == dram)
    {
        epochStatsFile << stats->latest_dram_stats.row_hits << ",";
        epochStatsFile << row_buffer_miss_rate << ",";
    }
    if (conf->prefetch)
    {
        epochStatsFile << stats->latest_cpu_stats.packets_prefetched << ",";
        epochStatsFile << stats->latest_cpu_stats.prefetchq_hits << ",";
    }
    epochStatsFile << stats->latest_l2_stats.cache_fill << ",";
    epochStatsFile << stats->latest_l2_stats.l2_read << ",";
    epochStatsFile << stats->latest_l2_stats.l2_vec_read << ",";
    epochStatsFile << stats->latest_l2_stats.l2_write << ",";
    epochStatsFile << stats->latest_l2_stats.l2_vec_write << ",";
    epochStatsFile << stats->cur_avg_in_queue_latency() << ",";
    epochStatsFile << stats->cur_avg_ram_latency() << ",";
    epochStatsFile << stats->cur_avg_out_queue_latency() << ",";
    epochStatsFile << stats->cur_avg_in_noc_latency() << ",";
    epochStatsFile << stats->cur_avg_out_noc_latency() << ",";
    if (conf->ram_mode == dram || conf->ram_mode == l2_cpu || conf->ram_mode == het_l2_reram)
    {
        epochStatsFile << stats->cur_avg_to_dram_noc_latency() << ",";
        epochStatsFile << stats->cur_avg_from_dram_noc_latency() << ",";
    }
    epochStatsFile << stats->latest_flit_jumps << ",";
    epochStatsFile << stats->latest_ram_stats.reram_read << ",";
    epochStatsFile << stats->latest_ram_stats.reram_write << ",";
    epochStatsFile << stats->latest_ram_stats.dram_read << ",";
    epochStatsFile << stats->latest_ram_stats.dram_write << ",";
    epochStatsFile << trans_power << ",";
    epochStatsFile << reram_power << ",";
    epochStatsFile << dram_power << ",";
    epochStatsFile << gteps_per_watt << ",";
    epochStatsFile << stats->total_ins_stats.read_single << ",";
    epochStatsFile << stats->total_ins_stats.read_vector << ",";
    epochStatsFile << stats->total_ins_stats.gather << ",";
    epochStatsFile << stats->total_ins_stats.write_single << ",";
    epochStatsFile << stats->total_ins_stats.write_vector << ",";
    epochStatsFile << stats->total_ins_stats.scatter << ",";
    epochStatsFile << stats->total_ins_stats.other << ",";
    epochStatsFile << "\n" << std::flush;

}

void print_epoch_stats_header()
{
    epochStatsFile << "Cycle," << "Util," << "Packets_sent," << "TEs,";
    epochStatsFile << "Ins_exec," << "BW," << "MPKI," << "cores_finished,";
    epochStatsFile << "L1_Misses," << "L1_hits," << "L1_WB," << "L2_misses," << "L2_hits," << "L2_wb,";
    if (conf->ram_mode == het_l2_reram || conf->ram_mode == dram)
        epochStatsFile << "Row Hits," << "Row Buffer Miss Rate,";
    if (conf->prefetch)
        epochStatsFile << "Packets Prefetched," << "Prefetch Queue hits,";
    epochStatsFile << "L2 Cache Fill," << "L2 Reads," << "L2 Vec Reads," << "L2 Write," << "L2 Vec Write,";
    epochStatsFile << "inQ_lat," << "RAM_lat," << "outQ_lat," << "In_NOC_lat," << "Out_NOC_lat,";
    if (conf->ram_mode == dram || conf->ram_mode == l2_cpu || conf->ram_mode == het_l2_reram)
        epochStatsFile << "To_DRAM_lat," << "From_DRAM_lat,";
    epochStatsFile << "Flit_Jumps," << "ReRAM_Reads," << "ReRAM_Writes," << "DRAM_Reads," << "DRAM_Writes,";
    epochStatsFile << "Transport_power," << "ReRAM Power," << "DRAM Power," << "GTEPS/W,";
    epochStatsFile << "Read Ins," << "Vec Read Ins," << "Gather Ins," << "Write Ins," << "Vec Write Ins," << "Scatter Ins," << "Other Ins,";
    epochStatsFile << "\n";
}

void print_final_stats(long long cycle, ram_controller*** my_RRAM)
{
    if (conf->ram_mode == dram || conf->ram_mode == het_l2_reram)
    {
        // Finish epoch output, remove last comma and append ]
        std::ofstream epoch_out(conf->dram_conf->json_epoch_name, std::ios_base::in |
            std::ios_base::out |
            std::ios_base::ate);
        epoch_out.seekp(-2, std::ios_base::cur);
        epoch_out.write("]", 1);
        epoch_out.close();

        std::ofstream json_out(conf->dram_conf->json_stats_name, std::ofstream::out);
        json_out << "{";

        // close it now so that each channel can handle it
        json_out.close();
        stats->total_dram_stats = dramsim3::noc_sim_dram_stats();
        for (int i = 0; i < conf->num_dram_tiles; i++)
        {
            int x, y;
            conf->nth_tile_type(i, dram_tile, &x, &y);
            my_RRAM[y][x]->PrintFinalStats();

            if (i != conf->num_dram_tiles - 1) {
                std::ofstream chan_out(conf->dram_conf->json_stats_name, std::ofstream::app);
                chan_out << "," << std::endl;
            }
        }
        json_out.open(conf->dram_conf->json_stats_name, std::ofstream::app);
        json_out << "}";
    }

    //Tabulate Stats
    stats->collect_stats();

    long long total_packets = 0, total_traversed_edges = 0, total_instructions_executed = 0, total_read_packets_sent = 0, total_write_packets_sent = 0;
    long long gather_width = 0, gather_cnt = 0, gather_miss_width = 0, prefetched_pac_cnt = 0, prefetch_hit_cnt = 0;
    long long total_flit_jumps, total_reram_reads, total_reram_writes, total_dram_reads, total_dram_writes;
    double trans_energy, reram_energy, dram_energy, refresh_energy, trans_power, reram_power, dram_power, gteps_per_watt;
    double bandwidth, throughput;
    double mpki = 0;
    double useful_data_PKI = 0;

    total_packets = stats->total_latency.tot_packets;
    total_traversed_edges = stats->total_cpu_stats.te_count;
    total_instructions_executed = stats->total_cpu_stats.ins_executed;
    prefetched_pac_cnt = stats->total_cpu_stats.packets_prefetched;
    prefetch_hit_cnt = stats->total_cpu_stats.prefetchq_hits;
    total_read_packets_sent = stats->total_l1_stats.rd_exec;
    total_write_packets_sent = stats->total_l1_stats.wr_exec;

    total_flit_jumps = stats->total_flit_jumps;
    total_reram_reads = stats->total_ram_stats.reram_read;
    total_reram_writes = stats->total_ram_stats.reram_write;
    total_dram_reads = stats->total_ram_stats.dram_read;
    total_dram_writes = stats->total_ram_stats.dram_write;

    refresh_energy = (conf->reram_settings.refresh_power + conf->dram_settings.refresh_power) * cycle * 1000; //J/s * ns (e-9) = pJ (e-12)
    trans_energy = total_flit_jumps * conf->transport_energy; //pJ (e-12)
    reram_energy = (total_reram_reads * conf->reram_settings.read_energy) + (total_reram_writes * conf->reram_settings.write_energy);
    dram_energy = stats->total_dram_stats.tot_energy;
    trans_power = (trans_energy / cycle) / 1000.0; //pJ (e-12) / ns (e-9) * (e3) = W
    reram_power = (reram_energy / cycle) / 1000.0;
    dram_power = (dram_energy / cycle) / 1000.0;
    gteps_per_watt = (total_traversed_edges / (trans_energy + reram_energy + dram_energy + refresh_energy)) * 1000; //TE (e9) / pJ (e-12) * e(3) = GTEPS/W

    double l1_miss_rate = ((double)stats->total_l1_stats.l1_miss / (double)(stats->total_l1_stats.l1_hit + stats->total_l1_stats.l1_miss)) * 100;
    double l2_miss_rate = ((double)stats->total_l2_stats.l2_miss / (double)(stats->total_l2_stats.l2_hit + stats->total_l2_stats.l2_miss)) * 100;
    double l2_gather_miss_rate = ((double)stats->total_l2_stats.l2_gather_miss / (double)(stats->total_l2_stats.l2_gather_hit + stats->total_l2_stats.l2_gather_miss)) * 100;
    double row_buffer_miss_rate = (1.0 - ((double)(stats->total_dram_stats.row_hits) / (double)(stats->total_dram_stats.reads + stats->total_dram_stats.writes))) * 100;

    printf("Sent %lld packets in %lld cycles.\n", total_packets, cycle);

    //assumes all requests processed made it back, and doesn't count unprocesssed ones
    //then divided by total time (cycles * cycle time * ns) then made into GigaTransactions/s
    throughput = stats->total_latency.ram_packets / (cycle * conf->cycle_time * 1e-9) / (1e9);
    bandwidth = (((total_reram_reads + total_reram_writes) * 8.0 * conf->reram_settings.fetch_width) + ((total_dram_reads + total_dram_writes) * 8.0 * conf->dram_settings.fetch_width)) / (cycle * conf->cycle_time * 1e-9) / (1e9);
    mpki = ((float)stats->total_l2_stats.l2_miss * 1000 / (float)stats->total_cpu_stats.ins_executed);
    useful_data_PKI = (((double)(stats->total_l1_stats.l1_hit + stats->total_l1_stats.l1_miss) / (double)stats->total_cpu_stats.ins_executed) * 1000 * 8); //in Bytes PKI

    epochStatsFile.close();

#if STATS_OUT
    printf("Inbound packets received: %lld, outbound packets received: %lld\n", (stats->total_latency.in_noc_packets), stats->total_latency.out_noc_packets);
    printf("Average network latency:  %.1f, inbound: %.1f, outbound: %.1f\n",
        stats->avg_noc_latency(), stats->avg_in_noc_latency(), stats->avg_out_noc_latency());
    printf("To DRAM: %.1f, from DRAM: %.1f\n", stats->avg_to_dram_noc_latency(), stats->avg_from_dram_noc_latency());
    printf("Packets entering in queue: %lld\n", stats->total_latency.in_queue_packets);
    printf("Average in queue latency:  %.1f\n", stats->avg_in_queue_latency());
    printf("RAM requests completed: %lld\n", stats->total_latency.ram_packets);
    printf("Average RAM latency:  %.1f\n", stats->avg_ram_latency());
    printf("Packets exiting out queue: %lld\n", stats->total_latency.out_queue_packets);
    printf("Average out queue latency:  %.1f\n", stats->avg_out_queue_latency());
    printf("Completed %lld requests in %lld cycles.\n", stats->total_latency.tot_packets, cycle);
    printf("Total average latency:  %.1f\n", stats->avg_latency());
    printf("\n");
    printf("Average throughput:  %.1f GT/s\n", throughput);
    printf("Average bandwidth:  %.1f GB/s\n", bandwidth);
    printf("Traversed Edges : %lld\n", total_traversed_edges);
    printf("GTEPS : %.1f\n", (float)total_traversed_edges / cycle);
    printf("Average Gather width : %.1f\n", (float)gather_width / gather_cnt);
    printf("Average Gather Miss Width : %.1f\n", (float)gather_miss_width / gather_cnt);
    printf("Gather cnt : %lld\n", gather_cnt);
    printf("Instructions Executed : %lld\n", total_instructions_executed);
    printf("IPC : %.1f INS/cycle\n", (float)total_instructions_executed / cycle);
    printf("Packets prefetched :  %lld\n", prefetched_pac_cnt);
    printf("Prefetched packets used :  %lld\n", prefetch_hit_cnt);
    printf("Read packets sent :  %lld\n", total_read_packets_sent);
    printf("write packets sent :  %lld\n", total_write_packets_sent);
    printf("Flit hops : %lld, ReRAM Reads : %lld, ReRAM Writes : %lld\n", total_flit_jumps, total_reram_reads, total_reram_writes);
    printf("DRAM Reads : %lld, DRAM Writes : %lld\n", total_dram_reads, total_dram_writes);
    printf("Transport Energy : %.1f pJ, Transport Power : %.1f W\n", trans_energy, trans_power);
    printf("ReRAM Energy : %.1f pJ, ReRAM Power : %.1f W\n", reram_energy, reram_power);
    printf("DRAM Energy : %.1f pJ, DRAM Power : %.1f W\n", dram_energy, dram_power);
    printf("Refresh Energy : %.1f pJ, Refresh Power : %.1f W\n", refresh_energy, conf->reram_settings.refresh_power + conf->dram_settings.refresh_power);
    printf("GTEPS/W : %.1f\n", gteps_per_watt);
#endif


#if CSV_OUT
    struct stat buf;
    ofstream outfile;
    string filename = "output.csv";
    if (stat(filename.c_str(), &buf) == -1) //need to make file
    {
        outfile.open(filename, std::ofstream::out);
        outfile << "title,";
        outfile << "mode,";
        outfile << "x,";
        outfile << "y,";
        outfile << "reram_controllers,";
        outfile << "dram_controllers,";
        outfile << "threads,";
        outfile << "reram_read_lat,";
        outfile << "reram_write_lat,";
        outfile << "reram_fetch_width,";
        outfile << "dram_fetch_width,";
        outfile << "phits,";
        outfile << "reram_banks,";
        outfile << "dram_banks,";
        outfile << "buffer_size,";
        outfile << "reram_queue_depth,";
        outfile << "dram_queue_depth,";
        if (!conf->trace)
        {
            outfile << "spatial_reuse,";
            outfile << "ins_per_mem_access,";
            outfile << "useful_data_PKI,"; // Useful data fetched per kilo instructions for synthetic benchmarks (BPKI)
        }
        outfile << "cycle,";
        outfile << "total_instructions_executed,";
        outfile << "mpki,";
        outfile << "GTEPS,";
        outfile << "IPC,";
        outfile << "throughput,";
        outfile << "bandwidth,";
        outfile << "tot_packets,";
        outfile << "avg_latency,";
        outfile << "trans_energy,";
        outfile << "reram_energy,";
        outfile << "dram_energy,";
        outfile << "trans_power,";
        outfile << "reram_power,";
        outfile << "dram_power,";
        outfile << "L1_misses,";
        outfile << "L1_hits,";
        outfile << "L1_wb,";
        outfile << "L1_miss_rate,";
        outfile << "L2_misses,";
        outfile << "L2_hits,";
        outfile << "L2_wb,";
        outfile << "L2_miss_rate,";
        outfile << "Row Hits,";
        outfile << "Row Miss Rate,";
        outfile << "Packets Prefetched,";
        outfile << "Prefetch Hits,";
        outfile << "L2 Cache Fill,";
        outfile << "L2 Reads,";
        outfile << "L2 Vec Reads,";
        outfile << "L2 Writes,";
        outfile << "L2 Vec Writes,";
        outfile << "avg_noc_latency,";
        outfile << "in_noc_packets,";
        outfile << "avg_in_noc_latency,";
        outfile << "to_dram_packets,";
        outfile << "avg_to_dram_latency,";
        outfile << "in_queue_packets,";
        outfile << "avg_in_queue_latency,";
        outfile << "ram_packets,";
        outfile << "avg_ram_latency,";
        outfile << "out_queue_packets,";
        outfile << "avg_out_queue_latency,";
        outfile << "from_dram_packets,";
        outfile << "avg_from_dram_latency,";
        outfile << "out_noc_packets,";
        outfile << "avg_out_noc_latency,";
        outfile << "total_flit_jumps,";
        outfile << "total_reram_reads,";
        outfile << "total_reram_writes,";
        outfile << "total_dram_reads,";
        outfile << "total_dram_writes,";
        outfile << "L2_gather_miss_rate,";
        outfile << "\n";
    }
    else
    {
        outfile.open(filename, std::ofstream::app);
    }
    outfile << conf->sim_title << ",";
    outfile << ((conf->ram_mode == dram) ? "D" : "R") << "RAM,";
    outfile << X_DIMENSION << ",";
    outfile << Y_DIMENSION << ",";
    outfile << (conf->num_reram_tiles) << ",";
    outfile << (conf->num_dram_tiles) << ",";
    outfile << conf->threads << ",";
    outfile << conf->reram_settings.read_lat << ",";
    outfile << conf->reram_settings.write_lat << ",";
    outfile << (conf->reram_settings.fetch_width * 8) << ",";
    outfile << (conf->dram_settings.fetch_width * 8) << ",";
    outfile << conf->phits << ",";
    outfile << conf->reram_settings.banks << ",";
    outfile << conf->dram_settings.banks << ",";
    outfile << conf->buffer_size << ",";
    outfile << conf->reram_settings.queue_depth << ",";
    outfile << conf->dram_settings.queue_depth << ",";
    if (!conf->trace)
    {
        outfile << conf->spatial_reuse << ",";
        outfile << conf->ins_per_mem_access << ",";
        outfile << useful_data_PKI << ","; // Useful data fetched per kilo instructions for synthetic benchmarks (BPKI)
    }
    outfile << cycle << ",";
    outfile << total_instructions_executed << ",";
    outfile << mpki << ",";
    outfile << ((float)total_traversed_edges / cycle) << ",";
    outfile << ((float)total_instructions_executed / cycle) << ",";
    outfile << throughput << ",";
    outfile << bandwidth << ",";
    outfile << stats->total_latency.tot_packets << ",";
    outfile << stats->avg_latency() << ",";
    outfile << trans_energy << ",";
    outfile << reram_energy << ",";
    outfile << dram_energy << ",";
    outfile << trans_power << ",";
    outfile << reram_power << ",";
    outfile << dram_power << ",";
    outfile << stats->total_l1_stats.l1_miss << ",";
    outfile << stats->total_l1_stats.l1_hit << ",";
    outfile << stats->total_l1_stats.wb_from_l1 << ",";
    outfile << l1_miss_rate << ",";
    outfile << stats->total_l2_stats.l2_miss << ",";
    outfile << stats->total_l2_stats.l2_hit << ",";
    outfile << stats->total_l2_stats.wb_from_l2 << ",";
    outfile << l2_miss_rate << ",";
    outfile << stats->total_dram_stats.row_hits << ",";
    outfile << row_buffer_miss_rate << ",";
    outfile << stats->total_cpu_stats.packets_prefetched << ",";
    outfile << stats->total_cpu_stats.prefetchq_hits << ",";
    outfile << stats->total_l2_stats.cache_fill << ",";
    outfile << stats->total_l2_stats.l2_read << ",";
    outfile << stats->total_l2_stats.l2_vec_read << ",";
    outfile << stats->total_l2_stats.l2_write << ",";
    outfile << stats->total_l2_stats.l2_vec_write << ",";
    outfile << stats->avg_noc_latency() << ",";
    outfile << (stats->total_latency.in_noc_packets) << ",";
    outfile << stats->avg_in_noc_latency() << ",";
    outfile << stats->total_latency.controller_packets << ",";
    outfile << stats->avg_to_dram_noc_latency() << ",";
    outfile << stats->total_latency.in_queue_packets << ",";
    outfile << stats->avg_in_queue_latency() << ",";
    outfile << stats->total_latency.ram_packets << ",";
    outfile << stats->avg_ram_latency() << ",";
    outfile << stats->total_latency.out_queue_packets << ",";
    outfile << stats->avg_out_queue_latency() << ",";
    outfile << stats->total_latency.from_dram_packets << ",";
    outfile << stats->avg_from_dram_noc_latency() << ",";
    outfile << stats->total_latency.out_noc_packets << ",";
    outfile << stats->avg_out_noc_latency() << ",";
    outfile << total_flit_jumps << ",";
    outfile << total_reram_reads << ",";
    outfile << total_reram_writes << ",";
    outfile << total_dram_reads << ",";
    outfile << total_dram_writes << ",";
    outfile << l2_gather_miss_rate << ",";
    outfile << "\n";
    outfile.close();
#endif

#if MEM_LEAK_DEBUG
    outfile.open("load_balance.csv", std::ofstream::app);
    for (int i = 0; i < conf->threads; i++) {
        outfile << tracer->ins_count[i] << ",";
    }
    outfile << "\n";
    outfile.close();
#endif

    stats->print_distrib();

}

void read_mem_trace(traffic_generator ***my_CORE)
{
   //mem_trace *tracer = new mem_trace(my_CORE);
   tracer = new mem_trace(my_CORE);
   tracer->extractMemory(conf->trace_file);
}

#if NOC_SIM_SNIPER
void ReRAM_Backend::spawn()
{
  m__thread = _Thread::create(this);
  m__thread->run();
}
#endif


#if !NOC_SIM_SNIPER // DY:  remove main for integration into sniper
int main(int argc, char *argv[])
{
  int i, j;// , k;
  router ***my_NOC;
  L2cache ***my_CACHE;
  ram_controller ***my_RRAM;
  traffic_generator ***my_CORE;
  long long cycle;

  //mcheck(&abort);

  parse_config(argc, argv);

  epochStatsFile.open(conf->out_file.c_str(), std::ofstream::app);

  /* create the routers */
  my_NOC = new router**[Y_DIMENSION];
  for (i = 0; i < Y_DIMENSION; i++) {
    my_NOC[i] = new router*[X_DIMENSION];
    for (j = 0; j < X_DIMENSION; j++) {
      my_NOC[i][j] = new router(j, i, conf->buffer_size);
    }
  }

  /* create the caches and reram*/
  my_CACHE = new L2cache**[Y_DIMENSION];
  for (i = 0; i < Y_DIMENSION; i++) {
      my_CACHE[i] = new L2cache*[X_DIMENSION];
      for (j = 0; j < X_DIMENSION; j++) {
          my_CACHE[i][j] = new L2cache(j, i);
      }
  }

  if (!conf->integrate_cache_rram)
  {
	  /* create the RRAM banks */
	  my_RRAM = new ram_controller**[Y_DIMENSION];
	  for (i = 0; i < Y_DIMENSION; i++) {
		  my_RRAM[i] = new ram_controller*[X_DIMENSION];
		  for (j = 0; j < X_DIMENSION; j++) {
              if(conf->get_tile_type(j, i) == dram_tile)
                my_RRAM[i][j] = new dram_controller(j, i);
              else
			    my_RRAM[i][j] = new rram_controller(j, i);
		  }
	  }
  }

  /* create the traffic generators */
  my_CORE = new traffic_generator**[Y_DIMENSION];
  for (i = 0; i < Y_DIMENSION; i++) {
    my_CORE[i] = new traffic_generator*[X_DIMENSION];
    for (j = 0; j < X_DIMENSION; j++) {
      my_CORE[i][j] = new traffic_generator(j, i, conf->packets);
    }
  }
  if (conf->packets == 0)
  {
    my_CORE[2][2]->set_npackets(1);
  }

  /* link up routers and RRAM banks */
  init_NOC_channels(my_NOC);
  init_CORE_channels(my_NOC, my_CORE);
  if (!conf->integrate_cache_rram)
  {
	  init_RRAM_channels(my_CACHE, my_RRAM, my_NOC);
  }
  init_CACHE_channels(my_NOC, my_CACHE);
  
  if (conf->trace)
  {
    //read in the trace file in a separate thread
    std::thread trace(&read_mem_trace, my_CORE);

    //tracer->extractMemory(conf->trace_file);

    /* main sim loop */
    cycle = simulate(my_NOC, my_CORE, my_RRAM, my_CACHE);
    print_final_stats(cycle, my_RRAM);

    //trace.join();
    trace.detach();
  }
  else
  {
    /* main sim loop */
    cycle = simulate(my_NOC, my_CORE, my_RRAM, my_CACHE);
    print_final_stats(cycle, my_RRAM);
  }


#if BANK_PROF_OUT
  dump_bank_profile();
#endif
  
#ifdef _MSC_VER   //if visual studio
    getchar();
#endif
}
#endif

void dump_bank_profile()
{
  int i, j, k, max, m_i, m_j, m_k;
  int num_banks_used = 0;
  ofstream bankProfFile;
  bankProfFile.open("bank_profile.csv", std::ofstream::trunc);

  int max_banks = (conf->ram_mode == dram) ? conf->dram_settings.banks :
    (conf->dram_settings.banks > conf->reram_settings.banks) ? conf->dram_settings.banks : conf->reram_settings.banks;
  

  m_i = m_j = m_k = 0;

  max = 0;
  //bankProfFile<<"X,Y,bank,usage\n";
  for(k = 0; k < max_banks; k++)
      bankProfFile<<","<<k;
  bankProfFile<<"\n";
  
  for (i = 0; i < conf->x_max; i++)
  {
    for (j = 0; j < conf->y_max; j++)
    {
        bankProfFile << i*conf->y_max + j;
        for (k = 0; k < max_banks; k++)
        {
            bankProfFile<< "," << stats->bank_profile[i][j][k];
            if (stats->bank_profile[i][j][k])
            {
                //printf("bank %d, %d, %d:  %lld\n", i, j, k, stats->bank_profile[i][j][k]);
                num_banks_used++;
                if (stats->bank_profile[i][j][k] > max)
                {
                    max = stats->bank_profile[i][j][k];
                    m_i = i;
                    m_j = j;
                    m_k = k;
                }
            }
        }
        bankProfFile << "\n";
    }
  }
  bankProfFile.close();
  printf("%d banks utilized (%f)\n", num_banks_used, (float)num_banks_used / BANKS);
  printf("Worst case bank-%d at tile (%d,%d) with %d accesses\n", m_k, m_i, m_j, max);
}
