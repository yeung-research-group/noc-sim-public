#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_reram_pipeline::L2cache_reram_pipeline(int x, int y) : L2cache_bank(x, y), L2cache_pipeline(x, y), L2cache_reram(x,y)
{
    //nothing to set up
}

void L2cache_reram_pipeline::cycle(long long cur_cycle)
{
    stall_reram = false;
    //route out and only proceed if output is cleared
    route_out(cur_cycle);
    if (output_queue.size() < conf->buffer_size)  //TODO: better size limit?
    {
        move_pipeline(cur_cycle);
        check_incoming_reram(cur_cycle);
        send_request();
    }

    //route and transfer packets from NOC
    if (request_rob.size() < conf->l2_settings.rob_entries)
    {
        route_in(cur_cycle);
    }
}


void L2cache_reram_pipeline::check_incoming_reram(long long cur_cycle)
{
    if (!bank_schedule.empty() && bank_schedule.top().cycle <= cur_cycle)
    {
        if (!stall_reram && !decode_active_latch)
        {
            if (bank_schedule.top().read)
            {
                if (move_req(reram_banks[bank_schedule.top().bank].cur_op, rr_r, false))
                {
                    stats->set_from_bank(decode_latch->packet_id, cur_cycle, x_coord, y_coord);
                    decode_latch->valid[SECTOR_ID(decode_latch->bank)] = true;
                    decode_latch->valid_data = true;

                    reram_banks[bank_schedule.top().bank].busy = false;
                    bank_schedule.pop();
                }
            }
            else  //write is done - frees bank, need to check for next item
            {
                if(move_req(reram_banks[bank_schedule.top().bank].cur_op, mshr_r, false))
                {
                    mshr_latch->valid_data = true;
                    reram_banks[bank_schedule.top().bank].busy = false;
                    bank_schedule.pop();
                }
            }
        }
    }
}

void L2cache_reram_pipeline::reram_send_exec(long long cur_cycle)
{
    if (decode_stage->type == read_block)
    {
        //Check if bank can accept request and send if it can
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            if (decode_stage->mshr->sectors_recv[i] == pending_req &&
                !reram_banks[decode_stage->bank + i].busy)
            {
                mem_request* next_req = new mem_request(*decode_stage);
                next_req->bank += i;
                reram_send_req(cur_cycle, next_req);
                decode_stage->mshr->sectors_recv[i] = sent_req;
            }
        }

        delete decode_stage;
    }
    else if(!reram_banks[decode_stage->bank].busy)  //TODO: figure this out beforehand
    {
        reram_send_req(cur_cycle, decode_stage);
        if(decode_stage->type == read_dw)
            decode_stage->mshr->sectors_recv[SECTOR_ID(decode_stage->bank)] = sent_req;
    }
    else if(decode_stage->type == write_back)
    {
        //need to add write back to wb queue if unable to be serviced
        if (decode_stage->type == write_back)
        {
            wb_entry wb = { decode_stage->addr , 1 };
            wb_queue.push_back(wb);
        }
        delete decode_stage;
    }
    else
    {
        decode_stage->cur_stage = mshr_r;
        decode_stage->next_stage = mshr_r;
        /*
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; ReRAM bank already in use.";
        throw std::invalid_argument(errMsg.str());
        */
    }

}

void L2cache_reram_pipeline::mshr_update_exec(long long cur_cycle)
{
    if (mshr_stage->valid_data)  //return request from ReRAM
    {
        //write data into MSHR
        update_mshr(mshr_stage);

        //is all requested MSHR data received?
        if (check_mshr_complete(mshr_stage))
        {
            mem_request* new_req = new mem_request(*mshr_stage);
            new_req->type = cache_fill;
            new_req->next_stage = d;

            request_rob.push_front(*new_req);
            delete new_req;
        }

        //send off to find next reram request
        //move_req(mshr_stage, mshr_r, false);
        //attempt to send requests to ReRAM
        mshr_stage->cur_stage = mshr_r;
        mshr_reram_exec(cur_cycle);
    }
    else  //new request
    {
        first_mshr_update(cur_cycle);
    }
}

void L2cache_reram_pipeline::mshr_reram_exec(long long cur_cycle)
{
    if (mshr_stage->valid_data)  //returned request from ReRAM
    {
        //find any reram requests to send unless write
        mem_request* next_req = nullptr;
        //writeback queue full -> prioritize wb
        if (wb_queue.size() > 128)
        {
            next_req = find_wb_request(mshr_stage->bank);

            if (next_req == nullptr)
            {
                next_req = find_next_request(mshr_stage->subbank, mshr_stage->bank);
            }
        }
        //writeback queue not full -> prioritize read
        else
        {
            next_req = find_next_request(mshr_stage->subbank, mshr_stage->bank);

            if (next_req == nullptr)
            {
                next_req = find_wb_request(mshr_stage->bank);
            }
        }

        if (next_req != nullptr && next_req->cur_stage != rr_s)
        {
            move_req(next_req, rr_s, true);
        }

        if (mshr_stage->type == read_block)
        {
            //for blocks, we made extra requests for each reram bank accessed
            //we need to merge them with the original request, then delete
            mem_request* hold = mshr_stage;
            mshr_stage = mshr_stage->read_sub->req;
            mshr_stage->valid_data = true;

            for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                mshr_stage->valid[i] |= hold->valid[i];

            delete hold;
        }

        if (mshr_stage->type == write_back)
        {
            //done with that request
            delete mshr_stage;
        }
        else if (check_mshr_complete(mshr_stage))
        {
            //send back requests
            //use of a copy so we can delete requests as output without endagering the held request
            move_req(new mem_request(*mshr_stage), mshr_o, false);
        }
    }
    else  //new request
    {
        if (mshr_stage->type == read_dw)
        {
            //Check if bank can accept request and send if it can
            if (!reram_banks[mshr_stage->bank].busy && mshr_stage->mshr->sectors_recv[SECTOR_ID(mshr_stage->bank)] == pending_req)
            {
                stall++;
                move_req(mshr_stage, rr_s, true);
            }
        }
        else if (mshr_stage->type == read_block)
        {

            //Check if bank can accept request and send if it can
            unsigned i;
            for (i = 0; i < conf->l2_settings.n_sectors; i++)
            {
                if (mshr_stage->mshr->sectors_recv[i] == pending_req &&
                    !reram_banks[mshr_stage->bank + i].busy &&
                    !(decode_active && decode_stage->cur_stage == rr_s && decode_stage->bank == (mshr_stage->bank + i)))
                {
                    mem_request* vec_req = new mem_request(*mshr_stage); //so we don't delete it in send reram
                    move_req(vec_req, rr_s, true);
                    break;
                }
            }
        }
        else
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid request type for the MSHR stage.";
            throw std::invalid_argument(errMsg.str());
        }
    }
}



