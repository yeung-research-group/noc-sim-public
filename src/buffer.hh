#pragma once
/*
  buffer.hh

  Donald Yeung
  24 July 2017

  Buffer class.  Buffers are on input channels to routers.
 */

enum buffer_position { first, second };

class buffer
{
private:
  uint64 *the_buffer;
  int size;
  int head;
  int tail;
  int routed; /* is currently sending a packet to an output port */

public:
  buffer(int buffer_size);
  ~buffer();
  int is_empty();
  int is_full();
  int free_entries();
  int entries();

  int is_x_routable();
  int is_y_routable();
  void set_routed();
  void clear_routed();
  int is_x_positive();
  int is_x_negative();
  int is_y_positive();
  int is_y_negative();
  int is_y_zero();

  uint64 remove_from_head();
  uint64 peak_at_second();
  void insert_at_tail(uint64 flit);
};


uint64 decrement_x(uint64 flit);
uint64 decrement_y(uint64 flit);
