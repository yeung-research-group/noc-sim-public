#pragma once

typedef struct L2_cache_line {
    L2_cache_line(uint64 val_tag);
    L2_cache_line(const L2_cache_line& line);
    ~L2_cache_line();

    uint64 tag;
    bool any_dirty;
    bool* dirty;
    bool* valid;

    struct L2_cache_line* LRU_next;
    struct L2_cache_line* LRU_prev;
} L2_cache_line;

typedef struct tag_res {
    tag_res()
        : hit(false),
        valid(),
        evict_dirty(false) {};
    bool hit;
    bool valid[8];
    bool evict_dirty;
} tag_res;

class L2cache_array
{
public:
    L2cache_array(int x, int y);
    tag_res tag_look_up(uint64 addr);
    L2_cache_line* tag_update(mem_request req);

private:
    int x_coord;
    int y_coord;

    L2_cache_line** LRU_head;
    L2_cache_line** LRU_tail;

    unsigned* set_contents;
    int contents;
    typedef struct cache_stat_ {
        int accesses;			/* number of memory references */
        int misses;			/* number of cache misses */
        int replacements;		/* number of misses that cause replacments */
        int demand_fetches;		/* number of fetches */
        int copies_back;		/* number of write backs */
    } cache_stat;
    cache_stat L2_stat;

    //std::deque<wb_entry> wb_queue;

    //Global Objects
    Stats* stats;
    Conf_ReRAM* conf;

    //Cache core functions
    void make_cache();
    L2_cache_line* find_cache_line(uint64 addr);
    void flush_cache();
    void delete_entry(L2_cache_line** head, L2_cache_line** tail, L2_cache_line* item);
    void insert(L2_cache_line** head, L2_cache_line** tail, L2_cache_line* item);

    //int perform_cache_access(uint64 addr, cache_access access_type, bool vector);
    //int hit_miss(uint64 addr, cache_access access_type, bool vector);
    //int cache_block_valid_check(L2_cache_line* cache_line);
    //L2_cache_line* probe(uint64 addr, cache_access access_type, bool vector);
    //int hit(uint64 address, int rw, long long cur_cycle, int vector);
};


