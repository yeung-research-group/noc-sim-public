#pragma once

#include <list>

class L2cache_pipeline : virtual public L2cache_bank
{
public:
    L2cache_pipeline(int x, int y);
    virtual void cycle(long long cur_cycle) override = 0;

protected:
    //Pipeline
    mem_request* decode_stage;
    mem_request* decode_latch;
    bool decode_active;
    bool decode_active_latch;
    void decode_exec(long long cur_cycle);
    virtual void reram_send_exec(long long cur_cycle) = 0;

    mem_request* tag_stage;
    mem_request* tag_latch;
    bool tag_active;
    bool tag_active_latch;
    void tag_exec(long long cur_cycle);

    mem_request* data_stage;
    mem_request* data_latch;
    bool data_active;
    bool data_active_latch;
    void data_exec(long long cur_cycle);

    mem_request* mux_stage;
    mem_request* mux_latch;
    bool mux_active;
    bool mux_active_latch;
    void mux_exec(long long cur_cycle);

    mem_request* output_stage;
    mem_request* output_latch;
    bool output_active;
    bool output_active_latch;
    void output_exec(long long cur_cycle);

    mem_request* mshr_stage;
    mem_request* mshr_latch;
    bool mshr_active;
    bool mshr_active_latch;
    void mshr_exec(long long cur_cycle);
    void mshr_search_exec(long long cur_cycle);
    virtual void mshr_update_exec(long long cur_cycle) = 0;
    void first_mshr_update(long long cur_cycle);
    virtual void mshr_reram_exec(long long cur_cycle) = 0;
    void mshr_output_exec(long long cur_cycle);

    bool move_req(mem_request* req, pipeline_stage ns, bool error);

    int stall;
    bool stall_reram;
    bool stalled_last_cycle;
    bool after_write_stall;

    void move_pipeline(long long cur_cycle);
    void send_request();

#if PIPELINE_OUTPUT
    ofstream pipefile;
    void print_cur_pipeline(long long cur_cycle);
    void setup_pipeline_print();
#endif
    std::string print_pipeline_state();
    string translate_stage(pipeline_stage stage);
    string translate_type(request_type type);
};


