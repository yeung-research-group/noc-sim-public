#pragma once
/*
  Devesh Singh
  24 July 2018
  */

template <class valueType>
class dynamic_queue
{

private:
    struct q_element{
        valueType data;
        q_element * next;
    };

    q_element * head;
    q_element * tail;
    int count;

public:    

    dynamic_queue();
    void pop_front();
    void push_back(valueType);
    valueType& front();
    int empty();
    int size();
    void clear();
};

template <class valueType>
dynamic_queue<valueType>::dynamic_queue(){
    head = tail = NULL;
    count = 0;

}

template <typename valueType>
void dynamic_queue<valueType>::push_back(valueType value) {
    //q_element *item = new q_element();
    q_element *item = new q_element();
    item->data = value;
    item->next = NULL;
    if(head == NULL) {
        head = item;
    }
    else {
        tail->next = item;
    }

    tail = item;
    count++;
}

template <typename valueType>
void dynamic_queue<valueType>::pop_front() {
    if(this->empty()){
        printf("Error - trying to deque from empty queue");
        return;
    }

    q_element * temp = head;
    head = head->next;
    if(head == NULL)
        tail = NULL;
    count --;
    //delete &temp->data;
    delete temp;

}

template <typename valueType>
valueType& dynamic_queue<valueType>::front(){
    if(head ==NULL){
        printf("ERROR : Trying to access element from empty queue");
        //return -1;
    }
    valueType & ref = head->data;
    return ref;

}


template <typename valueType>
int dynamic_queue<valueType>::empty(){
    if(count <= 0)
        return 1;
    else
        return 0;

}

template <typename valueType>
int dynamic_queue<valueType>::size(){
    return count;
}

template <typename valuetype>
void dynamic_queue<valuetype>::clear(){
    while(count > 0)
        pop_front();
}



