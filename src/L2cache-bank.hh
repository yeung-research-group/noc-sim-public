#pragma once

enum request_type { read_dw, write_dw, read_block, write_block, cache_fill, write_back, close_bank };
enum noc_state { sHeader, sReadAddr, sWriteAddr, sData };
enum request_state { pending_req, sent_req, recv_req, unneeded_req };

//decode, tag, bit read (data array), bit write, mux, mux and tag, mux and output, 
//mux and tag and output, mshr search/update, mshr output, reram request receive, reram request send
enum pipeline_stage { d, t, b_r, b_w, m, mt, mo, mot, o, mshr_s, mshr_u, mshr_r, mshr_o, rr_r, rr_s, rr };

struct read_subentry;
struct mshr_entry;
struct tag_res;
struct L2_cache_line;
class L2cache_array;

typedef struct mem_request {
    mem_request(uint64 wbaddr, unsigned wbbank, unsigned wbsubbank, unsigned count)
        : type(write_back),
        packet_id(0),
        addr(wbaddr),
        bank(wbbank),
        subbank(wbsubbank),
        dw_count(count),
        valid_data(true),
        dirty_data(true),
        dirty(),
        valid(),
        mshr(nullptr),
        read_sub(nullptr),
        cur_stage(mshr_r),
        next_stage(rr_s) {}
    mem_request(unsigned id)
        : type(),
        packet_id(id),
        addr(0),
        bank(0),
        subbank(0),
        dw_count(0),
        valid_data(false),
        dirty_data(false),
        dirty(),
        valid(),
        mshr(nullptr),
        read_sub(nullptr),
        cur_stage(d),
        next_stage(d) {}
    //mem_request(const mem_request& req);

    request_type type;
    unsigned packet_id;
    uint64 addr;
    unsigned bank;
    unsigned subbank;
    unsigned dw_count;
    bool valid_data;
    bool dirty_data;
    bool dirty[8];
    bool valid[8];
    mshr_entry* mshr;
    read_subentry* read_sub;
    pipeline_stage cur_stage;
    pipeline_stage next_stage;
} mem_request;

typedef struct read_subentry {
    read_subentry() {}
    read_subentry(mem_request* mem_req)
        : packet_id(mem_req->packet_id),
          vector(mem_req->type == read_block),
          sector(-1),
          req(mem_req) {}

    unsigned packet_id;
    bool vector;
    int sector;
    mem_request* req;
} read_subentry;

typedef struct mshr_entry {
    mshr_entry() {}
    mshr_entry(const mshr_entry& mshr);
    mshr_entry(mem_request* mem_req, uint64 address);
    ~mshr_entry();

    bool done;
    uint64 addr;
    unsigned subbank;
    int data_to_recv;
    request_state sectors_recv[8];
    unsigned read_subentries;
    read_subentry* read_sub;
    bool valid[8];
    bool dirty[8];
} mshr_entry;
bool mshr_done(const mshr_entry& val);

typedef struct {
    uint64 addr;
    unsigned data_flits;
} wb_entry;

class L2cache_bank {
public:
    L2cache_bank(int x, int y);
    virtual void cycle(long long cur_cycle) = 0;

    //main network ports
    virtual buffer* get_input_buffer() { return (input); };
    virtual void set_output_buffer(buffer* out) { output = out; };

    //possible network ports
    virtual buffer* get_mem_in_buffer() = 0;
    virtual void set_mem_out_buffer(buffer* out) = 0;
    virtual buffer* get_dram_in_buffer() = 0;
    virtual void set_dram_out_buffer(buffer* out) = 0;

protected:
    int x_coord;
    int y_coord;
    int x_extent;
    int y_extent;

    //Global objects
    Stats* stats;
    Conf_ReRAM* conf;

    //NOC
    buffer* input;
    buffer* output;
    mem_request* input_req;
    mem_request* out_req;
    noc_state noc_read_state;
    noc_state noc_write_state;
    void route_in(long long cur_cycle);
    void route_out(long long cur_cycle);

    L2cache_array* cache_array;

    std::list<mem_request> request_rob;
    mem_request* next_request();
    virtual mem_request* get_next_request() = 0;

    bool tag_lookup(mem_request* req);

    //MSHRs
    std::list<mshr_entry>* mshrs;
    unsigned max_mshrs;
    mshr_entry* check_mshrs(mem_request* req);
    mshr_entry* handle_mshr_miss(mem_request* req);
    void update_mshr(mem_request* req);
    void add_read_subentry(mem_request* req);
    void write_mshr(mem_request* req);
    bool check_read_valid(mem_request* req);
    bool check_mshr_complete(mem_request* req);
    void delete_mshr(mshr_entry* mshr, unsigned subbank);
    mshr_entry* find_orignal_mshr(mem_request* req);

    //WB
    std::list<wb_entry> wb_queue;
    virtual void writeback_req(L2_cache_line* evicted) = 0;

    //Output
    std::queue<mem_request> output_queue;

    //Address and packets
    uint64 get_delta_flit(unsigned packet_id);
    uint64 get_delta_flit(unsigned packet_id, int delta_x, int delta_y);

    void address_translation(uint64 addr, int* delta_x, int* delta_y);
    void get_random_dAddress(int* delta_x, int* delta_y);
    void get_local_dAddress(int* delta_x, int* delta_y);
    void get_mapped_dAddress(uint64 addr, int* delta_x, int* delta_y);
    void get_random_rAddress(int* delta_x, int* delta_y);
    void get_local_rAddress(int* delta_x, int* delta_y);
    void get_mapped_rAddress(uint64 addr, int* delta_x, int* delta_y);
};
