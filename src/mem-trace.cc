
#include <stdlib.h>
#include <regex>
#include <cstdint>
#include "noc-sim.hh"


#define OLD 1

using namespace std;

mem_trace::mem_trace(traffic_generator ***my_CORE)
{
  conf = Conf_ReRAM::get();
  stats = Stats::get();
  core = my_CORE;
#ifdef MEM_LEAK_DEBUG
  ins_count = new uint64_t[conf->threads];
  for(int i=0; i<conf->threads; i++)
     ins_count[i] = 0;
#endif

  int threads = conf->threads;
  reg_refs = new instruction**[threads]; //number of threads
  for(int i = 0; i < threads; i++)
  {
    reg_refs[i] = new instruction*[32];
    for(int j = 0; j < 32; j++)
    {
      reg_refs[i][j] = NULL;
    }
  }

  max_mask_id = 10;
  thread_mask = new int*[threads];
  for(int i=0; i<threads; i++){
    thread_mask[i] = new int[max_mask_id];
    for(int j=0; j<max_mask_id; j++)
      thread_mask[i][j] = (conf->max_simd_width==8)?255:65535; //0xFF or 0xFFFF
  }
}

void mem_trace::readReq(stringstream *req, int ins, int threadID)
{
  int size = 0;
  uint64_t address = 0,PC = 0;
  //char addr_s[20];
  int simd_width = 0;
#ifdef _MSC_VER   //if visual studio
  uint64 addresses[16];
#else  //part of C99, not C++ std
  uint64 addresses[conf->max_simd_width];
#endif
  ins_type type = other;
  int vector_mask_id = -1; 

  instruction *Pins;
  //cerr << "READ REQUEST " << ins << ":\n" << req->str() << "\n";

  for (string line; getline(*req, line);)
  {
    //TID1004: Read 0x6e2e = *(UINT64*)0x00007f222e377180
    if (sscanf(line.c_str(), "%*s %*s %*s %*s *(UINT%d*)%llx", &size, &address) == 2) 
    {
        if(size <= 64) //normal read
        {
            addresses[simd_width] = address;
            simd_width++;
            type = single_read;
        }
        else //vector read
        {
            type = vector_read;
            for (simd_width = 0; simd_width < conf->max_simd_width; simd_width++)
            {
                addresses[simd_width] = address;
                address += 64/conf->max_simd_width;
            }
        }

      continue;
    }
    //	   00000000_00004ad1_00000000_00006e2e = *(UINT512*)0x00007f222f35ece0
    else if (sscanf(line.c_str(), " %*s %*s *(UINT%d*)%llx", &size, &address) == 2)    //vector read
    {
      type = vector_read;
      for (simd_width = 0; simd_width < conf->max_simd_width; simd_width++)
      {
        addresses[simd_width] = address;
        address += 64/conf->max_simd_width;
      }

      continue;
    }
    else  if (line.find("INS") != string::npos)
    {
      //TID1: INS 0x00000000004013c6 ....................
      sscanf(line.c_str(),"%*s %*s %llx",&PC); 
      //Extracting the mask ID in case vector load is masked. Can handle single digit mask IDs only yet.
      if(line.find("vmov") != string::npos){ //vector load
        std::size_t found_idx = line.find("{k");
          if(found_idx != string::npos){ // masked
            vector_mask_id = line.c_str()[found_idx+2]-'0';
            if(line[found_idx+3] != '}'){
              printf("Mask ID is double digit. Not prepared for that!");
            }
          }
      }

      if (line.find("gather") != string::npos)
        type = gather;


      break;
    }

  }

  while (ins > 0)
  {
    ins--;
    Pins = new instruction(other);
    insert_instruction(threadID, Pins);
    
    delete Pins;
    //insert_instruction(threadID, new instruction(other));
  }

  if (type == single_read)
  {
    Pins = new instruction(type, addresses[0], PC);
    insert_instruction(threadID, Pins);
    
    delete Pins;
    //insert_instruction(threadID, new instruction(type, addresses[0]));
  }
  else if (type == vector_read || type == gather)
  {
    if(vector_mask_id >= 0)
        Pins = new instruction(type, addresses, simd_width, PC, thread_mask[threadID][vector_mask_id]);
    else // instruction is unmasked
        Pins = new instruction(type, addresses, simd_width, PC, (conf->max_simd_width==8)?255:65535);
    insert_instruction(threadID, Pins);
    
    delete Pins;
    //insert_instruction(threadID, new instruction(type, addresses, simd_width));
  }
  else
  {
    cerr << "Error: unknown type for \n\" " << req->str() << "\"\n";
  }

  req->str(std::string());
  req->clear();

}

void mem_trace::writeReq(stringstream *req, int ins, int threadID)
{
  int size = 0;
  uint64_t address = 0;

  //char addr_s[20];
  int simd_width = 0;
#ifdef _MSC_VER   //if visual studio
  uint64 addresses[16];
#else  //part of C99, not C++ std
  uint64 addresses[conf->max_simd_width];
#endif 
  uint64 PC = 0;
  ins_type type = other;
  Stats *stats = Stats::get();
  instruction *Pins;
  //cerr << "WRITE REQUEST " << ins << ":\n" << req->str() << "\n";

  for (string line; getline(*req, line);)
  {
#if NOC_SIM_SNIPER
    while (stats->queues_full())  //every thread has elements
#else
	while (!stats->any_under_watemark_threads())
#endif
      std::this_thread::sleep_for(std::chrono::milliseconds(100));

    //TID1004: Write *(UINT64*)0x00007f222de15b10 = 0x6e2e
    if (sscanf(line.c_str(), "%*s %*s *(UINT%d*)%llx", &size, &address) == 2)    //if write
    {
      if (size == 512) {
        for (simd_width = 0; simd_width < conf->max_simd_width; simd_width++)
        {
          addresses[simd_width] = address;
          address += 64/conf->max_simd_width;
        }
        type = vector_write;
      }
      else

      {
        if (type != scatter)
          type = single_write;
        addresses[simd_width] = address;
        simd_width++;
      }
      continue;
    }

    else  if (line.find("INS") != string::npos)
    {
      //TID1: INS 0x00000000004013c6 ....................
      sscanf(line.c_str(),"%*s %*s %llx",&PC);
      if (line.find("scatter") != string::npos)
        type = scatter;
      continue;
    }

  }

  while (ins > 0)
  {
    ins--;
    Pins = new instruction(other);
    insert_instruction(threadID, Pins);
    delete Pins;
    //insert_instruction(threadID, new instruction(other));
  }

  if (type == single_write)
  {
    Pins = new instruction(type,addresses[0],PC);
    insert_instruction(threadID, Pins);
    delete Pins;
    //insert_instruction(threadID, new instruction(type, addresses[0]));
  }
  else if (type == vector_write || type == scatter)
  {
    Pins = new instruction(type,addresses, simd_width, PC, (conf->max_simd_width==8)?255:65535);
    insert_instruction(threadID, Pins);
    delete Pins;
    //insert_instruction(threadID, new instruction(type, addresses, simd_width));
  }
  else
  {
    cerr << "Error: unknown type for \n\" "  << req->str() << "\"\n";
  }

  req->str(std::string());
  req->clear();
}

void mem_trace::lockReq(int ins, int threadID)
{
  while (ins > 0)
  {
    ins--;
    insert_instruction(threadID, new instruction(other));
  }

  insert_instruction(threadID, new instruction(lock_ins));
}


void mem_trace::insert_instruction(int threadID, instruction* ins)
{
  if(!ins)
    cout<<"Mem alloc failed!\n";
  int thread, x, y;
#ifdef MEM_LEAK_DEBUG
  ins_count[threadID] ++;
#endif

  switch (ins->type)
  {
  case ins_type::single_read: stats->total_ins_stats.read_single++;
      break;
  case ins_type::single_write: stats->total_ins_stats.write_single++;
      break;
  case ins_type::vector_read: stats->total_ins_stats.read_vector++;
      break;
  case ins_type::vector_write: stats->total_ins_stats.write_vector++;
      break;
  case ins_type::scatter: stats->total_ins_stats.scatter++;
      break;
  case ins_type::gather: stats->total_ins_stats.gather++;
      break;
  default: stats->total_ins_stats.other++;
      break;
  }

  conf->threads_tile(threadID, &x, &y, &thread);

  core[y][x]->add_instruction(ins, thread);
}

void mem_trace::extractMemory(string inputfile)
{
  ifstream infile;
  ofstream outfile;
  int threadID;
  int threads = conf->threads;
  stringstream *readStr = new stringstream[threads];
  stringstream *writeStr = new stringstream[threads];
  int *ins = new int[threads];
  bool *read = new bool[threads];
  bool *write = new bool[threads];

  for (int i = 0; i < threads; i++)
  {
    ins[i] = 0;
    read[i] = false;
    write[i] = false;
  }

  infile.open(inputfile);

  //read trace
  for (string line; getline(infile, line);)
  {

    sscanf(line.c_str(), "TID%d:", &threadID);

    if (line.find("INS") != string::npos)
    {
      if (line.find("hint-taken") != string::npos)
      {
        lockReq(ins[threadID], threadID);
        writeStr[threadID].clear();
        ins[threadID] = 0;
      }
      else if (read[threadID]) //this is the read instruction
      {
        readStr[threadID] << line;
        readReq(&readStr[threadID], ins[threadID], threadID);
        read[threadID] = false;
        ins[threadID] = 0;
      }
      else if (write[threadID]) //the last line was the end of the write
      {
        writeReq(&writeStr[threadID], ins[threadID], threadID);
        writeStr[threadID] << line << "\n";
        write[threadID] = false;
        ins[threadID] = 0;
      }
      else //could be the start of a write request
      {
        writeStr[threadID].str(std::string());
        writeStr[threadID].clear();
        writeStr[threadID] << line << "\n";
        ins[threadID]++;
      }
    }
    else if (line.find("Read") != string::npos)
    {
      if (write[threadID]) //the last line was the end of the write
      {
        writeReq(&writeStr[threadID], ins[threadID], threadID);
        write[threadID] = false;
        ins[threadID] = 0;
      }
      
      readStr[threadID] << line << "\n";
      read[threadID] = true;
    }
    else if (line.find("Write") != string::npos)
    {
      writeStr[threadID] << line << "\n";
      write[threadID] = true;
      if (read[threadID])
      {
        throw std::invalid_argument("Error: write command in pending read command");
      }
    }
    else
    {
      if (write[threadID]) //the last line was the end of the write
      {
        writeReq(&writeStr[threadID], ins[threadID], threadID);
        writeStr[threadID] << line << "\n";
        write[threadID] = false;
        ins[threadID] = 0;
      }
      else if (read[threadID])//part of a read request
      {
        readStr[threadID] << line << "\n";
      }

      if(line.find("\tk") != string::npos){
        unsigned maskVAL;
        int maskID;
        //TID0: 	k1 := 00000000_000000ff
        sscanf(line.c_str(), "%*s k%d %*s 00000000_%x",&maskID, &maskVAL);
        thread_mask[threadID][maskID] = maskVAL;
      }
      //else just info about status of zmm regs
      //DS: mask info can be extracted here!
    }
  }

  for (int i = 0; i < threads; i++)
  {
   /* if (read[i])
    {
      cerr << "ERROR: Read request missing";
      readReq(&readStr[i], ins[i], i);
      read[i] = false;
    }
    else if (write[i])
    {
      writeReq(&writeStr[i], ins[i], i);
      write[i] = false;
      ins[i] = 0;
    }
    else*/ if (ins[i] > 0)
    {
      while (ins[i] > 0)
      {
        ins[i]--;
        insert_instruction(i, new instruction(other));
      }
    }
  }

  Stats::get()->set_trace_done();
  printf("Finished reading trace\n");

  infile.close();
  delete[] ins;
  delete[] read;
  delete[] write;
  delete[] readStr;
  delete[] writeStr;
}
