/*
  rram-bank.cc

  Candace Walden
  20 August 2017

  RRAM banks simulate 8 subarrays each handling 8 bit increments. It 
  handles latency for reads and writes and replies to traffic.
 */


#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"


rram_bank::rram_bank(int x, int y, int bank, ram_char *ram_settings, std::priority_queue<schedule_item> *scheduler)
{
  x_coord = x;
  y_coord = y;
  b_coord = bank;
  cycle_complete = 0;
  row = 0;

  busy = false;

  bank_schedule = scheduler;

  cur_req = nullptr;

  conf = Conf_ReRAM::get();
  stats = Stats::get();
  settings = ram_settings;
}

void rram_bank::send_request(long long cur_cycle, ram_request* req)
{
    if(busy)
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; ReRAM bank already in use.";
        throw std::invalid_argument(errMsg.str());
    }
    
    stats->set_to_bank(req->packet_id, cur_cycle, x_coord, y_coord);
    busy = true;
    cur_req = req;

    if (req->type == read_dw || req->type == read_block)
    {
        read_request(cur_cycle);
    }
    else
    {
        write_request(cur_cycle);
    }

    //schedule for cur_cycle + latency
    schedule_item sch_item = { cycle_complete, this };
    bank_schedule->push(sch_item);

#if BANK_UTILIZATION_TRACKING
    stats->tile_ram_stats[IND(x_coord, y_coord)].busy_bank_cycles += (cycle_complete - cur_cycle);
    if (cycle_complete > (cur_cycle + (EPOCH_PERIOD - cur_cycle % EPOCH_PERIOD)))
        stats->tile_ram_stats[IND(x_coord, y_coord)].extra_cycles_from_next_epoch += cycle_complete - (cur_cycle + (EPOCH_PERIOD - cur_cycle % EPOCH_PERIOD));
#endif

#if RRAM_BANK_OUT
    printf("(%d, %d, %d): RRAM bank received packet!\n", y_coord, x_coord, b_coord);
#endif
}

void rram_bank::complete_request(long long cur_cycle)
{
    stats->set_from_bank(cur_req->packet_id, cur_cycle, x_coord, y_coord);

    if (cur_req->type == read_dw || cur_req->type == read_block)
    {
        stats->tile_ram_stats[IND(x_coord, y_coord)].reram_read++;

#if RRAM_BANK_OUT
        printf("(%d, %d, %d): RRAM bank completed read operation!\n", y_coord, x_coord, b_coord);
#endif
    }
    else
    {
        stats->tile_ram_stats[IND(x_coord, y_coord)].reram_write++;

#if RRAM_BANK_OUT
        printf("(%d, %d, %d): RRAM bank completed write operation!\n", y_coord, x_coord, b_coord);
#endif
    }
    stats->bank_profile_incr(x_coord, y_coord, b_coord);

    delete cur_req;

    if (settings->ram_page_policy == closed_page)
    {
        //schedule for cur_cycle + row close latency
        cycle_complete = cur_cycle + settings->row_close_latency;
        schedule_item sch_item = { cycle_complete, this };
        bank_schedule->push(sch_item);

        busy = true;
        cur_req = new ram_request(close_bank, b_coord);

#if BANK_UTILIZATION_TRACKING
        stats->tile_ram_stats[IND(x_coord, y_coord)].busy_bank_cycles += (cycle_complete - cur_cycle);
        if (cycle_complete > (cur_cycle + (EPOCH_PERIOD - cur_cycle % EPOCH_PERIOD)))
            stats->tile_ram_stats[IND(x_coord, y_coord)].extra_cycles_from_next_epoch += cycle_complete - (cur_cycle + (EPOCH_PERIOD - cur_cycle % EPOCH_PERIOD));
#endif
    }
    else
    {
        busy = false;
    }
}

void rram_bank::read_request(long long cur_cycle)
{
    uint64 address = cur_req->addr;
    if (settings->ram_page_policy == open_page)
    {
        if ((address & conf->row_bits) == row)
        {
            cycle_complete = settings->read_lat / conf->cycle_time + cur_cycle;
            stats->tile_ram_stats[IND(x_coord, y_coord)].row_buf_hits++;
        }
        else
        {
            cycle_complete = (settings->read_lat + settings->row_close_latency + settings->row_open_latency) / conf->cycle_time + cur_cycle;
            row = address & conf->row_bits;
            stats->tile_ram_stats[IND(x_coord, y_coord)].row_buf_misses++;
        }
    }
    else if (settings->ram_page_policy == closed_page)
    {
        cycle_complete = (settings->read_lat + settings->row_open_latency) / conf->cycle_time + cur_cycle;
    }
    else //perfect page
    {
        cycle_complete = settings->read_lat / conf->cycle_time + cur_cycle;
    }
}

void rram_bank::write_request(long long cur_cycle)
{
    uint64 address = cur_req->addr;
    if (settings->ram_page_policy == open_page)
    {
        if ((address & conf->row_bits) == row)
        {
            cycle_complete = settings->write_lat / conf->cycle_time + cur_cycle;
            stats->tile_ram_stats[IND(x_coord, y_coord)].row_buf_hits++;
        }
        else
        {
            cycle_complete = (settings->write_lat + settings->row_close_latency + settings->row_open_latency) / conf->cycle_time + cur_cycle;
            row = address & conf->row_bits;
            stats->tile_ram_stats[IND(x_coord, y_coord)].row_buf_misses++;
        }
    }
    else if (settings->ram_page_policy == closed_page)
    {
        cycle_complete = (settings->write_lat + settings->row_open_latency) / conf->cycle_time + cur_cycle;
    }
    else //perfect page
    {
        cycle_complete = settings->write_lat / conf->cycle_time + cur_cycle;
    }
}

void rram_bank::bank_close_page(long long cur_cycle)
{
    busy = false;
    delete cur_req;
}


