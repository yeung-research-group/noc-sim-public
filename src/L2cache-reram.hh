#pragma once


typedef struct bank_reram {
    bool busy;
    unsigned long long uses;
    mem_request* cur_op;
} bank_reram;

struct reram_task {
    long long cycle;
    unsigned bank;
    bool read;
};
bool operator<(const reram_task& a, const reram_task& b);

class L2cache_reram : public virtual L2cache_bank
{
public:
    L2cache_reram(int x, int y);
    void cycle(long long cur_cycle) override;

    //possible network ports
    buffer* get_mem_in_buffer() override { return nullptr; };
    void set_mem_out_buffer(buffer* out) override { return; };
    buffer* get_dram_in_buffer() override { return nullptr; };
    void set_dram_out_buffer(buffer* out) override { return; };

protected:
    bank_reram* reram_banks;

    //MSHRs
    mem_request* find_next_request(unsigned subbank, unsigned bank);
    mem_request* find_wb_request(unsigned bank);

    //Bank Scheduling Queue
    std::priority_queue<reram_task> bank_schedule;

    int n_reram_banks;

    //int read_service_priority;

    mem_request* cur_req;
    long long cycle_complete;
    void handle_request(long long cur_cycle);

    void reram_send_req(long long cur_cycle, mem_request* req);
    //void check_incoming_reram(long long cur_cycle);
    void writeback_req(L2_cache_line* evicted) override;
    mem_request* get_next_request() override;
};
