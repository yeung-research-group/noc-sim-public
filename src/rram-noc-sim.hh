#pragma once
/*
  rram-noc-sim.hh

  Donald Yeung
  24 July 2017

  Simulator of a MESH NOC connected to RRAM banks
 */

#if NOC_SIM_SNIPER
#include "_thread.h"

class ReRAM_Backend : public Runnable
{
public:
	ReRAM_Backend();
	void spawn();
	traffic_generator *getCoreForThread(int tid);

private:
	int x_extent;
	int y_extent;

	router ***my_NOC;
	L2cache ***my_CACHE;
	ram_controller ***my_RRAM;
	traffic_generator ***my_CORE;

	_Thread *m__thread;

	void run();
        void parse_config();
#else
        void parse_config(int argc, char *argv[]);
#endif
	void init_NOC_channels(router ***my_NOC);
	void init_RRAM_channels(L2cache ***my_CACHE, ram_controller ***my_RRAM, router ***my_NOC);
	void init_CORE_channels(router ***my_NOC, traffic_generator ***my_CORE);
	void init_CACHE_channels(router ***my_NOC, L2cache ***my_CACHE);

#if NOC_SIM_SNIPER
};
#endif

void init_CORE_backdoor(L2cache***my_CACHE, traffic_generator ***my_CORE);
long long simulate(router ***my_NOC, traffic_generator ***my_CORE, ram_controller ***my_RRAM, L2cache***my_CACHE);
void print_final_stats(long long cycle, ram_controller*** my_RRAM);
void print_epoch_stats(long long cycle, traffic_generator*** my_CORE, long long prev_num_packets_sent, long long cores_finished_trace, ram_controller*** my_RRAM);
void print_epoch_stats_header();
void read_mem_trace(traffic_generator ***my_CORE);
void dump_bank_profile();

#if NOC_SIM_PARA
void run_tile(int id, long long cycle, router* noc, traffic_generator* core, ram_controller* ram, L2cache* cache);
void run_tile_int(int id, long long cycle, router* noc, traffic_generator* core, L2cache* cache);
#endif
