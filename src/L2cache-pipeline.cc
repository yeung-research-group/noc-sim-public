#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_pipeline::L2cache_pipeline(int x, int y) : L2cache_bank(x, y)
{

    stall = 0;
    stall_reram = false;
    stalled_last_cycle = false;
    after_write_stall = false;

    //set everything to inactive
    decode_active = false;
    decode_active_latch = false;
    tag_active = false;
    tag_active_latch = false;
    data_active = false;
    data_active_latch = false;
    mux_active = false;
    mux_active_latch = false;
    output_active = false;
    output_active_latch = false;
    mshr_active = false;
    mshr_active_latch = false;

    decode_stage = nullptr;
    tag_stage = nullptr;
    data_stage = nullptr;
    mux_stage = nullptr;
    output_stage = nullptr;
    mshr_stage = nullptr;

    decode_latch = nullptr;
    tag_latch = nullptr;
    data_latch = nullptr;
    mux_latch = nullptr;
    output_latch = nullptr;
    mshr_latch = nullptr;

#if PIPELINE_OUTPUT
    if(x_coord == 0 && y_coord == 4)
        setup_pipeline_print();
#endif
}

//have the banks handle a cycle
void L2cache_pipeline::move_pipeline(long long cur_cycle)
{
    decode_stage = decode_latch;
    tag_stage = tag_latch;
    data_stage = data_latch;
    mux_stage = mux_latch;
    output_stage = output_latch;
    mshr_stage = mshr_latch;

    decode_active = decode_active_latch;
    tag_active = tag_active_latch;
    data_active = data_active_latch;
    mux_active = mux_active_latch;
    output_active = output_active_latch;
    mshr_active = mshr_active_latch;

    if (decode_active)
        decode_stage->cur_stage = decode_stage->next_stage;
    if (tag_active)
        tag_stage->cur_stage = tag_stage->next_stage;
    if (data_active)
        data_stage->cur_stage = data_stage->next_stage;
    if (mux_active)
        mux_stage->cur_stage = mux_stage->next_stage;
    if (output_active)
        output_stage->cur_stage = output_stage->next_stage;
    if (mshr_active)
        mshr_stage->cur_stage = mshr_stage->next_stage;

    decode_active_latch = false;
    tag_active_latch = false;
    data_active_latch = false;
    mux_active_latch = false;
    output_active_latch = false;
    mshr_active_latch = false;

#if PIPELINE_OUTPUT
    if (x_coord == 0 && y_coord == 4 &&
        cur_cycle > 100000 && cur_cycle < 200000)
        print_cur_pipeline(cur_cycle);
#endif

    mux_exec(cur_cycle);
    mshr_exec(cur_cycle);
    tag_exec(cur_cycle);
    data_exec(cur_cycle);
    output_exec(cur_cycle);
    decode_exec(cur_cycle);
}

void L2cache_pipeline::decode_exec(long long cur_cycle)
{
    if (!decode_active) return;

    if (decode_stage->cur_stage == d)
    {
        //decode stuff
        if (stall > 0 || tag_active_latch)
        {
            //hold off a cycle by putting this back in ROB
            request_rob.push_front(*decode_stage);
            delete decode_stage;
        }
        else
        {
            if (move_req(decode_stage, t, false))
            {
                if (decode_stage->type == cache_fill)
                    stats->tile_l2_stats[IND(x_coord, y_coord)].cache_fill++;
                else if (decode_stage->type == read_dw)
                    stats->tile_l2_stats[IND(x_coord, y_coord)].l2_read++;
                else if (decode_stage->type == read_block)
                    stats->tile_l2_stats[IND(x_coord, y_coord)].l2_vec_read++;
                else if (decode_stage->type == write_dw)
                    stats->tile_l2_stats[IND(x_coord, y_coord)].l2_write++;
                else if (decode_stage->type == write_block)
                    stats->tile_l2_stats[IND(x_coord, y_coord)].l2_vec_write++;
            }
        }
    }
    else if (decode_stage->cur_stage == rr_s)
    {
        reram_send_exec(cur_cycle);
    }
    else if (decode_stage->cur_stage == rr_r)
    {
        if (move_req(decode_stage, mshr_u, false))
        {
            stall++;
            decode_stage->valid_data = true;
        }
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid state for decode stage.\n";
        errMsg << print_pipeline_state();
        throw std::invalid_argument(errMsg.str());
    }
}

void L2cache_pipeline::tag_exec(long long cur_cycle)
{
    if (!tag_active) return;

    if (tag_stage->cur_stage == t)
    {
        bool valid = tag_lookup(tag_stage);
        bool dirty = tag_stage->dirty_data;
        bool moved;

        if ((tag_stage->type == cache_fill && dirty) ||
            ((tag_stage->type == read_dw || tag_stage->type == read_block) && valid))
        {
            //go to b_r
            moved = move_req(tag_stage, b_r, false);
        }
        else if ((tag_stage->type == cache_fill && !dirty) ||
            ((tag_stage->type == write_dw || tag_stage->type == write_block) && valid))
        {
            //got to mt
            moved = move_req(tag_stage, mt, false);
        }
        else //read or write and invalid
        {
            //go to mshr_s
            moved = move_req(tag_stage, mshr_s, false);
        }

        if (!moved || tag_stage->type == cache_fill)
        {
            if (valid)
            {
                stats->tile_l2_stats[IND(x_coord, y_coord)].l2_hit--;
                if (tag_stage->type == read_dw)
                    stats->tile_l2_stats[IND(x_coord, y_coord)].l2_gather_hit--;
            }
            else
            {
                stats->tile_l2_stats[IND(x_coord, y_coord)].l2_miss--;
                if (tag_stage->type == read_dw)
                    stats->tile_l2_stats[IND(x_coord, y_coord)].l2_gather_miss--;
            }
        }

    }
    else if (tag_stage->cur_stage == mt)
    {
        //update tag array
        L2_cache_line* evicted = cache_array->tag_update(*tag_stage);

        if (evicted != nullptr)
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): There should be no dirty evicted line in this state.\n";
            errMsg << print_pipeline_state();
            throw std::invalid_argument(errMsg.str());
        }

        //let MUX move data to data latch

    }
    else if (tag_stage->cur_stage == mot)
    {
        //update tag array
        L2_cache_line* evicted = cache_array->tag_update(*tag_stage);

        if (evicted != nullptr)
        {
            writeback_req(evicted);
        }
        //else //test error
        //{
        //   std::stringstream errMsg;
        //   errMsg << "Error: (" << y_coord << ", " << x_coord << "): There should be a dirty evicted line in this state.";
        //   throw std::invalid_argument(errMsg.str());
        //}

        //let MUX move data to MUX latch
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid state for tag stage.\n";
        errMsg << print_pipeline_state();
        throw std::invalid_argument(errMsg.str());
    }
}

void L2cache_pipeline::data_exec(long long cur_cycle)
{
    if (!data_active) return;

    if (data_stage->cur_stage == b_r)
    {
        //read data - cache handles access accounting with tag lookups

        if (data_stage->type == read_dw || data_stage->type == read_block)
        {
            move_req(data_stage, mo, false);
        }
        else //write or cache fill
        {
            move_req(data_stage, mot, false);
        }
    }
    else if (data_stage->cur_stage == b_w)
    {
        //tag update handles accounting
        //delete request
        if (data_stage->type != cache_fill)
            stats->set_from_L2(data_stage->packet_id, cur_cycle, 1, x_coord, y_coord);

        delete(data_stage);
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid state for data stage.\n";
        errMsg << print_pipeline_state();
        throw std::invalid_argument(errMsg.str());
    }
}

void L2cache_pipeline::mux_exec(long long cur_cycle)
{
    if (!mux_active) return;

    if (mux_stage->cur_stage == m || mux_stage->cur_stage == mt)
    {
        move_req(mux_stage, b_w, false);
    }
    else if (mux_stage->cur_stage == mot)
    {
        move_req(mux_stage, m, false);
    }
    else if (mux_stage->cur_stage == mo)
    {
        //let output delete request
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid state for mux stage.\n";
        errMsg << print_pipeline_state();
        throw std::invalid_argument(errMsg.str());
    }
}

void L2cache_pipeline::output_exec(long long cur_cycle)
{
    if (!output_active) return;

    if (output_stage->cur_stage == o || output_stage->cur_stage == mo)
    {
        output_queue.push(*output_stage);
        delete output_stage;
    }
    else if (output_stage->cur_stage == mot)
    {
        //write victim line to write buffer is handled in tag
        //Let MUX move to MUX latch
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid state for output stage.\n";
        errMsg << print_pipeline_state();
        throw std::invalid_argument(errMsg.str());
    }

}

void L2cache_pipeline::mshr_exec(long long cur_cycle)
{
    if (!mshr_active) return;

    if (mshr_stage->cur_stage == mshr_s)
    {
        mshr_search_exec(cur_cycle);
    }
    else if (mshr_stage->cur_stage == mshr_u)
    {
        mshr_update_exec(cur_cycle);
    }
    else if (mshr_stage->cur_stage == mshr_r)
    {
        mshr_reram_exec(cur_cycle);
    }
    else if (mshr_stage->cur_stage == mshr_o)
    {
        mshr_output_exec(cur_cycle);
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid state for MSHR stage.\n";
        errMsg << print_pipeline_state();
        throw std::invalid_argument(errMsg.str());
    }
}

void L2cache_pipeline::mshr_search_exec(long long cur_cycle)
{
    //search MSHRs for addr
    mshr_entry* hit = check_mshrs(mshr_stage);

    if (hit == nullptr)
    {
        if (mshr_stage->type == write_dw || mshr_stage->type == write_block)
        {
            if (mshr_stage->dirty_data)
            {
                move_req(mshr_stage, b_r, false);
            }
            else
            {
                move_req(mshr_stage, mt, false);
            }
        }
        else  //read MSHR miss
        {
            //create top level MSHR
            handle_mshr_miss(mshr_stage);

            //move_req(mshr_stage, mshr_u, false);
            //update MSHRs
            mshr_stage->cur_stage = mshr_u;
            mshr_update_exec(cur_cycle);
        }
    }
    else  //hit in MSHRs
    {
        //move_req(mshr_stage, mshr_u, false);
        //update MSHRs
        mshr_stage->cur_stage = mshr_u;
        mshr_update_exec(cur_cycle);
    }
}

void L2cache_pipeline::first_mshr_update(long long cur_cycle)
{
    if (mshr_stage->type == write_dw || mshr_stage->type == write_block)
    {
        //write data into MSHR
        write_mshr(mshr_stage);
        stats->set_from_L2(mshr_stage->packet_id, cur_cycle, 1, x_coord, y_coord);
        //done with this request
        delete(mshr_stage);
    }
    //TODO: may cause RAW error
    else if (mshr_stage->type == read_dw || mshr_stage->type == read_block)
    {
        //add read subentry
        bool data_valid = check_read_valid(mshr_stage);

        if (data_valid)
        {
            move_req(mshr_stage, mshr_o, false);
        }
        else
        {
            //create Read Subentry
            add_read_subentry(mshr_stage);

            //attempt to send requests to ReRAM
            mshr_stage->cur_stage = mshr_r;
            mshr_reram_exec(cur_cycle);
        }
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid request type for the MSHR stage.\n";
        errMsg << print_pipeline_state();
        throw std::invalid_argument(errMsg.str());
    }
}


void L2cache_pipeline::mshr_output_exec(long long cur_cycle)
{
    mshr_entry* mshr = mshr_stage->mshr;

    //all done
    if (check_mshr_complete(mshr_stage))
    {
        mem_request* next_req = mshr->read_sub[--mshr->read_subentries].req;
        if (move_req(next_req, o, false))
        {
            if (mshr->read_subentries > 0)  //not the last request
            {
                move_req(mshr_stage, mshr_o, false);
            }
            else
            {
                //completed the mshr
                delete_mshr(mshr, mshr_stage->subbank);

                //also done with last held request
                delete mshr_stage;
            }
        }
        else
        {
            mshr->read_subentries++;  //un-do decrement
            move_req(mshr_stage, mshr_o, false);  //stall in place
        }
    }
    else  //only the current request is done, no read subentry, and MSHR has other entries
    {
        move_req(mshr_stage, o, false);
    }
}


bool L2cache_pipeline::move_req(mem_request* req, pipeline_stage ns, bool error)
{
    bool moved = true;

    //special cases
    if(error)
    {
        //dirty write miss -> dirty write miss -> clean write
        if(ns == b_r && data_active_latch && mux_active_latch && tag_active_latch && req->next_stage == mot &&
           mux_latch->next_stage == mt && mux_latch->cur_stage == t && data_latch->cur_stage == m)
        {
            //take the clean write and put it back in the queue, take away the hit
            mux_latch->cur_stage = d;
            mux_latch->next_stage = d;
            request_rob.push_front(*mux_latch);
            delete mux_latch;
            mux_active_latch = false;
            tag_active_latch = false;
            stats->tile_l2_stats[IND(x_coord, y_coord)].l2_hit--;

            return move_req(req, mot, false);
        }

        //Dirty Cache Fill -> Read Hit -> Write Hit
        if (ns == b_r && data_active_latch && mux_active_latch && tag_active_latch && req->next_stage == mo &&
            mux_latch->next_stage == mt && mux_latch->cur_stage == t && data_latch->cur_stage == m)
        {
            //hold the clean write in the tag stage and take away the hit (will get next cycle)
            mux_latch->next_stage = t;
            mux_active_latch = false;
            stats->tile_l2_stats[IND(x_coord, y_coord)].l2_hit--;

            return move_req(req, mo, false);
        }

        //Dirty Cache Fill -> Read Hit -> ReRAM Read Complete
        if(ns == b_r && data_active_latch && output_active_latch && data_active && req->next_stage == mo &&
           data_latch->type == cache_fill && data_latch->next_stage == b_w)
        {
            //put cache fill back in queue
            data_latch->cur_stage = d;
            data_latch->next_stage = d; 
            request_rob.push_front(*data_latch);
            delete data_latch;
            data_active_latch = false;

            //move keep read where it is by completing the move
        }
    }

    if (ns == d || ns == rr_r || ns == rr_s)
    {
        if (!decode_active_latch)
        {
            decode_active_latch = true;
            req->next_stage = ns;
            decode_latch = req;
        }
        else if (error)
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; decode stage already in use.\n";
            errMsg << print_pipeline_state();
            throw std::invalid_argument(errMsg.str());
        }
        else
        {
            moved = false;
        }
    }

    if (ns == t || ns == mt || ns == mot)
    {
        if (!tag_active_latch)
        {
            tag_active_latch = true;
            req->next_stage = ns;
            tag_latch = req;
        }
        else if (error)
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; tag stage already in use.\n";
            errMsg << print_pipeline_state();
            throw std::invalid_argument(errMsg.str());
        }
        else
        {
            moved = false;
        }
    }

    if (ns == b_r || ns == b_w)
    {
        if (!data_active_latch)
        {
            data_active_latch = true;
            req->next_stage = ns;
            data_latch = req;
        }
        else if (error)
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; data stage already in use.\n";
            errMsg << print_pipeline_state();
            throw std::invalid_argument(errMsg.str());
        }
        else
        {
            moved = false;
        }
    }

    if (moved && (ns == m || ns == mt || ns == mo || ns == mot))
    {
        if (!mux_active_latch)
        {
            mux_active_latch = true;
            req->next_stage = ns;
            mux_latch = req;
        }
        else if (error)
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; mux stage already in use.\n";
            errMsg << print_pipeline_state();
            throw std::invalid_argument(errMsg.str());
        }
        else
        {
            moved = false;
            if(ns == mt || ns == mot)
                tag_active_latch = false;
        }
    }

    if (moved && (ns == mo || ns == mot || ns == o))
    {
        if (!output_active_latch)
        {
            output_active_latch = true;
            req->next_stage = ns;
            output_latch = req;
        }
        else if(error)
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; output stage already in use.\n";
            errMsg << print_pipeline_state();
            throw std::invalid_argument(errMsg.str());
        }
        else
        {
            moved = false;
            if(ns == mo)
                mux_active_latch = false;

            if(ns == mot)
            {
                mux_active_latch = false;
                tag_active_latch = false;
            }
        }
    }

    if (ns == mshr_s || ns == mshr_u || ns == mshr_r || ns == mshr_o)
    {
        if (!mshr_active_latch)
        {
            mshr_active_latch = true;
            req->next_stage = ns;
            mshr_latch = req;
            stall_reram = true;
        }
        else if (error)
        {
            std::stringstream errMsg;
            errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; mshr stage already in use.\n";
            errMsg << print_pipeline_state();
            throw std::invalid_argument(errMsg.str());
        }
        else
        {
            moved = false;
        }
    }

    //if we couldn't move it, need to keep it in the same state
    if (!moved)
    {
        req->next_stage = ns;
        move_req(req, req->cur_stage, true);
    }

    return moved;
}

//send request into the pipeline
void L2cache_pipeline::send_request()
{
    //stall in pipeline
    if (stall > 0)
    {
        stall--;
        stalled_last_cycle = true;
        return;
    }

    if (after_write_stall)
    {
        after_write_stall = false;
        stalled_last_cycle = true;
        return;
    }

    if (!decode_active_latch && !stall_reram)
    {
        mem_request* first_req = get_next_request();

        if (first_req != nullptr)
        {
            if (first_req->type == write_dw || first_req->type == write_block || first_req->type == cache_fill)
            {
                //if (!stalled_last_cycle)
                if(tag_active_latch && tag_latch->next_stage == t)
                {
                    stalled_last_cycle = true;
                    request_rob.push_front(*first_req);
                    delete first_req;
                    return;  //stall 1 cycle before a write
                }
                else
                {
                    after_write_stall = true;	//stall once after write as well
                }
            }

            //send the request to the pipeline
            move_req(first_req, first_req->next_stage, true);
            stalled_last_cycle = false;
        }
        else //no cache requests to decode
        {
            stalled_last_cycle = true;
        }
    }
}

#if PIPELINE_OUTPUT
void L2cache_pipeline::setup_pipeline_print()
{
    pipefile.open("pipeline.csv", std::ofstream::out);

    pipefile << "Cycle," << "Decode," << "Tag," << "MSHR," << "MUX," << "Data," << "Output," << "\n";
}

void L2cache_pipeline::print_cur_pipeline(long long cur_cycle)
{
    pipefile << cur_cycle << ",";

    if (decode_active)
    {
        pipefile << translate_type(decode_stage->type) << " " << decode_stage->packet_id << " - " << translate_stage(decode_stage->cur_stage);
    }
    pipefile << ",";

    if (tag_active)
    {
        pipefile << translate_type(tag_stage->type) << " " << tag_stage->packet_id << " - " << translate_stage(tag_stage->cur_stage);
    }
    pipefile << ",";

    if (mshr_active)
    {
        pipefile << translate_type(mshr_stage->type) << " " << mshr_stage->packet_id << " - " << translate_stage(mshr_stage->cur_stage);
    }
    pipefile << ",";
    
    if (mux_active)
    {
        pipefile << translate_type(mux_stage->type) << " " << mux_stage->packet_id << " - " << translate_stage(mux_stage->cur_stage);
    }
    pipefile << ",";

    if (data_active)
    {
        pipefile << translate_type(data_stage->type) << " " << data_stage->packet_id << " - " << translate_stage(data_stage->cur_stage);
    }
    pipefile << ",";

    if (output_active)
    {
        pipefile << translate_type(output_stage->type) << " " << output_stage->packet_id << " - " << translate_stage(output_stage->cur_stage);
    }
    pipefile << ",";

    pipefile << "\n";
}
#endif

std::string L2cache_pipeline::print_pipeline_state()
{
    std::stringstream pipe;

    pipe << "Decode: ";
    if (decode_active)
    {
        pipe << translate_type(decode_stage->type) << " " << decode_stage->packet_id << " - ";
        pipe << translate_stage(decode_stage->cur_stage) << "->" << translate_stage(decode_stage->next_stage);
    }
    pipe << "\n";
    pipe << "Decode Latch: ";
    if (decode_active_latch)
    {
        pipe << translate_type(decode_latch->type) << " " << decode_latch->packet_id << " - ";
        pipe << translate_stage(decode_latch->cur_stage) << "->" << translate_stage(decode_latch->next_stage);
    }
    pipe << "\n";

    pipe << "Tag: ";
    if (tag_active)
    {
        pipe << translate_type(tag_stage->type) << " " << tag_stage->packet_id << " - ";
        pipe << translate_stage(tag_stage->cur_stage) << "->" << translate_stage(tag_stage->next_stage);
    }
    pipe << "\n";
    pipe << "Tag Latch: ";
    if (tag_active_latch)
    {
        pipe << translate_type(tag_latch->type) << " " << tag_latch->packet_id << " - ";
        pipe << translate_stage(tag_latch->cur_stage) << "->" << translate_stage(tag_latch->next_stage);
    }
    pipe << "\n";

    pipe << "MSHR: ";
    if (mshr_active)
    {
        pipe << translate_type(mshr_stage->type) << " " << mshr_stage->packet_id << " - ";
        pipe << translate_stage(mshr_stage->cur_stage) << "->" << translate_stage(mshr_stage->next_stage);
    }
    pipe << "\n";
    pipe << "MSHR Latch: ";
    if (mshr_active_latch)
    {
        pipe << translate_type(mshr_latch->type) << " " << mshr_latch->packet_id << " - ";
        pipe << translate_stage(mshr_latch->cur_stage) << "->" << translate_stage(mshr_latch->next_stage);
    }
    pipe << "\n";

    pipe << "MUX: ";
    if (mux_active)
    {
        pipe << translate_type(mux_stage->type) << " " << mux_stage->packet_id << " - ";
        pipe << translate_stage(mux_stage->cur_stage) << "->" << translate_stage(mux_stage->next_stage);
    }
    pipe << "\n";
    pipe << "MUX Latch: ";
    if (mux_active_latch)
    {
        pipe << translate_type(mux_latch->type) << " " << mux_latch->packet_id << " - ";
        pipe << translate_stage(mux_latch->cur_stage) << "->" << translate_stage(mux_latch->next_stage);
    }
    pipe << "\n";

    pipe << "Data: ";
    if (data_active)
    {
        pipe << translate_type(data_stage->type) << " " << data_stage->packet_id << " - ";
        pipe << translate_stage(data_stage->cur_stage) << "->" << translate_stage(data_stage->next_stage);
    }
    pipe << "\n";
    pipe << "Data Latch: ";
    if (data_active_latch)
    {
        pipe << translate_type(data_latch->type) << " " << data_latch->packet_id << " - ";
        pipe << translate_stage(data_latch->cur_stage) << "->" << translate_stage(data_latch->next_stage);
    }
    pipe << "\n";

    pipe << "Output: ";
    if (output_active)
    {
        pipe << translate_type(output_stage->type) << " " << output_stage->packet_id << " - ";
        pipe << translate_stage(output_stage->cur_stage) << "->" << translate_stage(output_stage->next_stage);
    }
    pipe << "\n";
    pipe << "Output Latch: ";
    if (output_active_latch)
    {
        pipe << translate_type(output_latch->type) << " " << output_latch->packet_id << " - ";
        pipe << translate_stage(output_latch->cur_stage) << "->" << translate_stage(output_latch->next_stage);
    }
    pipe << "\n";

    return pipe.str();
}

//d, t, b_r, b_w, m, mt, mo, mot, o, mshr_s, mshr_u, mshr_r, mshr_o, rr_r, rr_s, rr
string L2cache_pipeline::translate_stage(pipeline_stage stage)
{
    switch (stage)
    {
    case d: return "d";
    case t: return "t";
    case b_r: return "b_r";
    case b_w: return "b_w";
    case m: return "m";
    case mt: return "mt";
    case mo: return "mo";
    case mot: return "mot";
    case o: return "o";
    case mshr_s: return "mshr_s";
    case mshr_u: return "mshr_u";
    case mshr_r: return "mshr_r";
    case mshr_o: return "mshr_o";
    case rr_r: return "rr_r";
    case rr_s: return "rr_s";
    case rr: return "rr";
    default: return "ERR";
    }
}


//read_dw, write_dw, read_block, write_block, cache_fill, write_back, close
string L2cache_pipeline::translate_type(request_type type)
{
    switch (type)
    {
    case read_dw: return "read_dw";
    case write_dw: return "write_dw";
    case read_block: return "read_block";
    case write_block: return "write_block";
    case cache_fill: return "cache_fill";
    case write_back: return "write_back";
    case close_bank: return "close";
    default: return "ERR";
    }
}

