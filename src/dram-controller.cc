
/*
  rram-controller.cc

  Donald Yeung
  25 July 2017

  Rram controller class simulates an end-point on the network that contains
  RRAM banks.
 */


#include <stdio.h>
#include <stdlib.h>
 //#include <unistd.h> 
#include <iostream>
#include "noc-sim.hh"

dram_controller::dram_controller(int x, int y) : ram_controller(x, y)
{
    if (active_type == dram_tile)
    {
        //controller
        create_controller();
        sendTrans = false;
    }
}

void dram_controller::create_controller()
{
    int channel_n;
    int n_x, n_y;

    //inefficient, but should only have a few number of channels anyway
    for (int i = 0; i < conf->num_dram_tiles; i++)
    {
        conf->nth_tile_type(i, tile_type::dram_tile, &n_x, &n_y);
        if (n_x == x_coord && n_y == y_coord)
        {
            channel_n = i;
            break;
        }
    }

    channel = new dramsim3::Controller(channel_n, *conf->dram_conf, *conf->dram_timing);
}

void dram_controller::cycle(long long cur_cycle)
{
    if (active_type != compute) {
        //check for finished requests
        check_controller(cur_cycle);

        //run dramsim tick
        //if DRAM clock and processor clock are the same
        if((cur_cycle % conf->cpu_mem_clk_ratio) == 0)
            channel->ClockTick();

        //route and transfer packets
        route_in(cur_cycle);
        route_out(cur_cycle);

#if BANK_CONFLICT_DETECTION
        if (x_coord == 0 && y_coord == 0 && (cur_cycle % 100) == 0 && cur_cycle >= 10000) {
            system("clear");
            print_bank_traffic();
            fflush(stdout);
        }
#endif
    }
}

void dram_controller::check_controller(long long cur_cycle)
{
    while (true)
    {
        auto tuple = channel->ReturnDoneTrans(cur_cycle);

        if (std::get<1>(tuple) == 1)
        {
            stats->set_from_bank(std::get<2>(tuple), cur_cycle, x_coord, y_coord);
            stats->tile_ram_stats[IND(x_coord, y_coord)].dram_write++;
#if RRAM_BANK_OUT
            printf("(%d, %d, %d): RRAM bank completed write operation!\n", y_coord, x_coord, b_coord);
#endif
        }
        else if (std::get<1>(tuple) == 0)
        {
            stats->set_from_bank(std::get<2>(tuple), cur_cycle, x_coord, y_coord);
            stats->tile_ram_stats[IND(x_coord, y_coord)].dram_read++;
#if RRAM_BANK_OUT
            printf("(%d, %d, %d): RRAM bank completed read operation!\n", y_coord, x_coord, b_coord);
#endif
            //recreate request and push to output queue - needed: type, addr, packet id, dw count
            ram_request req = ram_request(std::get<2>(tuple)); //packet id in slot 2
            req.type = read_dw;
            req.addr = std::get<0>(tuple) >> WORD_SIZE_OFFSET;
            req.dw_count = settings->fetch_width;

            output_requests.push(req);
        }
        else 
        {
            break;
        }

        //stats->bank_profile[x_coord][y_coord][b_coord]++;
    }
}

//Gather requests from the network and add attempt to send to channel
void dram_controller::route_in(long long cur_cycle)
{
    uint64 address;
    int vector;

    //try to schedule transaction we have read from the network
    if (sendTrans)
    {
        if (channel->WillAcceptTransaction(trans.added_cycle, trans.is_write))
        {
            channel->AddTransaction(trans);
            sendTrans = false;
            stats->set_to_bank(trans.packet_id, cur_cycle, x_coord, y_coord);
        }
        else //still holding onto the last transaction, don't read more from network
        {
            return;
        }
    }

    //take as many packets as channel is wide 
    //(this isn't perfect, but should be reasonable)
    for (int i = 0; i < conf->phits; i++)
    {
        if (input->is_empty()) return; //nothing to read

        uint64 flit = input->remove_from_head();

        if (noc_read_state == sHeader)
        {
            input_req = new ram_request(PACKET_ID(flit));
            vector = flit & BYTES_MASK;

            if (flit & READ_FLIT_MASK)
            {
                input_req->type = (vector == 1) ? read_block : read_dw;
                input_req->dw_count = (vector == 1) ? settings->vector_size * settings->fetch_width : settings->fetch_width;
                noc_read_state = sReadAddr;
            }
            else if (flit & WRITE_FLIT_MASK)
            {
                input_req->type = (vector == 1) ? write_block : write_dw;
                noc_read_state = sWriteAddr;
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Unknown packet " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (noc_read_state == sReadAddr)
        {
            if (flit & TAIL_FLIT_MASK)
            {
                address = flit & (~TAIL_FLIT_MASK);
                input_req->addr = address;

                dramsim3::Address mapped = conf->dram_conf->AddressMapping(address << WORD_SIZE_OFFSET);
                input_req->bank = mapped.bank;
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Expected address and tail for read packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (noc_read_state == sWriteAddr)
        {
            address = flit;
            input_req->addr = address;

            dramsim3::Address mapped = conf->dram_conf->AddressMapping(address << WORD_SIZE_OFFSET);
            input_req->bank = mapped.bank;

            noc_read_state = sData;
        }
        else  //sData
        {
            input_req->dw_count++;

            unsigned fetch_width;
            if (input_req->type == write_dw)
            {
                fetch_width = settings->fetch_width;
            }
            else
            {
                fetch_width = settings->vector_size * settings->fetch_width;
            }

            if (input_req->dw_count > fetch_width)
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Too many data flits for write packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }

        if (flit & TAIL_FLIT_MASK)
        {
            stats->set_to_controller(input_req->packet_id, cur_cycle, x_coord, y_coord);

#if BANK_CONFLICT_DETECTION
            bank_conflict_calc(cur_cycle, input_req->bank);
#endif
            trans = dramsim3::Transaction(input_req->addr << WORD_SIZE_OFFSET, 
                                          input_req->type == write_block || input_req->type == write_dw, 
                                          input_req->packet_id);

            if (channel->WillAcceptTransaction(trans.added_cycle, trans.is_write))
            {
                channel->AddTransaction(trans);
                stats->set_to_bank(trans.packet_id, cur_cycle, x_coord, y_coord);
            }
            else
            {
                sendTrans = true;
            }

#if RRAM_CONTR_OUT
            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): Controller received packet : " << input_req->packet_id << "\n";
            Stats::get()->printLog(msg.str(), false);
#endif

            //start over
            noc_read_state = sHeader;
            delete input_req;

            break;
        }
    }
}

void dram_controller::collect_stats()
{
    //get stats from channel
    stats->new_total_dram_stats += channel->collect_stats();
}

void dram_controller::PrintFinalStats() 
{
    channel->PrintFinalStats();
    stats->total_dram_stats += channel->collect_final_stats();
};
