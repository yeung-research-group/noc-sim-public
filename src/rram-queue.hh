
/*
  rram-queue.hh

  Candace Walden
  21 August 2017

  RRAM queue class.  Queues are on input channels to subarrays.
 */

class rram_queue
{
private:
  uint64 *the_queue;
  int size;
  int head;
  int tail;
  int routed; /* is currently sending a packet */

public:
  rram_queue(int queue_size);

  int is_empty();
  int is_full();
  int free_entries();
  int entries();

  int is_routable();
  void set_routed();
  void clear_routed();
  int is_routed();

  uint64 remove_from_head();
  void insert_at_tail(uint64 flit);
  uint64 peak_at_head();
};
