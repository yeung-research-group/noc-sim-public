/*
  traffic-queue.cc

  Devesh Singh
  3 Sep 2017

  To be configured for the traffic generators for customized traffic
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

traffic_queue::traffic_queue(int x,int y, int th_num)
{
  conf = Conf_ReRAM::get();
  x_crd = x;
  y_crd = y;
  thread_id = conf->tiles_thread(x, y, th_num);

  te_cnt = 0;
  gather_width = 0;
  gather_cnt = 0;
  gather_miss_width=0;

  done = true;

  if(!conf->trace && !conf->exec)
    create_my_seq();
  
}

void traffic_queue::create_random_sequence(int total_ins, int single_mem_ops, int simd_mem_ops, int simd_width)
{
    std::lock_guard<std::mutex> lock(barrier);

    for (int i = 0; i < total_ins; i++)
    {
        int type = random() % total_ins;
        int rw = random() % 10;
        if (type < simd_mem_ops)  //simd operation
        {
            if (rw > 3) //write = 0
            {
                the_queue.push_back(instruction(scatter, 0, simd_width)); //TODO: Fix random address
            }
            else
            {
                the_queue.push_back(instruction(gather, 0, simd_width)); //TODO: Fix random address
            }
        }
        else if (type < (simd_mem_ops + single_mem_ops))  //single operation
        {
            if (rw > 3) //write = 0
            {
                the_queue.push_back(instruction(single_write, 0)); //TODO: Fix random address
            }
            else
            {
                the_queue.push_back(instruction(single_read, 0)); //TODO: Fix random address
            }
        }
        else //basic instruction
        {
            the_queue.push_back(instruction(other));
        }
    }
}

void traffic_queue::create_my_seq(){
  //std::lock_guard<std::mutex> lock(barrier);
  double ins_per_mem_access = conf->ins_per_mem_access;

  double instructions_per_miss= 1000/(conf->mpki);
  double mpki = conf->mpki;
  int mem_accesses = conf->spatial_reuse + 1;
  double integral, fraction;
  int temp;
  the_queue.clear();
  uint64 addr[8],init_addr;
  int count;
  
  if(mpki <= 0){
      int block_offset_bits; //In multiples of doubles

      //TODO: get block offset from conf
    if(conf->ram_mode == dram){
      block_offset_bits = 7;
    }
    else{
      block_offset_bits = 3;
    }
    generate_random_addresses(&init_addr,1);
    init_addr = init_addr & (0xFFFFFFFFFFFFFFFF << block_offset_bits); // init_addr shifted to the beginning of cache block. For convenience in later part.
    for(int i=0; i<mem_accesses; i++){
      the_queue.push_back(instruction(single_read,(init_addr + i*8)));
      for(int j=0; j<ins_per_mem_access; j++){
        the_queue.push_back(instruction(other));
      }

    }
  }
  else{

    if(mpki <= 1){
      //MPKI = 0.1
      count = conf->micro_bench_simd_width;
      generate_random_addresses(addr,count);
      the_queue.push_back(instruction(gather,addr,count));
      instructions_per_miss = instructions_per_miss - 1;
        //insert_at_tail( new instruction(gather,addr,count));
      //for(int i=0; i<(instructions_per_miss*count); i++)
      for(int i=0; i<(instructions_per_miss); i++)
          //insert_at_tail(new instruction(other));
        the_queue.push_back(instruction(other));

    }
    else{
      count = conf->micro_bench_simd_width; 
      //fraction = modf((instructions_per_miss*count), & integral);
      fraction = modf((instructions_per_miss), & integral);
      temp = (int)(fraction*100);
      integral = integral - 1;
      for(int i=0; i<100; i++){
        generate_random_addresses(addr,count);
        the_queue.push_back(instruction(gather,addr,count));
          //insert_at_tail( new instruction(gather,addr,count));
        for(int j=0; j<integral; j++)
          //insert_at_tail(new instruction(other));
          the_queue.push_back(instruction(other));
           
        if(temp){
          the_queue.push_back(instruction(other));
          temp--;
        }
          
      }
    }
  }
  Stats::get()->set_queue(true, thread_id);
  done = false;
#if TRAFFIC_QUEUE_OUT
  printf("(%d,%d) Refilled traffic queue for thread -%d \n ",x_crd,y_crd,thread_id);
#endif
}

void traffic_queue::generate_random_addresses(uint64* array, int count) 
{
    for (int i = 0; i < count; i++) 
    {
        uint64 addr = rand();
        addr = addr << 32 | rand();
        array[i] = addr;
    }
}

void traffic_queue::rotate_element_at_head()
{
  std::lock_guard<std::mutex> lock(barrier);
  instruction temp = the_queue.front();
  the_queue.pop_front();
  the_queue.push_back(temp);
}

instruction* traffic_queue::peak_element_at_head()
{
  std::lock_guard<std::mutex> lock(barrier);
  if(the_queue.empty()){
    return NULL;
  }
  else
    return &(the_queue.front());
}

void traffic_queue::update_head(instruction* ins){
  std::lock_guard<std::mutex> lock(barrier);
  the_queue.front() = *ins;
}
 
instruction* traffic_queue::delete_element_at_head(){
  std::lock_guard<std::mutex> lock(barrier);
  instruction *temp = &the_queue.front();
  
#if TRAFFIC_QUEUE_OUT
  //printf("(%d, %d): Thread %d completed instruction! Type: %d addr- 0x%016llx\n", y_crd, x_crd, thread_id, ins->type, ins->address[0]);
 
  std::ostringstream msg;
  msg << "(" << x_crd << ", " << y_crd << "): Thread "<<thread_id<<" completed instruction! Type: "<<temp->type;

  if(temp->mem_locs > 0){
    msg << " addr -";
      for(int i = 0; i < temp->mem_locs ; i++)
	msg << " 0x" << setfill('0') << setw(16) << hex << temp->address[i];	
  }  
  msg << "\n";
  Stats::get()->printLog(msg.str(), false);
#endif

  the_queue.pop_front();
  if (the_queue.empty())
  {
    Stats::get()->set_queue(false, thread_id);

    done = true;
    if(!conf->trace && !conf->exec){
      create_my_seq();
    }
#if TRAFFIC_QUEUE_OUT
    std::ostringstream msg;
    //printf("queue empty for thread ID - %d\n",thread_id);
    msg << "queue empty for thread ID - "<< thread_id <<"\n";
    Stats::get()->printLog(msg.str(), false);
#endif
  }
#if !NOC_SIM_SNIPER
  if (the_queue.size() < QUEUE_OVERFILL)
  {
    Stats::get()->set_queue_watermark(false, thread_id);
  }
#endif
  return temp;
}

int traffic_queue::insert_at_tail(instruction* ins){
  std::lock_guard<std::mutex> lock(barrier);
  if (the_queue.empty())
  {
    Stats::get()->set_queue(true, thread_id);
    done = false;
  }
#if !NOC_SIM_SNIPER
  if (the_queue.size() == QUEUE_OVERFILL - 1)
  {
    Stats::get()->set_queue_watermark(true, thread_id);
  }
#endif

  the_queue.push_back(*ins);

#if TRAFFIC_QUEUE_OUT
  //printf("(%d, %d): Thread %d received instruction! Type: %d addr- 0x%016llx\n", y_crd, x_crd, thread_id, ins->type, ins->address[0]);
  std::ostringstream msg;
  msg << "(" << x_crd << ", " << y_crd << "): Thread "<<thread_id<<" received instruction! Type: "<<ins->type<<" ";

  if(ins->mem_locs > 0){
    msg << " addr -";
      for(int i = 0; i < ins->mem_locs ; i++)
	msg << " 0x" << setfill('0') << setw(16) << hex << ins->address[i];	
  }  
  msg << "\n";
  Stats::get()->printLog(msg.str(), true);

#endif

  //delete ins;
  return TRUE;
}
 
int traffic_queue::is_empty(){
  std::lock_guard<std::mutex> lock(barrier);
  return the_queue.empty();
}

#if !NOC_SIM_SNIPER
int traffic_queue::is_full(){
  std::lock_guard<std::mutex> lock(barrier);
  return the_queue.size() >= QUEUE_OVERFILL;
}
#endif


/*
int traffic_queue::traversed_edge_count(){
  int packet,hit_cnt;
  while(!this->is_empty()){
    packet = the_queue[head];
    head = (head+1)%size;
    if((packet & SCATTER_INS_MASK)||(packet & GATHER_INS_MASK)){
      te_cnt = te_cnt - (packet & WIDTH_MASK);
      hit_cnt = (packet & HIT_SIMD_WIDTH_MASK)>>20; //20 = bits set in WIDTH_MASK
      te_cnt = te_cnt - hit_cnt;
      
    }
  }
  return te_cnt;
}
*/
