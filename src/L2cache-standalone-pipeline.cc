#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_standalone_pipeline::L2cache_standalone_pipeline(int x, int y) : L2cache_bank(x, y), L2cache_pipeline(x, y), L2cache_standalone(x, y)
{

    //nothing to set
    return;
}

void L2cache_standalone_pipeline::cycle(long long cur_cycle)
{
    //route out to NoC and memory
    route_out(cur_cycle);
    send_ram_request(cur_cycle);

    stall_reram = false;
    
    //move pipeline if output is cleared
    if (output_queue.size() < conf->buffer_size && ram_req_queue.size() < conf->buffer_size)  //TODO: better size limit?
    {
        move_pipeline(cur_cycle);
        send_request();
    }
    
    //route in from memory
    if (ram_return_req.size() < conf->l2_settings.rob_entries)
        read_ram_reply(cur_cycle);

    //route in from NOC
    if (request_rob.size() < conf->l2_settings.rob_entries)
        route_in(cur_cycle);
}

void L2cache_standalone_pipeline::reram_send_exec(long long cur_cycle)
{
    std::stringstream errMsg;
    errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid request type standalone cache.";
    throw std::invalid_argument(errMsg.str());
}


void L2cache_standalone_pipeline::mshr_update_exec(long long cur_cycle)
{

    if (mshr_stage->valid_data)  //return request from ReRAM
    {
        //write data into MSHR
        update_mshr(mshr_stage);

        //is all requested MSHR data received?
        if (check_mshr_complete(mshr_stage))
        {
            //send back read replies
            move_req(new mem_request(*mshr_stage), mshr_o, true);

            //do cache fill
            stall++;

            //I think this was unnecesary as this comes from a queue and so is already a copy
            //unlike in the integrated case
            //allows deleting of requests as output without endagering the held request
            //mshr_stage = new mem_request(*mshr_stage);
            mshr_stage->type = cache_fill;
            mshr_stage->next_stage = d;

            request_rob.push_front(*mshr_stage);
        }
        
        delete mshr_stage;
    }
    else  //new request
    {
        first_mshr_update(cur_cycle);
    }
}

void L2cache_standalone_pipeline::mshr_reram_exec(long long cur_cycle)
{
    if (mshr_stage->type == read_dw)
    {
        //send to reram output queue if hasn't already
        if (mshr_stage->mshr->sectors_recv[SECTOR_ID_ADDR(mshr_stage->addr)] == pending_req)
        {
            ram_req_queue.push(*mshr_stage);
            mshr_stage->mshr->sectors_recv[SECTOR_ID_ADDR(mshr_stage->addr)] = sent_req;
        }
    }
    else
    {
        bool pending = false;
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            pending |= (mshr_stage->mshr->sectors_recv[i] == pending_req);
        }

        if(pending)
        {
            ram_req_queue.push(*mshr_stage);
            for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
            {
                if(mshr_stage->mshr->sectors_recv[i] == pending_req)
                    mshr_stage->mshr->sectors_recv[i] = sent_req;
            }
        }
    }

    //delete mshr_stage;  //deletes from read subentries
}
