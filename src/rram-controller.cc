
/*
  rram-controller.cc

  Donald Yeung
  25 July 2017

  Rram controller class simulates an end-point on the network that contains
  RRAM banks.
 */


#include <stdio.h>
#include <stdlib.h>
 //#include <unistd.h> 
#include <iostream>
#include "noc-sim.hh"


rram_controller::rram_controller(int x, int y) : ram_controller(x, y)
{
    if (active_type == reram_tile)
    {
        //work queues
        bank_schedule = std::priority_queue<schedule_item>();
        request_rob = std::list<ram_request>();
        vector_queue = std::list<ram_request>();

        //banks
        create_banks(x, y);
    }
}

void rram_controller::create_banks(int x, int y)
{
    //rram_queue **in_queues;
    //rram_queue **out_queues;

    banks = new rram_bank * [settings->banks];

    for (unsigned i = 0; i < settings->banks; i++)
    {
        banks[i] = new rram_bank(x, y, i, settings, &bank_schedule);
    }
}

void rram_controller::cycle(long long cur_cycle)
{
    if (active_type != compute) {
        //route and transfer packets
        if (request_rob.size() < settings->queue_depth)
            route_in(cur_cycle);
        check_banks(cur_cycle);
        route_out(cur_cycle);
#if BANK_CONFLICT_DETECTION
        if (x_coord == 0 && y_coord == 0 && (cur_cycle % 100) == 0 && cur_cycle >= 10000) {
            system("clear");
            print_bank_traffic();
            fflush(stdout);
        }
#endif
    }
}

//Gather requests from the network and add to ROB
void rram_controller::route_in(long long cur_cycle)
{
    uint64 address;
    int vector;

    //take as many packets as channel is wide 
    //(this isn't perfect, but should be reasonable)
    for (int i = 0; i < conf->phits; i++)
    {
        if (input->is_empty()) return; //nothing to read

        uint64 flit = input->remove_from_head();

        if (noc_read_state == sHeader)
        {
            input_req = new ram_request(PACKET_ID(flit));
            vector = flit & BYTES_MASK;

            if (flit & READ_FLIT_MASK)
            {
                input_req->type = (vector == 1) ? read_block : read_dw;
                input_req->dw_count = (vector == 1) ? 8 : 1;
                noc_read_state = sReadAddr;
            }
            else if (flit & WRITE_FLIT_MASK)
            {
                input_req->type = (vector == 1) ? write_block : write_dw;
                noc_read_state = sWriteAddr;
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Unknown packet " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (noc_read_state == sReadAddr)
        {
            if (flit & TAIL_FLIT_MASK)
            {
                address = flit & (~TAIL_FLIT_MASK);
                input_req->addr = address;
                input_req->bank = BANK_ID(address);
                input_req->sector = SECTOR_ID_ADDR(address);
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Expected address and tail for read packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (noc_read_state == sWriteAddr)
        {
            address = flit;
            input_req->addr = address;

            input_req->bank = BANK_ID(address);
            input_req->sector = SECTOR_ID_ADDR(address);

            noc_read_state = sData;
        }
        else  //sData
        {
            input_req->dw_count++;

            unsigned fetch_width;
            if (input_req->type == write_dw)
            {
                fetch_width = settings->fetch_width;
            }
            else
            {
                fetch_width = settings->vector_size * settings->fetch_width;
            }

            if (input_req->dw_count > fetch_width)
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Too many data flits for write packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }

        if (flit & TAIL_FLIT_MASK)
        {
            stats->set_to_controller(input_req->packet_id, cur_cycle, x_coord, y_coord);

#if RRAM_CONTR_OUT
            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): RRAM controller received write packet : " << packet_id << "\n";
            Stats::get()->printLog(msg.str(), false);
            //printf("(%d, %d): RRAM controller received packet!\n", y_coord, x_coord);
#endif

            //add input_req to ROB
            if (input_req->type == read_dw || input_req->type == write_dw)
            {
#if BANK_CONFLICT_DETECTION
                bank_conflict_calc(cur_cycle, input_req->bank)
#endif

                    if (banks[input_req->bank]->busy)
                    {
                        request_rob.push_back(*input_req);
                        //copy created -- delete original
                        delete input_req;
                    }
                    else
                    {
                        banks[input_req->bank]->send_request(cur_cycle, input_req);
                    }
            }
            else if (input_req->type == read_block)
            {
                //add request to vector queue and put each bank request separately in ROB
                vector_queue.push_back(*input_req);
                           
                for (unsigned j = 0; j < settings->vector_size; j++)
                {
                    ram_request* vec_req = new ram_request(*input_req);
                    vec_req->vector_req = &*vector_queue.rbegin();
                    vec_req->bank = (vec_req->bank + j) % settings->banks;
                    vec_req->sector = (input_req->sector + j) % settings->vector_size;

#if BANK_CONFLICT_DETECTION
                    bank_conflict_calc(cur_cycle, vec_req->bank)
#endif
                        if (banks[vec_req->bank]->busy)
                        {
                            request_rob.push_back(*vec_req);
                            delete vec_req;
                        }
                        else
                        {
                            banks[vec_req->bank]->send_request(cur_cycle, vec_req);
                        }
                }

                //copy created -- delete original
                delete input_req;
            }
            else //write block
            {
                for (unsigned j = 0; j < settings->vector_size; j++)
                {
                    ram_request* vec_req = new ram_request(*input_req);
                    vec_req->bank = (vec_req->bank + j) % settings->banks;
                    vec_req->sector = (input_req->sector + j) % settings->vector_size;

#if BANK_CONFLICT_DETECTION
                    bank_conflict_calc(cur_cycle, vec_req->bank)
#endif
                        if (banks[vec_req->bank]->busy)
                        {
                            request_rob.push_back(*vec_req);
                            delete vec_req;
                        }
                        else
                        {
                            banks[vec_req->bank]->send_request(cur_cycle, vec_req);
                        }
                }

                //copies created -- delete original
                delete input_req;
            }

#if RRAM_CONTR_OUT
            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): Controller received packet : " << input_req->packet_id << "\n";
            Stats::get()->printLog(msg.str(), false);
#endif

            //start over
            noc_read_state = sHeader;

            break;
        }
    }
}

void rram_controller::check_banks(long long cur_cycle)
{
    /* Crank RAM banks */
    while (!bank_schedule.empty() && bank_schedule.top().cycle <= cur_cycle)
    {
        rram_bank* bank = bank_schedule.top().bank;
        unsigned bank_id = bank->b_coord;
        if (bank->cur_req->type == read_dw)
        {
            //add copy of request to output
            output_requests.push(*bank->cur_req);
        }
        else if (bank->cur_req->type == read_block)
        {
            //update vector request
            if (vq_update(bank->cur_req))
            {
                //add request to output if complete
                output_requests.push(*bank->cur_req->vector_req);
                //remove from vq
                unsigned target_id = bank->cur_req->packet_id;
                vector_queue.remove_if([target_id](ram_request req) {return req.packet_id == target_id; });
            }
        }

        if (bank->cur_req->type == close_bank)
        {
            bank->bank_close_page(cur_cycle);
        }
        else
        {
            bank->complete_request(cur_cycle);
        }

        bank_schedule.pop();

        //send next request
        if (!bank->busy)  //bank may still be busy if closed page policy
            send_next_request(cur_cycle, bank_id);
    }
}

bool rram_controller::vq_update(ram_request * req)
{
    ram_request* vec_req = req->vector_req;
    vec_req->valid[req->sector] = true;

    bool done = true;
    for (unsigned i = 0; i < settings->vector_size; i++)
    {
        done &= vec_req->valid[i];
    }

    return done;
}

void rram_controller::send_next_request(long long cur_cycle, unsigned bank_id)
{
    //search through rob until request has bank id
    std::list<ram_request>::iterator end = request_rob.end();
    for (std::list<ram_request>::iterator req = request_rob.begin(); req != end; ++req)
    {
        if (req->bank == bank_id)
        {
            banks[bank_id]->send_request(cur_cycle, new ram_request(*req));
            request_rob.erase(req);
            return;
        }
    }
}

ram_request::ram_request(const ram_request* req)
{
    type = req->type;
    packet_id = req->packet_id;
    addr = req->addr;
    bank = req->bank;
    sector = req->sector;
    dw_count = req->dw_count;
    vector_req = req->vector_req;

    for (unsigned i = 0; i < 8; i++)
    {
        valid[i] = req->valid[i];
    }
}
