#pragma once
/*
stats.hh

Candace Walden
30 August 2017

Statistics and latency table for all packets
*/

#include <unordered_map>
#include <map>

enum packet_type { read_packet, write_packet };

struct packets {
    packets(packet_type ptype, long long cycle, int x, int y, uint64 addr, uint64 pc)
        : type(ptype),
        from_cpu(cycle),
        to_l2(-1),
        to_controller(-1),
        to_bank(-1),
        from_bank(-1),
        from_controller(-1),
        from_l2(-1),
        to_cpu(-1),
        recv(-1),
        x_coord(x),
        y_coord(y),
        l2_x_coord(-1),
        l2_y_coord(-1),
        addr(addr),
        PC(pc) {}

  packet_type type;   // 1 - WRITE, 0 - READ
  long long from_cpu;
  long long to_l2;
  long long to_controller;
  long long to_bank;
  long long from_bank;
  long long from_controller;
  long long from_l2;
  long long to_cpu;
  long long recv;

  int x_coord;
  int y_coord;
  int l2_x_coord;
  int l2_y_coord;
  uint64 addr;
  uint64 PC; //packets not corresponding to a read/write command e.g. lock or wb will have pc -1
};

struct latency {
    latency()
        : tot_packets(0),
    in_noc_packets(0),
    out_noc_packets(0),
    controller_packets(0),
    from_dram_packets(0),
    ram_packets(0),
    out_queue_packets(0),
    in_queue_packets(0),
    from_L2_packets(0),
    tot_latency(0),
    in_noc_latency(0),
    out_noc_latency(0),
    to_dram_latency(0),
    from_dram_latency(0),
    ram_latency(0),
    out_queue_latency(0),
    in_queue_latency(0),
    l2_latency(0) {}

    long long tot_packets;
    long long in_noc_packets;
    long long out_noc_packets;
    long long controller_packets;
    long long from_dram_packets;
    long long ram_packets;
    long long out_queue_packets;
    long long in_queue_packets;
    long long from_L2_packets;

    double tot_latency;
    double in_noc_latency;
    double out_noc_latency;
    double to_dram_latency;
    double from_dram_latency;
    double ram_latency;
    double out_queue_latency;
    double in_queue_latency;
    double l2_latency;

    latency& operator+=(const latency& a);
};


struct cpu_stats
{
    cpu_stats()
        : te_count(0),
          ins_executed(0),
          packets_prefetched(0),
          prefetchq_hits(0),
          cpu_idle_cycles(0) {}

    long long te_count;
    long long ins_executed;
    long long packets_prefetched;
    long long prefetchq_hits;
    long long cpu_idle_cycles;

    cpu_stats& operator+=(const cpu_stats& a);
};

struct l1_stats
{
    l1_stats()
        : rd_exec(0),
          wr_exec(0),
          wb_from_l1(0),
          l1_hit(0),
          l1_miss(0) {}

    long long rd_exec;
    long long wr_exec;
    long long wb_from_l1;
    long long l1_hit;
    long long l1_miss;

    l1_stats& operator+=(const l1_stats& a);
};

struct l2_stats
{
    l2_stats()
        : wb_from_l2(0),
          vec_wb_from_l2(0),
          l2_hit(0),
          l2_miss(0),
          l2_gather_miss(0),
          l2_gather_hit(0),
          l2_write(0),
          l2_vec_write(0),
          l2_read(0),
          l2_vec_read(0),
          cache_fill(0){}

    long long wb_from_l2;
    long long vec_wb_from_l2;
    long long l2_hit;
    long long l2_miss;
    long long l2_gather_miss;
    long long l2_gather_hit;
    long long l2_write;
    long long l2_vec_write;
    long long l2_read;
    long long l2_vec_read;
    long long cache_fill;

    l2_stats& operator+=(const l2_stats& a);
};


struct ram_stats
{
    ram_stats()
        : dram_read(0),
          dram_write(0),
          reram_read(0),
          reram_write(0),
          row_buf_misses(0),
#if BANK_UTILIZATION_TRACKING
          row_buf_hits(0),
          busy_bank_cycles(0),
          extra_cycles_from_next_epoch(0) {}
#else
          row_buf_hits(0) {}
#endif

    long long dram_read;
    long long dram_write;
    long long reram_read;
    long long reram_write;
    long long row_buf_misses;
    long long row_buf_hits;
#if BANK_UTILIZATION_TRACKING
    long long busy_bank_cycles;
    long long extra_cycles_from_next_epoch;
#endif

    ram_stats& operator+=(const ram_stats& a);
};

struct ins_stats
{
    ins_stats()
        : read_single(0),
        gather(0),
        read_vector(0),
        write_single(0),
        scatter(0),
        write_vector(0),
        other(0) {}


    long long read_single;
    long long gather;
    long long read_vector;
    long long write_single;
    long long scatter;
    long long write_vector;
    long long other;

    ins_stats& operator+=(const ins_stats& a);
};

class Stats
{
public:
  static Stats* get();

  //Latency Related Functions/Variables
  unsigned set_start(packet_type type, long long cycle, int x, int y, uint64 addr, int vector, uint64 PC);
  void set_to_L2(unsigned id, long long cycle, int x, int y);
  void set_to_controller(unsigned id, long long cycle, int x, int y);
  void set_to_bank(unsigned id, long long cycle, int x, int y);
  void set_from_bank(unsigned id, long long cycle, int x, int y);
  void set_from_controller(unsigned id, long long cycle, int x, int y);
  void set_from_L2(unsigned id, long long cycle, int hit, int x, int y);
  void set_to_cpu(unsigned id, long long cycle, int x, int y);
  void set_l2_xy(unsigned id, int x, int y);

  int get_x(unsigned id);
  int get_y(unsigned id);
  int get_l2_x(unsigned id);
  int get_l2_y(unsigned id);
  uint64 get_addr(unsigned id);
  uint64 get_PC(unsigned id);

  void print_distrib();

  long long sent_packets;

  void collect_stats();

  double avg_latency() { return (total_latency.tot_latency / total_latency.tot_packets); }
  double avg_in_noc_latency() { return (total_latency.in_noc_latency / (total_latency.in_noc_packets)); } //in bound latency
  double avg_out_noc_latency() { return (total_latency.out_noc_latency / total_latency.out_noc_packets); }  //out bound latency
  double avg_to_dram_noc_latency() { return (total_latency.to_dram_latency / total_latency.controller_packets); }  //to dram latency
  double avg_from_dram_noc_latency() { return (total_latency.from_dram_latency / total_latency.from_dram_packets); }  //from dram latency
  double avg_noc_latency() { return (total_latency.in_noc_latency + total_latency.out_noc_latency + total_latency.to_dram_latency + total_latency.from_dram_latency) / (total_latency.in_noc_packets); }  //average network latency
  double avg_ram_latency() { return (total_latency.ram_latency / total_latency.ram_packets); }
  double avg_out_queue_latency() { return (total_latency.out_queue_latency / total_latency.out_queue_packets); }
  double avg_in_queue_latency() { return (total_latency.in_queue_latency / total_latency.in_queue_packets); }
  double avg_l2_latency() { return (total_latency.l2_latency / total_latency.from_L2_packets); }

  double cur_avg_latency() { return (latest_latency.tot_latency / latest_latency.tot_packets); }
  double cur_avg_in_noc_latency() { return (latest_latency.in_noc_latency / (latest_latency.in_noc_packets)); } //in bound latency
  double cur_avg_out_noc_latency() { return (latest_latency.out_noc_latency / latest_latency.out_noc_packets); }  //out bound latency
  double cur_avg_to_dram_noc_latency() { return (latest_latency.to_dram_latency / latest_latency.controller_packets); }  //to dram latency
  double cur_avg_from_dram_noc_latency() { return (latest_latency.from_dram_latency / latest_latency.from_dram_packets); }  //from dram latency
  double cur_avg_noc_latency() { return (latest_latency.in_noc_latency + latest_latency.out_noc_latency + latest_latency.to_dram_latency + latest_latency.from_dram_latency) / (latest_latency.in_noc_packets); }  //average network latency
  double cur_avg_ram_latency() { return (latest_latency.ram_latency / latest_latency.ram_packets); }
  double cur_avg_out_queue_latency() { return (latest_latency.out_queue_latency / latest_latency.out_queue_packets); }
  double cur_avg_in_queue_latency() { return (latest_latency.in_queue_latency / latest_latency.in_queue_packets); }
  double cur_avg_l2_latency() { return (latest_latency.l2_latency / latest_latency.from_L2_packets); }

  //Latency
  latency* per_tile_latency;
  latency total_latency;
  latency latest_latency;

  //Traffic Gen
  cpu_stats* tile_cpu_stats;
  cpu_stats latest_cpu_stats;
  cpu_stats total_cpu_stats;

  //L1
  l1_stats* tile_l1_stats;
  l1_stats latest_l1_stats;
  l1_stats total_l1_stats;

  //L2
  l2_stats* tile_l2_stats;
  l2_stats latest_l2_stats;
  l2_stats total_l2_stats;

  //Router
  long long *tile_flit_jumps;
  long long latest_flit_jumps;
  long long total_flit_jumps;

  //RAM
  ram_stats* tile_ram_stats;
  ram_stats latest_ram_stats;
  ram_stats total_ram_stats;

  //DRAMSim
  dramsim3::noc_sim_dram_stats latest_dram_stats;
  dramsim3::noc_sim_dram_stats new_total_dram_stats;
  dramsim3::noc_sim_dram_stats total_dram_stats;

  //Instruction Types
  ins_stats total_ins_stats;


#if NOC_SIM_SNIPER
  int num_frontend_barriers;
  int frontend_barriers() { return num_frontend_barriers; }
#endif

  //Power Related Functions/Variables

  int max_banks;
  long long ***bank_profile;
  void bank_profile_incr(int x_coord, int y_coord, int bank);

  //Queue Fill Related Variables/Functions
  void set_queue(bool full, int threadID);
  void set_trace_done();
  bool check_trace_done() { return trace_done; };
#if NOC_SIM_SNIPER
  /* DY:  Leave fastfwd_mode */
  void set_detailed_mode();
  bool in_fastfwd();
  bool queues_full();
  bool queues_empty_for_bar();
  bool queues_empty();

  /* DY:  tracking of scheduling status from front-end */
  void set_running(bool running, int threadID);
  void incr_frontend_barriers() { num_frontend_barriers++; }
#else
  void set_queue_watermark(bool full, int threadID);
  bool any_empty_threads();
  bool any_under_watemark_threads();
#endif

#if OUTPUT_LOG
  void printLog(string s, bool t2);
#endif
  
  int map_size(){return map->size();};

private:
  Stats();
  static Stats *instance;
  std::mutex map_barrier;
  //std::unordered_map<unsigned, packets> *map;
  std::map<unsigned, packets> *map;
  unsigned packet_id;

  Conf_ReRAM *conf;

  std::vector<long> ram_latency_distrib;
  std::vector<long> tot_latency_distrib;
  void add_ram_latency(long long latency);
  void add_tot_latency(long long latency);

  //Queue Fill Related Variables/Functions
  std::mutex barrier;
  bool *threads;
  bool trace_done;
#if NOC_SIM_SNIPER
  bool full_queues;
  bool empty_queues;
  bool *threads_running;
  bool check_empty_queues();
  bool check_full_queues();
  bool fastfwd_mode;
#else
  bool *watermark;
  bool any_empty;
  bool check_any_empty();
  bool any_under_watemark;
  bool check_any_under_watemark();
#endif

#if OUTPUT_LOG
  //log
  std::mutex logMutex;
  ofstream log;  
  ofstream inLog;
#endif

};
