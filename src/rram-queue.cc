
/*
  rram-queue.cc

  Candace Walden
  21 August 2017

  RRAM queue class.  Queues are on input channels to sub-arrays.
 */

#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"


rram_queue :: rram_queue(int queue_size)
{
  the_queue = (uint64 *)malloc(sizeof(uint64) * queue_size);
  size = queue_size;
  head = tail = 0;
  routed = FALSE;
}


int rram_queue :: is_empty()
{
  if (head == tail)
    return TRUE;
  else
    return FALSE;
}

int rram_queue :: is_full()
{
  if (((tail+1) % size) == head)
    return TRUE;
  else
    return FALSE;
}

/* returns the number of free entries in the queue */
int rram_queue :: free_entries()
{
  if (head == tail) 
    return (size - 1);

  if (head > tail)
    return (head - tail - 1);

  /* otherwise tail > head */
  return ((size - tail - 1) + head);
}

/* returns the number of entries in the queue */
int rram_queue :: entries()
{
  int free = free_entries();

  return (size - free);
}


/* returns true if there is a subarray header flit at queue head */
int rram_queue :: is_routable()
{
  if (this->is_empty()) return FALSE;
  if (routed) return FALSE;

  if (the_queue[head] & HEADER_A_FLIT_MASK)
    return TRUE;
  else
    return FALSE;
}


void rram_queue :: set_routed()
{
  routed = TRUE;
}

void rram_queue :: clear_routed()
{
  routed = FALSE;
}

int rram_queue :: is_routed()
{
  return routed;
}


uint64 rram_queue :: remove_from_head()
{
  uint64 flit;

  flit = the_queue[head];
  head = (head + 1) % size;

  return(flit);
}

uint64 rram_queue :: peak_at_head()
{
  return the_queue[head];
}

void rram_queue :: insert_at_tail(uint64 flit)
{
  the_queue[tail] = flit;
  tail = (tail + 1) % size;
}
