#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_bank::L2cache_bank(int x, int y)
{
    conf = Conf_ReRAM::get();
    stats = Stats::get();

    int buffer_size = conf->buffer_size;
    input = new buffer(buffer_size);
    output = nullptr;

    x_coord = x;
    y_coord = y;
    x_extent = conf->x_max;
    y_extent = conf->y_max;

    input_req = nullptr;
    out_req = nullptr;

    //make rob
    request_rob = list<mem_request>();

    //make wb queue
    wb_queue = std::list<wb_entry>();

    //make output queue
    output_queue = std::queue<mem_request>();

    noc_read_state = sHeader;
    noc_write_state = sHeader;

    //make cache array
    cache_array = new L2cache_array(x, y);

    //make mshrs
    if (conf->l2_settings.banked_mshr)
    {
        mshrs = new list<mshr_entry>[conf->l2_settings.n_subbanks];
        for (unsigned i = 0; i < conf->l2_settings.n_subbanks; i++)
        {
            mshrs[i] = list<mshr_entry>();
        }
        max_mshrs = conf->l2_settings.banked_mshr_entries;
    }
    else
    {
        mshrs = new list<mshr_entry>();
        max_mshrs = conf->l2_settings.unified_mshr_entries;
    }
}

//Gather requests from the network and add to ROB
void L2cache_bank::route_in(long long cur_cycle)
{
    uint64 address;
    int vector;

    //take as many packets as channel is wide 
    //(this isn't perfect, but should be reasonable)
    for (int i = 0; i < conf->phits; i++)
    {
        if (input->is_empty()) return; //nothing to read

        uint64 flit = input->remove_from_head();

        if (noc_read_state == sHeader)
        {
            input_req = new mem_request(PACKET_ID(flit));
            vector = flit & BYTES_MASK;
            //if (conf->ram_mode == dram)  //TODO fix for actual DRAM vectors
            //    vector = 1;

            if (flit & READ_FLIT_MASK)
            {
                input_req->type = (vector == 1) ? read_block : read_dw;

                if (conf->ram_mode == dram && input_req->type == read_dw)
                    input_req->dw_count = conf->dram_settings.fetch_width;
                else if (conf->ram_mode == dram)
                    input_req->dw_count = conf->dram_settings.fetch_width * conf->dram_settings.vector_size;
                else if (input_req->type == read_dw)
                    input_req->dw_count = conf->reram_settings.fetch_width;
                else
                    input_req->dw_count = conf->reram_settings.fetch_width * conf->reram_settings.vector_size;

                noc_read_state = sReadAddr;
            }
            else if (flit & WRITE_FLIT_MASK)
            {
                input_req->type = (vector == 1) ? write_block : write_dw;
                noc_read_state = sWriteAddr;
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Unknown packet " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (noc_read_state == sReadAddr)
        {
            if (flit & TAIL_FLIT_MASK)
            {
                address = flit & (~TAIL_FLIT_MASK);
                input_req->addr = address;
                input_req->bank = BANK_ID(address);
                input_req->subbank = SUBBANK_ID(address);
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Expected address and tail for read packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (noc_read_state == sWriteAddr)
        {
            address = flit;
            input_req->addr = address;
            input_req->bank = BANK_ID(address);
            input_req->subbank = SUBBANK_ID(address);
            noc_read_state = sData;
        }
        else  //sData
        {
            input_req->dirty[input_req->dw_count++] = true;

            //TODO: Currently we assume in heterogenous systems all requests are initially to ReRAM -- if we change that assumption, this needs to be updated
            unsigned fetch_width;
            if (conf->ram_mode == dram && input_req->type == write_dw)
                fetch_width = conf->dram_settings.fetch_width;
            else if (conf->ram_mode == dram)
                fetch_width = conf->dram_settings.fetch_width * conf->dram_settings.vector_size;
            else if(input_req->type == write_dw)
                fetch_width = conf->reram_settings.fetch_width;
            else
                fetch_width = conf->reram_settings.fetch_width * conf->reram_settings.vector_size;

            if (input_req->dw_count > fetch_width)
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Too many data flits for write packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }

        if (flit & TAIL_FLIT_MASK)
        {

            stats->set_to_L2(input_req->packet_id, cur_cycle, x_coord, y_coord);
            stats->set_l2_xy(input_req->packet_id, x_coord, y_coord);

            //add input_req to ROB
            request_rob.push_back(*input_req);

            //copy created -- delete original
            delete input_req;

            //start over
            noc_read_state = sHeader;

#if L2_OUT
            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): L2 received packet : " << incomplete_req->packet_id << "\n";
            Stats::get()->printLog(msg.str(), false);
#endif

            break;
        }
    }
}

//send any needed read replies
void L2cache_bank::route_out(long long cur_cycle)
{
    uint64 flit;

    //no output to send
    if (output_queue.empty()) return;

    while (!output->is_full())
    {
        //start with the header
        if (noc_write_state == sHeader)
        {
            out_req = &output_queue.front();
            //send the header flit for the last read request
            flit = get_delta_flit(out_req->packet_id) | REPLY_FLIT_MASK;
            output->insert_at_tail(flit);
            noc_write_state = sData;
        }
        //output the data
        else if (noc_write_state == sData)
        {
            if (out_req->dw_count > 1)
            {
                flit = 0;                      //64 bits of data
                output->insert_at_tail(flit);
                out_req->dw_count--;
            }
            else //tail packet
            {
                flit = 0 | TAIL_FLIT_MASK;    //64 bits of data
                output->insert_at_tail(flit);

                noc_write_state = sHeader;

                //mo for hits, o for misses (though they may hit in the MSHR)
                stats->set_from_L2(out_req->packet_id, cur_cycle, out_req->cur_stage == mo, x_coord, y_coord);

                //finished this output
                output_queue.pop();
                break;
            }
        }
    }
}

//send request into the pipeline
mem_request* L2cache_bank::next_request()
{

    //no cache requests to decode
    if (request_rob.empty())
    {
        return nullptr;
    }

    mem_request* first_req = new mem_request(request_rob.front());

    if (conf->l2_settings.banked_mshr)
    {
        //find entry with non-blocked subbank
        if (mshrs[first_req->subbank].size() < max_mshrs || first_req->valid_data)
        {
            //remove the request from the controllers list
            request_rob.pop_front();
            return first_req;
        }
        else  //need to iterate through requests
        {
            delete first_req;

            std::list<mem_request>::iterator req = request_rob.begin();
            //skip the first element
            ++req;
            //loop control variable initialized to second element
            for (; req != request_rob.end(); ++req)
            {
                if (mshrs[req->subbank].size() < max_mshrs)
                {
                    first_req = new mem_request(*req);

                    //remove request from the controllers list
                    request_rob.erase(req);
                    return first_req;
                }
            }
        }
    }
    else if (mshrs->size() < max_mshrs || first_req->valid_data)
    {
        //remove the request from the controllers list
        request_rob.pop_front();
        return first_req;
    }

    delete first_req;

    return nullptr;
}

bool L2cache_bank::tag_lookup(mem_request* req)
{
    //lookup tag
    bool valid, dirty;

    tag_res tag = cache_array->tag_look_up(req->addr);
    dirty = tag.evict_dirty;

    req->dirty_data = dirty;

    valid = tag.hit;  //line exists which is good enough for write, but not read
    if (valid && req->type == read_dw)  //check if sector is there
    {
        valid = tag.valid[SECTOR_ID_ADDR(req->addr)];
    }
    else if (valid && req->type == read_block)  //check if all sectors are there
    {
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
            valid &= tag.valid[i];
    }

    if (valid)
    {
        stats->tile_l2_stats[IND(x_coord, y_coord)].l2_hit++;
        if (req->type == read_dw)
            stats->tile_l2_stats[IND(x_coord, y_coord)].l2_gather_hit++;
    }
    else
    {
        stats->tile_l2_stats[IND(x_coord, y_coord)].l2_miss++;
        if (req->type == read_dw)
            stats->tile_l2_stats[IND(x_coord, y_coord)].l2_gather_miss++;
    }

    return valid;
}

mshr_entry* L2cache_bank::check_mshrs(mem_request* req)
{
    list<mshr_entry>* mshr_list;
    uint64 addr = req->addr;

    if (conf->l2_settings.banked_mshr)
    {
        mshr_list = &mshrs[req->subbank];
    }
    else
    {
        mshr_list = mshrs;
    }

    addr = addr >> conf->mshr_tag_offset;

    for (std::list<mshr_entry>::reverse_iterator mshr = mshr_list->rbegin(); mshr != mshr_list->rend(); ++mshr)
    {
        if (mshr->addr == addr && mshr->subbank == req->subbank)
        {
            req->mshr = &*mshr;
            return &*mshr;
        }
    }

    return nullptr;
}

mshr_entry* L2cache_bank::handle_mshr_miss(mem_request* req)
{
    list<mshr_entry>* mshr_list;
    unsigned mshr_entries;
    mshr_entry new_entry;

    if (conf->l2_settings.banked_mshr)
    {
        mshr_list = &mshrs[req->subbank];
        mshr_entries = conf->l2_settings.banked_mshr_entries;
    }
    else
    {
        mshr_list = mshrs;
        mshr_entries = conf->l2_settings.unified_mshr_entries;
    }

    if (mshrs->size() > mshr_entries)
    {
        //TODO: stall until an entry is freed
        //more complex if banked - can handle requests for certain banks
        //for now do the braindead thing of just stalling 1 cycle at a time
        //it's currently handled in send_request 
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; out of MSHRs.";
        throw std::invalid_argument(errMsg.str());
    }

    //make MSHR
    new_entry = mshr_entry(req, req->addr >> conf->mshr_tag_offset);
    new_entry.read_sub = new read_subentry[conf->l2_settings.read_subentries];

    mshr_list->push_back(new_entry);
    req->mshr = &*mshr_list->rbegin();
    return &*mshr_list->rbegin();
}

//Add subentry
void L2cache_bank::add_read_subentry(mem_request* req)
{
    mshr_entry* mshr = req->mshr;
    unsigned ind = mshr->read_subentries;

    if (ind >= conf->l2_settings.read_subentries)
    {
        //make new mshr
        mshr = handle_mshr_miss(req);
        req->mshr = mshr;
        ind = mshr->read_subentries;
    }

    mshr->read_sub[ind] = read_subentry(req);
    mshr->read_subentries++;

    req->read_sub = &mshr->read_sub[ind];

    if (req->type == read_block)
    {
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            if (mshr->sectors_recv[i] == unneeded_req)
            {
                mshr->sectors_recv[i] = pending_req;
                mshr->data_to_recv++;
            }
        }
    }
    else if (req->type == read_dw)
    {
        unsigned sector = SECTOR_ID_ADDR(req->addr);
        mshr->read_sub[ind].sector = sector;

        if (mshr->sectors_recv[sector] == unneeded_req)
        {
            mshr->sectors_recv[sector] = pending_req;
            mshr->data_to_recv++;
        }
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Must be read request to make read subentry.";
        throw std::invalid_argument(errMsg.str());
    }
}

//Add subentry and return if it is completed
bool L2cache_bank::check_read_valid(mem_request* req)
{
    bool complete = true;
    mshr_entry* mshr = req->mshr;
    unsigned sector = SECTOR_ID_ADDR(req->addr);


    if (req->type == read_block)
    {
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            //if some piece of data isn't already there, it isn't complete
            complete &= mshr->valid[i];
        }
    }
    else if (req->type == read_dw)
    {
        complete = mshr->valid[sector];
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Must be read request to check valid.";
        throw std::invalid_argument(errMsg.str());
    }

    return complete;
}

void L2cache_bank::update_mshr(mem_request* req)
{
    mshr_entry* mshr = req->mshr;

    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
    {
        if (req->valid[i])
        {
            mshr->valid[i] = true;
            if (mshr->sectors_recv[i] == sent_req)
            {
                mshr->sectors_recv[i] = recv_req;
                mshr->data_to_recv--;
            }
        }
    }
}

void L2cache_bank::write_mshr(mem_request* req)
{
    mshr_entry* mshr = req->mshr;

    if (req->type == write_block)
    {
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            mshr->dirty[i] = true;
            mshr->valid[i] = true;
        }
    }
    else if (req->type == write_dw)
    {
        unsigned sector = SECTOR_ID_ADDR(req->addr);
        mshr->dirty[sector] = true;
        mshr->valid[sector] = true;
    }
}

//check MSHR completed
bool L2cache_bank::check_mshr_complete(mem_request* req)
{
    mshr_entry* mshr = req->mshr;

    //if we're waiting to send or receive a request, we aren't done
    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        if (mshr->sectors_recv[i] == pending_req || mshr->sectors_recv[i] == sent_req)
            return false;

    return true;
}

void L2cache_bank::delete_mshr(mshr_entry* mshr, unsigned subbank)
{
    mshr->done = true;

    //done with the whole MSHR, delete it from the list
    if (conf->l2_settings.banked_mshr)
    {
        mshrs[subbank].remove_if(mshr_done);
    }
    else
    {
        mshrs->remove_if(mshr_done);
    }
}

bool mshr_done(const mshr_entry& val)
{
    return val.done;
}

mshr_entry* L2cache_bank::find_orignal_mshr(mem_request* req)
{
    list<mshr_entry>* mshr_list;
    uint64 addr = req->addr;

    if (conf->l2_settings.banked_mshr)
    {
        mshr_list = &mshrs[req->subbank];
    }
    else
    {
        mshr_list = mshrs;
    }

    addr = addr >> conf->mshr_tag_offset;

    for (std::list<mshr_entry>::iterator mshr = mshr_list->begin(); mshr != mshr_list->end(); ++mshr)
    {
        if (mshr->addr == addr && mshr->subbank == req->subbank)
        {
            //find read subentry with correct packet id
            for (unsigned i = 0; i < mshr->read_subentries; i++)
            {
                if (mshr->read_sub[i].packet_id == req->packet_id)
                {
                    req->mshr = &*mshr;
                    return &*mshr;
                }
            }
        }
    }

    return nullptr;
}

uint64 L2cache_bank::get_delta_flit(unsigned packet_id)
{
    int delta_x, delta_y;

    delta_x = stats->get_x(packet_id);
    delta_y = stats->get_y(packet_id);

    delta_x = delta_x - x_coord;
    delta_y = delta_y - y_coord;

    return get_delta_flit(packet_id, delta_x, delta_y);
}

uint64 L2cache_bank::get_delta_flit(unsigned packet_id, int delta_x, int delta_y)
{
    uint64 flit, header_flit;

    flit = 0;
    if (delta_x < 0) {
        flit = DIRECTION_X_MASK >> X_OFFSET;
        delta_x = -delta_x;
    }
    flit = flit | delta_x;
    header_flit = flit << X_OFFSET;

    flit = 0;
    if (delta_y < 0) {
        flit = DIRECTION_Y_MASK >> Y_OFFSET;
        delta_y = -delta_y;
    }
    flit = flit | delta_y;
    header_flit = header_flit | (flit << Y_OFFSET);

    header_flit |= ((uint64)packet_id) << PACKET_OFFSET;

    return header_flit;
}


void L2cache_bank::address_translation(uint64 addr, int* delta_x, int* delta_y)
{
    if (conf->ram_mode == dram || (conf->ram_mode == het_l2_reram && ((addr & DRAM_ADDRESS_MASK) != 0)))
    {
        if (conf->addr_mode == random_addr) 
        {
            get_random_dAddress(delta_x, delta_y);
        }
        else if (conf->addr_mode == local_addr)
        {
            get_local_dAddress(delta_x, delta_y);
        }
        else
        {
            get_mapped_dAddress(addr, delta_x, delta_y);
        }
    }
    else
    {
        if (conf->addr_mode == random_addr)
        {
            get_random_rAddress(delta_x, delta_y);
        }
        else if (conf->addr_mode == local_addr)
        {
            get_local_rAddress(delta_x, delta_y);
        }
        else
        {
            get_mapped_rAddress(addr, delta_x, delta_y);
        }
    }
}


void L2cache_bank::get_random_dAddress(int* delta_x, int* delta_y)
{
    int tile = random() % (conf->num_dram_tiles);
    int x, y;

    conf->nth_tile_type(tile, dram_tile, &x, &y);
    *delta_x = x - x_coord;
    *delta_y = y - y_coord;
}

void L2cache_bank::get_local_dAddress(int* delta_x, int* delta_y) {
    int x, y;

    conf->local_tile(x_coord, y_coord, dram_tile, &x, &y);
    *delta_x = x - x_coord;
    *delta_y = y - y_coord;
}

void L2cache_bank::get_mapped_dAddress(uint64 addr, int* delta_x, int* delta_y) {
    int x, y, tile;

    //addr = addr & conf->channel_bits; //take only the channel bits
    //addr = addr >> conf->channel_offset;  //shift out the offset to get just channel
    //tile = addr % conf->num_dram_tiles; //should be unnecessary, but if config is done wrong, maybe not

    addr = addr << WORD_SIZE_OFFSET;
    dramsim3::Address mapped = conf->dram_conf->AddressMapping(addr);
    tile = mapped.channel;

    conf->nth_tile_type(tile, dram_tile, &x, &y);
    *delta_x = x - x_coord;
    *delta_y = y - y_coord;
}

void L2cache_bank::get_random_rAddress(int* delta_x, int* delta_y)
{
    if (conf->ram_mode == l2_cpu)
    {
        int value = random() % conf->cpu_tiles;
        int x, y;

        conf->nth_tile_type(value, reram_tile, &x, &y);

        *delta_x = x - x_coord;
        *delta_y = y - y_coord;
    }
    else
    {
        *delta_x = 0;
        *delta_y = 0;
    }
}

void L2cache_bank::get_local_rAddress(int* delta_x, int* delta_y) 
{
    //find closest reram controller
    if (conf->ram_mode == l2_cpu)
    {
        int x, y;
        conf->local_tile(x_coord, y_coord, reram_tile, &x, &y);
        *delta_x = x - x_coord;
        *delta_y = y - y_coord;
    }
    else  //have ReRAM on L2 tile
    {
        *delta_x = 0;
        *delta_y = 0;
    }
}

/*
Given a 64-bit byte address, maps to a particular bank and updates the values of delta_x, delta_y
*/
void L2cache_bank::get_mapped_rAddress(uint64 addr, int* delta_x, int* delta_y) 
{
    int x, y, n;

    n = TILE_ID(addr);

    //L2 without ReRAM
    if (conf->ram_mode == l2_cpu)
    {
        conf->nth_tile_type(n, reram_tile, &x, &y);
        *delta_x = x - x_coord;
        *delta_y = y - y_coord;
    }
    else //L2 with ReRAM
    {
        *delta_x = 0;
        *delta_y = 0;
    }
}

mshr_entry::mshr_entry(const mshr_entry& mshr)
{
    done = mshr.done;
    addr = mshr.addr;
    subbank = mshr.subbank;
    data_to_recv = mshr.data_to_recv;
    read_subentries = mshr.read_subentries;

    read_sub = new read_subentry[Conf_ReRAM::get()->l2_settings.read_subentries];
    for(unsigned i = 0; i < read_subentries; i++)
    {
        read_sub[i] = mshr.read_sub[i];
    }

    for(int i = 0; i < 8; i++)
    {
        sectors_recv[i] = mshr.sectors_recv[i];
        valid[i] = mshr.valid[i];
        dirty[i] = mshr.dirty[i];
    }
}

mshr_entry::mshr_entry(mem_request* mem_req, uint64 address)
    : done(false),
    addr(address),
    subbank(mem_req->subbank),
    data_to_recv(0),
    sectors_recv(),
    read_subentries(0),
    read_sub(nullptr),
    valid(),
    dirty()
{
    unsigned sectors = Conf_ReRAM::get()->l2_settings.n_sectors;
    for (unsigned i = 0; i < sectors; i++)
    {
        valid[i] = mem_req->valid[i];  //may have valid data already in cache, helps with vectors
        dirty[i] = false;
        sectors_recv[i] = mem_req->valid[i] ? recv_req : unneeded_req;  //if valid, count as received
    }
    for (unsigned i = sectors; i < 8; i++)
    {
        valid[i] = true;  //may have valid data already in cache, helps with vectors
        dirty[i] = false;
        sectors_recv[i] = unneeded_req;  //if valid, count as received
    }
}

mshr_entry::~mshr_entry()
{
    //done with the read subentries, delete them
    delete[] read_sub;
    done = true;
}

