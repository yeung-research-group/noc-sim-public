#!/bin/bash

mkfifo buf

./sde/sde64 -debugtrace -dt_lines 1 -dt_filter_rtn Pth_daxpy -dt_out buf -- ./bench/daxpy_example/daxpy_vectorized/daxpy_pthread 256 &
./noc-sim buf 

rm buf