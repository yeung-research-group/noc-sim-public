#pragma once

class mem_trace
{
public:
  mem_trace(traffic_generator ***my_CORE);
  void extractMemory(std::string inputfile);

//#ifdef MEM_LEAK_DEBUG
  uint64_t * ins_count;
//#endif

private:
  traffic_generator ***core;
  void readReq(std::stringstream *req, int ins, int threadID);
  void writeReq(std::stringstream *req, int ins, int threadID);
  void insert_instruction(int threadID, instruction* ins);
  void lockReq(int ins, int threadID);

  instruction ***reg_refs;
  Conf_ReRAM* conf;
  Stats* stats;

  int **thread_mask; //Keeps tracks of vector mask values for each thread
                     //to pass it to instructions.
  int max_mask_id;
};


