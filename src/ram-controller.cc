
/*
  ram-controller.hh

  Candace Walden
  01 September 2019

  RAM Controller: Base class for either DRAM or RRAM controller
 */


#include <stdio.h>
#include <stdlib.h>
 //#include <unistd.h> 
#include <iostream>
#include "noc-sim.hh"


ram_controller::ram_controller(int x, int y)
{
    x_coord = x;
    y_coord = y;

    conf = Conf_ReRAM::get();
    stats = Stats::get();

    output = nullptr;
    output_req = nullptr;

    active_type = conf->get_tile_type(x, y);
    if (active_type == reram_tile || active_type == dram_tile)
    {
        settings = &((active_type == dram_tile) ? conf->dram_settings : conf->reram_settings);

        //Input/output
        input = new buffer(conf->buffer_size);
        input_req = nullptr;
        noc_read_state = sHeader;
        noc_write_state = sHeader;

        //work queues
        output_requests = std::queue<ram_request>();

#if BANK_CONFLICT_DETECTION
        bank_accesses = new conflict_data[settings->banks];
        for (int i = 0; i < settings->banks; i++)
            bank_accesses[i].valid = 0;
#endif
    }

}

#if BANK_CONFLICT_DETECTION
void ram_controller::bank_conflict_calc(long long cur_cycle, unsigned bank)
{
    if (bank_accesses[bank].valid) {
        if ((cur_cycle - bank_accesses[bank].cycle_issued) < 20) {
            //Whine here
            uint64 prev_packet_id = bank_accesses[bank].packet_id;
            int prev_x, prev_y, new_x, new_y;
            uint64 prev_PC, new_PC;
            prev_x = stats->get_x(prev_packet_id);
            prev_y = stats->get_y(prev_packet_id);
            prev_PC = stats->get_PC(prev_packet_id);

            new_x = stats->get_x(packet_id);
            new_y = stats->get_y(packet_id);
            new_PC = stats->get_PC(packet_id);
            if (x_coord == 0 && y_coord == 0 && cur_cycle >= 10000)
                printf("R controller - (%2d, %2d, %3d) prev_tile - (%2d,%2d,0x%llx), new_tile - (%2d,%2d,0x%llx) diff cycle - %d addr - (0x%llx, 0x%llx)\n", x_coord, y_coord, bank,
                    prev_x, prev_y, prev_PC, new_x, new_y, new_PC, (cur_cycle - bank_accesses[bank].cycle_issued), stats->get_addr(prev_packet_id), stats->get_addr(packet_id));
        }
    }
    bank_accesses[bank].valid = 1;
    bank_accesses[bank].packet_id = packet_id;
    bank_accesses[bank].cycle_issued = cur_cycle;
}
#endif

//send any needed read replies
void ram_controller::route_out(long long cur_cycle)
{
    uint64 flit;

    //no output to send
    if (output_requests.empty()) return;

    while (!output->is_full())
    {
        //start with the header
        if (noc_write_state == sHeader)
        {
            output_req = &output_requests.front();
            //send the header flit
            flit = get_delta_flit(output_req->packet_id) | REPLY_FLIT_MASK;
            if (output_req->type == read_block)
                flit |= 1;


            output->insert_at_tail(flit);
            noc_write_state = sReadAddr;
#if ROUTER_OUT
            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): moving ";
            msg << "0x" << setfill('0') << setw(16) << hex << flit << " to P in\n";
            Stats::get()->printLog(msg.str(), false);
#endif
#if RRAM_CONTR_OUT
            int delta_x, delta_y;
            delta_x = flit & DELTA_X_MASK;
            delta_y = flit & DELTA_Y_MASK;
            if (flit & DIRECTION_X_MASK) {
                delta_x = -delta_x;
            }
            if (flit & DIRECTION_Y_MASK) {
                delta_y = -delta_y;
            }

            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): RRAM controller sending reply packet_id : " << packet_id_out << "to (" << (x_coord + delta_x) << ", " << (y_coord + delta_y) << ")" << "\n";
            Stats::get()->printLog(msg.str(), false);
#endif
        }
        //send the address
        else if (noc_write_state == sReadAddr)
        {
            flit = output_req->addr;
            output->insert_at_tail(flit);
            noc_write_state = sData;
        }
        //output the data
        else if (noc_write_state == sData)
        {
            if (output_req->dw_count > 1)
            {
                flit = 0;                      //64 bits of data
                output->insert_at_tail(flit);
                output_req->dw_count--;
            }
            else //tail packet
            {
                flit = 0 | TAIL_FLIT_MASK;    //64 bits of data
                output->insert_at_tail(flit);

                noc_write_state = sHeader;

                stats->set_from_controller(output_req->packet_id, cur_cycle, x_coord, y_coord);

                output_requests.pop();

                //finished this output
                return;
            }
        }
    }

#if RRAM_CONTR_OUT
    //if there's no more room, stop sending packets
    if (output->is_full()) {
        std::ostringstream msg;
        msg << "(" << x_coord << ", " << y_coord << "): RRAM output buffer full! \n";
        Stats::get()->printLog(msg.str(), false);
    }
#endif
}

uint64 ram_controller::get_delta_flit(int packet_id)
{
    uint64 flit, header_flit;
    int delta_x, delta_y;
    Stats* stats = Stats::get();

    //two hop (L2 not tied to specific controller)
    if (active_type == dram_tile ||  //every dram tile
        conf->ram_mode == l2_cpu)  //reram tiles IF L2 is not on same tile
      //TODO: DRAM tiles in het_l2_reram? Just not giving dram tiles L2 for now, but something to think about.
    {
        delta_x = stats->get_l2_x(packet_id);
        delta_y = stats->get_l2_y(packet_id);
    }
    //one hop (L2 with controller)
    else
    {
        delta_x = stats->get_x(packet_id);
        delta_y = stats->get_y(packet_id);
    }


    delta_x = delta_x - x_coord;
    delta_y = delta_y - y_coord;

    flit = 0;
    if (delta_x < 0)
    {
        flit = DIRECTION_X_MASK >> X_OFFSET;
        delta_x = -delta_x;
    }
    flit = flit | delta_x;
    header_flit = flit << X_OFFSET;

    flit = 0;
    if (delta_y < 0)
    {
        flit = DIRECTION_Y_MASK >> Y_OFFSET;
        delta_y = -delta_y;
    }
    flit = flit | delta_y;
    header_flit = header_flit | (flit << Y_OFFSET);

    header_flit |= ((uint64)packet_id) << PACKET_OFFSET;

    return header_flit;
}

