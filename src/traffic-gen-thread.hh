#pragma once
/* 
  traffic-gen-thread.hh

  Devesh Singh
  03 Sep 2017

  Each thread is attached to a certain core and generates random traffic which is injected into NOC
 */
class queue_entry{
  private: 

  public:
    unsigned packet_id;
    bool inflight;
    uint64 addr;
};

class traffic_gen_thread {
  private:
      Conf_ReRAM* conf;

  public:

    queue_entry * wait_queue; //Not exactly a queue.Insertion at tail and removal from any location.
    int wq_head,wq_tail,wq_size;

    traffic_queue * queue;
    int trace_finished;

    traffic_gen_thread(int x_coord, int y_coord, int th_num);
    ~traffic_gen_thread();
    int thread_ready();
    int wq_delete_element(unsigned packet_id);
    void wq_insert_at_tail(unsigned packet_id, uint64 address);

    
};


