#pragma once
/*
  rram-controller.hh

  Donald Yeung
  25 July 2017

  Rram bank class simulates an end-point on the network that contains
  RRAM sub-arrays.
 */

class rram_controller : virtual public ram_controller
{
private:
  rram_bank **banks;

  //work queues
  std::priority_queue<schedule_item> bank_schedule;
  std::list<ram_request> request_rob;
  std::list<ram_request> vector_queue;

  void create_banks(int x, int y);

  void check_banks(long long cur_cycle);
  void send_next_request(long long cur_cycle, unsigned bank_id);
  bool vq_update(ram_request* req);

  void route_in(long long cur_cycle) override;

public:
  rram_controller(int x, int y);

  void cycle(long long cur_cycle) override;
  void collect_stats() override {};
  void PrintEpochStats() override {};
  void PrintFinalStats() override {};

};
