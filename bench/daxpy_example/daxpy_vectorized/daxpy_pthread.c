/* File:     
 *     pth_daxpy.c 
 *
 * Purpose:  
 *     Computes a DAXPY, which is an update y += alpha*x, where
 *     x and y are vectors, and alpha is a scalar.  This program
 *     uses a block distribution of the vectors.
 *
 * Input:
 *     n: dimension of vectors
 *     x, y: the vectors
 *     alpha:  the scalar
 *
 * Output:
 *     y: the vector y after the update
 *
 * Compile:  gcc -g -Wall -o pth_daxpy pth_daxpy.c -lpthread
 * Usage:
 *     pth_daxpy <thread_count>
 *
 * Notes:  
 *     1.  Local storage for x and y is dynamically allocated.
 *     2.  Distribution of x, and y is logical:  both are 
 *         globally shared.
 *     3.  n should be evenly divisible by thread_count.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
# include <float.h>
# include <immintrin.h>

/* Global variables */
int     thread_count;
int     n;
double* x;
double* y;
double  alpha;

/* Serial functions */
void Usage(char* prog_name);

/* Parallel function */
void *Pth_daxpy(void* rank);

/*------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
   long       thread;
   pthread_t* thread_handles;
   int i;

   if (argc != 2) Usage(argv[0]);
   thread_count = strtol(argv[1],NULL,10);
   thread_handles = malloc(thread_count*sizeof(pthread_t));

   //n = 8192 * 8 * thread_count; //divisible by 8 and thread count
   n = 128 * 32768 * 32 * 10;
   
   x = malloc(n*sizeof(double));
   y = malloc(n*sizeof(double));
   
   for(i=0; i<n;i++){
     x[i] = 1.0;
     y[i] = 2.0;
   }

   alpha = 3.0;

   for (thread = 1; thread < thread_count; thread++)
      pthread_create(&thread_handles[thread], NULL,
         Pth_daxpy, (void*) thread);

   Pth_daxpy(0);
   
   for (thread = 1; thread < thread_count; thread++)
      pthread_join(thread_handles[thread], NULL);

   free(x);
   free(y);
   free(thread_handles);

   return 0;
}  /* main */


/*------------------------------------------------------------------
 * Function:  Usage
 * Purpose:   print a message showing what the command line should
 *            be, and terminate
 * In arg :   prog_name
 */
void Usage (char* prog_name) {
   fprintf(stderr, "usage: %s <thread_count>\n", prog_name);
   exit(0);
}  /* Usage */



/*------------------------------------------------------------------
 * Function:          Pth_daxpy
 * Purpose:           Update a double precision vector y += alpha*x
 * In arg:            rank
 * Global in vars:    x, n, alpha, thread_count
 * Global in/out var: y
 */
void *Pth_daxpy(void* rank) {
   long my_rank = (long) rank;
   int local_n = n/thread_count;
   int my_first_i = my_rank*local_n;
   int my_last_i = my_first_i + local_n;
   int i;
   
   double* x_l = x;
   double* y_l = y;


   __m512d zmm0,zmm1,zmm2;
    zmm2 = _mm512_set1_pd(alpha);

   for (i = my_first_i; i < my_last_i; i += 8) {
    zmm0 = _mm512_loadu_pd(x_l+i);
    zmm1 = _mm512_loadu_pd(y_l+i);
    zmm0 = _mm512_mul_pd(zmm0,zmm2);
    zmm1 = _mm512_add_pd(zmm1,zmm0);
    _mm512_storeu_pd(y_l+i,zmm1);
   }

   return NULL;
}  /* Pth_mat_vect */
