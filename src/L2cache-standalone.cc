#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_standalone::L2cache_standalone(int x, int y) : L2cache_bank(x, y)
{
    mem_out = nullptr;

    ram_read_state = sHeader;
    ram_write_state = sHeader;

    ram_req = nullptr;
    out_ram_req = nullptr;
    out_wb = nullptr;
    ram_req_queue = std::queue<mem_request>();

    cycle_complete = 0;
    cur_req = nullptr;


    mem_in = new buffer(conf->buffer_size);
}

void L2cache_standalone::set_mem_out_buffer(buffer* out)
{
    mem_out = out;
}

void L2cache_standalone::cycle(long long cur_cycle)
{

    //route out and only proceed if output is cleared
    route_out(cur_cycle);
    send_ram_request(cur_cycle);
    if (output_queue.size() < conf->buffer_size)  //TODO: better size limit?
    {
        read_ram_reply(cur_cycle);
    }

    if (cycle_complete <= cur_cycle)
    {
        handle_request(cur_cycle);
    }

    //route and transfer packets from NOC
    if (request_rob.size() < conf->l2_settings.rob_entries)
    {
        route_in(cur_cycle);
    }
}

void L2cache_standalone::handle_request(long long cur_cycle)
{
    if (cur_req != nullptr)
    {
        if (cur_req->next_stage == rr_r)  //receiving ReRAM reply
        {
            //check if mshr complete
            if (check_mshr_complete(cur_req))
            {
                L2_cache_line* evicted = cache_array->tag_update(*cur_req);

                if (evicted != nullptr)
                {
                    writeback_req(evicted);
                }

                //send back any read replies
                mshr_entry* mshr = cur_req->mshr;
                while (cur_req->mshr->read_subentries > 0)
                {
                    output_queue.push(*mshr->read_sub[--mshr->read_subentries].req);
                    delete mshr->read_sub[mshr->read_subentries].req;
                }

                //done with the read subentries, delete them
                delete_mshr(mshr, cur_req->subbank);
            }

            delete cur_req;
        }
        else if (cur_req->type == read_block || cur_req->type == read_dw)
        {
            //done with the read
            if (cur_req->valid_data)
            {
                output_queue.push(*cur_req);
                delete cur_req;
            }
            else
            {
                //send reram requests
                if (cur_req->type == read_dw)
                {
                    //send to reram output queue if hasn't already been sent
                    if (cur_req->mshr->sectors_recv[SECTOR_ID_ADDR(cur_req->addr)] == pending_req)
                    {
                        cur_req->mshr->sectors_recv[SECTOR_ID_ADDR(cur_req->addr)] = sent_req;
                        ram_req_queue.push(*cur_req);
                    }
                }
                else
                {
                    bool pending = false;
                    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                    {
                        pending |= (cur_req->mshr->sectors_recv[i] == pending_req);
                    }

                    if (pending)
                    {
                        ram_req_queue.push(*cur_req);
                        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                        {
                            if(cur_req->mshr->sectors_recv[i] == pending_req)
                                cur_req->mshr->sectors_recv[i] = sent_req;
                        }
                    }
                }

                //delete cur_req;  //deletes from read subentries
            }
        }
        else  //write request
        {
            //update cache tags if its not in the MSHR file or in the cache already
            if (cur_req->valid_data)
            {
                L2_cache_line* evicted = cache_array->tag_update(*cur_req);

                if (evicted != nullptr)
                {
                    writeback_req(evicted);
                }
            }

            stats->set_from_L2(cur_req->packet_id, cur_cycle, 1, x_coord, y_coord);
            delete cur_req;
        }
    }

    //get next request and find time to do it
    cur_req = get_next_request();
    if (cur_req != nullptr)
    {
        if (cur_req->next_stage == rr_r)
        {
            cycle_complete = cur_cycle + 4;

            update_mshr(cur_req);
            if (check_mshr_complete(cur_req))
            {
                cycle_complete += 4 + cur_req->mshr->read_subentries;

                //cache fill
                tag_res tag = cache_array->tag_look_up(cur_req->addr);

                if(tag.evict_dirty)
                    cycle_complete += 2;
            }
        }
        else
        {
            cycle_complete = cur_cycle + 4;

            bool valid = tag_lookup(cur_req);
            bool dirty = cur_req->dirty_data;
            cur_req->valid_data = valid;

            if (!valid)
            {
                //do mshr stuff
                mshr_entry* hit = check_mshrs(cur_req);
                if (cur_req->type == write_dw || cur_req->type == write_block)
                {
                    if (!hit)
                    {
                        if (dirty)
                            cycle_complete += 3;
                        else
                            cycle_complete += 1;

                        cur_req->valid_data = true;
                    }
                    else
                    {
                        //cycle_complete += 2;
                        //write data into MSHR
                        write_mshr(cur_req);
                    }
                }
                else  //read
                {
                    if (!hit)
                    {
                        //read without hit, make new mshr
                        cur_req->mshr = handle_mshr_miss(cur_req);
                        //create Read Subentry
                        add_read_subentry(cur_req);
                    }
                    else  //hit
                    {
                        valid = check_read_valid(cur_req);
                        if (valid)
                        {
                            cur_req->valid_data = true;
                        }
                        else
                        {
                            //create Read Subentry
                            add_read_subentry(cur_req);
                        }
                    }
                }
            }
        }
    }
}


bool L2cache_standalone::read_ram_reply(long long cur_cycle)
{
    uint64 address;
    int vector;

    //take as many packets as channel is wide 
    //(this isn't perfect, but should be reasonable)
    for (int i = 0; i < conf->phits; i++)
    {
        if (mem_in->is_empty()) return true;

        uint64 flit = mem_in->remove_from_head();

        if (ram_read_state == sHeader)
        {
            ram_req = new mem_request(PACKET_ID(flit));
            vector = flit & BYTES_MASK;
            //if (conf->ram_mode == dram)
            //    vector = 1;

            if (flit & REPLY_FLIT_MASK)
            {
                ram_req->type = (vector == 1) ? read_block : read_dw;
                ram_req->dw_count = 0;
                ram_read_state = sReadAddr;
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Unknown packet " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }
        else if (ram_read_state == sReadAddr)
        {
            address = flit & (~TAIL_FLIT_MASK);
            ram_req->addr = address;
            ram_req->bank = BANK_ID(address);
            ram_req->subbank = SUBBANK_ID(address);
            ram_read_state = sData;
            ram_req->valid_data = true;

            if (ram_req->type == read_block)
            {
                for (unsigned j = 0; j < conf->l2_settings.n_sectors; j++)
                {
                    ram_req->valid[j] = true;
                }
            }
            else
            {
                ram_req->valid[SECTOR_ID_ADDR(ram_req->addr)] = true;
            }

            //find mshr
            if(find_orignal_mshr(ram_req) == nullptr)
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot find MSHR.";
                throw std::invalid_argument(errMsg.str());
            }
        }
        else  //sData
        {
            unsigned fetch_width;
            if (ram_req->type == read_block)
            {
                if (conf->ram_mode == dram)
                    fetch_width = conf->dram_settings.vector_size * conf->dram_settings.fetch_width;
                else
                    fetch_width = conf->reram_settings.vector_size * conf->reram_settings.fetch_width;
            }
            else
            {
                if (conf->ram_mode == dram)
                    fetch_width = conf->dram_settings.fetch_width;
                else
                    fetch_width = conf->reram_settings.fetch_width;
            }

            if (ram_req->dw_count > fetch_width)
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Too many data flits for reply packet: " << flit;
                throw std::invalid_argument(errMsg.str());
            }
        }

        if (flit & TAIL_FLIT_MASK)
        {
            //send to request queue
            ram_req->next_stage = rr_r;
            ram_return_req.push(*ram_req);

            //start over
            ram_read_state = sHeader;
            delete ram_req;

#if L2_OUT
            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): L2 received reply packet : " << incomplete_req->packet_id << "\n";
            Stats::get()->printLog(msg.str(), false);
#endif

            return true;
        }

    }

    return false;
}

void L2cache_standalone::writeback_req(L2_cache_line * evicted)
{
    wb_entry wb;
    bool wb_vec_check = true;

    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
    {
        wb_vec_check &= evicted->valid[i];
    }

    unsigned fetch_width;
    if (wb_vec_check && conf->vector_cache_block_fetch)
    {
        if (conf->ram_mode == dram)
            fetch_width = conf->dram_settings.vector_size * conf->dram_settings.fetch_width;
        else
            fetch_width = conf->reram_settings.vector_size * conf->reram_settings.fetch_width;
    }
    else
    {
        if (conf->ram_mode == dram)
            fetch_width = conf->dram_settings.fetch_width;
        else
            fetch_width = conf->reram_settings.fetch_width;
    }

    if (wb_vec_check && conf->vector_cache_block_fetch)
    {
        // wb tag is just addr without sector shifted out, so shift back to get block addr
        wb = { (evicted->tag) << conf->l2_settings.index_mask_offset, fetch_width };
        wb_queue.push_back(wb);
        stats->tile_l2_stats[IND(x_coord, y_coord)].vec_wb_from_l2++;
    }
    else
    {
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            if (evicted->dirty[i])
            {
                //wb tag is just addr without sector, so add back to get word address
                wb = { (evicted->tag) << conf->l2_settings.index_mask_offset | ((uint64)i << conf->l2_settings.sector_mask_offset) , fetch_width };
                stats->tile_l2_stats[IND(x_coord, y_coord)].wb_from_l2++;
                wb_queue.push_back(wb);
            }
        }
    }

    delete evicted;
}


//send any needed ram requests
bool L2cache_standalone::send_ram_request(long long cur_cycle)
{
    uint64 flit;

    while (!mem_out->is_full() &&
           (!ram_req_queue.empty() || !wb_queue.empty()))
    {
        //start with the header
        if (ram_write_state == sHeader)
        {
            int delta_x, delta_y;
            if (ram_req_queue.empty())
            {
                //do writeback
                out_wb = &wb_queue.front();

                bool vector = (conf->ram_mode == dram) ? out_wb->data_flits > conf->dram_settings.fetch_width : out_wb->data_flits > conf->reram_settings.fetch_width;
                unsigned packet_id = stats->set_start(write_packet, cur_cycle, x_coord, y_coord, out_wb->addr, vector, -1);
                stats->set_to_L2(packet_id, cur_cycle, x_coord, y_coord);
                address_translation(out_wb->addr, &delta_x, &delta_y);

                //send the header flit for the last wb request
                flit = get_delta_flit(packet_id, delta_x, delta_y) | WRITE_FLIT_MASK;
                if(vector)
                    flit |= 1;  //mark as vector

                ram_write_state = sWriteAddr;
            }
            else
            {
                //do read request
                out_ram_req = &ram_req_queue.front();
                address_translation(out_ram_req->addr, &delta_x, &delta_y);

                //send the header flit for the last read request
                flit = get_delta_flit(out_ram_req->packet_id, delta_x, delta_y) | READ_FLIT_MASK;
                //if(out_ram_req->type == read_block && conf->ram_mode != dram)
                if(out_ram_req->type == read_block)
                    flit |= 1;  //mark as vector

                ram_write_state = sReadAddr;
            }

            mem_out->insert_at_tail(flit);
        }
        //read request
        else if (ram_write_state == sReadAddr)
        {
            flit = out_ram_req->addr | TAIL_FLIT_MASK;
            mem_out->insert_at_tail(flit);

            ram_write_state = sHeader;

            ram_req_queue.pop();
            return true;
        }
        //writeback
        else if (ram_write_state == sWriteAddr)
        {
            flit = out_wb->addr;
            mem_out->insert_at_tail(flit);

            ram_write_state = sData;
        }
        //output the data
        else if (ram_write_state == sData)
        {
            if (out_wb->data_flits > 1)
            {
                flit = 0;                      //64 bits of data
                mem_out->insert_at_tail(flit);
                out_wb->data_flits--;
            }
            else //tail packet
            {
                flit = 0 | TAIL_FLIT_MASK;    //64 bits of data
                mem_out->insert_at_tail(flit);

                ram_write_state = sHeader;

                //finished this output
                wb_queue.pop_front();
                return true;
            }
        }
    }

    return false;
}

mem_request* L2cache_standalone::get_next_request()
{
    mem_request* req;
    req = next_ram_request();
    if (req == nullptr)
        req = next_request();

    return req;
}

//return the next ram request
mem_request* L2cache_standalone::next_ram_request()
{
    //no cache requests to decode
    if (ram_return_req.empty())  return nullptr;

    mem_request* first_req = new mem_request(ram_return_req.front());
    //remove the request from the controllers list
    ram_return_req.pop();
    return first_req;
}
