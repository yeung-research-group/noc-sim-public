#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

prefetcher::prefetcher(cacheL1 *cache, int x, int y)
{
    conf = Conf_ReRAM::get();
    stats = Stats::get();
    make_prefetcher(cache);
    x_coord = x;
    y_coord = y;

}

void prefetcher::make_prefetcher(cacheL1 *cache){
    prefetch_depth = conf->prefetch_depth; //=conf->prefetch_depth;
    buf_size = 2*prefetch_depth+2; //TODO check for unaligned mem accesses.
    max_streams = conf->prefetch_streams_per_thread*conf->threads; //=conf->rpt_size;
    //rpt_head = (Prpt_entry)malloc(sizeof(rpt_entry));
    //rpt_tail = (Prpt_entry)malloc(sizeof(rpt_entry));
    rpt_head = (Prpt_entry)NULL;
    rpt_tail = (Prpt_entry)NULL;
    rpt_entry_count = 0;
    Pcache = cache;
    cache_block_addr_mask = 0xFFFFFFFFFFFFFFFF<<conf->l1_settings.index_mask_offset;
    //dw_aligned_addr_mask = 0xFFFFFFFFFFFFFFFF<<(LOG2(conf->max_simd_width-1));

    stream_buffer = new std::unordered_map<uint64, Pstream_buffer_entry>;
}

/* Called from the core to check if there is an rpt entry corresponding to a particular address
   Return value ->
   0: stream_buf_miss
   1: stream_buf_hit and data in flight
   2: stream_buf_hit and data in buffer 
   Arguments ->
   vec_first_part - A cache block unaligned vector_read instruction if split across cache
   blocks will arrive in two parts. This bit is set when first part arrives.
   Mask simplifying case - For our case mask only takes a limited number of values for vector load
    and gather(000, 001, 011, 111 only other values will not be there ). So calculation of mask
    width can be simplified. 001 -> width of 1. 111-> width of 3.
    Handles masked and unaligned vector loads. But only works for unmasked (or mask = 111...11)
    gathers. 
   */
int prefetcher::check_rpt(uint64 PC, int thread_id, uint64 *addr, ins_type type,
            int mask, int vec_first_part, int cache_hit, int *reqs_sent, 
            unsigned *packet_id)
{//TODO : take cache hit into account when checking stream buffer.
    Prpt_entry stream, evicted_stream;
    int mask_width=0;
    int stream_buf_hm;
    if(type == vector_read){
        while(mask){
            mask_width += mask & 0x1;
            mask >>= 1;
        }
    }
    stream = find_stream(PC, thread_id);
    if(stream){
        if(stream->type != type)
            printf("ERROR!!!");
        //Unaligned vector load updates state only once.
        //if(!vector || (vector & vec_first_part))
        if((type == vector_read ||(type == gather))&vec_first_part)
            update_stream(stream, addr, mask_width, type);
        stream_buf_hm = check_my_stream_buf(stream, addr[0], packet_id, type, vec_first_part);
        if(stream_buf_hm != stream_buf_miss)
            stats->tile_cpu_stats[IND(x_coord, y_coord)].prefetchq_hits++;
        return stream_buf_hm;
    }
    else{
        stream = make_stream(PC, thread_id, addr, type, mask);
        insert_stream(stream);
        rpt_entry_count ++;
        if(rpt_entry_count > max_streams){
            evicted_stream = rpt_tail;
            delete_stream(evicted_stream);
            rpt_entry_count --;
        }
    }
    //if(type == vector_read)
        //return check_all_stream_buf(addr[0], packet_id); //TODO
    //else
        return stream_buf_miss;
}

/* Given a PC check if there is a stream corresponding to it. TODO - If multiple threads how to handle
   the interference? */
prefetcher::Prpt_entry prefetcher::find_stream(uint64 PC, int thread_id){
    Prpt_entry stream = rpt_head;
    int hit;
    hit = FALSE;
    while(stream){
        if(stream->PC == PC && stream->thread_id == thread_id){
            hit = TRUE;
            break;
        }
        stream = stream->LRU_next;
    }
    if(hit)
        return stream;
    else 
        return (Prpt_entry)NULL;
}


prefetcher::Prpt_entry prefetcher::make_stream(uint64 PC, int thread_id, uint64 *addr, 
        ins_type type, int mask_width){
    Prpt_entry stream = (Prpt_entry)malloc(sizeof(rpt_entry));
    stream->PC = PC;
    stream->thread_id = thread_id;
    stream->type = type;
    if(type == gather){
        stream->last_addr = (uint64 *)malloc(sizeof(uint64)*conf->max_simd_width);
        for(int i=0; i<mask_width; i++){
            stream->last_addr[i] = addr[i];
        }
    }
    else{
        stream->last_addr = (uint64 *)malloc(sizeof(uint64)*1);
        stream->last_addr[0] = addr[0];
    }
    //stream->last_addr = addr;
    stream->stride = 0;
    stream->state = init;
    
    stream->Pstream_buf = new stream_buffer_entry[buf_size+1]; 
    int element_count = (type == gather)?conf->max_simd_width:1;
    for(int i=0; i<buf_size+1; i++){
        stream->Pstream_buf[i].avail = new bool[element_count];
        stream->Pstream_buf[i].packet_id = new unsigned[element_count];
        stream->Pstream_buf[i].address = new uint64[element_count];
    }

    //stream->Pstream_buf = (Pbuf_entry)malloc((buf_size+1)*sizeof(buf_entry));
    stream->LRU_prev = (Prpt_entry)NULL;
    stream->LRU_next = (Prpt_entry)NULL;
    stream->buf_head = 0;
    stream->buf_tail = 0;
    return stream;

}

/* Insert at the head of the list */
void prefetcher::insert_stream(Prpt_entry stream){
    stream->LRU_next = rpt_head;
    stream->LRU_prev = (Prpt_entry)NULL;
    if(stream->LRU_next){
        stream->LRU_next->LRU_prev = stream;
    }
    else{
        rpt_tail = stream;
    }
    rpt_head = stream;
}

void prefetcher::lru_stream(Prpt_entry stream) {
    if (stream->LRU_prev)
        stream->LRU_prev->LRU_next = stream->LRU_next;
    else
        rpt_head = stream->LRU_next;

    if (stream->LRU_next)
        stream->LRU_next->LRU_prev = stream->LRU_prev;
    else
        rpt_tail = stream->LRU_prev;

    insert_stream(stream);
}

void prefetcher::delete_stream(Prpt_entry stream){
    if(stream->LRU_prev)
        stream->LRU_prev->LRU_next = stream->LRU_next;
    else
        rpt_head = stream->LRU_next;

    if(stream->LRU_next)
        stream->LRU_next->LRU_prev = stream->LRU_prev;
    else
        rpt_tail = stream->LRU_prev;

    //delete buffer entries
    while(stream->buf_head != stream->buf_tail)
        sbuf_delete_top_element(stream);

    delete[] stream->Pstream_buf;
    //free(evicted_stream->Pstream_buf);
    free(stream->last_addr);
    free(stream);
}




void prefetcher::update_stream(Prpt_entry stream, uint64 *addr, int mask, ins_type type){
    /* update the state and LRU priority */
    update_state(stream, addr, mask, type);
    lru_stream(stream);
    //insert_stream(stream);
}

void prefetcher::update_state(prefetcher::Prpt_entry stream, uint64_t *addr, int mask, ins_type type){
    rpt_state cur_state = stream->state;
    //rpt_state new_state;
    int new_stride, gather_new_stride_found=TRUE;
    if(type == gather){
        new_stride = addr[0] - stream->last_addr[0];
        for(int i=1; i<conf->max_simd_width; i++){
            if(new_stride != (addr[i] - stream->last_addr[i])){
                gather_new_stride_found = FALSE;
            }  
        }
    }
    else
        new_stride = addr[0] - stream->last_addr[0];
    uint64 cache_aligned_addr = addr[0] & cache_block_addr_mask; // For vector
    
    if((new_stride == stream->stride) && ((type==gather)?gather_new_stride_found:TRUE)) {
    //Correct prediction
        if(cur_state == init || cur_state == transient){
            if(type == gather){
                issue_prefetch_request(stream, addr, type);
                for(int i=0; i<conf->max_simd_width;i++){
                    stream->last_addr[i] = addr[i];
                }
            }
            else{
                issue_prefetch_request(stream, &cache_aligned_addr, type); //TODO - pref whole buffer
                stream->last_addr[0] = (type==vector_read) ? (addr[0] + (mask-1)*(64/conf->max_simd_width)) : addr[0];
            }
            stream->state =  steady;
            
        }
        if(cur_state == steady){
            if(type == gather){
                issue_prefetch_request(stream, addr, type);
                for(int i=0; i<conf->max_simd_width;i++){
                    stream->last_addr[i] = addr[i];
                }
            }
            else{
                issue_prefetch_request(stream, &cache_aligned_addr, type); //TODO - pref whole buffer
                stream->last_addr[0] = (type==vector_read) ? (addr[0] + (mask-1)*(64/conf->max_simd_width)) : addr[0];
            }
        }
        if(cur_state == no_prediction){
            if(type == gather){
                for(int i=0; i<conf->max_simd_width;i++){
                    stream->last_addr[i] = addr[i];
                }
            }
            else{
                stream->last_addr[0] = (type==vector_read) ? (addr[0] + (mask-1)*(64/conf->max_simd_width)) : addr[0];
            }
            stream->state = transient;
        }
    }
    else{
    //incorrect prediction
        if(cur_state == init){
            if(type == gather){
                for(int i=0; i<conf->max_simd_width;i++){
                    stream->last_addr[i] = addr[i];
                }
                if(gather_new_stride_found)
                    stream->stride = new_stride;
            }
            else{
                stream->last_addr[0] = (type==vector_read) ? (addr[0] + (mask-1)*(64/conf->max_simd_width)) : addr[0];
                stream->stride = new_stride;
            }
            stream->state = transient;

        }
        if(cur_state == transient || cur_state == no_prediction){
            if(type == gather){
                for(int i=0; i<conf->max_simd_width;i++){
                    stream->last_addr[i] = addr[i];
                }
                if(gather_new_stride_found)
                    stream->stride = new_stride;
            }
            else{
                stream->last_addr[0] = (type==vector_read) ? (addr[0] + (mask-1)*(64/conf->max_simd_width)) : addr[0];
            stream->stride = new_stride;
            }
            stream->state = no_prediction;
        }
        if(cur_state == steady){
            if(type == gather){
                for(int i=0; i<conf->max_simd_width;i++){
                    stream->last_addr[i] = addr[i];
                }
                if(gather_new_stride_found)
                    stream->stride = new_stride;
            }
            else{
                stream->last_addr[0] = (type==vector_read) ? (addr[0] + (mask-1)*(64/conf->max_simd_width)) : addr[0];
            }
            stream->state = init;
        }
    }

}

/* Checks buffer of current stream for hit on the element at the top. The prefetch buffer
 * will have entrees in increasing order of addr.;  */
int prefetcher::check_my_stream_buf(Prpt_entry stream, uint64 addr, unsigned *packet_id,
        ins_type type, int vec_first_part)
{
    prefetch_status return_value =  stream_buf_miss;
    uint64 cache_aligned_addr = addr & cache_block_addr_mask;
    std::unordered_map<uint64, Pstream_buffer_entry>::const_iterator buf;

    if(type == gather)
       buf = stream_buffer->find(addr);
    else
        buf = stream_buffer->find(cache_aligned_addr);

    if (buf == stream_buffer->end())
    {
        return stream_buf_miss;
    }
    else
    {
        stream_buffer_entry* Pbuf = buf->second;

        if (type == gather) 
        {
            for (int i = 0; i < conf->max_simd_width; i++) 
            {
                if (addr == Pbuf->address[i]) 
                {
                    if (Pbuf->avail[i]) 
                    {
                        return stream_buf_hit;
                    }
                    else 
                    {
                        *packet_id = Pbuf->packet_id[i];
                        if (*packet_id == 0) 
                        { 
                            // Prefetch request could not be issued most likely due to 
                            //contention so delete it and issue a regular memory access.
                            for (std::deque<pref_req_entry>::iterator it = pref_req_queue.begin();
                                it != pref_req_queue.end(); ++it) 
                            {
                                if (it->corresponding_buf_entry == Pbuf) 
                                {
                                    pref_req_queue.erase(it);
                                    break;
                                }
                            }
                            stream_buffer->erase(buf);
                            return stream_buf_miss;
                        }
                        return in_flight;
                    }
                }
            }
        }
        else
        {
            if (Pbuf->avail[0])
                return stream_buf_hit;
            else {
                *packet_id = Pbuf->packet_id[0];
                if (*packet_id == 0) {// Prefetch request could not be issued most likely due to
                                //contention so delete it and issue a regular memory access.
                    for (std::deque<pref_req_entry>::iterator it = pref_req_queue.begin();
                        it != pref_req_queue.end(); ++it) {
                        if (it->corresponding_buf_entry == Pbuf) {
                            pref_req_queue.erase(it);
                            break;
                        }
                    }
                    stream_buffer->erase(buf);
                    return stream_buf_miss;
                }
                return in_flight;
            }
        }
    }
    return return_value;

}

/* Checks all the stream buffers for hit on the element at the top. argument pop determines  */
int prefetcher::check_all_stream_buf(uint64 addr, unsigned *packet_id){
    stream_buffer_entry *toq;
    //uint64 cache_aligned_addr = addr & cache_block_addr_mask;

    std::unordered_map<uint64, Pstream_buffer_entry>::const_iterator buf = stream_buffer->find(addr);

    if (buf == stream_buffer->end())
    {
        return stream_buf_miss;
    }
    else
    {
        toq = buf->second;
        if (toq->avail[0])
            return stream_buf_hit;
        else {
            *packet_id = toq->packet_id[0];
            if (*packet_id == 0)
                return stream_buf_miss;
            return in_flight;
        }
    }
}

void prefetcher::issue_prefetch_request(Prpt_entry stream, uint64 *addr, ins_type type){
    //Issue a prefetch request into local request queue.
    
    //Insert the inflight request into the buffer
    int mem_access_size = (type==vector_read)?conf->max_simd_width:1;
    uint64 pref_addr[conf->max_simd_width];
    if(type==gather){
        for(int i=0; i<conf->max_simd_width; i++){
            pref_addr[i] = addr[i]+stream->stride*prefetch_depth*mem_access_size;
        }
    }   
    else
        pref_addr[0] = addr[0] + stream->stride*prefetch_depth*mem_access_size;

    //If data already prefetched in the last request return
    if (type == vector_read)
    {
        if(stream_buffer->find(pref_addr[0]) != stream_buffer->end())
            return;
    }
    

    stream_buffer_entry* corresponding_buf_entry = &(stream->Pstream_buf[stream->buf_tail]);
    sbuf_insert_at_tail(stream, pref_addr, type);
    if(type == gather){
        stats->tile_cpu_stats[IND(x_coord, y_coord)].packets_prefetched+=8;
    }
    else
        stats->tile_cpu_stats[IND(x_coord, y_coord)].packets_prefetched++;
    
    pref_req_entry pref_req;
    if(type==gather){
        for(int i=0;i<conf->max_simd_width; i++){
            pref_req.type = type;
            pref_req.addr = pref_addr[i];
            pref_req.corresponding_buf_entry = corresponding_buf_entry;
            pref_req_queue.push_back(pref_req);
        }
    }
    else{
        pref_req.type = type;
        pref_req.addr = pref_addr[0];
        pref_req.corresponding_buf_entry = corresponding_buf_entry;
        pref_req_queue.push_back(pref_req);
    }

}

void prefetcher::sbuf_delete_top_element(Prpt_entry stream){
    if (stream->Pstream_buf[stream->buf_head].type == gather) 
    {
        for (int i = 0; i < conf->max_simd_width; i++) 
        {
            stream_buffer->erase(stream->Pstream_buf[stream->buf_head].address[i]);
        }
    }
    else 
    {
        stream_buffer->erase(stream->Pstream_buf[stream->buf_head].address[0]);
    }
    stream->buf_head = (stream->buf_head+1)%buf_size;
}

void prefetcher::sbuf_insert_at_tail(Prpt_entry stream, uint64* address, ins_type type ){
    if ((stream->buf_tail + 1) % buf_size == stream->buf_head)
        sbuf_delete_top_element(stream);

    Pstream_buffer_entry buf = &(stream->Pstream_buf[stream->buf_tail]);

    stream->Pstream_buf[stream->buf_tail].type = type; //TODO VECTOR ARTIFACT
     //Packet ID will be assigned when prefetch request is issued on the noc.
    if(type == gather){
        for(int i=0; i<conf->max_simd_width; i++){
            buf->avail[i] = FALSE;
            buf->packet_id[i] = 0;
            buf->address[i] = address[i];
            stream_buffer->emplace(address[i], buf);
        }
    }
    else{
        buf->avail[0] = FALSE;
        buf->packet_id[0] = 0;
        buf->address[0] = address[0];
        
        stream_buffer->emplace(address[0], buf);
    }

    stream->buf_tail = (stream->buf_tail + 1)%buf_size;
}


void prefetcher::receive_packet_from_noc(uint64 packet_id){
    Prpt_entry stream = rpt_head;
    
    while(stream){
        
        if(sbuf_check_matching_request(stream, packet_id))
            return;
        stream = stream->LRU_next;
    }
}

int prefetcher::sbuf_check_matching_request(Prpt_entry stream, uint64 packet_id){
    int index = stream->buf_head;
    int element_count = (stream->type==gather)?conf->max_simd_width:1;
    while(index != stream->buf_tail){
        for(int i=0; i<element_count;i++){
            if(stream->Pstream_buf[index].packet_id[i] == packet_id){
                stream->Pstream_buf[index].avail[i] = 1;
                return TRUE;
            }
        }
        index = (index+1)%buf_size;
    }
    return FALSE;
}

/* Prefetch request queue tries to insert request into noc if buffer space available
   . To give it lower priority over regular cache misses prefetcher is cycled in the 
   end of traffic-gen when cache has already been called. */
void prefetcher::cycle(long long cur_cycle){
    if(pref_req_queue.empty())
        return;
    pref_req_entry pref_req = pref_req_queue.front();
    stream_buffer_entry *Pbuffer_entry;
    unsigned packet_id;
    int vec = (pref_req.type==vector_read)?TRUE:FALSE;
    int sg = (pref_req.type==gather)?TRUE:FALSE;
    int reqs_sent = Pcache->read_request(cur_cycle, pref_req.addr, vec, sg, &packet_id, 0);
    if(reqs_sent == 0) //Not enough room
        return;
    Pbuffer_entry = pref_req.corresponding_buf_entry;
    int element_count = (Pbuffer_entry->type==gather)?conf->max_simd_width:1;
    for(int i=0; i<element_count; i++){
        if(Pbuffer_entry->address[i] == pref_req.addr) //Check to make sure the entry was not deleted.
            Pbuffer_entry->packet_id[i] = packet_id;
    }
    pref_req_queue.pop_front();

}

stream_buffer_entry_::~stream_buffer_entry_(){
    delete [] avail;
    delete [] packet_id;
    delete [] address;
}

