#pragma once

class L2cache_reram_pipeline : public L2cache_pipeline, public L2cache_reram
{
public:
    L2cache_reram_pipeline(int x, int y);
    void cycle(long long cur_cycle) override;

private:
    //Pipeline
    void reram_send_exec(long long cur_cycle) override;

    void mshr_update_exec(long long cur_cycle) override;
    void mshr_reram_exec(long long cur_cycle) override;

    void check_incoming_reram(long long cur_cycle);
};