#pragma once

class L2cache_bank;

class L2cache {
public:
    L2cache(int x, int y);
    void cycle(long long cur_cycle);

    //main network ports
    buffer* get_input_buffer() { return (input); };
    void set_output_buffer(buffer* out);

    //possible network ports
    buffer* get_mem_in_buffer() { return (mem_in); };
    void set_mem_out_buffer(buffer* out);
    buffer* get_dram_in_buffer() { return (dram_in); };
    void set_dram_out_buffer(buffer* out);

    bool active;

private:
    int x_coord;
    int y_coord;
    int x_extent;
    int y_extent;
    int banks;

    L2cache_bank** cache_banks;
    buffer** cache_outputs;
    buffer** cache_inputs;
    buffer** cache_mem_in;
    buffer** cache_mem_out;
    buffer** cache_dram_in;
    buffer** cache_dram_out;
    bool multibank;

    int out_routed;
    int in_routed;
    int mem_out_routed;
    int mem_in_routed;
    int dram_out_routed;
    int dram_in_routed;

    //round robin
    int last_out_routed;
    int last_mem_out_routed;
    int last_dram_out_routed;

    void multibank_setup();
    void singlebank_setup();

    //Global objects
    Stats* stats;
    Conf_ReRAM* conf;

    //NOC
    buffer* input;
    buffer* output;
    buffer* mem_in;
    buffer* mem_out;
    buffer* dram_in;
    buffer* dram_out;
    void route_in(long long cur_cycle);
    void route_out(long long cur_cycle);
    void route_mem_in(long long cur_cycle);
    void route_mem_out(long long cur_cycle);
    void route_dram_in(long long cur_cycle);
    void route_dram_out(long long cur_cycle);
};