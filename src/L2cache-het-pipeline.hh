#pragma once

#include <list>

class L2cache_het_pipeline : public L2cache_pipeline, public L2cache_het, public L2cache_standalone
{
public:
    L2cache_het_pipeline(int x, int y);
    void cycle(long long cur_cycle) override;


    buffer* get_mem_in_buffer() override { return (mem_in); };
    void set_mem_out_buffer(buffer* out) override;
    buffer* get_dram_in_buffer() override { return (dram_in); };
    void set_dram_out_buffer(buffer* out) override;

private:
    bool dram_last;

    void writeback_req(L2_cache_line* evicted) override;

    //Pipeline
    void reram_send_exec(long long cur_cycle) override;

    void mshr_update_exec(long long cur_cycle) override;
    void mshr_reram_exec(long long cur_cycle) override;
};


