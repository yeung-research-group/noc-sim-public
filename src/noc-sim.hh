#pragma once

/*
  noc-sim.hh
  
  Candace Walden
  22 August 2017

  Includes and other globals for all files
*/

#include <stdint.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <cstring>
#include <string>
#include <iostream>
#include <iomanip>
#include <deque>
#include <thread>
#include <mutex>
#include <queue>
#include <list>
#include <math.h>
#ifdef _MSC_VER   //if visual studio
//#include "../ext/dramsim3/src/dramsim3.h"
#include "../ext/dramsim3/src/configuration.h"
#include "../ext/dramsim3/src/common.h"
#include "../ext/dramsim3/src/controller.h"
#include "../ext/dramsim3/src/timing.h"
#else
//#include "dramsim3.h"
#include "configuration.h"
#include "common.h"
#include "controller.h"
#include "timing.h"
#endif


#ifndef uint64
  #define uint64 uint64_t
#endif

/*
#ifndef random
  #define random rand
#endif
  */

#define NOC_SIM_SNIPER 0
#define NOC_SIM_PARA 0

//Output controls
#define CSV_OUT 1
#define STATS_OUT 1
#define SIM_SETTING_OUT 1
#define RRAM_BANK_OUT 0
#define RRAM_CONTR_OUT 0
#define ROUTER_OUT 0
#define CPU_OUT 0
#define TRAFFIC_QUEUE_OUT 0
#define L2_OUT 0
#define CACHE_OUT_L1 0
#define MEM_LEAK_DEBUG 0
#define BANK_PROF_OUT 1
#define PIPELINE_OUTPUT 1

//Benchmark Perfomance debugging tools
#define BANK_CONFLICT_DETECTION 0
#define BANK_IN_QUEUE_HIST 0
#define CPU_PRINT 0
#define BANK_UTILIZATION_TRACKING 1
#define INSTRUCTION_QUEUE_STATS 1

#define OUTPUT_LOG CACHE_OUT_L1|L2_OUT|ROUTER_OUT|RRAM_CONTR_OUT|TRAFFIC_QUEUE_OUT|TRAFFIC_QUEUE_OUT|CPU_OUT

#define EPOCH_PERIOD 100000

#include "config.hh"
#include "instruction.hh"
#include "rram-queue.hh"

struct output_request_entry{
  rram_queue* queue;
  int bank;
};

#include "config.hh"
#include "stats.hh"
#include "rram-bank.hh"
#include "buffer.hh"
#include "L2cache.hh"
#include "L2cache-bank.hh"
#include "L2cache-pipeline.hh"
#include "L2cache-standalone.hh"
#include "L2cache-standalone-pipeline.hh"
#include "L2cache-reram.hh"
#include "L2cache-reram-pipeline.hh"
#include "L2cache-het.hh"
#include "L2cache-het-pipeline.hh"
#include "L2cache-array.hh"
#include "cacheL1.hh"
#include "prefetcher.hh"
#include "dynamic_queue.hh"
#include "traffic-queue.hh"
#include "traffic-gen-thread.hh"
#include "ram-controller.hh"
#include "rram-controller.hh"
#include "dram-controller.hh"
#include "traffic-gen.hh"
#include "mem-trace.hh"
#include "router.hh"
#include "rram-noc-sim.hh"

#define TRUE 1
#define FALSE 0
#define WORD_SIZE 8
#define WORD_SIZE_OFFSET 3
#define QUEUE_OVERFILL 5

/*
 * Packets - 64 bits
 * 
 *  Header:
 *  TRWE 0 DX5-X0 DY5-Y0 P31-P0 B12-B0
 *  Read, write, reply, tail, direction x offset, direction y offset, packet id, bytes requested
 *
 *  Address
 *  T000...0 B19-B0
 *  possible tail, padding, bank
 *
 *  Data
 *  T000 0...0
 */

#define TAIL_FLIT_MASK     0x8000000000000000
#define READ_FLIT_MASK     0x4000000000000000
#define WRITE_FLIT_MASK    0x2000000000000000
#define REPLY_FLIT_MASK    0x1000000000000000
#define HEADER_A_FLIT_MASK 0x0800000000000000
#define HEADER_FLIT_MASK   0x7000000000000000
#define ADDRESS_MASK       0x0FFFFFFFFFFFFFFF
#define DRAM_ADDRESS_MASK  0x0800000000000000

#define DIRECTION_X_MASK   0x0400000000000000
#define DELTA_X_MASK       0x03F0000000000000
#define X_DECREMENT        0x0010000000000000
#define X_OFFSET           52
#define DIRECTION_Y_MASK   0x0008000000000000
#define DELTA_Y_MASK       0x0007E00000000000
#define Y_DECREMENT        0x0000200000000000
#define Y_OFFSET           45
#define DIRECTION_MASK     DIRECTION_X_MASK|DELTA_X_MASK|DIRECTION_Y_MASK|DELTA_Y_MASK

#define PACKET_MASK        0x00001FFFFFFFE000
#define PACKET_OFFSET      13
#define BYTES_MASK         0x0000000000001FFF

#define BANK_MASK         0x00000000000FFFFF


#define WRITE_INS_MASK           0x80000000
#define READ_INS_MASK            0x40000000
#define SCATTER_INS_MASK         0x20000000
#define GATHER_INS_MASK          0x10000000
#define NON_MEM_INS_CNT_MASK     0x08000000
#define PREFETCH_WIDTH_MASK      0x04000000
#define WIDTH_MASK               0x000FFFFF   //For the non_memory_instruction_count and missed_simd_memory_accesses
#define HIT_SIMD_WIDTH_MASK      0x03F00000




/* Macros */
#define IND(x, y) (int)(x * conf->y_max + y)
#define CIND(x, y) (int)(x * y_max + y)
#define LOG2(x) ((int)( log((double)(x)) / log(2) ))
#define PACKET_ID(x) ((unsigned)((x & PACKET_MASK) >> PACKET_OFFSET))
#define CACHE_BANK_ID(addr) (unsigned)((addr & conf->cache_bank_bits) >> conf->cache_bank_offset) ^ \
                            (unsigned)((addr & conf->cache_bank_bits_y) >> conf->cache_bank_offset_y) ^ \
                            (unsigned)((addr & conf->cache_bank_bits_z) >> conf->cache_bank_offset_z)
#define BANK_ID(addr) ((unsigned)(((addr & conf->upper_bank_bits) >> (conf->upper_bank_offset - conf->n_sector_bits)) ^ \
                       (unsigned)((addr & conf->upper_bank_bits_y) >> (conf->upper_bank_offset_y - conf->n_sector_bits)) ^ \
                       (unsigned)((addr & conf->upper_bank_bits_z) >> (conf->upper_bank_offset_z - conf->n_sector_bits))) | \
                       (unsigned)((addr & conf->sector_bits) >> conf->sector_offset))
#define SUBBANK_ID(addr) (unsigned)((addr & conf->upper_bank_bits) >> conf->upper_bank_offset) ^ \
                         (unsigned)((addr & conf->upper_bank_bits_y) >> conf->upper_bank_offset_y) ^ \
                         (unsigned)((addr & conf->upper_bank_bits_z) >> conf->upper_bank_offset_z)
#define SECTOR_ID(bank) (unsigned)(bank & conf->sector_bits)
#define SECTOR_ID_ADDR(addr) (unsigned)((addr & conf->sector_bits) >> conf->sector_offset)
#define TILE_ID(addr) (unsigned)((addr & conf->tile_id_bits) >> conf->tile_id_offset)


