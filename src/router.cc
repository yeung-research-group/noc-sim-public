/*
  router.cc

  Donald Yeung
  24 July 2017

  Router class simulates 2D e-cube routing.  It models the buffers and
  switch inside one of the NOC routers.  Accounts for contention that
  will occur at a NOC router point.
 */


#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"


router::router(int x, int y, int buffer_size)
{
    x_coord = x;
    y_coord = y;
    conf = Conf_ReRAM::get();

    /* Create the input buffers */
    p_in1 = new buffer(buffer_size);
    //p_in1 = new buffer(buffer_size*buffer_size);
    pos_x_in1 = new buffer(buffer_size);
    neg_x_in1 = new buffer(buffer_size);
    pos_y_in1 = new buffer(buffer_size);
    neg_y_in1 = new buffer(buffer_size);
    p_in2 = new buffer(buffer_size);
    pos_x_in2 = new buffer(buffer_size);
    neg_x_in2 = new buffer(buffer_size);
    pos_y_in2 = new buffer(buffer_size);
    neg_y_in2 = new buffer(buffer_size);
    if (conf->ram_mode == dram || conf->ram_mode == l2_cpu)
    {
        p_in3 = new buffer(buffer_size);
        pos_x_in3 = new buffer(buffer_size);
        neg_x_in3 = new buffer(buffer_size);
        pos_y_in3 = new buffer(buffer_size);
        neg_y_in3 = new buffer(buffer_size);
        p_in4 = new buffer(buffer_size);
        pos_x_in4 = new buffer(buffer_size);
        neg_x_in4 = new buffer(buffer_size);
        pos_y_in4 = new buffer(buffer_size);
        neg_y_in4 = new buffer(buffer_size);
    }
    else
    {
        p_in3 = nullptr;
        pos_x_in3 = nullptr;
        neg_x_in3 = nullptr;
        pos_y_in3 = nullptr;
        neg_y_in3 = nullptr;
        p_in4 = nullptr;
        pos_x_in4 = nullptr;
        neg_x_in4 = nullptr;
        pos_y_in4 = nullptr;
        neg_y_in4 = nullptr;
    }

    /* make all ports initially idle */
    pos_x_out1_status = IDLE;
    neg_x_out1_status = IDLE;
    pos_y_out1_status = IDLE;
    neg_y_out1_status = IDLE;
    p_out1_status = IDLE;
    pos_x_out2_status = IDLE;
    neg_x_out2_status = IDLE;
    pos_y_out2_status = IDLE;
    neg_y_out2_status = IDLE;
    p_out2_status = IDLE;
    pos_x_out3_status = IDLE;
    neg_x_out3_status = IDLE;
    pos_y_out3_status = IDLE;
    neg_y_out3_status = IDLE;
    p_out3_status = IDLE;
    pos_x_out4_status = IDLE;
    neg_x_out4_status = IDLE;
    pos_y_out4_status = IDLE;
    neg_y_out4_status = IDLE;
    p_out4_status = IDLE;


    pos_x_out1_last = 0;
    neg_x_out1_last = 0;
    pos_y_out1_last = 0;
    neg_y_out1_last = 0;
    p_out1_last = 0;
    pos_x_out2_last = 0;
    neg_x_out2_last = 0;
    pos_y_out2_last = 0;
    neg_y_out2_last = 0;
    p_out2_last = 0;
    pos_x_out3_last = 0;
    neg_x_out3_last = 0;
    pos_y_out3_last = 0;
    neg_y_out3_last = 0;
    p_out3_last = 0;
    pos_x_out4_last = 0;
    neg_x_out4_last = 0;
    pos_y_out4_last = 0;
    neg_y_out4_last = 0;
    p_out4_last = 0;

    pos_x_out1 = nullptr;
    neg_x_out1 = nullptr;
    pos_y_out1 = nullptr;
    neg_y_out1 = nullptr;
    p_out1 = nullptr;
    pos_x_out2 = nullptr;
    neg_x_out2 = nullptr;
    pos_y_out2 = nullptr;
    neg_y_out2 = nullptr;
    p_out2 = nullptr;
    pos_x_out3 = nullptr;
    neg_x_out3 = nullptr;
    pos_y_out3 = nullptr;
    neg_y_out3 = nullptr;
    p_out3 = nullptr;
    pos_x_out4 = nullptr;
    neg_x_out4 = nullptr;
    pos_y_out4 = nullptr;
    neg_y_out4 = nullptr;
    p_out4 = nullptr;
}

void router::set_positive_x_link(router* next_router)
{
    if (next_router)
    {
        pos_x_out1 = next_router->get_pos_x1_buffer();
        pos_x_out2 = next_router->get_pos_x2_buffer();
        pos_x_out3 = next_router->get_pos_x3_buffer();
        pos_x_out4 = next_router->get_pos_x4_buffer();
    }
    else
    {
        pos_x_out1 = NULL;
        pos_x_out2 = NULL;
        pos_x_out3 = NULL;
        pos_x_out4 = NULL;
    }
}

void router::set_negative_x_link(router* next_router)
{
    if (next_router)
    {
        neg_x_out1 = next_router->get_neg_x1_buffer();
        neg_x_out2 = next_router->get_neg_x2_buffer();
        neg_x_out3 = next_router->get_neg_x3_buffer();
        neg_x_out4 = next_router->get_neg_x4_buffer();
    }
    else
    {
        neg_x_out1 = NULL;
        neg_x_out2 = NULL;
        neg_x_out3 = NULL;
        neg_x_out4 = NULL;
    }
}

void router::set_positive_y_link(router* next_router)
{
    if (next_router)
    {
        pos_y_out1 = next_router->get_pos_y1_buffer();
        pos_y_out2 = next_router->get_pos_y2_buffer();
        pos_y_out3 = next_router->get_pos_y3_buffer();
        pos_y_out4 = next_router->get_pos_y4_buffer();
    }
    else
    {
        pos_y_out1 = NULL;
        pos_y_out2 = NULL;
        pos_y_out3 = NULL;
        pos_y_out4 = NULL;
    }
}

void router::set_negative_y_link(router* next_router)
{
    if (next_router)
    {
        neg_y_out1 = next_router->get_neg_y1_buffer();
        neg_y_out2 = next_router->get_neg_y2_buffer();
        neg_y_out3 = next_router->get_neg_y3_buffer();
        neg_y_out4 = next_router->get_neg_y4_buffer();
    }
    else
    {
        neg_y_out1 = NULL;
        neg_y_out2 = NULL;
        neg_y_out3 = NULL;
        neg_y_out4 = NULL;
    }
}

void router::set_p_out_link1(L2cache* memory)
{
    p_out1 = memory->get_input_buffer();
}

void router::set_p_out_link2(traffic_generator* core)
{
    p_out2 = core->get_input_buffer();
}

void router::set_p_out_link3(ram_controller* memory)
{
    p_out3 = memory->get_input_buffer();
}

void router::set_p_out_link4(L2cache* memory)
{
    p_out4 = memory->get_mem_in_buffer();
}

void router::set_p_out_link4_dram(L2cache* memory)
{
    p_out4 = memory->get_dram_in_buffer();
}

/* On each cycle, this function picks a routing action for each output
   port.  For now, we always consider input buffers in a certain
   order.  */
void router::decide_route_actions()
{
    int start, i;

    start = pos_x_out1_last;
    i = (start + 1) % 2;
    while (pos_x_out1_status == IDLE) {
        /* look for packet at pos_x_in1 and p_in1 */
        switch (i) {
        case 0:
            if (pos_x_in1->is_x_routable() && pos_x_in1->is_x_positive()) {
                pos_x_out1_status = POS_X1;
                pos_x_in1->set_routed();
                pos_x_out1_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (p_in1->is_x_routable() && p_in1->is_x_positive()) {
                pos_x_out1_status = P_IN1;
                p_in1->set_routed();
                pos_x_out1_last = 1;
                break;
            }
        }

        if (i == 0) break;
        else       i = 0;
    }

    start = pos_x_out2_last;
    i = (start + 1) % 2;
    while (pos_x_out2_status == IDLE) {
        /* look for packet at pos_x_in2 and p_in2 */
        switch (i) {
        case 0:
            if (pos_x_in2->is_x_routable() && pos_x_in2->is_x_positive()) {
                pos_x_out2_status = POS_X2;
                pos_x_in2->set_routed();
                pos_x_out2_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (p_in2->is_x_routable() && p_in2->is_x_positive()) {
                pos_x_out2_status = P_IN2;
                p_in2->set_routed();
                pos_x_out2_last = 1;
                break;
            }
        }

        if (i == 0) break;
        else       i = 0;
    }

    start = neg_x_out1_last;
    i = (start + 1) % 2;
    while (neg_x_out1_status == IDLE) {
        /* look for packet at neg_x_in1 and p_in1 */
        switch (i) {
        case 0:
            if (neg_x_in1->is_x_routable() && neg_x_in1->is_x_negative()) {
                neg_x_out1_status = NEG_X1;
                neg_x_in1->set_routed();
                neg_x_out1_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (p_in1->is_x_routable() && p_in1->is_x_negative()) {
                neg_x_out1_status = P_IN1;
                p_in1->set_routed();
                neg_x_out1_last = 1;
                break;
            }
        }

        if (i == 0) break;
        else       i = 0;
    }

    start = neg_x_out2_last;
    i = (start + 1) % 2;
    while (neg_x_out2_status == IDLE) {
        /* look for packet at neg_x_in2 and p_in2 */
        switch (i) {
        case 0:
            if (neg_x_in2->is_x_routable() && neg_x_in2->is_x_negative()) {
                neg_x_out2_status = NEG_X2;
                neg_x_in2->set_routed();
                neg_x_out2_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (p_in2->is_x_routable() && p_in2->is_x_negative()) {
                neg_x_out2_status = P_IN2;
                p_in2->set_routed();
                neg_x_out2_last = 1;
                break;
            }
        }

        if (i == 0) break;
        else       i = 0;
    }

    start = pos_y_out1_last;
    i = (start + 1) % 4;
    while (pos_y_out1_status == IDLE) {
        /* look for packet at pos_y1, pos_x1, neg_x1, or p_in1 */
        switch (i) {
        case 0:
            if (pos_y_in1->is_y_routable() && pos_y_in1->is_y_positive()) {
                pos_y_out1_status = POS_Y1;
                pos_y_in1->set_routed();
                pos_y_out1_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (pos_x_in1->is_y_routable() && pos_x_in1->is_y_positive()) {
                pos_y_out1_status = POS_X1;
                pos_x_in1->set_routed();
                pos_y_out1_last = 1;
                break;
            }
            if (start == 1) break;
        case 2:
            if (neg_x_in1->is_y_routable() && neg_x_in1->is_y_positive()) {
                pos_y_out1_status = NEG_X1;
                neg_x_in1->set_routed();
                pos_y_out1_last = 2;
                break;
            }
            if (start == 2) break;
        case 3:
            if (p_in1->is_y_routable() && p_in1->is_y_positive()) {
                pos_y_out1_status = P_IN1;
                p_in1->set_routed();
                pos_y_out1_last = 3;
                break;
            }
        }

        if (i == 0) break;
        else        i = 0;
    }


    start = pos_y_out2_last;
    i = (start + 1) % 4;
    while (pos_y_out2_status == IDLE) {
        /* look for packet at pos_y2, pos_x2, neg_x2, or p_in2 */
        switch (i) {
        case 0:
            if (pos_y_in2->is_y_routable() && pos_y_in2->is_y_positive()) {
                pos_y_out2_status = POS_Y2;
                pos_y_in2->set_routed();
                pos_y_out2_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (pos_x_in2->is_y_routable() && pos_x_in2->is_y_positive()) {
                pos_y_out2_status = POS_X2;
                pos_x_in2->set_routed();
                pos_y_out2_last = 1;
                break;
            }
            if (start == 1) break;
        case 2:
            if (neg_x_in2->is_y_routable() && neg_x_in2->is_y_positive()) {
                pos_y_out2_status = NEG_X2;
                neg_x_in2->set_routed();
                pos_y_out2_last = 2;
                break;
            }
            if (start == 2) break;
        case 3:
            if (p_in2->is_y_routable() && p_in2->is_y_positive()) {
                pos_y_out2_status = P_IN2;
                p_in2->set_routed();
                pos_y_out2_last = 3;
                break;
            }
        }

        if (i == 0) break;
        else        i = 0;
    }

    start = neg_y_out1_last;
    i = (start + 1) % 4;
    while (neg_y_out1_status == IDLE) {
        /* look for packet at neg_y1, pos_x1, neg_x1, or p_in1 */
        switch (i) {
        case 0:
            if (neg_y_in1->is_y_routable() && neg_y_in1->is_y_negative()) {
                neg_y_out1_status = NEG_Y1;
                neg_y_in1->set_routed();
                neg_y_out1_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (pos_x_in1->is_y_routable() && pos_x_in1->is_y_negative()) {
                neg_y_out1_status = POS_X1;
                pos_x_in1->set_routed();
                neg_y_out1_last = 1;
                break;
            }
            if (start == 1) break;
        case 2:
            if (neg_x_in1->is_y_routable() && neg_x_in1->is_y_negative()) {
                neg_y_out1_status = NEG_X1;
                neg_x_in1->set_routed();
                neg_y_out1_last = 2;
                break;
            }
            if (start == 2) break;
        case 3:
            if (p_in1->is_y_routable() && p_in1->is_y_negative()) {
                neg_y_out1_status = P_IN1;
                p_in1->set_routed();
                neg_y_out1_last = 3;
                break;
            }
        }

        if (i == 0) break;
        else        i = 0;
    }

    start = neg_y_out2_last;
    i = (start + 1) % 4;
    while (neg_y_out2_status == IDLE) {
        /* look for packet at neg_y2, pos_x2, neg_x2, or p_in2 */
        switch (i) {
        case 0:
            if (neg_y_in2->is_y_routable() && neg_y_in2->is_y_negative()) {
                neg_y_out2_status = NEG_Y2;
                neg_y_in2->set_routed();
                neg_y_out2_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (pos_x_in2->is_y_routable() && pos_x_in2->is_y_negative()) {
                neg_y_out2_status = POS_X2;
                pos_x_in2->set_routed();
                neg_y_out2_last = 1;
                break;
            }
            if (start == 1) break;
        case 2:
            if (neg_x_in2->is_y_routable() && neg_x_in2->is_y_negative()) {
                neg_y_out2_status = NEG_X2;
                neg_x_in2->set_routed();
                neg_y_out2_last = 2;
                break;
            }
            if (start == 2) break;
        case 3:
            if (p_in2->is_y_routable() && p_in2->is_y_negative()) {
                neg_y_out2_status = P_IN2;
                p_in2->set_routed();
                neg_y_out2_last = 3;
                break;
            }
        }

        if (i == 0) break;
        else        i = 0;
    }

    start = p_out1_last;
    i = (start + 1) % 5;
    while (p_out1_status == IDLE) {
        /* look for packet at pos_y1, neg_y1, pos_x1, neg_x1, or p_in1 */
        switch (i) {
        case 0:
            if (pos_y_in1->is_y_routable() && pos_y_in1->is_y_zero()) {
                p_out1_status = POS_Y1;
                pos_y_in1->set_routed();
                p_out1_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (neg_y_in1->is_y_routable() && neg_y_in1->is_y_zero()) {
                p_out1_status = NEG_Y1;
                neg_y_in1->set_routed();
                p_out1_last = 1;
                break;
            }
            if (start == 1) break;
        case 2:
            if (pos_x_in1->is_y_routable() && pos_x_in1->is_y_zero()) {
                p_out1_status = POS_X1;
                pos_x_in1->set_routed();
                p_out1_last = 2;
                break;
            }
            if (start == 2) break;
        case 3:
            if (neg_x_in1->is_y_routable() && neg_x_in1->is_y_zero()) {
                p_out1_status = NEG_X1;
                neg_x_in1->set_routed();
                p_out1_last = 3;
                break;
            }
            if (start == 3) break;
        case 4:
            if (p_in1->is_y_routable() && p_in1->is_y_zero()) {
                p_out1_status = P_IN1;
                p_in1->set_routed();
                p_out1_last = 4;
                break;
            }
        }

        if (i == 0) break;
        else        i = 0;
    }

    start = p_out2_last;
    i = (start + 1) % 5;
    while (p_out2_status == IDLE) {
        /* look for packet at pos_y2, neg_y2, pos_x2, neg_x2, or p_in2 */
        switch (i) {
        case 0:
            if (pos_y_in2->is_y_routable() && pos_y_in2->is_y_zero()) {
                p_out2_status = POS_Y2;
                pos_y_in2->set_routed();
                p_out2_last = 0;
                break;
            }
            if (start == 0) break;
        case 1:
            if (neg_y_in2->is_y_routable() && neg_y_in2->is_y_zero()) {
                p_out2_status = NEG_Y2;
                neg_y_in2->set_routed();
                p_out2_last = 1;
                break;
            }
            if (start == 1) break;
        case 2:
            if (pos_x_in2->is_y_routable() && pos_x_in2->is_y_zero()) {
                p_out2_status = POS_X2;
                pos_x_in2->set_routed();
                p_out2_last = 2;
                break;
            }
            if (start == 2) break;
        case 3:
            if (neg_x_in2->is_y_routable() && neg_x_in2->is_y_zero()) {
                p_out2_status = NEG_X2;
                neg_x_in2->set_routed();
                p_out2_last = 3;
                break;
            }
            if (start == 3) break;
        case 4:
            if (p_in2->is_y_routable() && p_in2->is_y_zero()) {
                p_out2_status = P_IN2;
                p_in2->set_routed();
                p_out2_last = 4;
                break;
            }
        }

        if (i == 0) break;
        else        i = 0;
    }

    if (conf->ram_mode == dram || conf->ram_mode == l2_cpu)
    {
        start = pos_x_out3_last;
        i = (start + 1) % 2;
        while (pos_x_out3_status == IDLE) {
            /* look for packet at pos_x_in3 and p_in3 */
            switch (i) {
            case 0:
                if (pos_x_in3->is_x_routable() && pos_x_in3->is_x_positive()) {
                    pos_x_out3_status = POS_X3;
                    pos_x_in3->set_routed();
                    pos_x_out3_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (p_in3->is_x_routable() && p_in3->is_x_positive()) {
                    pos_x_out3_status = P_IN3;
                    p_in3->set_routed();
                    pos_x_out3_last = 1;
                    break;
                }
            }

            if (i == 0) break;
            else       i = 0;
        }

        start = pos_x_out4_last;
        i = (start + 1) % 2;
        while (pos_x_out4_status == IDLE) {
            /* look for packet at pos_x_in4 and p_in4 */
            switch (i) {
            case 0:
                if (pos_x_in4->is_x_routable() && pos_x_in4->is_x_positive()) {
                    pos_x_out4_status = POS_X4;
                    pos_x_in4->set_routed();
                    pos_x_out4_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (p_in4->is_x_routable() && p_in4->is_x_positive()) {
                    pos_x_out4_status = P_IN4;
                    p_in4->set_routed();
                    pos_x_out4_last = 1;
                    break;
                }
            }

            if (i == 0) break;
            else       i = 0;
        }

        start = neg_x_out3_last;
        i = (start + 1) % 2;
        while (neg_x_out3_status == IDLE) {
            /* look for packet at neg_x_in3 and p_in3 */
            switch (i) {
            case 0:
                if (neg_x_in3->is_x_routable() && neg_x_in3->is_x_negative()) {
                    neg_x_out3_status = NEG_X3;
                    neg_x_in3->set_routed();
                    neg_x_out3_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (p_in3->is_x_routable() && p_in3->is_x_negative()) {
                    neg_x_out3_status = P_IN3;
                    p_in3->set_routed();
                    neg_x_out3_last = 1;
                    break;
                }
            }

            if (i == 0) break;
            else       i = 0;
        }

        start = neg_x_out4_last;
        i = (start + 1) % 2;
        while (neg_x_out4_status == IDLE) {
            /* look for packet at neg_x_in4 and p_in4 */
            switch (i) {
            case 0:
                if (neg_x_in4->is_x_routable() && neg_x_in4->is_x_negative()) {
                    neg_x_out4_status = NEG_X4;
                    neg_x_in4->set_routed();
                    neg_x_out4_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (p_in4->is_x_routable() && p_in4->is_x_negative()) {
                    neg_x_out4_status = P_IN4;
                    p_in4->set_routed();
                    neg_x_out4_last = 1;
                    break;
                }
            }

            if (i == 0) break;
            else       i = 0;
        }

        start = pos_y_out3_last;
        i = (start + 1) % 4;
        while (pos_y_out3_status == IDLE) {
            /* look for packet at pos_y1, pos_x1, neg_x1, or p_in3 */
            switch (i) {
            case 0:
                if (pos_y_in3->is_y_routable() && pos_y_in3->is_y_positive()) {
                    pos_y_out3_status = POS_Y3;
                    pos_y_in3->set_routed();
                    pos_y_out3_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (pos_x_in3->is_y_routable() && pos_x_in3->is_y_positive()) {
                    pos_y_out3_status = POS_X3;
                    pos_x_in3->set_routed();
                    pos_y_out3_last = 1;
                    break;
                }
                if (start == 1) break;
            case 2:
                if (neg_x_in3->is_y_routable() && neg_x_in3->is_y_positive()) {
                    pos_y_out3_status = NEG_X3;
                    neg_x_in3->set_routed();
                    pos_y_out3_last = 2;
                    break;
                }
                if (start == 2) break;
            case 3:
                if (p_in3->is_y_routable() && p_in3->is_y_positive()) {
                    pos_y_out3_status = P_IN3;
                    p_in3->set_routed();
                    pos_y_out3_last = 3;
                    break;
                }
            }

            if (i == 0) break;
            else        i = 0;
        }


        start = pos_y_out4_last;
        i = (start + 1) % 4;
        while (pos_y_out4_status == IDLE) {
            /* look for packet at pos_y2, pos_x2, neg_x2, or p_in4 */
            switch (i) {
            case 0:
                if (pos_y_in4->is_y_routable() && pos_y_in4->is_y_positive()) {
                    pos_y_out4_status = POS_Y4;
                    pos_y_in4->set_routed();
                    pos_y_out4_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (pos_x_in4->is_y_routable() && pos_x_in4->is_y_positive()) {
                    pos_y_out4_status = POS_X4;
                    pos_x_in4->set_routed();
                    pos_y_out4_last = 1;
                    break;
                }
                if (start == 1) break;
            case 2:
                if (neg_x_in4->is_y_routable() && neg_x_in4->is_y_positive()) {
                    pos_y_out4_status = NEG_X4;
                    neg_x_in4->set_routed();
                    pos_y_out4_last = 2;
                    break;
                }
                if (start == 2) break;
            case 3:
                if (p_in4->is_y_routable() && p_in4->is_y_positive()) {
                    pos_y_out4_status = P_IN4;
                    p_in4->set_routed();
                    pos_y_out4_last = 3;
                    break;
                }
            }

            if (i == 0) break;
            else        i = 0;
        }

        start = neg_y_out3_last;
        i = (start + 1) % 4;
        while (neg_y_out3_status == IDLE) {
            /* look for packet at neg_y1, pos_x1, neg_x1, or p_in3 */
            switch (i) {
            case 0:
                if (neg_y_in3->is_y_routable() && neg_y_in3->is_y_negative()) {
                    neg_y_out3_status = NEG_Y3;
                    neg_y_in3->set_routed();
                    neg_y_out3_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (pos_x_in3->is_y_routable() && pos_x_in3->is_y_negative()) {
                    neg_y_out3_status = POS_X3;
                    pos_x_in3->set_routed();
                    neg_y_out3_last = 1;
                    break;
                }
                if (start == 1) break;
            case 2:
                if (neg_x_in3->is_y_routable() && neg_x_in3->is_y_negative()) {
                    neg_y_out3_status = NEG_X3;
                    neg_x_in3->set_routed();
                    neg_y_out3_last = 2;
                    break;
                }
                if (start == 2) break;
            case 3:
                if (p_in3->is_y_routable() && p_in3->is_y_negative()) {
                    neg_y_out3_status = P_IN3;
                    p_in3->set_routed();
                    neg_y_out3_last = 3;
                    break;
                }
            }

            if (i == 0) break;
            else        i = 0;
        }

        start = neg_y_out4_last;
        i = (start + 1) % 4;
        while (neg_y_out4_status == IDLE) {
            /* look for packet at neg_y2, pos_x2, neg_x2, or p_in4 */
            switch (i) {
            case 0:
                if (neg_y_in4->is_y_routable() && neg_y_in4->is_y_negative()) {
                    neg_y_out4_status = NEG_Y4;
                    neg_y_in4->set_routed();
                    neg_y_out4_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (pos_x_in4->is_y_routable() && pos_x_in4->is_y_negative()) {
                    neg_y_out4_status = POS_X4;
                    pos_x_in4->set_routed();
                    neg_y_out4_last = 1;
                    break;
                }
                if (start == 1) break;
            case 2:
                if (neg_x_in4->is_y_routable() && neg_x_in4->is_y_negative()) {
                    neg_y_out4_status = NEG_X4;
                    neg_x_in4->set_routed();
                    neg_y_out4_last = 2;
                    break;
                }
                if (start == 2) break;
            case 3:
                if (p_in4->is_y_routable() && p_in4->is_y_negative()) {
                    neg_y_out4_status = P_IN4;
                    p_in4->set_routed();
                    neg_y_out4_last = 3;
                    break;
                }
            }

            if (i == 0) break;
            else        i = 0;
        }

        start = p_out3_last;
        i = (start + 1) % 5;
        while (p_out3_status == IDLE) {
            /* look for packet at pos_y1, neg_y1, pos_x1, neg_x1, or p_in3 */
            switch (i) {
            case 0:
                if (pos_y_in3->is_y_routable() && pos_y_in3->is_y_zero()) {
                    p_out3_status = POS_Y3;
                    pos_y_in3->set_routed();
                    p_out3_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (neg_y_in3->is_y_routable() && neg_y_in3->is_y_zero()) {
                    p_out3_status = NEG_Y3;
                    neg_y_in3->set_routed();
                    p_out3_last = 1;
                    break;
                }
                if (start == 1) break;
            case 2:
                if (pos_x_in3->is_y_routable() && pos_x_in3->is_y_zero()) {
                    p_out3_status = POS_X3;
                    pos_x_in3->set_routed();
                    p_out3_last = 2;
                    break;
                }
                if (start == 2) break;
            case 3:
                if (neg_x_in3->is_y_routable() && neg_x_in3->is_y_zero()) {
                    p_out3_status = NEG_X3;
                    neg_x_in3->set_routed();
                    p_out3_last = 3;
                    break;
                }
                if (start == 3) break;
            case 4:
                if (p_in3->is_y_routable() && p_in3->is_y_zero()) {
                    p_out3_status = P_IN3;
                    p_in3->set_routed();
                    p_out3_last = 4;
                    break;
                }
            }

            if (i == 0) break;
            else        i = 0;
        }

        start = p_out4_last;
        i = (start + 1) % 5;
        while (p_out4_status == IDLE) {
            /* look for packet at pos_y2, neg_y2, pos_x2, neg_x2, or p_in4 */
            switch (i) {
            case 0:
                if (pos_y_in4->is_y_routable() && pos_y_in4->is_y_zero()) {
                    p_out4_status = POS_Y4;
                    pos_y_in4->set_routed();
                    p_out4_last = 0;
                    break;
                }
                if (start == 0) break;
            case 1:
                if (neg_y_in4->is_y_routable() && neg_y_in4->is_y_zero()) {
                    p_out4_status = NEG_Y4;
                    neg_y_in4->set_routed();
                    p_out4_last = 1;
                    break;
                }
                if (start == 1) break;
            case 2:
                if (pos_x_in4->is_y_routable() && pos_x_in4->is_y_zero()) {
                    p_out4_status = POS_X4;
                    pos_x_in4->set_routed();
                    p_out4_last = 2;
                    break;
                }
                if (start == 2) break;
            case 3:
                if (neg_x_in4->is_y_routable() && neg_x_in4->is_y_zero()) {
                    p_out4_status = NEG_X4;
                    neg_x_in4->set_routed();
                    p_out4_last = 3;
                    break;
                }
                if (start == 3) break;
            case 4:
                if (p_in4->is_y_routable() && p_in4->is_y_zero()) {
                    p_out4_status = P_IN4;
                    p_in4->set_routed();
                    p_out4_last = 4;
                    break;
                }
            }

            if (i == 0) break;
            else        i = 0;
        }
    }
}


/* On each cycle, this function considers each output port, and tries
   to transfer a flit to the output port. */
void router::perform_route_actions()
{
    uint64 flit;
    Stats* stats = Stats::get();
    int phits = conf->phits;
    int sent;

    sent = 0;
    if (pos_x_out2_status != IDLE && !pos_x_out2->is_full() && this->avail_flit(pos_x_out2_status)) {
        while (sent < phits && pos_x_out2_status != IDLE && !pos_x_out2->is_full() && this->avail_flit(pos_x_out2_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_x_out2_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord + 1 << ", " << y_coord << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to positive X out\n", y_coord, x_coord, flit);
#endif
            }
            pos_x_out2->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_x_out2_status);
                pos_x_out2_status = IDLE;
            }
        }
    }
    else if (pos_x_out4_status != IDLE && !pos_x_out4->is_full() && this->avail_flit(pos_x_out4_status)) {
        while (sent < phits && pos_x_out4_status != IDLE && !pos_x_out4->is_full() && this->avail_flit(pos_x_out4_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_x_out4_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord + 1 << ", " << y_coord << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }
                //printf("(%d, %d): moving %#.16llx to positive X out\n", y_coord, x_coord, flit);
#endif
            }
            pos_x_out4->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_x_out4_status);
                pos_x_out4_status = IDLE;
            }
        }
    }
    //sent = 0;
    else if (pos_x_out1_status != IDLE && !pos_x_out1->is_full() && this->avail_flit(pos_x_out1_status)) {
        while (sent < phits && pos_x_out1_status != IDLE && !pos_x_out1->is_full() && this->avail_flit(pos_x_out1_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_x_out1_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET) << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }
                //printf("(%d, %d): moving %#.16llx to positive X out\n", y_coord, x_coord, flit);
#endif
            }
            pos_x_out1->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_x_out1_status);
                pos_x_out1_status = IDLE;
            }
        }
    }
    else if (pos_x_out3_status != IDLE && !pos_x_out3->is_full() && this->avail_flit(pos_x_out3_status)) {
        while (sent < phits && pos_x_out3_status != IDLE && !pos_x_out3->is_full() && this->avail_flit(pos_x_out3_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_x_out3_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord + 1 << ", " << y_coord << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }
                //printf("(%d, %d): moving %#.16llx to positive X out\n", y_coord, x_coord, flit);
#endif
            }
            pos_x_out3->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_x_out3_status);
                pos_x_out3_status = IDLE;
            }
        }
    }

    sent = 0;
    if (neg_x_out2_status != IDLE && !neg_x_out2->is_full() && this->avail_flit(neg_x_out2_status)) {
        while (sent < phits && neg_x_out2_status != IDLE && !neg_x_out2->is_full() && this->avail_flit(neg_x_out2_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_x_out2_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): out 2 moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord - 1 << ", " << y_coord << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }
                //printf("(%d, %d): moving %#.16llx to negative X out\n", y_coord, x_coord, flit);
#endif
            }
            neg_x_out2->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_x_out2_status);
                neg_x_out2_status = IDLE;
            }
        }
    }
    else if (neg_x_out4_status != IDLE && !neg_x_out4->is_full() && this->avail_flit(neg_x_out4_status)) {
        while (sent < phits && neg_x_out4_status != IDLE && !neg_x_out4->is_full() && this->avail_flit(neg_x_out4_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_x_out4_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord - 1 << ", " << y_coord << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }
                //printf("(%d, %d): moving %#.16llx to negative X out\n", y_coord, x_coord, flit);
#endif
            }
            neg_x_out4->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_x_out4_status);
                neg_x_out4_status = IDLE;
            }
        }
    }
    //sent = 0;
    else if (neg_x_out1_status != IDLE && !neg_x_out1->is_full() && this->avail_flit(neg_x_out1_status)) {
        while (sent < phits && neg_x_out1_status != IDLE && !neg_x_out1->is_full() && this->avail_flit(neg_x_out1_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_x_out1_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "):out 1 moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord - 1 << ", " << y_coord << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }
                // printf("(%d, %d): moving %#.16llx to negative X out\n", y_coord, x_coord, flit);
#endif
            }
            neg_x_out1->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_x_out1_status);
                neg_x_out1_status = IDLE;
            }
        }
    }
    else if (neg_x_out3_status != IDLE && !neg_x_out3->is_full() && this->avail_flit(neg_x_out3_status)) {
        while (sent < phits && neg_x_out3_status != IDLE && !neg_x_out3->is_full() && this->avail_flit(neg_x_out3_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_x_out3_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_X_MASK)) {
                flit = decrement_x(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord - 1 << ", " << y_coord << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to negative X out\n", y_coord, x_coord, flit);
#endif
            }
            neg_x_out3->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_x_out3_status);
                neg_x_out3_status = IDLE;
            }
        }
    }


    sent = 0;
    if (pos_y_out2_status != IDLE && !pos_y_out2->is_full() && this->avail_flit(pos_y_out2_status)) {
        while (sent < phits && pos_y_out2_status != IDLE && !pos_y_out2->is_full() && this->avail_flit(pos_y_out2_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_y_out2_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord << ", " << y_coord + 1 << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to positive Y out\n", y_coord, x_coord, flit);
#endif
            }
            pos_y_out2->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_y_out2_status);
                pos_y_out2_status = IDLE;
            }
        }
    }
    else if (pos_y_out4_status != IDLE && !pos_y_out4->is_full() && this->avail_flit(pos_y_out4_status)) {
        while (sent < phits && pos_y_out4_status != IDLE && !pos_y_out4->is_full() && this->avail_flit(pos_y_out4_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_y_out4_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord << ", " << y_coord + 1 << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to positive Y out\n", y_coord, x_coord, flit);
#endif
            }
            pos_y_out4->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_y_out4_status);
                pos_y_out4_status = IDLE;
            }
        }
    }
    //sent = 0;
    else if (pos_y_out1_status != IDLE && !pos_y_out1->is_full() && this->avail_flit(pos_y_out1_status)) {
        while (sent < phits && pos_y_out1_status != IDLE && !pos_y_out1->is_full() && this->avail_flit(pos_y_out1_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_y_out1_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord << ", " << y_coord + 1 << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to positive Y out\n", y_coord, x_coord, flit);
#endif
            }
            pos_y_out1->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_y_out1_status);
                pos_y_out1_status = IDLE;
            }
        }
    }
    else if (pos_y_out3_status != IDLE && !pos_y_out3->is_full() && this->avail_flit(pos_y_out3_status)) {
        while (sent < phits && pos_y_out3_status != IDLE && !pos_y_out3->is_full() && this->avail_flit(pos_y_out3_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(pos_y_out3_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord << ", " << y_coord + 1 << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to positive Y out\n", y_coord, x_coord, flit);
#endif
            }
            pos_y_out3->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(pos_y_out3_status);
                pos_y_out3_status = IDLE;
            }
        }
    }

    sent = 0;
    if (neg_y_out2_status != IDLE && !neg_y_out2->is_full() && this->avail_flit(neg_y_out2_status)) {
        while (sent < phits && neg_y_out2_status != IDLE && !neg_y_out2->is_full() && this->avail_flit(neg_y_out2_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_y_out2_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord << ", " << y_coord - 1 << ")" << "\n";

                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to negative Y out\n", y_coord, x_coord, flit);
#endif
            }
            neg_y_out2->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_y_out2_status);
                neg_y_out2_status = IDLE;
            }
        }
    }
    else if (neg_y_out4_status != IDLE && !neg_y_out4->is_full() && this->avail_flit(neg_y_out4_status)) {
        while (sent < phits && neg_y_out4_status != IDLE && !neg_y_out4->is_full() && this->avail_flit(neg_y_out4_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_y_out4_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET) << "\n";
                    msg << "to" << "(" << x_coord << ", " << y_coord - 1 << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to negative Y out\n", y_coord, x_coord, flit);
#endif
            }
            neg_y_out4->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_y_out4_status);
                neg_y_out4_status = IDLE;
            }
        }
    }
    //sent = 0;
    else if (neg_y_out1_status != IDLE && !neg_y_out1->is_full() && this->avail_flit(neg_y_out1_status)) {
        while (sent < phits && neg_y_out1_status != IDLE && !neg_y_out1->is_full() && this->avail_flit(neg_y_out1_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_y_out1_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET);
                    msg << "to" << "(" << x_coord << ", " << y_coord - 1 << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to negative Y out\n", y_coord, x_coord, flit);
#endif
            }
            neg_y_out1->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_y_out1_status);
                neg_y_out1_status = IDLE;
            }
        }
    }
    else if (neg_y_out3_status != IDLE && !neg_y_out3->is_full() && this->avail_flit(neg_y_out3_status)) {
        while (sent < phits && neg_y_out3_status != IDLE && !neg_y_out3->is_full() && this->avail_flit(neg_y_out3_status)) {
            sent++;
            stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(neg_y_out3_status);
            if ((flit & HEADER_FLIT_MASK) && (flit & DELTA_Y_MASK)) {
                flit = decrement_y(flit);
#if ROUTER_OUT
                if (flit & HEADER_FLIT_MASK) {
                    std::ostringstream msg;
                    msg << "(" << x_coord << ", " << y_coord << "): moving " << ((flit & PACKET_MASK) >> PACKET_OFFSET) << "\n";
                    msg << "to" << "(" << x_coord << ", " << y_coord - 1 << ")" << "\n";
                    Stats::get()->printLog(msg.str(), false);
                }

                //printf("(%d, %d): moving %#.16llx to negative Y out\n", y_coord, x_coord, flit);
#endif
            }
            neg_y_out3->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(neg_y_out3_status);
                neg_y_out3_status = IDLE;
            }
        }
    }


    sent = 0;
    if (p_out2_status != IDLE && !p_out2->is_full() && this->avail_flit(p_out2_status)) {
        while (sent < phits && p_out2_status != IDLE && !p_out2->is_full() && this->avail_flit(p_out2_status)) {
            sent++;
            //stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(p_out2_status);
#if ROUTER_OUT
            if (flit & HEADER_FLIT_MASK) {
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving to P out 2 " << ((flit & PACKET_MASK) >> PACKET_OFFSET) << "\n";
                Stats::get()->printLog(msg.str(), false);
            }

            if (flit & HEADER_FLIT_MASK) {
                //printf("(%d, %d): moving %#.16llx to P out\n", y_coord, x_coord, flit);
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving ";
                msg << "0x" << setfill('0') << setw(16) << hex << flit << " to P out 2\n";
                Stats::get()->printLog(msg.str(), false);
            }
#endif
            p_out2->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(p_out2_status);
                p_out2_status = IDLE;
            }
        }
    }
    else if (p_out4_status != IDLE && !p_out4->is_full() && this->avail_flit(p_out4_status)) {
        while (sent < phits && p_out4_status != IDLE && !p_out4->is_full() && this->avail_flit(p_out4_status)) {
            sent++;
            //stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(p_out4_status);
#if ROUTER_OUT
            if (flit & HEADER_FLIT_MASK) {
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving to P out 4 " << ((flit & PACKET_MASK) >> PACKET_OFFSET) << "\n";
                Stats::get()->printLog(msg.str(), false);
            }

            if (flit & HEADER_FLIT_MASK) {
                //printf("(%d, %d): moving %#.16llx to P out\n", y_coord, x_coord, flit);
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving ";
                msg << "0x" << setfill('0') << setw(16) << hex << flit << " to P out 4\n";

                Stats::get()->printLog(msg.str(), false);
            }
#endif
            p_out4->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(p_out4_status);
                p_out4_status = IDLE;
            }
        }
    }
    //sent = 0;
    else if (p_out1_status != IDLE && !p_out1->is_full() && this->avail_flit(p_out1_status)) {
        while (sent < phits && p_out1_status != IDLE && !p_out1->is_full() && this->avail_flit(p_out1_status)) {
            sent++;
            //stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(p_out1_status);
#if ROUTER_OUT
            if (flit & HEADER_FLIT_MASK) {
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving to P out 1 " << ((flit & PACKET_MASK) >> PACKET_OFFSET) << "\n";
                Stats::get()->printLog(msg.str(), false);
            }

            if (flit & HEADER_FLIT_MASK) {
                //printf("(%d, %d): moving %#.16llx to P out\n", y_coord, x_coord, flit);
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving ";
                msg << "0x" << setfill('0') << setw(16) << hex << flit << " to P out 1\n";

                Stats::get()->printLog(msg.str(), false);
            }
#endif
            p_out1->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(p_out1_status);
                p_out1_status = IDLE;
            }
        }
    }
    else if (p_out3_status != IDLE && !p_out3->is_full() && this->avail_flit(p_out3_status)) {
        while (sent < phits && p_out3_status != IDLE && !p_out3->is_full() && this->avail_flit(p_out3_status)) {
            sent++;
            //stats->tile_flit_jumps[IND(x_coord, y_coord)]++;
            flit = this->get_flit(p_out3_status);
#if ROUTER_OUT
            if (flit & HEADER_FLIT_MASK) {
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving to Pout 3 " << ((flit & PACKET_MASK) >> PACKET_OFFSET) << "\n";
                Stats::get()->printLog(msg.str(), false);
            }

            if (flit & HEADER_FLIT_MASK) {
                //printf("(%d, %d): moving %#.16llx to P out\n", y_coord, x_coord, flit);
                std::ostringstream msg;
                msg << "(" << x_coord << ", " << y_coord << "): moving ";
                msg << "0x" << setfill('0') << setw(16) << hex << flit << " to P out 3\n";

                Stats::get()->printLog(msg.str(), false);
            }
#endif
            p_out3->insert_at_tail(flit);
            if (flit & TAIL_FLIT_MASK) {
                this->clear_routed(p_out3_status);
                p_out3_status = IDLE;
            }
        }
    }

}


uint64 router::get_flit(route_action from_buffer)
{
    uint64 flit;

    switch (from_buffer) {
    case P_IN1:
        flit = p_in1->remove_from_head();
        break;
    case P_IN2:
        flit = p_in2->remove_from_head();
        break;
    case POS_X1:
        flit = pos_x_in1->remove_from_head();
        break;
    case POS_X2:
        flit = pos_x_in2->remove_from_head();
        break;
    case NEG_X1:
        flit = neg_x_in1->remove_from_head();
        break;
    case NEG_X2:
        flit = neg_x_in2->remove_from_head();
        break;
    case POS_Y1:
        flit = pos_y_in1->remove_from_head();
        break;
    case POS_Y2:
        flit = pos_y_in2->remove_from_head();
        break;
    case NEG_Y1:
        flit = neg_y_in1->remove_from_head();
        break;
    case NEG_Y2:
        flit = neg_y_in2->remove_from_head();
        break;
    case P_IN3:
        flit = p_in3->remove_from_head();
        break;
    case P_IN4:
        flit = p_in4->remove_from_head();
        break;
    case POS_X3:
        flit = pos_x_in3->remove_from_head();
        break;
    case POS_X4:
        flit = pos_x_in4->remove_from_head();
        break;
    case NEG_X3:
        flit = neg_x_in3->remove_from_head();
        break;
    case NEG_X4:
        flit = neg_x_in4->remove_from_head();
        break;
    case POS_Y3:
        flit = pos_y_in3->remove_from_head();
        break;
    case POS_Y4:
        flit = pos_y_in4->remove_from_head();
        break;
    case NEG_Y3:
        flit = neg_y_in3->remove_from_head();
        break;
    case NEG_Y4:
        flit = neg_y_in4->remove_from_head();
        break;
    default:
        std::stringstream msg;
        msg << "Error: (" << x_coord << ", " << y_coord << "): Invalid router buffer\n";
        throw std::invalid_argument(msg.str());
    }

    return (flit);
}

int router::avail_flit(route_action from_buffer)
{
    switch (from_buffer) {
    case P_IN1:
        return !p_in1->is_empty();
        break;
    case P_IN2:
        return !p_in2->is_empty();
        break;
    case POS_X1:
        return !pos_x_in1->is_empty();
        break;
    case POS_X2:
        return !pos_x_in2->is_empty();
        break;
    case NEG_X1:
        return !neg_x_in1->is_empty();
        break;
    case NEG_X2:
        return !neg_x_in2->is_empty();
        break;
    case POS_Y1:
        return !pos_y_in1->is_empty();
        break;
    case POS_Y2:
        return !pos_y_in2->is_empty();
        break;
    case NEG_Y1:
        return !neg_y_in1->is_empty();
        break;
    case NEG_Y2:
        return !neg_y_in2->is_empty();
        break;
    case P_IN3:
        return !p_in3->is_empty();
        break;
    case P_IN4:
        return !p_in4->is_empty();
        break;
    case POS_X3:
        return !pos_x_in3->is_empty();
        break;
    case POS_X4:
        return !pos_x_in4->is_empty();
        break;
    case NEG_X3:
        return !neg_x_in3->is_empty();
        break;
    case NEG_X4:
        return !neg_x_in4->is_empty();
        break;
    case POS_Y3:
        return !pos_y_in3->is_empty();
        break;
    case POS_Y4:
        return !pos_y_in4->is_empty();
        break;
    case NEG_Y3:
        return !neg_y_in3->is_empty();
        break;
    case NEG_Y4:
        return !neg_y_in4->is_empty();
        break;
    default:
        std::stringstream msg;
        msg << "Error: (" << x_coord << ", " << y_coord << "): Invalid router buffer\n";
        throw std::invalid_argument(msg.str());
    }

    return 0;
}

void router::clear_routed(route_action buffer)
{
    switch (buffer) {
    case P_IN1:
        p_in1->clear_routed();
        break;
    case POS_X1:
        pos_x_in1->clear_routed();
        break;
    case NEG_X1:
        neg_x_in1->clear_routed();
        break;
    case POS_Y1:
        pos_y_in1->clear_routed();
        break;
    case NEG_Y1:
        neg_y_in1->clear_routed();
        break;
    case P_IN2:
        p_in2->clear_routed();
        break;
    case POS_X2:
        pos_x_in2->clear_routed();
        break;
    case NEG_X2:
        neg_x_in2->clear_routed();
        break;
    case POS_Y2:
        pos_y_in2->clear_routed();
        break;
    case NEG_Y2:
        neg_y_in2->clear_routed();
        break;
    case P_IN3:
        p_in3->clear_routed();
        break;
    case POS_X3:
        pos_x_in3->clear_routed();
        break;
    case NEG_X3:
        neg_x_in3->clear_routed();
        break;
    case POS_Y3:
        pos_y_in3->clear_routed();
        break;
    case NEG_Y3:
        neg_y_in3->clear_routed();
        break;
    case P_IN4:
        p_in4->clear_routed();
        break;
    case POS_X4:
        pos_x_in4->clear_routed();
        break;
    case NEG_X4:
        neg_x_in4->clear_routed();
        break;
    case POS_Y4:
        pos_y_in4->clear_routed();
        break;
    case NEG_Y4:
        neg_y_in4->clear_routed();
        break;
    default:
        std::stringstream msg;
        msg << "Error: (" << x_coord << ", " << y_coord << "): Invalid router buffer\n";
        throw std::invalid_argument(msg.str());
    }
}
