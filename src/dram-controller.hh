#pragma once

/*
  dram-controller.hh

  Candace Walden
  01 September 2019

  Interfaces with the DRAMSim dram controller
 */

class dram_controller : virtual public ram_controller
{
private:
    dramsim3::Controller *channel;
    void create_controller();
    void check_controller(long long cur_cycle);

    dramsim3::Transaction trans;
    bool sendTrans;
    void route_in(long long cur_cycle) override;

public:
    dram_controller(int x, int y);

    void cycle(long long cur_cycle) override;

    void collect_stats() override;
    void PrintEpochStats() override { channel->PrintEpochStats(); };
    void PrintFinalStats() override;
};
