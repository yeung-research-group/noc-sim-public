#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

cacheL1::cacheL1(int x, int y, buffer *out)
{
  conf = Conf_ReRAM::get();
  stats = Stats::get();
  input_buffer = new buffer(conf->buffer_size);
  backdoor_out = out;
  output_buffer = nullptr;

  x_coord = x;
  y_coord = y;
  x_extent = conf->x_max;
  y_extent = conf->y_max;

  am_packet = 0;

  if(conf->max_simd_width == 8){
      vector_entry_size = 3;
      vector_cache_mask = 0x7;
      vector_entry_size_bytes = 8;
  }
  else{ //SIMD_WIDTH == 16
      vector_entry_size = 2;
      vector_cache_mask = 0xf;
      vector_entry_size_bytes = 4;
  }

   make_cache();

}

void cacheL1::set_buffer_link(buffer *my_buffer)
{
  output_buffer = my_buffer;
}

void cacheL1::make_cache(){
  LRU_head = new Pcache_line[conf->l1_settings.n_sets];
  LRU_tail = new Pcache_line[conf->l1_settings.n_sets];
  set_contents = new unsigned[conf->l1_settings.n_sets];
  for (unsigned i = 0; i < conf->l1_settings.n_sets; i++) {
    LRU_head[i] = (Pcache_line)NULL;
    LRU_tail[i] = (Pcache_line)NULL;
    set_contents[i] = 0;
  }

  contents = 0;
  L1_stat.accesses = 0;
  L1_stat.misses = 0;
  L1_stat.replacements = 0;
  L1_stat.demand_fetches = 0;
  L1_stat.copies_back = 0;
}

//Similar to the perform access function of the original cache model.
int cacheL1::hit(uint64 address,int rw, long long cur_cycle,int vector, int sg )
{

  //TODO: Fix
  int hm;
  wb_entry wb;
  if(rw)
    hm = perform_cache_access(address,store, vector, sg);
  else
    hm = perform_cache_access(address,load, vector, sg);
#if CACHE_OUT_L1
  //if(x_coord==1 && y_coord==1)
    //printf("(%d,%d) Tile had a %d for address - 0x%016llx\n",x_coord,y_coord,hm, address);
#endif 
  //if(!wb_queue.empty()){
  while(!wb_queue.empty()){
    
    wb = wb_queue.front();
    if(write_request(cur_cycle,wb.addr,wb.vec,0, 1, -1)){
      wb_queue.pop_front();  
      stats->tile_l1_stats[IND(x_coord, y_coord)].wb_from_l1++;
      

#if CACHE_OUT_L1
    if(x_coord==1 && y_coord==1)
    
      printf("(%d,%d) Tile Issueing writeback for addr - 0x%016llx from cache block - %d on cycle -  %lld\n",x_coord,y_coord,wb.addr,((wb.addr & index_mask) >>conf->l1.index_mask_offset),cur_cycle);
#endif
      break;
    }
    else{  //Buffer full so try later.
      break;
    }
    
  }

  if (hm)
    stats->tile_l1_stats[IND(x_coord, y_coord)].l1_hit++;
  else
    stats->tile_l1_stats[IND(x_coord, y_coord)].l1_miss++;

  return hm;
 // return random() % 2;
  //return 0;
}
int cacheL1::perform_cache_access(uint64 addr, cache_access access_type, int vector, int sg) {

    Pcache_line evict;
    wb_entry wb;

    if ((access_type == store) && (!conf->l1_settings.cache_writeback))
        CACHE_WRITETHROUGH(&L1_stat);

    CACHE_ACCESS(&L1_stat);
    if (hit_miss(addr, access_type, vector, sg))
        return TRUE;
    CACHE_MISS(&L1_stat);

    if (!((access_type == store) && (!conf->l1_settings.cache_writealloc))) 
    {
        /* allocate the block */
        CACHE_FETCH_BLOCK(&L1_stat);
        evict = probe(addr, access_type, vector, sg);
        if (evict) 
        {
            CACHE_REPLACE(&L1_stat);
            if (evict->dirty) 
            {
                uint64 evict_addr;
                bool wb_vec_check = true;
                for (unsigned i = 0; i < conf->l1_settings.n_sectors; i++) 
                {
                    wb_vec_check = wb_vec_check & evict->valid[i];
                }
                if (wb_vec_check && conf->vector_cache_block_fetch) 
                {
                    evict_addr = (evict->tag) << conf->l1_settings.index_mask_offset;
                    wb.addr = evict_addr;
                    wb.vec = TRUE;
                    wb_queue.push_back(wb);
                }
                else 
                {
                    for (unsigned i = 0; i < conf->l1_settings.n_sectors; i++) 
                    {
                        if (evict->valid[i]) 
                        {
                            evict_addr = (evict->tag) << conf->l1_settings.index_mask_offset | (i << conf->l1_settings.sector_mask_offset);
                            wb.addr = evict_addr;
                            wb.vec = FALSE;
                            wb_queue.push_back(wb);
                        }
                    }
                }
                CACHE_WRITEBACK(&L1_stat);
            }
            delete[] evict->valid;
            free(evict);
        }
    }
    else 
    {
        if (conf->l1_settings.cache_writeback)
            CACHE_WRITETHROUGH(&L1_stat);
    }
    return FALSE; //TODO- check
}


/* Given an address, <addr>, this routine probes the
   cache to see whether the data is contained in the cache.  If a miss
   occurs, the routine will return FALSE.  If a hit occurs, the
   routine will return true, and will also perform the access by
   updating the LRU list.  */
int cacheL1::hit_miss(uint64 addr,int access_type, int vector, int sg)
{
  uint64 index;
  int hit,sector;
  Pcache_line cache_line;
  uint64 addr_tag;

  addr_tag = addr >> conf->l1_settings.index_mask_offset;
  index = (addr & conf->l1_settings.index_mask) >> conf->l1_settings.index_mask_offset;
  sector = (addr & conf->l1_settings.sector_mask) >> conf->l1_settings.sector_mask_offset;
  cache_line = LRU_head[index];
  hit = FALSE;
  while (cache_line) {
    if (cache_line->tag == addr_tag && (((vector == FALSE)&&(cache_line->valid[sector] == TRUE)) || ((vector==TRUE)&&(cache_block_valid_check(cache_line))))) {
      /* "touch" the cache line */
      if ((access_type == store) && (conf->l1_settings.cache_writeback))
        cache_line->dirty = TRUE;
      delete_entry(&(LRU_head[index]), &(LRU_tail[index]),
         cache_line);
      insert(&(LRU_head[index]), &(LRU_tail[index]),
         cache_line);

      hit = TRUE;
      break;
    }
    cache_line = cache_line->LRU_next;
  }

  return(hit);
}

//Checking if all the sectors of the cache block are valid for vector reads and writes
int cacheL1::cache_block_valid_check(Pcache_line cache_line) 
{
    int valid = TRUE;
    for (unsigned i = 0; i < conf->l1_settings.n_sectors; i++) 
    {
        if (cache_line->valid[i] == FALSE) 
        {
            valid = FALSE;
            break;
        }
    }
    return valid;
}

/* This routine places the cache line associated with <addr> in the
   cache, and returns any cache line that was evicted as a result
   of the probe.  */
cacheL1::Pcache_line cacheL1::probe(uint64 addr, int access_type, int vector, int sg)
{
    uint64 index;
    Pcache_line new_line, evict;
    uint64 addr_tag;
    unsigned sector;

    addr_tag = addr >> conf->l1_settings.index_mask_offset;
    new_line = make_cache_line(addr_tag);
    if ((access_type == store) && (conf->l1_settings.cache_writeback)) 
    {
        new_line->dirty = TRUE;
    }
    evict = (Pcache_line)NULL;

    index = (addr & conf->l1_settings.index_mask) >> conf->l1_settings.index_mask_offset;
    sector = (addr & conf->l1_settings.sector_mask) >> conf->l1_settings.sector_mask_offset;
    if (vector || 
        (!vector && !sg && conf->scalar_cache_block_fetch && (access_type == load)) || 
        (!vector && sg && conf->sg_cache_block_fetch && (access_type == load))) 
    {
        unsigned idx;
        if (access_type == store)
            idx = sector; //Since processor can start writing in the middle of the block. Only validate what is written
        else
            idx = 0;

        for (; idx < conf->l1_settings.n_sectors; idx++) 
        {
            new_line->valid[idx] = TRUE;
        }
    }
    else
        new_line->valid[sector] = TRUE;

    insert(&(LRU_head[index]), &(LRU_tail[index]), new_line);
    set_contents[index]++;
    contents++;

    /* evict if necessary */
    if (set_contents[index] > conf->l1_settings.assoc) 
    {
        evict = LRU_tail[index];
        delete_entry(&(LRU_head[index]), &(LRU_tail[index]), evict);
        set_contents[index]--;
        contents--;
    }
    return evict;

}

int cacheL1::flush_cache() {

    unsigned i, j;
    Pcache_line current;
    uint64 evict_addr;
    wb_entry wb;
    bool wb_vec_check;

    for (i = 0; i < conf->l1_settings.n_sets; i++) 
    {
        for (j = 0; j < set_contents[i]; j++) 
        {
            current = LRU_head[i];
            LRU_head[i] = current->LRU_next;

            if (current->dirty)
                wb_vec_check = TRUE;
            for (unsigned k = 0; k < conf->l1_settings.n_sectors; k++) 
            {
                wb_vec_check = wb_vec_check & current->valid[k];
            }
            if (wb_vec_check) 
            {
                evict_addr = (current->tag) << conf->l1_settings.index_mask_offset;
                wb.addr = evict_addr;
                wb.vec = FALSE;
                wb_queue.push_back(wb);

            }
            else 
            {
                for (unsigned k = 0; k < conf->l1_settings.n_sectors; k++)
                {
                    if (current->valid[k]) 
                    {
                        evict_addr = (current->tag) << conf->l1_settings.index_mask_offset | (k << conf->l1_settings.sector_mask_offset);
                        wb.addr = evict_addr;
                        wb.vec = FALSE;
                        wb_queue.push_back(wb);
                    }
                }
            }
            CACHE_WRITEBACK(&L1_stat);
            // TODO add writebacks;
            delete[] current->valid;
            free(current);
        }
        set_contents[i] = 0;
        LRU_tail[i] = (Pcache_line)NULL;
    }

    return conf->l1_settings.lines / conf->l1_settings.block_size;
}

//To check if all the writebacks are flushed in case of a synchronization barrier.
int cacheL1::is_wb_queue_empty(){
    return wb_queue.empty();
}

cacheL1::Pcache_line cacheL1::make_cache_line(uint64 tag)
  //unsigned tag;
{

  Pcache_line answer;
  answer = (Pcache_line)malloc(sizeof(cache_line));
  answer->tag = tag;
  answer->dirty = FALSE;
  answer->LRU_next = (Pcache_line)NULL;
  answer->LRU_prev = (Pcache_line)NULL;
  answer->valid = new int[conf->l1_settings.n_sectors];
  for(unsigned i=0; i< conf->l1_settings.n_sectors; i++){
    answer->valid[i] = 0;
  }
  return(answer);
}


void cacheL1::delete_entry(Pcache_line *head, Pcache_line *tail, Pcache_line item)
{
  if (item->LRU_prev) {
    item->LRU_prev->LRU_next = item->LRU_next;
  } else {
    /* item at head */
    *head = item->LRU_next;
  }

  if (item->LRU_next) {
    item->LRU_next->LRU_prev = item->LRU_prev;
  } else {
    /* item at tail */
    *tail = item->LRU_prev;
  }
}

/* inserts at the head of the list */
void cacheL1::insert(Pcache_line *head, Pcache_line *tail,Pcache_line item)
{
  item->LRU_next = *head;
  item->LRU_prev = (Pcache_line)NULL;

  if (item->LRU_next)
    item->LRU_next->LRU_prev = item;
  else
    *tail = item;

  *head = item;
}


int cacheL1::write_request(long long cur_cycle, uint64 address, int vector, int sg, int wb, uint64 PC)
{
  uint64 header_flit, flit;
  int delta_x, delta_y;
  int reqs = 0;
  unsigned packet_id;
  int fetch_width;

  //TODO: Currently we assume in heterogenous systems all requests are initially to ReRAM -- if we change that assumption, this needs to be updated
  if (conf->ram_mode == dram)
  {
      fetch_width = conf->dram_settings.fetch_width;
      address = address & (conf->dram_settings.fw_mask);
  }
  else
    fetch_width = conf->reram_settings.fetch_width;
  

  vector = (vector || (!sg && !wb && conf->scalar_cache_block_fetch) || (sg && conf->sg_cache_block_fetch));

  if (vector)
  {
    if (output_buffer->free_entries() < (8 + 2))
      return reqs;  //not enough room

    //reqs = conf->SIMD_WIDTH - ((address >> vector_entry_size) & vector_cache_mask); //TODO - DRAM_VECTOR
    reqs = (conf->l1_settings.block_size/vector_entry_size_bytes) - ((address >> vector_entry_size) & vector_cache_mask); //Total entrees fetched - Useless entrees(the one's before the starting address of vector instruction)
    address = address & (~0x3F);
  }
  else
  {
    if(output_buffer->free_entries() < (fetch_width + 2))
        return reqs;  //not enough room

    reqs = 1;
  }

  packet_id = stats->set_start(write_packet, cur_cycle, x_coord, y_coord, address, vector, PC);
  address_translation(address, &delta_x, &delta_y);
  //if((x_coord+delta_x == 16) &&( y_coord+delta_y==0) && (bank == 78))
    //printf("Writing (16,0,78) from (%d,%d) with addr - 0x%016llx at cycle - %lld\n",x_coord,y_coord,address,cur_cycle);

  //Header Flit
  header_flit = WRITE_FLIT_MASK;
  header_flit = make_header(header_flit, delta_x, delta_y, packet_id);
  header_flit |= vector;
  output_buffer->insert_at_tail(header_flit); //p1 - Header

  //Address Flit
  flit = address >> WORD_SIZE_OFFSET;
  output_buffer->insert_at_tail(flit); //p2 - Address

  //Data Flits
  flit = 0;
  if (vector)
    for (int i = 0; i < 7; i++)
      output_buffer->insert_at_tail(flit);
  else
    for (int i = 0; i < (fetch_width - 1); i++)
      output_buffer->insert_at_tail(flit);
  flit |= TAIL_FLIT_MASK;
  output_buffer->insert_at_tail(flit); //p3 - dummy data with tail

  stats->tile_l1_stats[IND(x_coord, y_coord)].wr_exec++;
#if CPU_OUT
  //printf("(%d, %d): CPU sent write packet %d to (%d, %d)!\n", y_coord, x_coord, packet_id, delta_y + y_coord, delta_x + x_coord);

      std::ostringstream msg;
      msg << "(" << x_coord << ", " << y_coord << "): CPU sent write packet " << packet_id << " to (" << x_coord + delta_x << ", " << y_coord + delta_y << ")\n";	
      Stats::get()->printLog(msg.str(), false);
#endif

  return reqs;
}

//Assume in heterogenous systems are requests are initially to ReRAM
int cacheL1::read_request(long long cur_cycle, uint64 address, int vector,int sg, unsigned *packet_id, uint64 PC)
{
  uint64 header_flit, flit;
  int delta_x, delta_y;
  int reqs;
  reqs = 0;
  if (output_buffer->free_entries() < (2)) return 0;  //not enough room

  if (address == 0) {
    //printf("DEADLOCK (%d,%d)in L1 addr = 0\n",x_coord,y_coord);  
    printf("Buffer size not enough at (%d,%d)in L1\n",x_coord,y_coord);  
    return -1; //TODO - Take care of it in traffic_queue
  }

  vector = (vector || (!sg && conf->scalar_cache_block_fetch) || (sg && conf->sg_cache_block_fetch));
  if (conf->ram_mode == dram && !vector)
  {
      address = address & (conf->dram_settings.fw_mask);
      reqs = 1;
  }
  else if (vector)
  {
    //reqs = conf->SIMD_WIDTH - ((address >> vector_entry_size) & vector_cache_mask); //TODO - DRAM_VECTOR
    reqs = (conf->l1_settings.block_size/vector_entry_size_bytes) - ((address >> vector_entry_size) & vector_cache_mask); //Total entrees fetched - Useless entrees(the one's before the starting address of vector instruction)
    address = address & (~0x3F);
  }
  else
  {
    reqs = 1;
  }

  *packet_id = stats->set_start(read_packet, cur_cycle, x_coord, y_coord, address, vector, PC);
  address_translation(address, &delta_x, &delta_y);
  //if((x_coord+delta_x == 16) &&( y_coord+delta_y==0) && (bank == 78))
    //printf("Accessing (16,0,78) from (%d,%d) with addr - 0x%016llx at cycle - %lld\n",x_coord,y_coord,address,cur_cycle);

  //Header Flit
  header_flit = READ_FLIT_MASK;
  header_flit = make_header(header_flit, delta_x, delta_y, *packet_id);
  header_flit |= vector;
  output_buffer->insert_at_tail(header_flit); //p1 - Header

  //Address Flit
  flit = address >> WORD_SIZE_OFFSET;
  flit |= TAIL_FLIT_MASK;
  output_buffer->insert_at_tail(flit); //p2 - Address

  stats->tile_l1_stats[IND(x_coord, y_coord)].rd_exec++;
#if CPU_OUT
  //printf("(%d, %d): CPU sent read packet %d to (%d, %d)!\n", y_coord, x_coord, packet_id, delta_y + y_coord, delta_x + x_coord);


      std::ostringstream msg;
      msg << "(" << x_coord << ", " << y_coord << "): CPU sent read packet " << *packet_id << " to (" << x_coord + delta_x << ", " << y_coord + delta_y << ") address: ";	
      msg << "0x" << setfill('0') << setw(16) << hex << address << " at cycle: "<< dec <<cur_cycle << "\n";
      Stats::get()->printLog(msg.str(), false);
#endif
  return reqs;
}


uint64 cacheL1::make_header(uint64 header_flit, int delta_x, int delta_y, unsigned packet_id)
{
  uint64 flit = 0;
  if (delta_x < 0) {
    flit = DIRECTION_X_MASK >> X_OFFSET;
    delta_x = -delta_x;
  }
  flit = flit | delta_x;
  header_flit = header_flit | (flit << X_OFFSET);

  flit = 0;
  if (delta_y < 0) {
    flit = DIRECTION_Y_MASK >> Y_OFFSET;
    delta_y = -delta_y;
  }
  flit = flit | delta_y;
  header_flit = header_flit | (flit << Y_OFFSET);
  header_flit |= ((uint64)packet_id) << PACKET_OFFSET;

  return header_flit;
}

void cacheL1::cycle(long long cur_cycle)
{
    unsigned packet_id;
    wb_entry wb;

    if (!wb_queue.empty())
    {
        //while(!wb_queue.empty()){
        wb = wb_queue.front();
        if (write_request(cur_cycle, wb.addr, wb.vec, 0, 1, -1))
        {
            wb_queue.pop_front();
            stats->tile_l1_stats[IND(x_coord, y_coord)].wb_from_l1++;

#if CACHE_OUT_L1
            if (x_coord == 1 && y_coord == 1)
                printf("(%d,%d) Tile Issueing writeback for addr - 0x%016llx from cache block - %d on cycle -  %lld\n", 
                    x_coord, y_coord, wb.addr, ((wb.addr & index_mask) >> conf->l1.index_mask_offset), cur_cycle);
#endif
        }
    }

    while (!backdoor_out->is_full() && !input_buffer->is_empty())
    {
        //TODO: Add something to insert the data into the cache
        uint64 flit = input_buffer->remove_from_head();
        if (flit & WRITE_FLIT_MASK || flit & READ_FLIT_MASK)
        {
            if (flit & REPLY_FLIT_MASK)
            {
                am_packet = 1;
            }
            else
            {
                std::stringstream errMsg;
                errMsg << "Error: (" << y_coord << ", " << x_coord << "): Read or Write packet " << flit << " at CPU";
                throw std::invalid_argument(errMsg.str());
            }
        }

        if (flit & REPLY_FLIT_MASK)
        {
            packet_id = PACKET_ID(flit);
            backdoor_out->insert_at_tail(packet_id);
            stats->set_to_cpu(packet_id, cur_cycle, x_coord, y_coord);
#if CPU_OUT
            //printf("(%d, %d): CPU received packet %d!\n", y_coord, x_coord, packet_id);

            std::ostringstream msg;
            msg << "(" << x_coord << ", " << y_coord << "): CPU received packet " << packet_id << "at cycle: " << cur_cycle << "\n";
            Stats::get()->printLog(msg.str(), false);
#endif
        }

        if (am_packet && (flit & TAIL_FLIT_MASK))
        {
            unsigned data = flit & 0xFFFFFFFF;
            backdoor_out->insert_at_tail(data);
            am_packet = 0;
        }
    }
}

void cacheL1::address_translation(uint64 addr, int* delta_x, int* delta_y)
{
  if (conf->addr_mode == random_addr){
    get_random_Address(delta_x, delta_y);
  }
  else if (conf->addr_mode == local_addr)
  {
    get_local_Address(addr, delta_x, delta_y);
  }
  else
  {
    get_mapped_Address(addr, delta_x, delta_y);
  }
}

void cacheL1::get_random_Address(int* delta_x, int* delta_y)
{
  int value = random() % conf->cpu_tiles;
  int x, y;

  if(conf->ram_mode == l2_reram || conf->ram_mode == het_l2_reram)  //l2 is only reram tiles
    conf->nth_tile_type(value, reram_tile, &x, &y);
  else //l2 is on compute tiles
    conf->nth_tile_type(value, compute, &x, &y);

  *delta_x = x - x_coord;
  *delta_y = y - y_coord;
}

void cacheL1::get_local_Address(uint64 addr, int* delta_x, int* delta_y) {
  //go to closest controller
  if (conf->ram_mode == l2_reram || conf->ram_mode == het_l2_reram)
  {
    int x, y;
    conf->local_tile(x_coord, y_coord, reram_tile, &x, &y);
    *delta_x = x - x_coord;
    *delta_y = y - y_coord;
  }
  else  //have L2 on compute tile
  {
    *delta_x = 0;
    *delta_y = 0;
  }
}

/*
Given a 64-bit byte address, maps to a particular bank and updates the values of delta_x, delta_y
*/
void cacheL1::get_mapped_Address(uint64 addr, int* delta_x, int* delta_y) {
  int x, y, n;

  addr = addr >> WORD_SIZE_OFFSET;  //word address
  n = TILE_ID(addr);
  
  //L2 with ReRAM
  if (conf->ram_mode == l2_reram || conf->ram_mode == het_l2_reram)
  {
    conf->nth_tile_type(n, reram_tile, &x, &y);
  }
  //L2 with CPU
  else
  {
    conf->nth_tile_type(n, compute, &x, &y);
  }

  *delta_x = x - x_coord;
  *delta_y = y - y_coord;
}
