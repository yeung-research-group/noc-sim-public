#pragma once
/*
  config.hh

  Candace Walden
  22 August 2017

  Config for all parameters
*/

#include <string>
#include <vector>
#include <algorithm>
using namespace std;

#ifndef uint64
  #define uint64 uint64_t
#endif

enum page_policy { perfect_page = 0, closed_page = 1, open_page = 2 };
enum simulation_coupling { reram_cpu = 0, dram = 1, l2_cpu = 2, l2_reram = 3, het_l2_reram = 4 };  //TODO: make these all work
enum addr_modes { tile_first = 0, bank_first = 1, random_addr = 2, local_addr = 3 };
enum ram_distrib { stripe = 0, half_chip = 1 };
enum tile_type { compute = 0, dram_tile = 1, reram_tile = 2 };

struct coord{
  int x;
  int y;
  int t;
};

struct ram_char {
  bool dram;
  unsigned banks;
  unsigned fetch_width;
  uint64 fw_mask;
  int read_lat;
  int write_lat;
  int row_open_latency;
  int row_close_latency;
  page_policy ram_page_policy;
  unsigned queue_depth;
  float read_energy;
  float write_energy;
  float refresh_power;
  unsigned vector_size;
};

struct cache_char {
    //settings
    unsigned cache_lat;
    unsigned size;
    unsigned block_size;
    unsigned assoc;
    unsigned banks;
    int cache_writeback;
    int cache_writealloc;
    unsigned rob_entries;
    bool banked_mshr;
    bool pipelined;
    unsigned unified_mshr_entries;
    unsigned banked_mshr_entries;
    unsigned read_subentries;
    //calculated
    unsigned lines;
    unsigned words_per_block;
    unsigned sector_size;
    unsigned n_sets;
    unsigned n_sectors;
    unsigned n_subbanks;
    uint64 index_mask;
    int index_mask_offset;
    uint64 sector_mask;
    int sector_mask_offset;
    uint64 offset_mask;

    int reram_fetch_shift;
    int dram_fetch_shift;
    int main_mem_fetch_shift;
};

class Conf_ReRAM
{
public:
  static Conf_ReRAM* get();
  static Conf_ReRAM* get(const string& config_file_name);
  
  //Sim Settings
  bool trace;
  bool exec;
  int packets;
  simulation_coupling ram_mode;
  addr_modes addr_mode;
  string trace_file;
  string out_file;  
  string sim_title;
  int bucket_size;
  int threads;
  bool thread_zero_empty;

  //NOC Structure
  int x_max;
  int y_max;
  int phits;
  unsigned buffer_size;
  int num_reram_tiles;
  int num_dram_tiles;
  int cpu_tiles;
  bool integrate_cache_rram;

  //Timing
  double cycle_time;
  int cpu_mem_clk_ratio;

  //Core Settings
  int max_threads_per_core;
  int max_simd_width; // can be 8 or 16.
  int context_switch_delay;
  int vector_cache_block_fetch; // Determined whether to fetch 8B at a time or the whole cache block for vector load/store
  int scalar_cache_block_fetch; // Determined whether to fetch 8B or the whole cache block for scalar load/store
  int sg_cache_block_fetch; // Determined whether to fetch 8B or the whole cache block for each scatter gather.

  //Prefetching
  int prefetch;
  int prefetch_depth;
  int prefetch_streams_per_thread;


  //RAM Settings
  ram_char dram_settings;
  ram_char reram_settings;

  dramsim3::Config *dram_conf;
  dramsim3::Timing* dram_timing;

  //Cache Settings
  cache_char l1_settings;
  cache_char l2_settings;

  //Note: we are now assuming DW addressing rather than byte, 
  //so subtract 3 from offsets and shift 3 right for bits
  /* * * * * * * * * *
 * DRAM ADDRESSING
 * assumes
 * 32b fetch  -> 32 channels, 1kb row buffer
 * 64b fetch  -> 16 channels, 2kb row buffer
 * 128b fetch -> 8 channels, 4kb row buffer
 * XORS bank bits with lower bits of row
* * * * * * * * * 
  uint64 col_bits;
  uint64 dram_bank_bits;
  uint64 bank_xor_bits;
  int dram_bank_offset;
  int bank_xor_offset;
  //offset bits are 32B [4,0], 64B [5,0], 128B [6,0]
  //channel bits are 32B[9,5], 64B [9,6], 128B [9,7]
  uint64 offset_bits;
  uint64 channel_bits;
  int channel_offset;*/

  /* * * * * * * * * *
  * ReRAM ADDRESSING
  * assumes
  * 8b fetch & 64b blocks
  * this means the upper bank bits and lower bank bits may be split
  * |   index   | upper bank | tile id | chunk | cache bank | lower bank | offset |
  * |   index   | tile id | chunk | upper bank | cache bank | lower bank | offset |
  * |63         |                                           |5          3|2      0|
  * * * * * * * * * */
  uint64 upper_bank_bits;
  uint64 upper_bank_bits_y;
  uint64 upper_bank_bits_z;
  uint64 cache_bank_bits_y;
  uint64 cache_bank_bits_z;
  uint64 sector_bits;
  uint64 tile_id_bits;
  uint64 chunk_bits;
  uint64 cache_bank_bits;
  int upper_bank_offset;
  int upper_bank_offset_y;
  int upper_bank_offset_z;
  int cache_bank_offset_y;
  int cache_bank_offset_z;
  int sector_offset;
  int n_sector_bits;
  int tile_id_offset;
  int chunk_offset;
  int cache_bank_offset;
  int mshr_tag_offset;
  uint64 row_bits;


  //Power
  float transport_energy;

  //Synthetic Benchmark
  float mpki;
  int micro_bench_simd_width;
  int spatial_reuse;
  int ins_per_mem_access;

  bool is_ram_tile(int x, int y, tile_type type);
  bool is_ram_tile(int x, int y);
  tile_type get_tile_type(int x, int y);
  void nth_tile_type(int n, tile_type type, int *x, int *y);
  void local_tile(int tile_x,int tile_y, tile_type type, int *x, int *y);
  void threads_tile(int threadID, int *x, int *y, int *t);
  int tiles_thread(int x, int y, int t);
  int * num_threads;

private:
  Conf_ReRAM(const std::string& config_file_name);
  static Conf_ReRAM *conf;
  std::vector<int> reram_tiles_list;
  std::vector<int> dram_tiles_list;
  std::vector<int> cpu_tiles_list;
  //coord find_local_tile(int x, int y, int t);
  coord* thread_tile_map;
  int*** tile_thread_map;
  coord* local_dram_tile_map;
  coord* local_reram_tile_map;
  tile_type* ram_tiles;

  void set_up_reram(int chunk_size);
  void set_up_l2cache();
  void set_up_l1cache();
  void set_local_tiles(ram_distrib ram_tile_loc);
  void set_local_dram_tiles();
  void set_local_reram_tiles(ram_distrib ram_tile_loc);
  void map_threads();
  void map_ram_tiles(const string& dram_tile_s, ram_distrib ram_tile_loc);
  void map_full_reram_tiles();
  void map_part_reram_tiles(ram_distrib ram_tile_loc);
  void map_dram_tiles(const std::string& dram_tile_s);
};
