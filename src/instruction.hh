#pragma once

enum ins_type { single_read, single_write, vector_read, vector_write, scatter, gather, end_thread, lock_ins, other };

class instruction
{
  public:
    instruction(const instruction& ins);    
    instruction(ins_type type);
    instruction(ins_type type, uint64 address);  //single/vector read or write
    instruction(ins_type type, uint64 address[], int addresses);  //scatter or gather
    instruction(ins_type type, uint64 address, uint64 pc);  //single read or write
    instruction(ins_type type, uint64 address[], int addresses, uint64 pc, int vector_mask);  //scatter or gather or vector read/write
    ~instruction();
    ins_type type;
    int mem_locs;
    uint64 *address;
    uint64 PC;
    int vector_mask;
};

