
/*
  buffer.cc

  Donald Yeung
  24 July 2017

  Buffer class.  Buffers are on input channels to routers.
 */

#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"

buffer :: buffer(int buffer_size)
{
  the_buffer = new uint64[buffer_size];
  size = buffer_size;
  head = tail = 0;
  routed = FALSE;
}

buffer :: ~buffer()
{
    delete[] the_buffer;
}


int buffer :: is_empty()
{
  if (head == tail)
    return TRUE;
  else
    return FALSE;
}

//room for another flit of X phits
int buffer :: is_full()
{
  if (((tail + 1) % size) == head)
    return TRUE;
  else
    return FALSE;
}

/* returns the number of free entries in the buffer */
int buffer :: free_entries()
{
  if (head == tail) 
    return (size - 1);

  if (head > tail)
    return (head - tail - 1);

  /* otherwise tail > head */
  return ((size - tail - 1) + head);
}

/* returns the number of free entries in the buffer */
int buffer::entries()
{
    if (head == tail)
        return 0;

    if (head < tail)
        return (tail - head);

    /* otherwise tail < head */
    return (size - (head - tail));
}

/* returns true if there is an X header flit at queue head */
int buffer :: is_x_routable()
{
  if (this->is_empty()) return FALSE;
  if (routed) return FALSE;

  if ((the_buffer[head] & HEADER_FLIT_MASK) && (the_buffer[head] & DELTA_X_MASK))
    return TRUE;
  else
    return FALSE;
}

/* returns true if X is 0 */
int buffer :: is_y_routable()
{
  if (this->is_empty()) return FALSE;
  if (routed) return FALSE;

  if ((the_buffer[head] & HEADER_FLIT_MASK) && !(the_buffer[head] & DELTA_X_MASK))
    return TRUE;
  else
    return FALSE;
}


void buffer :: set_routed()
{
  routed = TRUE;
}

void buffer :: clear_routed()
{
  routed = FALSE;
}


/* There is a header flit at head; see if it's routing in the positive
   direction in x dimension */
int buffer :: is_x_positive()
{
  uint64 flit;

  flit = the_buffer[head];

  if (!(flit & DIRECTION_X_MASK) && (flit & DELTA_X_MASK))
    return TRUE;
  else 
    return FALSE;
}

/* There is a header flit at head; see if it's routing in the positive
direction in y dimension */
int buffer::is_y_positive()
{
  uint64 flit;

  flit = the_buffer[head];

  if (!(flit & DIRECTION_Y_MASK) && (flit & DELTA_Y_MASK))
    return TRUE;
  else
    return FALSE;
}


/* There is a header flit at head; see if it's routing in the negative
   direction in x dimension */
int buffer :: is_x_negative()
{
  uint64 flit;

  flit = the_buffer[head];

  if ((flit & DIRECTION_X_MASK) && (flit & DELTA_X_MASK))
    return TRUE;
  else 
    return FALSE;
}

/* There is a header flit at head; see if it's routing in the negative
direction in y dimension */
int buffer::is_y_negative()
{
  uint64 flit;

  flit = the_buffer[head];

  if ((flit & DIRECTION_Y_MASK) && (flit & DELTA_Y_MASK))
    return TRUE;
  else
    return FALSE;
}

/* There is a header flit at head; see if it's done routing in y
dimension */
int buffer::is_y_zero()
{
  uint64 flit;

  flit = the_buffer[head];

  if ((flit & DELTA_Y_MASK) == 0)
    return TRUE;
  else
    return FALSE;
}

uint64 buffer :: remove_from_head()
{
  uint64 flit;

  flit = the_buffer[head];
  head = (head + 1) % size;

  return(flit);
}

uint64 buffer::peak_at_second()
{
  int second = (head + 1) % size;
  return the_buffer[second];
}


void buffer :: insert_at_tail(uint64 flit)
{
  the_buffer[tail] = flit;
  tail = (tail + 1) % size;
}


/* For current encoding, we should just be able to decrement the flit
   because we separate sign and magnitude. */
uint64 decrement_x(uint64 flit)
{
  flit = flit - X_DECREMENT;
  return(flit);
}

/* For current encoding, we should just be able to decrement the flit
because we separate sign and magnitude. */
uint64 decrement_y(uint64 flit)
{
  flit = flit - Y_DECREMENT;
  return(flit);
}
