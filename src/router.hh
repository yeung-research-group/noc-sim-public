#pragma once

/*
  router.hh

  Donald Yeung
  24 July 2017

  Router class simulates 2D e-cube routing.  It models the buffers and
  switch inside one of the NOC routers.  Accounts for contention that
  will occur at a NOC router point.
 */


/* possible routing directions */
enum route_action {
  IDLE,
  P_IN1,
  POS_X1,
  NEG_X1,
  POS_Y1,
  NEG_Y1,
  P_IN2,
  POS_X2,
  NEG_X2,
  POS_Y2,
  NEG_Y2,
  P_IN3,
  POS_X3,
  NEG_X3,
  POS_Y3,
  NEG_Y3,
  P_IN4,
  POS_X4,
  NEG_X4,
  POS_Y4,
  NEG_Y4
};


class router
{
private:
  /* router's position in the network */
  int x_coord;
  int y_coord;

  Conf_ReRAM* conf;

  /* input buffers into this router */
  buffer *p_in1;
  buffer *pos_x_in1;
  buffer *neg_x_in1;
  buffer *pos_y_in1;
  buffer *neg_y_in1;
  buffer *p_in2;
  buffer *pos_x_in2;
  buffer *neg_x_in2;
  buffer *pos_y_in2;
  buffer *neg_y_in2;
  buffer *p_in3;
  buffer *pos_x_in3;
  buffer *neg_x_in3;
  buffer *pos_y_in3;
  buffer *neg_y_in3;
  buffer *p_in4;
  buffer *pos_x_in4;
  buffer *neg_x_in4;
  buffer *pos_y_in4;
  buffer *neg_y_in4;

  /* links (channels) to input buffers at next router */
  buffer *pos_x_out1;
  buffer *neg_x_out1;
  buffer *pos_y_out1;
  buffer *neg_y_out1;
  buffer *p_out1;
  buffer *pos_x_out2;
  buffer *neg_x_out2;
  buffer *pos_y_out2;
  buffer *neg_y_out2;
  buffer *p_out2;
  buffer *pos_x_out3;
  buffer *neg_x_out3;
  buffer *pos_y_out3;
  buffer *neg_y_out3;
  buffer *p_out3;
  buffer *pos_x_out4;
  buffer *neg_x_out4;
  buffer *pos_y_out4;
  buffer *neg_y_out4;
  buffer *p_out4;

  /* actions occurring on each output port */
  route_action pos_x_out1_status;
  route_action neg_x_out1_status;
  route_action pos_y_out1_status;
  route_action neg_y_out1_status;
  route_action p_out1_status;
  route_action pos_x_out2_status;
  route_action neg_x_out2_status;
  route_action pos_y_out2_status;
  route_action neg_y_out2_status;
  route_action p_out2_status;
  route_action pos_x_out3_status;
  route_action neg_x_out3_status;
  route_action pos_y_out3_status;
  route_action neg_y_out3_status;
  route_action p_out3_status;
  route_action pos_x_out4_status;
  route_action neg_x_out4_status;
  route_action pos_y_out4_status;
  route_action neg_y_out4_status;
  route_action p_out4_status;

  int pos_x_out1_last;
  int neg_x_out1_last;
  int pos_y_out1_last;
  int neg_y_out1_last;
  int p_out1_last;
  int pos_x_out2_last;
  int neg_x_out2_last;
  int pos_y_out2_last;
  int neg_y_out2_last;
  int p_out2_last;
  int pos_x_out3_last;
  int neg_x_out3_last;
  int pos_y_out3_last;
  int neg_y_out3_last;
  int p_out3_last;
  int pos_x_out4_last;
  int neg_x_out4_last;
  int pos_y_out4_last;
  int neg_y_out4_last;
  int p_out4_last;


public:
  router(int x, int y, int buffer_size);
  void set_positive_x_link(router *next_router);
  void set_negative_x_link(router *next_router);
  void set_positive_y_link(router *next_router);
  void set_negative_y_link(router *next_router);
  void set_p_out_link1(L2cache *memory);
  void set_p_out_link2(traffic_generator *core);
  void set_p_out_link3(ram_controller *memory);
  void set_p_out_link4(L2cache*memory);
  void set_p_out_link4_dram(L2cache*memory);

  buffer *get_p_in1_buffer() { return (p_in1); };
  buffer *get_pos_x1_buffer() { return (pos_x_in1); };
  buffer *get_neg_x1_buffer() { return (neg_x_in1); };
  buffer *get_pos_y1_buffer() { return (pos_y_in1); };
  buffer *get_neg_y1_buffer() { return (neg_y_in1); };
  buffer *get_p_out1_buffer() { return (p_out1); };
  buffer *get_p_in2_buffer() { return (p_in2); };
  buffer *get_pos_x2_buffer() { return (pos_x_in2); };
  buffer *get_neg_x2_buffer() { return (neg_x_in2); };
  buffer *get_pos_y2_buffer() { return (pos_y_in2); };
  buffer *get_neg_y2_buffer() { return (neg_y_in2); };
  buffer *get_p_out2_buffer() { return (p_out2); };
  buffer *get_p_in3_buffer() { return (p_in3); };
  buffer *get_pos_x3_buffer() { return (pos_x_in3); };
  buffer *get_neg_x3_buffer() { return (neg_x_in3); };
  buffer *get_pos_y3_buffer() { return (pos_y_in3); };
  buffer *get_neg_y3_buffer() { return (neg_y_in3); };
  buffer *get_p_out3_buffer() { return (p_out3); };
  buffer *get_p_in4_buffer() { return (p_in4); };
  buffer *get_pos_x4_buffer() { return (pos_x_in4); };
  buffer *get_neg_x4_buffer() { return (neg_x_in4); };
  buffer *get_pos_y4_buffer() { return (pos_y_in4); };
  buffer *get_neg_y4_buffer() { return (neg_y_in4); };
  buffer *get_p_out4_buffer() { return (p_out4); };

  void decide_route_actions();
  void perform_route_actions();
  uint64 get_flit(route_action from_buffer);
  int avail_flit(route_action from_buffer);
  void clear_routed(route_action buffer);
};
