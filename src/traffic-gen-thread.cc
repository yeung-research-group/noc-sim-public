/* 
  traffic-gen-thread.cc

  Devesh Singh
  03 Sep 2017

  Each thread is attached to a certain core and generates random traffic which is injected into NOC
 */
#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"


traffic_gen_thread::traffic_gen_thread(int x_coord, int y_coord ,int th_num){

  conf = Conf_ReRAM::get();

  wq_size = 2*conf->max_simd_width+2;
  wait_queue = new queue_entry[wq_size];
  wq_head = wq_tail = 0;
  
  queue = new traffic_queue(x_coord, y_coord, th_num);
  trace_finished = 0;

}

traffic_gen_thread::~traffic_gen_thread()
{
    delete[] wait_queue;
}

int traffic_gen_thread::thread_ready(){ // or wait_queue_is_empty()
  if (queue->done)
    trace_finished = 1;
  else
    trace_finished = 0;

  if((wq_head == wq_tail) && (!trace_finished))
    return 1;
  else 
    return 0;
}
 
int traffic_gen_thread::wq_delete_element(unsigned packet_id)
{
  int pointer;
  pointer = wq_head; 
  while(pointer!=wq_tail){
    if(wait_queue[pointer].packet_id == packet_id){
      wait_queue[pointer] = wait_queue[wq_head];
      wait_queue[wq_head].inflight = FALSE;
      wq_head = (wq_head+1)%wq_size;
      return 1;
    }
    else{
      pointer = (pointer+1)%wq_size;
    }
  }
  return 0;
}
void traffic_gen_thread::wq_insert_at_tail(unsigned packet_id, uint64 address)
{
  wait_queue[wq_tail].packet_id =   packet_id;
  wait_queue[wq_tail].addr =   address;
  wait_queue[wq_tail].inflight =   TRUE;
  wq_tail = (wq_tail+1)%wq_size;
  if(wq_tail == wq_head){
    printf("ERROR wait queue full\n");
  }
}

