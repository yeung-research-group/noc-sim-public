#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_array::L2cache_array(int x, int y)
{
    conf = Conf_ReRAM::get();
    stats = Stats::get();

    x_coord = x;
    y_coord = y;

    make_cache();
}

void L2cache_array::make_cache() {
    LRU_head = new L2_cache_line * [conf->l2_settings.n_sets];
    LRU_tail = new L2_cache_line * [conf->l2_settings.n_sets];

    set_contents = new unsigned[conf->l2_settings.n_sets];
    for (unsigned i = 0; i < conf->l2_settings.n_sets; i++) {
        LRU_head[i] = nullptr;
        LRU_tail[i] = nullptr;
        set_contents[i] = 0;
    }

    contents = 0;
    L2_stat.accesses = 0;
    L2_stat.misses = 0;
    L2_stat.replacements = 0;
    L2_stat.demand_fetches = 0;
    L2_stat.copies_back = 0;
}


//Look up tag and update LRU
tag_res L2cache_array::tag_look_up(uint64 addr)
{
    uint64 index;
    L2_cache_line* cache_line;
    tag_res res = tag_res();

    CACHE_ACCESS(&L2_stat);
    index = (addr & conf->l2_settings.index_mask) >> conf->l2_settings.index_mask_offset;

    cache_line = find_cache_line(addr);
    if (cache_line != nullptr)
    {
        res.hit = true;
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
            res.valid[i] = cache_line->valid[i];

        //update LRU
        delete_entry(&(LRU_head[index]), &(LRU_tail[index]), cache_line);
        insert(&(LRU_head[index]), &(LRU_tail[index]), cache_line);
    }

    if (!res.hit)  //if not valid we may need to evict when placing
    {
        CACHE_MISS(&L2_stat);

        /* check if line to be evicted is dirty or clean */
        if (set_contents[index] >= conf->l2_settings.assoc) {
            res.evict_dirty = LRU_tail[index]->any_dirty;
        }
    }

    return res;
}


//Updates the cache, returns a dirty evicted line
L2_cache_line* L2cache_array::tag_update(mem_request req)
{
    uint64 index;
    L2_cache_line* cache_line, * evict;
    uint64 addr_tag;
    int sector;

    addr_tag = req.addr >> conf->l2_settings.index_mask_offset;
    index = (req.addr & conf->l2_settings.index_mask) >> conf->l2_settings.index_mask_offset;
    sector = (req.addr & conf->l2_settings.sector_mask) >> conf->l2_settings.sector_mask_offset;

    //search cache
    cache_line = find_cache_line(req.addr);

    if (cache_line == nullptr)
    {
        cache_line = new L2_cache_line(addr_tag);
        insert(&(LRU_head[index]), &(LRU_tail[index]), cache_line);
        set_contents[index]++;
        contents++;
    }
    else
    {
        //update LRU
        delete_entry(&(LRU_head[index]), &(LRU_tail[index]), cache_line);
        insert(&(LRU_head[index]), &(LRU_tail[index]), cache_line);
    }

    if (req.type == write_dw)
    {
        cache_line->any_dirty = true;
        cache_line->valid[sector] = true;
        cache_line->dirty[sector] = true;
    }
    else if (req.type == write_block)
    {
        cache_line->any_dirty = true;
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            cache_line->dirty[i] = true;
            cache_line->valid[i] = true;
        }
    }
    else  //cache fill
    {
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            cache_line->any_dirty |= req.dirty[i];
            cache_line->dirty[i] |= req.dirty[i];  //may already be data there
            cache_line->valid[i] |= req.valid[i];
        }
    }

    /* evict if necessary */
    evict = nullptr;
    if (set_contents[index] > conf->l2_settings.assoc) {
        CACHE_REPLACE(&L2_stat);
        evict = LRU_tail[index];
        delete_entry(&(LRU_head[index]), &(LRU_tail[index]), evict);
        set_contents[index]--;
        contents--;

        if (evict->any_dirty)
        {
            CACHE_WRITEBACK(&L2_stat);
            return evict;
        }
        else
        {
            delete evict;
        }
    }

    return nullptr;
}

L2_cache_line* L2cache_array::find_cache_line(uint64 addr)
{
    uint64 index, addr_tag;
    L2_cache_line* cache_line;

    addr_tag = addr >> conf->l2_settings.index_mask_offset;
    index = (addr & conf->l2_settings.index_mask) >> conf->l2_settings.index_mask_offset;

    cache_line = LRU_head[index];
    while (cache_line) {
        if (cache_line->tag == addr_tag) {
            return cache_line;
        }
        cache_line = cache_line->LRU_next;
    }

    return nullptr;
}

void L2cache_array::flush_cache() {

    unsigned i, j;
    L2_cache_line* current;

    for (i = 0; i < conf->l2_settings.n_sets; i++) {
        for (j = 0; j < set_contents[i]; j++) {
            current = LRU_head[i];
            LRU_head[i] = current->LRU_next;

            if (current->any_dirty)
                CACHE_WRITEBACK(&L2_stat);
            delete current;
        }
        LRU_tail[i] = nullptr;
    }
}

L2_cache_line::L2_cache_line(uint64 val_tag) : 
    tag(val_tag),
    any_dirty(false),
    dirty(),
    valid(),
    LRU_next(nullptr),
    LRU_prev(nullptr)
{
    Conf_ReRAM* conf = Conf_ReRAM::get();

    valid = new bool[conf->l2_settings.n_sectors];
    dirty = new bool[conf->l2_settings.n_sectors];
    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++) {
        valid[i] = false;
        dirty[i] = false;
    }
}

L2_cache_line::L2_cache_line(const L2_cache_line& line)
{
    Conf_ReRAM* conf = Conf_ReRAM::get();

    tag = line.tag;
    any_dirty = line.any_dirty;
    LRU_next = line.LRU_next;
    LRU_prev = line.LRU_prev;

    valid = new bool[conf->l2_settings.n_sectors];
    dirty = new bool[conf->l2_settings.n_sectors];
    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++) {
        valid[i] = line.valid[i];
        dirty[i] = line.dirty[i];
    }
}

L2_cache_line::~L2_cache_line()
{
    delete[] valid;
    delete[] dirty;
}


void L2cache_array::delete_entry(L2_cache_line * *head, L2_cache_line * *tail, L2_cache_line * item)
{
    if (item->LRU_prev) {
        item->LRU_prev->LRU_next = item->LRU_next;
    }
    else {
        /* item at head */
        *head = item->LRU_next;
    }

    if (item->LRU_next) {
        item->LRU_next->LRU_prev = item->LRU_prev;
    }
    else {
        /* item at tail */
        *tail = item->LRU_prev;
    }
}

/* inserts at the head of the list */
void L2cache_array::insert(L2_cache_line * *head, L2_cache_line * *tail, L2_cache_line * item)
{
    item->LRU_next = *head;
    item->LRU_prev = nullptr;

    if (item->LRU_next)
        item->LRU_next->LRU_prev = item;
    else
        *tail = item;

    *head = item;
}
