/* prefetcher.hh
 *
 * Devesh Singh
 * 1 March 2018
 *
 * Keeps track of all prefetch streams using RPT(Reference Prediction Table) and stores 
 * their corresponding data in prefetch buffers of chosen depth. */
#include <deque>
#include <unordered_map>

#ifndef uint64
  #define uint64 uint64_t
#endif


enum rpt_state {init, transient, steady, no_prediction};
enum prefetch_status {stream_buf_miss=0, in_flight=1, stream_buf_hit=2};
//enum ins_type {scalar_load=0,vecrtor_read=1, gather_ins = 2}

typedef struct stream_buffer_entry_{
public:
    bool *avail;
    
    ins_type type;
    unsigned *packet_id;
    uint64 *address;

    ~stream_buffer_entry_();
} stream_buffer_entry, * Pstream_buffer_entry;


class prefetcher
{
public:
    //prefetch datastructures
  /*  typedef struct stream_buffer_entry_{
        bool avail;
        bool vector;
        unsigned packet_id;
        uint64 address;
        
    } buf_entry, *Pbuf_entry;*/


    typedef struct rpt_entry_{
        uint64 PC;
        int thread_id;
        ins_type type;
        uint64 *last_addr;
        int stride;
        rpt_state state;
        rpt_entry_ *LRU_next;
        rpt_entry_ *LRU_prev;
        stream_buffer_entry *Pstream_buf;
        int buf_head, buf_tail;
        
    } rpt_entry, *Prpt_entry;

private:
    
    std::unordered_map<uint64, Pstream_buffer_entry>* stream_buffer;

    int buf_size;
    Prpt_entry rpt_head;
    Prpt_entry rpt_tail;
    int rpt_entry_count;

    typedef struct pref_req_queue_entry_{
        ins_type type;
        uint64 addr;
        stream_buffer_entry *corresponding_buf_entry; // TO put the packet id
    } pref_req_entry, *Ppref_req_entry;

    deque<pref_req_entry> pref_req_queue;

    //parameters
    uint64 cache_block_addr_mask;
    int prefetch_depth;
    int max_streams;
    int x_coord;
    int y_coord;

    //Global objects
    Conf_ReRAM *conf;
    Stats *stats;
    cacheL1 *Pcache;  //Need it to inject requests into the noc from prefetcher.

public:
    prefetcher(cacheL1 *cache, int x, int y);
    void make_prefetcher(cacheL1 *cache);
    int check_rpt(uint64 PC, int thread_id, uint64 *addr, ins_type type,
            int mask, int unaligned_vec_first_part, int cache_hit, int *reqs_sent, 
            unsigned *packet_id);
    Prpt_entry find_stream(uint64 PC, int thread_id);
    Prpt_entry make_stream(uint64 PC, int thread_id, uint64 *addr, 
            ins_type type, int mask_width);
    void insert_stream(Prpt_entry stream);
    void delete_stream(Prpt_entry stream);
    void lru_stream(Prpt_entry stream);
    void update_stream(Prpt_entry stream, uint64 *addr, int mask, ins_type type);
    //rpt_state update_state(Prpt_entry stream, uint64 *addr, int mask, ins_type type);
    void update_state(Prpt_entry stream, uint64 *addr, int mask, ins_type type);
    int check_all_stream_buf(uint64 addr, unsigned *packet_id); 
    int check_my_stream_buf(Prpt_entry stream, uint64 addr, unsigned *packet_id,
            ins_type type, int vec_first_part); 
    void issue_prefetch_request(Prpt_entry stream, uint64 *addr, ins_type type);

    int sbuf_peek_top_element(Prpt_entry stream, stream_buffer_entry* Pbuf);
    void sbuf_delete_top_element(Prpt_entry stream);
    void sbuf_insert_at_tail(Prpt_entry stream, uint64* address, ins_type type );
    int sbuf_check_matching_request(Prpt_entry stream, uint64 packet_id);

    void receive_packet_from_noc(uint64 packet_id);

    void cycle(long long); 

};
/* Macros */
#define LOG2(x) ((int)( log((double)(x)) / log(2) ))
