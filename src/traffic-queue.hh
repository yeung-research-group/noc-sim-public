#pragma once
/*
  traffic-queue.hh

  Devesh Singh
  3 Sep 2017

  To be configured for the traffic generators for customized traffic
  each entry in the queue could be-
  0: non memory op
  1: Load/Store
 */

class traffic_queue
{
private:
  //dynamic_queue<instruction> the_queue;
  std::deque<instruction> the_queue;
  int x_crd;
  int y_crd;
  long te_cnt; //Traversed edge count
  int gather_width;
  long gather_cnt;
  int gather_miss_width;

  void create_random_sequence(int total_ins, int single_mem_ops, int simd_mem_ops, int simd_width);
  void create_my_seq();
  void generate_random_addresses(uint64 *array, int count);
  std::mutex barrier;

  Conf_ReRAM* conf;
  
public:
  traffic_queue(int x, int y, int th_num);
  void rotate_element_at_head(); // Removes the element at head and inserts it at the tail - non-trace simulations
  instruction* peak_element_at_head();
  void update_head(instruction* ins);
  instruction* delete_element_at_head();
  int insert_at_tail(instruction* ins);
  int is_empty();
#if !NOC_SIM_SNIPER
  int is_full();
#endif
  long traversed_edge_count();
  int gather_width_cnt() {return gather_width;};
  long gather_ins_cnt() {return gather_cnt;};
  int gather_miss_width_cnt() {return gather_miss_width;};
  int traffic_queue_size() {return the_queue.size();};

  bool done;
  int thread_id;
    
};
