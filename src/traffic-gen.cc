/*
  traffic-gen.cc

  Donald Yeung
  26 July 2017

  Traffic-gen class injects messsages into the NOC, either using
  randomly generated accesses, or from traces.
 */


#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"


#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

 //#define random rand
traffic_generator::traffic_generator(int x, int y, int npackets)
{
    int l;
    conf = Conf_ReRAM::get();
    stats = Stats::get();
    x_coord = x;
    y_coord = y;
    x_extent = conf->x_max;
    y_extent = conf->y_max;

    if (conf->ram_mode == l2_cpu || conf->ram_mode == l2_reram || conf->ram_mode == het_l2_reram)
        active = !conf->is_ram_tile(x, y);
    else
        active = true;

    if (!active) return;

    cache_in = new buffer(conf->buffer_size);
    cache = new cacheL1(x, y, cache_in);
    if (conf->prefetch)
        Pprefetcher = new prefetcher(cache, x, y);

    num_threads = conf->num_threads[IND(x, y)];
    cur_thread = 0;
    my_threads = new traffic_gen_thread * [num_threads];
    for (l = 0; l < num_threads; l++) {
        my_threads[l] = new traffic_gen_thread(x_coord, y_coord, l);
    }

    simd_width = 0;
    context_switch_counter = 0;
    miss_retry = FALSE;


    trace_finished = 0;

    pref = 0;
    dram_vec = 0;

    ins = NULL;

    number_packets_to_inject = 0;
    rw = -1;
    sg = -1;
    vector = -1;

}

traffic_generator::~traffic_generator()
{
    delete[] my_threads;
}

void traffic_generator::add_instruction(instruction* ins, int threadID)
{
    my_threads[threadID]->queue->insert_at_tail(ins);
}

int traffic_generator::get_available_thread()
{
    int id = cur_thread;
    for (int i = 0; i < num_threads; i++)
    {
        if (my_threads[id]->thread_ready()) {
            return id;
        }
        else {
            id = (id + 1) % num_threads;
        }
    }

    if (stats->check_trace_done())
    {
        int j = 0;
        for (int i = 0; i < num_threads; i++) {
            j = j + my_threads[i]->trace_finished;
        }
        if (j == num_threads) { //All threads reached the end of trace.
            trace_finished = 1;
            printf("Core on tile (%d, %d) is out of trace elements!!\n", y_coord, x_coord);
            return -2;
        }
    }

    // All threads are unavailable
    return -1;
}

void traffic_generator::cycle(long long cur_cycle)
{
    if (!active) return;

    Stats* stats = Stats::get();
    int thread_id;
    unsigned packet_id;
    int reqs_sent = 0;
    int check = 0;
    int vector_cache_mask;
    int vector_entry_size; // log(vector_entry_size)
    int vector_entry_size_bytes;
    int stream_buf_hit;
    if (conf->max_simd_width == 8)
    {
        vector_entry_size = 3;
        vector_cache_mask = 0x7;
        vector_entry_size_bytes = 8;
    }
    else  //SIMD_WIDTH == 16
    {
        vector_entry_size = 2;
        vector_cache_mask = 0xf;
        vector_entry_size_bytes = 4;
    }


    cache->cycle(cur_cycle);
    while (!cache_in->is_empty()) //TODO - as of now taking all the packets in a single cycle from backdoor make it more realistic
    {
        packet_id = (unsigned int)cache_in->remove_from_head();

        if (conf->prefetch) {
            Pprefetcher->receive_packet_from_noc(packet_id);
        }
        for (int i = 0; i < num_threads; i++) {
            if (my_threads[i]->wq_delete_element(packet_id)) {
                check = 1;
                break;
            }
        }
        if (!check) { // Not valid anymore because write is generating non-blocking read requests
          //printf("Bad CPU packet %lld: (%d,%d) %#010x\n", cur_cycle, x_coord, y_coord, packet_id);
        //TODO: some how make a check to ensure we're only getting packets we want
        }
        check = 0;
    }

    if (trace_finished) return;  //This core is out of trace elements to play

    if (context_switch_counter)
    {
        context_switch_counter--;
        return;
    }

    if (!simd_width)
    {
        thread_id = get_available_thread();
        if (thread_id < 0)
        {
            //don't have any work to do currently
            stats->tile_cpu_stats[IND(x_coord, y_coord)].cpu_idle_cycles++;
            return;
        }

        if (thread_id != cur_thread)
        {
            context_switch_counter = conf->context_switch_delay;  //If changing the currently executing thread

#if CPU_PRINT
            if (x_coord == 10 && y_coord == 10)
                printf("Switching to thread - %d\n", thread_id);
#endif
            cur_thread = thread_id;
            return;
        }


        //TODO: Why do we get NULL? Is this an error we should be catching?
        ins = my_threads[cur_thread]->queue->peak_element_at_head();
        if (ins == NULL)
            return;

        stats->tile_cpu_stats[IND(x_coord, y_coord)].ins_executed++;

        sg = 0;
        vector = 0;
        switch (ins->type) {

        case end_thread:
            int i, num_finished_threads;
            my_threads[cur_thread]->trace_finished = 1;
            for (i = 0, num_finished_threads = 0; i < num_threads; i++)
                if (my_threads[i]->trace_finished) num_finished_threads++;
            if (num_finished_threads == num_threads) trace_finished = true;
            my_threads[cur_thread]->queue->delete_element_at_head();
            return;

        case other:    my_threads[cur_thread]->queue->delete_element_at_head();
#if CPU_PRINT
            if (x_coord == 10 && y_coord == 10 && cur_thread == 0)
                printf("Executing non memory instruction.\n", thread_id);
#endif

            return;

        case vector_read:
            simd_width = conf->max_simd_width;
            rw = 0;
            vector = conf->vector_cache_block_fetch;
            dram_vec = (conf->ram_mode == dram) ? 1 : 0;
            break;

        case vector_write:
            simd_width = conf->max_simd_width;
            rw = 1;
            //vector = (conf->dram_mode) ? 0 : 1;
            vector = conf->vector_cache_block_fetch;
            dram_vec = (conf->ram_mode == dram) ? 1 : 0;
            break;

        case gather:
            simd_width = ins->mem_locs;
            rw = 0;
            sg = 1;
            break;

        case scatter:
            simd_width = ins->mem_locs;
            rw = 1;
            sg = 1;
            break;

        case single_read:
            simd_width = 1;
            rw = 0;
            break;

        case single_write:
            simd_width = 1;
            rw = 1;
            break;

        default:
            throw std::invalid_argument("Error: unknown instruction type");
        }

#if CPU_OUT
        std::ostringstream msg;
        msg << "(" << x_coord << ", " << y_coord << "): CPU received instruction! Type: " << ins->type;

        if (ins->mem_locs > 0) {
            msg << " addr -";
            for (int i = 0; i < ins->mem_locs; i++)
                msg << " 0x" << setfill('0') << setw(16) << hex << ins->address[i];
        }
        msg << "\n";
        Stats::get()->printLog(msg.str(), false);
#endif
    }

    if (!miss_retry) {
        if (vector == 1) {
            miss_retry = (!cache->hit(ins->address[conf->max_simd_width - simd_width], rw, cur_cycle, vector, sg));
            if (rw == 0 && conf->prefetch) { //Only send the read stream to prefetcher.
                stream_buf_hit = Pprefetcher->check_rpt(ins->PC, cur_thread, &(ins->address[conf->max_simd_width-simd_width]),
                vector_read, ins->vector_mask, ((simd_width==conf->max_simd_width)?TRUE:FALSE), TRUE, &reqs_sent,
                &packet_id);
                if (stream_buf_hit) {
                    if (stream_buf_hit == 1) //TODO: ask Devesh if this is correct or re-look through my changes
                        my_threads[cur_thread]->wq_insert_at_tail(packet_id, ins->address[conf->max_simd_width - simd_width]);//address_flit);
                    stats->tile_cpu_stats[IND(x_coord, y_coord)].prefetchq_hits++;
                    miss_retry = FALSE;
                }
            }

#if CPU_PRINT
            if (x_coord == 10 && y_coord == 10 && cur_thread == 0)
                printf("(%2d,%2d) cycle - %lld, thread - %d, miss - %d, rw - %d, vector - %d, addr - %llx, PC - %llx\n",
                    x_coord, y_coord, cur_cycle, cur_thread, miss_retry, rw, vector, ins->address[conf->max_simd_width - simd_width], ins->PC);
#endif

        }
        else {
            miss_retry = (!cache->hit(ins->address[simd_width - 1], rw, cur_cycle, vector, sg));
            if(ins->type == gather && conf->prefetch==1 && ins->mem_locs == conf->max_simd_width){
                stream_buf_hit = Pprefetcher->check_rpt(ins->PC, cur_thread, &(ins->address[ins->mem_locs-simd_width]),
                      gather, ins->mem_locs, ((simd_width==conf->max_simd_width)?TRUE:FALSE), TRUE, &reqs_sent,
                      &packet_id);
                if(stream_buf_hit){
                    if(stream_buf_hit == 1)
                        my_threads[cur_thread]->wq_insert_at_tail(packet_id, ins->address[conf->max_simd_width - simd_width]);//address_flit);
                    stats->tile_cpu_stats[IND(x_coord, y_coord)].prefetchq_hits++;
                    miss_retry = FALSE;
              }
            }

#if CPU_PRINT
            if (x_coord == 10 && y_coord == 10 && cur_thread == 0)
                printf("(%2d,%2d) cycle - %lld, thread - %d, miss - %d, rw - %d, vector - %d, addr - %llx, PC - %llx\n",
                    x_coord, y_coord, cur_cycle, cur_thread, miss_retry, rw, vector, ins->address[simd_width - 1], ins->PC);
#endif
        }

    }

    //if it didn't hit in the cache (miss_retry == 1) try to send request
    if (miss_retry)
    {

#if CPU_OUT
        std::ostringstream msg;
        msg << "(" << x_coord << ", " << y_coord << "): Attempt Request remaining: " << simd_width;
        if (vector == 1)
        {
            msg << " addr -" << " 0x" << setfill('0') << setw(16) << hex << ins->address[conf->max_simd_width - simd_width] << "\n";
        }
        else
        {
            msg << " addr -" << " 0x" << setfill('0') << setw(16) << hex << ins->address[simd_width - 1] << "\n";
        }
        Stats::get()->printLog(msg.str(), false);
#endif

        // 0 for read, 1 for write
        if (rw == 1)
        {

            if (vector == 1)
                reqs_sent = (conf->l1_settings.block_size / vector_entry_size_bytes) - ((ins->address[conf->max_simd_width - simd_width] >> vector_entry_size) & vector_cache_mask); //Total entrees fetched - Useless entrees(the one's before the starting address of vector instruction)
              //reqs_sent = cache->write_request(cur_cycle, ins->address[conf->max_simd_width-simd_width], vector, sg, 0, ins->PC);

            else
                reqs_sent = 1;
            //reqs_sent = cache->write_request(cur_cycle, ins->address[simd_width-1], vector, sg, 0, ins->PC);
            // Since no sectored cache for DRAM we have to fetch the cache block. CB_128 dependent
            if (conf->ram_mode == dram) {
                unsigned temp_packet_id;

                if (!vector) // DRAM vector writes just write the cache like ReRAM and no read requests.
                    reqs_sent = cache->read_request(cur_cycle, ins->address[simd_width - 1], vector, sg, &temp_packet_id, ins->PC);
            }

            if (reqs_sent == 0)
                return;  //not enough room

            miss_retry = FALSE;
        }
        else if (rw == 0)
        {
            uint64 address_flit;
            if (vector == 1)
                address_flit = ins->address[conf->max_simd_width - simd_width];
            else
                address_flit = ins->address[simd_width - 1];
            reqs_sent = cache->read_request(cur_cycle, address_flit, vector, sg, &packet_id, ins->PC);
            if (reqs_sent == 0)
                return;  //not enough room
            miss_retry = FALSE;
            if (reqs_sent != -1)
                my_threads[cur_thread]->wq_insert_at_tail(packet_id, address_flit);
        }

        number_packets_to_inject--;
    }
    else
    {
        if (vector)
        {
            //reqs_sent = 8 - ((ins->address[8-simd_width] >> 3) & 0x7);
            //reqs_sent = conf->max_simd_width - ((ins->address[conf->max_simd_width-simd_width] >> (vector_entry_size)) & vector_cache_mask);
            reqs_sent = (conf->l1_settings.block_size / vector_entry_size_bytes) - ((ins->address[conf->max_simd_width - simd_width] >> vector_entry_size) & vector_cache_mask); //Total entrees fetched - Useless entrees(the one's before the starting address of vector instruction)

        }
        else
        {
            reqs_sent = 1;
        }
    }

    if (sg) {
        stats->tile_cpu_stats[IND(x_coord, y_coord)].te_count++;
    }
    if (reqs_sent == -1)
        reqs_sent = 1;
    simd_width = simd_width - reqs_sent;

    if (simd_width <= 0 && rw != 2) {//If this was the last mem_ins then delete the element from the traffic queue
        my_threads[cur_thread]->queue->delete_element_at_head();
        //else
          //my_threads[cur_thread]->queue->rotate_element_at_head();
        //stats->mem_ins_executed++;//CHECK - do prefetch count in instructions executed?
        simd_width = 0;
    }

    if (conf->prefetch)
        Pprefetcher->cycle(cur_cycle);

}


void traffic_generator::set_npackets(int npackets)
{
    number_packets_to_inject = npackets;
}
