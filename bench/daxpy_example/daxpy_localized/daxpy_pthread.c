/* File:     
 *     pth_daxpy.c 
 *
 * Purpose:  
 *     Computes a DAXPY, which is an update y += alpha*x, where
 *     x and y are vectors, and alpha is a scalar.  This program
 *     uses a block distribution of the vectors.
 *
 * Input:
 *     n: dimension of vectors
 *     x, y: the vectors
 *     alpha:  the scalar
 *
 * Output:
 *     y: the vector y after the update
 *
 * Compile:  gcc -g -Wall -o pth_daxpy pth_daxpy.c -lpthread
 * Usage:
 *     pth_daxpy <thread_count>
 *
 * Notes:  
 *     1.  Local storage for x and y is dynamically allocated.
 *     2.  Distribution of x, and y is logical:  both are 
 *         globally shared.
 *     3.  n should be evenly divisible by thread_count.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
# include <float.h>
# include <immintrin.h>
#include <stdint.h>

/* Global variables */
int     thread_count;
int     n,shift, nchunks;
double* x;
double* y;
double* z;
double  alpha;
int dram_mode;
pthread_barrier_t barrier;

/* Serial functions */
void Usage(char* prog_name);

/* Parallel function */
void *Pth_daxpy(void* rank);

/*------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
   long       thread;
   pthread_t* thread_handles;
   int i;
   double *x_original, *y_original;

   if (argc != 3) Usage(argv[0]);
   thread_count = strtol(argv[1],NULL,10);
   thread_handles = malloc(thread_count*sizeof(pthread_t));
   dram_mode = strtol(argv[2],NULL,0);
   

   n = 32768*256;
   nchunks = 176;
   int chunk_size = 67108864;
   shift = 67108864/sizeof(double);

   int shift_bits = 26; // offset bits for chunk per tile + bits for total number of tiles (18+9 for 256KB and 400 tiles)
   uint64_t mask = 0;
   mask = ~mask;
   mask = mask<<shift_bits;


   x = (double *)malloc((size_t)chunk_size*(nchunks+1));
   x_original = x;
   if(x==NULL){
       printf("Allocation for x failed \n");
       return 0;
   }
   y = (double *)malloc((size_t)chunk_size*(nchunks+1));
   y_original = y;
   if(y==NULL){
       printf("Allocation for y failed \n");
       return 0;
   }
   x = (double *)((uint64_t)x & mask);
   x = (double *)((uint64_t)x + (1<<shift_bits));

   y = (double *)((uint64_t)y & mask);
   y = (double *)((uint64_t)y + (1<<shift_bits));

   
   for(i=0; i<n;i++){
     x[i] = 1.0;
     y[i] = 2.0;
   }
   for(i=n-1; i< (n+shift); i++){
      y[i] = 2.0;
   }

   alpha = 3.0;
   pthread_barrier_init(&barrier,NULL,thread_count);

   for (thread = 1; thread < thread_count; thread++)
      pthread_create(&thread_handles[thread], NULL,
         Pth_daxpy, (void*) thread);

   Pth_daxpy(0);


   for (thread = 1; thread < thread_count; thread++)
      pthread_join(thread_handles[thread], NULL);

   free(x_original);
   free(y_original);
   free(thread_handles);

   return 0;
}  /* main */


/*------------------------------------------------------------------
 * Function:  Usage
 * Purpose:   print a message showing what the command line should
 *            be, and terminate
 * In arg :   prog_name
 */
void Usage (char* prog_name) {
   fprintf(stderr, "usage: %s <thread_count> <dram_mode>\n", prog_name);
   exit(0);
}  /* Usage */



/*------------------------------------------------------------------
 * Function:          Pth_daxpy
 * Purpose:           Update a double precision vector y += alpha*x
 * In arg:            rank
 * Global in vars:    x, n, alpha, thread_count
 * Global in/out var: y
 */
void *Pth_daxpy(void* rank) {
   long my_rank = (long) rank;
   int local_n = n/thread_count;
   int my_first_i = my_rank*local_n;
   int my_last_i = my_first_i + local_n;
   int i;
   int my_chunk_count=0;
   
   double* x_l = x;
   double* y_l = y;
   pthread_barrier_wait(&barrier);


   __m512d zmm0,zmm1,zmm2;
    zmm2 = _mm512_set1_pd(alpha);
   for(my_chunk_count=0; my_chunk_count<nchunks; my_chunk_count++){ 
        for (i = my_first_i; i < my_last_i; i += 8) {
            zmm0 = _mm512_loadu_pd(x_l+i);
            zmm1 = _mm512_loadu_pd(y_l+i);
            zmm0 = _mm512_mul_pd(zmm0,zmm2);
            zmm1 = _mm512_add_pd(zmm1,zmm0);
            _mm512_storeu_pd(y_l+i,zmm1);
        }
        my_first_i = my_first_i + shift;
        my_last_i = my_last_i + shift;
   }

   return NULL;
}  /* Pth_mat_vect */
