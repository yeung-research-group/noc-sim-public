#pragma once
enum cache_access {load, store};

class cacheL1
{
public:
  cacheL1(int x, int y, buffer *out);
  int hit(uint64 address,int rw, long long cur_cycle, int vector, int sg);
  int write_request(long long cur_cycle, uint64 address, int vector, int sg, int wb, uint64 PC);
  int read_request(long long cur_cycle, uint64 address, int vector, int sg, unsigned *packet_id, uint64 PC);
  void cycle(long long cur_cycle);
  int flush_cache();
  int is_wb_queue_empty();

  //Set Up
  void set_buffer_link(buffer *my_buffer);
  buffer *get_input_buffer() { return (input_buffer); };


private:
  //Info
  int x_coord;
  int y_coord;
  int x_extent;
  int y_extent;

  typedef struct cache_line_ {
    uint64 tag;
    int dirty;

    struct cache_line_ *LRU_next;
    struct cache_line_ *LRU_prev;
    int * valid; //zz
  } cache_line, *Pcache_line;
  Pcache_line *LRU_head;
  Pcache_line *LRU_tail;
  unsigned *set_contents;
  int contents;
  typedef struct cache_stat_ {
    int accesses;			/* number of memory references */
    int misses;			/* number of cache misses */
    int replacements;		/* number of misses that cause replacments */
    int demand_fetches;		/* number of fetches */
    int copies_back;		/* number of write backs */
  } cache_stat, *Pcache_stat;
  cache_stat L1_stat;

  typedef struct {
    uint64 addr;
    bool vec;
  } wb_entry;

  int vector_cache_mask;
  int vector_entry_size; // log(vector_entry_size)
  int vector_entry_size_bytes;

  //Writeback
  std::deque<wb_entry> wb_queue;
  //Routing
  buffer *output_buffer; //to NOC
  buffer *input_buffer;  //from NOC
  buffer *backdoor_out;  //to processor

  bool am_packet;
  
  //Cache core functions
  void make_cache();
  int perform_cache_access(uint64 addr, cache_access access_type, int vector, int sg);
  int hit_miss(uint64 addr, int access_type, int vector, int sg);
  int cache_block_valid_check(Pcache_line cache_line);
  cacheL1::Pcache_line probe(uint64 addr, int access_type, int vector, int sg);
  cacheL1::Pcache_line make_cache_line(uint64 tag);
  void delete_entry(Pcache_line *head, Pcache_line *tail, Pcache_line item);
  void insert(Pcache_line *head, Pcache_line *tail,Pcache_line item);


  //Address Calculation
  void get_random_Address(int* delta_x, int* delta_y);
  void get_local_Address(uint64 addr, int* delta_x, int* delta_y);
  void get_mapped_Address(uint64 addr, int* delta_x, int* delta_y);
  void address_translation(uint64 addr, int* delta_x, int* delta_y);

  //Flit Creation
  uint64 make_header(uint64 header_flit, int delta_x, int delta_y, unsigned packet_id);

  //Buffer check
  int out_buffer_space_check(int vector);


  //Global objects
  Stats *stats;
  Conf_ReRAM *conf;
};

/* Macros */
#define CACHE_ACCESS(x) (*(x)).accesses++;
#define CACHE_MISS(x) (*(x)).misses++;
#define CACHE_REPLACE(x) (*(x)).replacements++;
#define CACHE_FETCH_BLOCK(x) (*(x)).demand_fetches+=8;
#define CACHE_FETCH_WORD(x) (*(x)).demand_fetches++
#define CACHE_WRITEBACK(x) (*(x)).copies_back+=8;
#define CACHE_WRITETHROUGH(x) (*(x)).copies_back++;
