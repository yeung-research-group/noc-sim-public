class cacheL2 : public L2cache
{
public:
  cacheL2(int y, int x);
  void cycle(long long cur_cycle);

  buffer *get_mem_in_buffer() { return (mem_in); };
  buffer *get_dram_in_buffer() { return (dram_in); };
  void set_mem_out_buffer(buffer* out);
  void set_dram_out_buffer(buffer* out);

private:

  typedef struct cache_line_ {
    uint64 tag;
    int dirty;
    int *valid;

    struct cache_line_ *LRU_next;
    struct cache_line_ *LRU_prev;
  } cache_line, *Pcache_line;
  Pcache_line *LRU_head;
  Pcache_line *LRU_tail;
  int *set_contents;
  int contents;
  typedef struct cache_stat_ {
    int accesses;			/* number of memory references */
    int misses;			/* number of cache misses */
    int replacements;		/* number of misses that cause replacments */
    int demand_fetches;		/* number of fetches */
    int copies_back;		/* number of write backs */
  } cache_stat, *Pcache_stat;
  cache_stat L2_stat;

  std::deque<wb_entry> wb_queue;

  memory_mode mode;
  memory_mode out_mode;
  bool read_out_cpu;
  bool dram_packet;
  int init_write;
  int init_wb;
  int wb_data_flit_cnt;
  int w_hit;
  int latency;
  uint64 hflit;
  uint64 aflit;
  uint64 out_hflit;

  int read_miss_retry;
  int write_miss_retry;

  //CPU
  buffer *backdoor_out;
  //RAM
  buffer *mem_in;
  buffer *mem_out;
  //DRAM
  buffer *dram_in;
  buffer *dram_out;

  //Cache core functions
  void make_cache();
  int perform_cache_access(uint64 addr, cache_access access_type, int vector);
  int hit_miss(uint64 addr, cache_access access_type, int vector);
  int cache_block_valid_check(Pcache_line cache_line);
  cacheL2::Pcache_line probe(uint64 addr, cache_access access_type, int vector);
  void flush_cache();
  cacheL2::Pcache_line make_cache_line(uint64 tag);
  void delete_entry(Pcache_line *head, Pcache_line *tail, Pcache_line item);
  void insert(Pcache_line *head, Pcache_line *tail,Pcache_line item);
  
  int hit(uint64 address, int rw, long long cur_cycle, int vector);
  void route_in(long long cur_cycle);
  void route_out(long long cur_cycle);
  void send_read_reply(unsigned packet_id, long long cur_cycle);

  void address_translation(uint64 addr, int* delta_x, int* delta_y, int * bank);
  void get_random_dAddress(int* delta_x, int* delta_y, int * bank);
  void get_local_dAddress(uint64 addr, int* delta_x, int* delta_y, int * bank);
  void get_mapped_dAddress(uint64 addr, int* delta_x, int* delta_y, int * bank);
  //void get_tileint_dAddress(uint64 addr, int* delta_x, int* delta_y, int * bank);
  void get_random_rAddress(int* delta_x, int* delta_y, int * bank);
  void get_local_rAddress(uint64 addr, int* delta_x, int* delta_y, int * bank);
  void get_bankint_rAddress(uint64 addr, int* delta_x, int* delta_y, int * bank);
  void get_tileint_rAddress(uint64 addr, int* delta_x, int* delta_y, int * bank);
  void get_random_rAddress_wb(int* bank);
  void get_local_rAddress_wb(uint64 addr, int* bank);
  void get_bankint_rAddress_wb(uint64 addr, int* bank);
  void get_tileint_rAddress_wb(uint64 addr, int* bank);


  uint64 make_header(uint64 header_flit, int delta_x, int delta_y, unsigned packet_id);
};


