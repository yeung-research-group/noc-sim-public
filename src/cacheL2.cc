#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

static int words_per_block;

cacheL2::cacheL2(int x, int y) : L2cache(x, y)
{
  int buffer_size = conf->buffer_size;
  mem_in = new buffer(buffer_size);

  if (conf->ram_mode == het_l2_reram)
  {
    dram_in = new buffer(buffer_size);
  }
  else
  {
    dram_in = mem_in;
  }

  mode = idle_mode;
  out_mode = idle_mode;
  read_out_cpu = FALSE;
  latency = 0;
  hflit = 0;
  aflit = 0;

  read_miss_retry = 0;
  write_miss_retry = 0;

  w_hit = 0;
  init_write = 0;
  init_wb = 0;
  wb_data_flit_cnt = 0;
  if (active)
  {
     make_cache(); // TODO- add these param 
  }
}

void cacheL2::set_mem_out_buffer(buffer *out) 
{
  mem_out = out;

  //alias mem_out and dram_out
  if (conf->ram_mode == dram)
    dram_out = out;
}
void cacheL2::set_dram_out_buffer(buffer *out) 
{ 
  dram_out = out;

  //alias mem_out and dram_out
  if (conf->ram_mode == dram)
    dram_out = out;
}

void cacheL2::make_cache(){
  int i;

  LRU_head = (Pcache_line *)malloc(sizeof(Pcache_line)* conf->l2_settings.n_sets);
  LRU_tail = (Pcache_line *)malloc(sizeof(Pcache_line)* conf->l2_settings.n_sets);
  set_contents = (int *)malloc(sizeof(int)* conf->l2_settings.n_sets);
  for (i = 0; i < conf->l2_settings.n_sets; i++) {
    LRU_head[i] = (Pcache_line)NULL;
    LRU_tail[i] = (Pcache_line)NULL;
    set_contents[i] = 0;
  }

  contents = 0;
  L2_stat.accesses = 0;
  L2_stat.misses = 0;
  L2_stat.replacements = 0;
  L2_stat.demand_fetches = 0;
  L2_stat.copies_back = 0;
}

//Similar to the perform access function of the original cache model.
int cacheL2::hit(uint64 address, int rw, long long cur_cycle, int vector)
{

  //TODO: Fix
  int hm;
  //uint64 wb_addr;
  if (rw)
    hm = perform_cache_access(address, store, vector);
  else
    hm = perform_cache_access(address, load, vector);
#if L2_OUT
  //if(x_coord==1 && y_coord==1)
  //
  //printf("(%d,%d) Tile had a %d for address - 0x%016llx\n",x_coord,y_coord,hm, address);
#endif 

  if (hm)
    stats->l2_hit++;
  else
    stats->l2_miss++;

  return hm;
}

int cacheL2::perform_cache_access(uint64 addr, cache_access access_type, int vector){

  Pcache_line evict;
  uint64 evict_addr;
  bool wb_vec_check = 0;
  wb_entry wb;

  if ((access_type == store) && (!conf->l2_settings.cache_writeback))
    CACHE_WRITETHROUGH(&L2_stat);

  CACHE_ACCESS(&L2_stat);
  if (hit_miss(addr, access_type, vector))
    return TRUE;
  CACHE_MISS(&L2_stat);

  if (!((access_type == store) && (!conf->l2_settings.cache_writealloc))) {
    /* allocate the block */
    CACHE_FETCH_BLOCK(&L2_stat);
    evict = probe(addr, access_type, vector);
    if (evict) {
      CACHE_REPLACE(&L2_stat);
      if (evict->dirty){
        wb_vec_check = TRUE;
        for (int i = 0; i < conf->l2_settings.n_sectors; i++){
          wb_vec_check = wb_vec_check & evict->valid[i];
        }
        if (wb_vec_check && conf->vector_cache_block_fetch){
          evict_addr = (evict->tag) << conf->l2_settings.index_mask_offset;
          wb.addr = evict_addr;
          wb.data_flits = TRUE;
          wb_queue.push_back(wb);

        }
        else{
          for (int i = 0; i < conf->l2_settings.n_sectors; i++){
            if (evict->valid[i]){
              evict_addr = (evict->tag) << conf->l2_settings.index_mask_offset | (i << conf->l2_settings.sector_mask_offset);
              wb.addr = evict_addr;
              wb.data_flits = FALSE;
              wb_queue.push_back(wb);
            }
          }
        }
        CACHE_WRITEBACK(&L2_stat);
      }
      free(evict->valid);
      free(evict);
    }
  }
  else {
    if (conf->l2_settings.cache_writeback)
      CACHE_WRITETHROUGH(&L2_stat);
  }
  return FALSE; //TODO- check
}


/* Given an address, <addr>, this routine probes the
   cache to see whether the data is contained in the cache.  If a miss
   occurs, the routine will return FALSE.  If a hit occurs, the
   routine will return true, and will also perform the access by
   updating the LRU list.  */
int cacheL2::hit_miss(uint64 addr, cache_access access_type, int vector)
{
  uint64 index;
  int hit, sector;
  Pcache_line cache_line;
  uint64 addr_tag;

  addr_tag = addr >> conf->l2_settings.index_mask_offset;
  index = (addr & conf->l2_settings.index_mask) >> conf->l2_settings.index_mask_offset;
  sector = (addr & conf->l2_settings.sector_mask) >> conf->l2_settings.sector_mask_offset;
  cache_line = LRU_head[index];
  hit = FALSE;
  while (cache_line) {
    if (cache_line->tag == addr_tag && (((vector == FALSE) && (cache_line->valid[sector] == TRUE)) || ((vector == TRUE) && (cache_block_valid_check(cache_line))))) {
      /* "touch" the cache line */
      if ((access_type == store) && (conf->l2_settings.cache_writeback)){
        cache_line->dirty = TRUE;
      }
      delete_entry(&(LRU_head[index]), &(LRU_tail[index]),
        cache_line);
      insert(&(LRU_head[index]), &(LRU_tail[index]),
        cache_line);

      hit = TRUE;
      break;
    }
    cache_line = cache_line->LRU_next;
  }

  return(hit);
}


/* Checking if all the sectors of the cache block are valid for vector
   reads and writes*/
int cacheL2::cache_block_valid_check(Pcache_line cache_line){
  int valid = TRUE, i;
  for (i = 0; i < conf->l2_settings.n_sectors; i++){
    if (cache_line->valid[i] == FALSE){
      valid = FALSE;
      break;
    }
  }
  return valid;
}


/* This routine places the cache line associated with <addr> in the
   cache, and returns any cache line that was evicted as a result
   of the probe.  */
cacheL2::Pcache_line cacheL2::probe(uint64 addr, cache_access access_type, int vector)
{
  uint64 index;
  Pcache_line new_line, evict;
  uint64 addr_tag;
  int sector, idx;

  addr_tag = addr >> conf->l2_settings.index_mask_offset;
  new_line = make_cache_line(addr_tag);
  if ((access_type == store) && (conf->l2_settings.cache_writeback)){
    new_line->dirty = TRUE;
  }
  evict = (Pcache_line)NULL;

  index = (addr & conf->l2_settings.index_mask) >> conf->l2_settings.index_mask_offset;
  sector = (addr & conf->l2_settings.sector_mask) >> conf->l2_settings.sector_mask_offset;
  if (vector){
    for (idx = 0; idx < conf->l2_settings.n_sectors; idx++){
      new_line->valid[idx] = TRUE;
    }
  }
  else
    new_line->valid[sector] = 1;

  insert(&(LRU_head[index]), &(LRU_tail[index]), new_line);
  set_contents[index]++;
  contents++;

  /* evict if necessary */
  if (set_contents[index] > conf->l2_settings.assoc) {
    evict = LRU_tail[index];
    delete_entry(&(LRU_head[index]), &(LRU_tail[index]), evict);
    set_contents[index]--;
    contents--;
  }

  return evict;
}

void cacheL2::flush_cache(){

  int total, i, j;
  Pcache_line current;

  total = 0;
  for (i = 0; i < conf->l2_settings.n_sets; i++) {
    for (j = 0; j < set_contents[i]; j++) {
      current = LRU_head[i];
      LRU_head[i] = current->LRU_next;

      if (current->dirty)
        CACHE_WRITEBACK(&L2_stat);
      free(current);
    }
    LRU_tail[i] = (Pcache_line)NULL;
  }
}

cacheL2::Pcache_line cacheL2::make_cache_line(uint64 tag)
//unsigned tag;
{

  Pcache_line answer;
  answer = (Pcache_line)malloc(sizeof(cache_line));
  answer->tag = tag;
  answer->dirty = FALSE;
  answer->LRU_next = (Pcache_line)NULL;
  answer->LRU_prev = (Pcache_line)NULL;
  answer->valid = (int *)malloc(sizeof(int)* conf->l2_settings.n_sectors);
  for (int i = 0; i < conf->l2_settings.n_sectors; i++){
    answer->valid[i] = 0;
  }
  return(answer);
}


void cacheL2::delete_entry(Pcache_line *head, Pcache_line *tail, Pcache_line item)
{
  if (item->LRU_prev) {
    item->LRU_prev->LRU_next = item->LRU_next;
  }
  else {
    /* item at head */
    *head = item->LRU_next;
  }

  if (item->LRU_next) {
    item->LRU_next->LRU_prev = item->LRU_prev;
  }
  else {
    /* item at tail */
    *tail = item->LRU_prev;
  }
}

/* inserts at the head of the list */
void cacheL2::insert(Pcache_line *head, Pcache_line *tail, Pcache_line item)
{
  item->LRU_next = *head;
  item->LRU_prev = (Pcache_line)NULL;

  if (item->LRU_next)
    item->LRU_next->LRU_prev = item;
  else
    *tail = item;

  *head = item;
}



void cacheL2::cycle(long long cur_cycle)
{
  if (active)
  {
    //route and transfer packets from NOC
    route_in(cur_cycle);
    route_out(cur_cycle);
  }
}


void cacheL2::route_in(long long cur_cycle)
{
  uint64 address, flit;
  unsigned int packet_id;
  //int  dx, dy, bank;
  int bank;
  int vector;


  if (mode == idle_mode)
  {
    if (input->entries() <= 2){
      /*  if(!wb_queue.empty()){//If nothing from noc check if any wb packets are waiting to be sent.
        mode = CACHE_WB;
        init_wb = 1;
        }
        else*/
      return; //we need at least the address
    }
    hflit = input->remove_from_head();

    //check if read, write or reply flit
  if (hflit & READ_FLIT_MASK)
    {
      aflit = input->remove_from_head();
      mode = read_mode;
      latency = conf->l2_settings.cache_lat;
      packet_id = PACKET_ID(hflit);
      stats->set_to_L2(packet_id, cur_cycle, 0);
      stats->set_l2_xy(packet_id, x_coord, y_coord);
#if L2_OUT
      std::ostringstream msg;
      msg << "(" << x_coord << ", " << y_coord << "): L2 received read packet : " << packet_id << "\n"; 
      Stats::get()->printLog(msg.str(), false);
      //printf("(%d, %d): L2 Cache received read packet %d!\n", y_coord, x_coord, packet_id);
#endif
    }
    else if (hflit & WRITE_FLIT_MASK)
    {
      init_write = 1;
      mode = write_mode;
      latency = conf->l2_settings.cache_lat;
      packet_id = PACKET_ID(hflit);
      address = stats->get_addr(packet_id);
      stats->set_l2_xy(packet_id, x_coord, y_coord);
      stats->set_to_L2(packet_id, cur_cycle, 1);
      //stats->set_l2_xy(packet_id, x_coord, y_coord);
      aflit = input->remove_from_head();
#if L2_OUT
      std::ostringstream msg;
      msg << "(" << x_coord << ", " << y_coord << "): L2 received write packet : " << packet_id << "\n"; 
      Stats::get()->printLog(msg.str(), false);
      //printf("(%d, %d): L2 Cache started to receive write packet %d!\n", y_coord, x_coord, packet_id);
#endif
    }
    else if (hflit & REPLY_FLIT_MASK)
    {
      mode = cpu_mode;
      init_write = 1;
    }
    else
    {
      std::stringstream errMsg;
      errMsg << "Error: (" << y_coord << ", " << x_coord << "): Unknown packet " << hflit;
      throw std::invalid_argument(errMsg.str());
    }
  }
  else if (mode == read_mode)
  {
    latency--;
    if (latency <= 0)
    {
      //Get address
      packet_id = PACKET_ID(hflit);
      address = stats->get_addr(packet_id);
      vector = hflit & BYTES_MASK;
      if (!read_miss_retry){
        read_miss_retry = !(hit(address, load, cur_cycle, vector));
      }
      //if (hit(address,LOAD,cur_cycle,vector)) //send to CPU
      if (!read_miss_retry) //send to CPU
      {
        read_out_cpu = TRUE;
        if (out_mode != idle_mode) return; //currently sending packet
        if (hflit&BYTES_MASK && conf->ram_mode != dram) //vector read //TODO DRAM_VECT
        {
          if (output->free_entries() < (8 + 1)) return;  //not enough room
        }
        else //single read
        {
          int fetch_width = (conf->ram_mode == dram) ? conf->dram_settings.fetch_width : conf->reram_settings.fetch_width;
          if (output->free_entries() < (fetch_width + 1)) return;  //not enough room
        }
        send_read_reply(packet_id, cur_cycle);
        stats->set_from_L2(packet_id, cur_cycle, 1);
        read_out_cpu = FALSE;
#if L2_OUT
        std::ostringstream msg;
        msg << "(" << x_coord << ", " << y_coord << "): L2 sent read reply packet : " << packet_id << "\n"; 
        Stats::get()->printLog(msg.str(), false);
        //printf("(%d, %d): L2 Cache sent read reply %d!\n", y_coord, x_coord, packet_id);
#endif
      }
      else  //send to RAM
      {
        if (mem_out->free_entries() < 2) return; //not enought room
        if (conf->ram_mode == reram_cpu || conf->ram_mode == l2_reram || conf->ram_mode == het_l2_reram){
          mem_out->insert_at_tail(hflit);
          bank = (int)(aflit && BANK_MASK);
          if (!vector)
            stats->bank_profile[x_coord][y_coord][bank]++;
          else{
            for (int idx = 0; idx < 8; idx++){
			  stats->bank_profile[x_coord][y_coord][bank]++;
              bank = (bank + 1) % conf->reram_settings.banks;
            }
          }
        }
        else //need new header
        {
          flit = get_dram_header(hflit, &aflit, packet_id, address, cur_cycle);
          mem_out->insert_at_tail(flit);
          stats->set_l2_xy(packet_id, x_coord, y_coord);
          aflit |= TAIL_FLIT_MASK;
        }
        mem_out->insert_at_tail(aflit);
        read_miss_retry = FALSE;
      }

      mode = idle_mode;
    }
  }
  else if (mode == write_mode)
  {
    latency--;
    if (latency <= 0)
    {
      if (init_write)
      {
        //Get address
        packet_id = PACKET_ID(hflit);
        address = stats->get_addr(packet_id);
        vector = hflit & BYTES_MASK;
        if (!write_miss_retry) {
          w_hit = hit(address, store, cur_cycle, vector);
          write_miss_retry = !w_hit;
        }

        if (!w_hit && conf->ram_mode == dram && !vector) //send READ to DRAM if not vector (to fill cache block)
        {
          if (mem_out->free_entries() < 2) return; //not enough room

          packet_id = stats->set_start(read_packet, cur_cycle, x_coord, y_coord, address, vector, -1);
          stats->set_to_L2(packet_id, cur_cycle, 1);
          flit = READ_FLIT_MASK;
          flit |= vector;
          flit = get_dram_header(flit, &aflit, packet_id, address, cur_cycle);
          mem_out->insert_at_tail(flit);
          stats->set_l2_xy(packet_id, x_coord, y_coord);
          aflit |= TAIL_FLIT_MASK;
          mem_out->insert_at_tail(aflit);
        }

        write_miss_retry = FALSE;
        init_write = 0;
      }

      while (!input->is_empty()){ // Finish reading the write data

        flit = input->remove_from_head();
        if (flit & TAIL_FLIT_MASK){
          packet_id = PACKET_ID(hflit);
#if L2_OUT
          printf("(%d, %d): L2 Cache received write packet %d!\n", y_coord, x_coord, packet_id);
#endif
          stats->set_from_L2(packet_id, cur_cycle, 1); // Getting rid of write packet
          w_hit = 0;
          mode = idle_mode;
          break;    //end of write packet
        }
      }

      //Read the rest of the write packet
      /*
        while (!input->is_empty() && !mem_out->is_full()) { //read tail end of write
        if (!w_hit)  //send to RAM
        {
        mem_out->insert_at_tail(flit);
        }
        }
        */
    }
  }
  else if (mode == cpu_mode)
  {
    //Get address
    packet_id = PACKET_ID(hflit);
    if (init_write)
    {
      if (output->is_full() || !(out_mode == idle_mode)) return; //not enought room or currently sending something else
      //get the correct header flit back to CPU
      flit = get_delta_flit(packet_id) | REPLY_FLIT_MASK;
      output->insert_at_tail(flit);
      stats->set_from_L2(packet_id, cur_cycle, 0);

      init_write = 0;
    }

    //Read the rest of the reply packet
    while (!input->is_empty() && !output->is_full() && out_mode == idle_mode) {
      flit = input->remove_from_head();
      output->insert_at_tail(flit);

      if (flit & TAIL_FLIT_MASK){
#if L2_OUT
        std::ostringstream msg;
        msg << "(" << x_coord << ", " << y_coord << "): L2 sent read reply packet : " << packet_id << "\n"; 
        Stats::get()->printLog(msg.str(), false);
        //printf("(%d, %d): L2 Cache sent reply packet %d from DRAM!\n", y_coord, x_coord, packet_id);
#endif
        mode = idle_mode;
        break;    //end of reply packet
      }
    }
  }
  else if (mode == cache_wb_mode){
    uint64 header_flit, addr_flit;
    wb_entry wb;
    unsigned packet_id;
    vector = 0;
    int delta_x, delta_y, bank;
    if (init_wb){
      if (mem_out->free_entries() < 2) return; //Not enough room
      wb = wb_queue.front();
      wb_queue.pop_front();
      packet_id = stats->set_start(write_packet, cur_cycle, x_coord, y_coord, wb.addr, wb.data_flits, -1);
      stats->set_to_L2(packet_id, cur_cycle, 1);
      address_translation(wb.addr, &delta_x, &delta_y, &bank);

      header_flit = WRITE_FLIT_MASK;
      header_flit |= wb.data_flits;
      header_flit = make_header(header_flit, delta_x, delta_y, packet_id);
      //header_flit |= 1;
      if (conf->ram_mode == reram_cpu || conf->ram_mode == l2_reram)
        mem_out->insert_at_tail(header_flit);
      else  //need new header
      {
        flit = get_dram_header(header_flit, &addr_flit, packet_id, wb.addr, cur_cycle);
        mem_out->insert_at_tail(flit);
        stats->set_l2_xy(packet_id, x_coord, y_coord);
      }

      flit = bank;
      mem_out->insert_at_tail(flit);

      init_wb = 0;
      if (wb.data_flits)
        wb_data_flit_cnt = 64 / 8; // size of vector = 64B and flit size is 8B
      else
        wb_data_flit_cnt = conf->l2_settings.sector_size / 8; // 8 is flit size

      if (conf->ram_mode != dram){
        if (!wb.data_flits)
			stats->bank_profile[delta_x + x_coord][delta_y + y_coord][bank]++;
        else{
          for (int wb_idx = 0; wb_idx < 8; wb_idx++){
			  stats->bank_profile[delta_x + x_coord][delta_y + y_coord][bank]++;
            bank = (bank + 1) % conf->reram_settings.banks;
          }
        }
      }

    }
    while (wb_data_flit_cnt > 0 && !mem_out->is_full()) { //send write data
      flit = 0;
      if (wb_data_flit_cnt == 1){
        flit |= TAIL_FLIT_MASK;
        mode = idle_mode;
        stats->wb_from_l2++;
      }

      mem_out->insert_at_tail(flit);
      wb_data_flit_cnt--;

      if (wb_data_flit_cnt == 0) return;
    }

  }
  if (mode == idle_mode){ //Finished with a packet from noc. Now sending a writeback packet if present in wb queue
    if (!wb_queue.empty()){
      mode = cache_wb_mode;
      init_wb = 1;
    }
  }
}

void cacheL2::route_out(long long cur_cycle)
{
  uint64 flit;
  unsigned packet_id;

  if (conf->ram_mode == reram_cpu || conf->ram_mode == l2_reram)
  {
    while (!output->is_full() && !mem_in->is_empty())
    {
      //TODO: Add something to insert the data into the cache
      flit = mem_in->remove_from_head();
      if (flit & HEADER_FLIT_MASK)
      {
        packet_id = PACKET_ID(flit);
        stats->set_from_L2(packet_id, cur_cycle, 0);
        out_mode = cpu_mode;
      }

      if (flit & TAIL_FLIT_MASK)
      {
        out_mode = idle_mode;
        if (mode == read_mode && latency < 0){
          output->insert_at_tail(flit);
          break;
        }
      }
#if ROUTER_OUT
      //printf("(%d, %d): moving %#.16llx to P in 2\n", y_coord, x_coord, flit);

      std::ostringstream msg;
      msg << "(" << x_coord << ", " << y_coord << "): moving ";
      msg << "0x" << setfill('0') << setw(16) << hex << flit << " to P in 2\n";	
      Stats::get()->printLog(msg.str(), false);
#endif
      output->insert_at_tail(flit);
    }
  }
  else
  {
    while (!output->is_full() && !mem_in->is_empty())
    {
      //if (out_mode == idle_state && !(mode == read_mode && latency < 0 ) )
      if (out_mode == idle_mode && !(read_out_cpu))
      {
        out_hflit = mem_in->remove_from_head();

        if (out_hflit & REPLY_FLIT_MASK)
        {
          out_mode = cpu_init_mode;
        }
        else
        {
          std::stringstream errMsg;
          errMsg << "Error: (" << y_coord << ", " << x_coord << "): Unknown packet " << out_hflit;
          throw std::invalid_argument(errMsg.str());
        }
      }
      else if (out_mode == cpu_init_mode)
      {
        //Get address
        packet_id = PACKET_ID(out_hflit);
        //get the correct header flit back to CPU
        flit = get_delta_flit(packet_id) | REPLY_FLIT_MASK;
        output->insert_at_tail(flit);
        stats->set_from_L2(packet_id, cur_cycle, 0);
        out_mode = cpu_mode;
#if L2_OUT
        std::ostringstream msg;
        msg << "(" << x_coord << ", " << y_coord << "): L2 sent read reply packet : " << packet_id << "\n"; 
        Stats::get()->printLog(msg.str(), false);
        //printf("(%d, %d): L2 Cache sent reply packet from DRAM!\n", y_coord, x_coord);
#endif
      }
      else if (out_mode == cpu_mode)
      {

        //Read the rest of the reply packet
        flit = mem_in->remove_from_head();
        output->insert_at_tail(flit);

        if (flit & TAIL_FLIT_MASK){
#if L2_OUT
          //printf("(%d, %d): L2 Cache sent reply packet from DRAM!\n", y_coord, x_coord);
#endif
          out_mode = idle_mode;
        }
      }
      else
      {
        break;
      }
    }
  }
}

void cacheL2::send_read_reply(unsigned packet_id, long long cur_cycle)
{
  uint64 flit;
  int data_flits;

  if (conf->ram_mode == dram)
    data_flits = conf->dram_settings.fetch_width;
  else if (hflit&BYTES_MASK)
    data_flits = 8;
  else //in heterogenous system assume all CPU/L1 traffic is at the ReRAM granularity
    data_flits = conf->reram_settings.fetch_width;

  flit = get_delta_flit(packet_id) | REPLY_FLIT_MASK;
  output->insert_at_tail(flit);
#if L2_OUT
  uint64 dx_temp, dy_temp;
  dx_temp = (flit & DELTA_X_MASK) >> X_OFFSET;
  dy_temp = (flit & DELTA_Y_MASK) >> Y_OFFSET;
  if(flit & DIRECTION_X_MASK)
    dx_temp = -dx_temp;
  if(flit & DIRECTION_Y_MASK)
    dy_temp = -dy_temp;
  std::ostringstream msg;
  msg << "(" << x_coord << ", " << y_coord << "): L2 sent read reply packet : " << packet_id << "to (" << (x_coord+dx_temp) << ", " << (y_coord+dy_temp) << ")" <<"\n"; 
  Stats::get()->printLog(msg.str(), false);
  //printf("(%d, %d): L2 Cache sent read reply %d!\n", y_coord, x_coord, packet_id);
#endif
  flit = 0;    //64 bits of data
  for (int i = 0; i < (data_flits - 1); i++)
  {
    output->insert_at_tail(flit);
  }

  flit = 0 | TAIL_FLIT_MASK;    //64 bits of data
  output->insert_at_tail(flit);
}

void cacheL2::address_translation(uint64 addr, int* delta_x, int* delta_y, int * bank)
{
  int bank_interleave = 0;

  if (conf->ram_mode == dram)
  {
    if (conf->addr_mode == random_addr){
      get_random_dAddress(delta_x, delta_y, bank);
    }
    else if (conf->addr_mode == local_addr)
    {
      get_local_dAddress(addr, delta_x, delta_y, bank);
    }
    else
    {
      get_mapped_dAddress(addr, delta_x, delta_y, bank);
    }
  }
  else if (conf->ram_mode == l2_cpu)
  {
    if (conf->addr_mode == random_addr) {
      get_random_rAddress(delta_x, delta_y, bank);
    }
    else if (conf->addr_mode == local_addr)
    {
      get_local_rAddress(addr, delta_x, delta_y, bank);
    }
    else if (conf->addr_mode == bank_first)
    {
      get_bankint_rAddress(addr, delta_x, delta_y, bank);
    }
    else
    {
      get_tileint_rAddress(addr, delta_x, delta_y, bank);
    }
  }
  else
  {
    *delta_x = 0;
    *delta_y = 0;
    if (conf->addr_mode == random_addr){
      get_random_rAddress_wb(bank);
    }
    else if (conf->addr_mode == local_addr)
    {
      get_local_rAddress_wb(addr, bank);
    }
    else if (conf->addr_mode == bank_first)
    {
      get_bankint_rAddress_wb(addr, bank);
    }
    else
    {
      get_tileint_rAddress_wb(addr, bank);
    }
  }

  //bank_profile[*delta_x + x_coord][*delta_y + y_coord][*bank]++;
}


void cacheL2::get_random_dAddress(int* delta_x, int* delta_y, int* bank)
{
  int tile = random() % (conf->num_dram_tiles);
  int x, y;
  *bank = random() % (conf->dram_settings.banks);

  conf->nth_tile_type(tile, dram_tile, &x, &y);
  *delta_x = x - x_coord;
  *delta_y = y - y_coord;
}

void cacheL2::get_local_dAddress(uint64 addr, int* delta_x, int* delta_y, int* bank) {
  int x, y;

  *bank = (int)(((addr & conf->dram_bank_bits) >> conf->dram_bank_offset) ^ ((addr & conf->bank_xor_bits) >> conf->bank_xor_offset));

  conf->local_tile(x_coord, y_coord, dram_tile, &x, &y);
  *delta_x = x - x_coord;
  *delta_y = y - y_coord;
}

void cacheL2::get_mapped_dAddress(uint64 addr, int* delta_x, int* delta_y, int* bank) {
  int x, y, tile;
  uint64 dbank, dxor, dres;
  
  dbank = conf->dram_bank_bits;
  dbank &= addr;
  dbank = dbank >> conf->dram_bank_offset;
  dxor = conf->bank_xor_bits;
  dxor &= addr;
  dxor = dxor >> conf->bank_xor_offset;
  dres = dbank ^ dxor;
  *bank = (int)dres;

  //*bank = (int)(((addr & DRAM_BANK_BITS) >> DRAM_BANK_OFFSET) ^ ((addr & BANK_XOR_BITS) >> BANK_XOR_OFFSET));

  addr = addr & conf->channel_bits; //take only the channel bits
  addr = addr >> conf->channel_offset;  //shift out the offset to get just channel
  tile = addr % conf->num_dram_tiles; //should be unnecessary, but if config is done wrong, maybe not

  conf->nth_tile_type(tile, dram_tile, &x, &y);
  *delta_x = x - x_coord;
  *delta_y = y - y_coord;
}

void cacheL2::get_random_rAddress(int* delta_x, int* delta_y, int* bank)
{
  int value = random() % conf->cpu_tiles;
  int x, y;

  conf->nth_tile_type(value, reram_tile, &x, &y);

  *delta_x = x - x_coord;
  *delta_y = y - y_coord;

  *bank = random() % (conf->reram_settings.banks);
}

void cacheL2::get_local_rAddress(uint64 addr, int* delta_x, int* delta_y, int* bank) {
  int x, y;

  addr = addr >> conf->l2_settings.reram_fetch_shift;  //address for bytes fetched from each bank

  //go to closest controller then interleave accross banks
  conf->local_tile(x_coord, y_coord, reram_tile, &x, &y);
  *delta_x = x - x_coord;
  *delta_y = y - y_coord;

  *bank = addr % conf->reram_settings.banks;
}

/*
Given a 64-bit byte address, maps to a particular bank and updates the values of delta_x, delta_y, and bank
assume subarray is 0
Assume stride byte-wise across all subarrays
*/
void cacheL2::get_bankint_rAddress(uint64 addr, int* delta_x, int* delta_y, int* bank) {
  int x, y, n;

  addr = addr >> conf->l2_settings.reram_fetch_shift;  //address for bytes fetched from each bank

  *bank = addr % conf->reram_settings.banks;

  n = addr / conf->reram_settings.banks % (conf->num_reram_tiles);
  conf->nth_tile_type(n, reram_tile, &x, &y);

  *delta_x = x - x_coord;
  *delta_y = y - y_coord;
}

/*
Given a 64-bit byte address, maps to a particular bank and updates the values of delta_x, delta_y, and bank
assume subarray is 0
Assume stride byte-wise across all subarrays
*/
void cacheL2::get_tileint_rAddress(uint64 addr, int* delta_x, int* delta_y, int* bank) {
  int x, y, n;
  uint64 addr_tile;

  int bank_shift_per_vector = 6 - conf->l2_settings.reram_fetch_shift;
  if (bank_shift_per_vector < 0)
    bank_shift_per_vector = 0;
  int banks_per_vector = pow(2, bank_shift_per_vector);

  addr = addr >> conf->l2_settings.reram_fetch_shift;  //address for bytes fetched from each bank
  addr_tile = addr >> bank_shift_per_vector;  //allows placing banks for a vector access on same tile

  n = addr_tile % (conf->num_reram_tiles);
  conf->nth_tile_type(n, reram_tile, &x, &y);

  *bank = ((addr_tile / (conf->cpu_tiles)) * banks_per_vector + (addr % banks_per_vector)) % conf->reram_settings.banks;
  *delta_x = x - x_coord;
  *delta_y = y - y_coord;

}

void cacheL2::get_random_rAddress_wb(int* bank)
{
  *bank = random() % (conf->reram_settings.banks);
}

void cacheL2::get_local_rAddress_wb(uint64 addr, int* bank) {
  int bank_interleave = 0;
  Conf_ReRAM *conf = Conf_ReRAM::get();

  addr = addr >> conf->l2_settings.reram_fetch_shift;  //address for bytes fetched from each bank

  *bank = addr % conf->reram_settings.banks;
}

/*
Given a 64-bit byte address, maps to a particular bank and updates the values of delta_x, delta_y, and bank
assume subarray is 0
Assume stride byte-wise across all subarrays
*/
void cacheL2::get_bankint_rAddress_wb(uint64 addr, int* bank) {
  addr = addr >> conf->l2_settings.reram_fetch_shift;  //address for bytes fetched from each bank

  *bank = addr % conf->reram_settings.banks;
}

/*
Given a 64-bit byte address, maps to a particular bank and updates the values of delta_x, delta_y, and bank
assume subarray is 0
Assume stride byte-wise across all subarrays
*/
void cacheL2::get_tileint_rAddress_wb(uint64 addr, int* bank) {
  uint64 addr_tile;

  int bank_shift_per_vector = 6 - conf->l2_settings.reram_fetch_shift;
  if (bank_shift_per_vector < 0)
    bank_shift_per_vector = 0;
  int banks_per_vector = pow(2, bank_shift_per_vector);
  addr = addr >> conf->l2_settings.reram_fetch_shift;  //address for bytes fetched from each bank
  addr_tile = addr >> bank_shift_per_vector;  //allows placing banks for a vector access on same tile

  *bank = ((addr_tile / (conf->cpu_tiles)) * banks_per_vector + (addr % banks_per_vector)) % conf->reram_settings.banks;
}


uint64 cacheL2::make_header(uint64 header_flit, int delta_x, int delta_y, unsigned packet_id)
{
  uint64 flit = 0;
  if (delta_x < 0) {
    flit = DIRECTION_X_MASK >> X_OFFSET;
    delta_x = -delta_x;
  }
  flit = flit | delta_x;
  header_flit = header_flit | (flit << X_OFFSET);

  flit = 0;
  if (delta_y < 0) {
    flit = DIRECTION_Y_MASK >> Y_OFFSET;
    delta_y = -delta_y;
  }
  flit = flit | delta_y;
  header_flit = header_flit | (flit << Y_OFFSET);
  header_flit |= ((uint64)packet_id) << PACKET_OFFSET;

  return header_flit;
}
