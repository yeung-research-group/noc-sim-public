#pragma once

#include <list>

class L2cache_het : virtual public L2cache_bank
{
public:
    L2cache_het(int x, int y);
    virtual void cycle(long long cur_cycle) override = 0;

    buffer* get_dram_in_buffer() override { return (dram_in); };
    void set_dram_out_buffer(buffer* out) override;
    virtual buffer* get_mem_in_buffer() override = 0;
    virtual void set_mem_out_buffer(buffer* out) override = 0;


protected:
    //Network
    buffer* dram_in;
    buffer* dram_out;

    //DRAM network state
    mem_request* dram_req;
    mem_request* out_dram_req;
    wb_entry* out_wb;
    noc_state dram_read_state;
    noc_state dram_write_state;

    //mem_request* cur_req;
    //long long cycle_complete;
    //void handle_request(long long cur_cycle);

    //DRAM request queue
    std::queue<mem_request> dram_req_queue;

    std::list<wb_entry> dram_wb_queue;
    void writeback_req(L2_cache_line* evicted) override;

    bool send_dram_request(long long cur_cycle);
    bool read_dram_reply(long long cur_cycle);
};


