import ConfigParser
import os
import subprocess
import shlex


for x in range (8, 33):
	base_ini_config = open("config_base.ini", 'r')
	new_ini_config = open("config.ini", 'w')
	threads = 1024/(x*x)
	packets = 1000*threads;
#DRAM settings
	ini_config = ConfigParser.RawConfigParser()
	ini_config.optionxform = str
	ini_config.readfp(base_ini_config)

	ini_config.set('sim_settings','trace_file', "filteredTraceDRAM.txt")
	ini_config.set('sim_settings','dram_mode', 1)
        
      
	ini_config.set('noc_structure','x_max', x)
	ini_config.set('noc_structure','y_max', x)
	ini_config.set('noc_structure','banks', 64)
	ini_config.set('noc_structure','packets', packets)
	ini_config.set('noc_structure','fetch_width', 16)
	ini_config.set('noc_structure','phits',8)

	ini_config.set('noc_timing','read_lat', 55)
	ini_config.set('noc_timing','write_lat', 55)

	ini_config.set('core_settings','threads_per_core', threads)

	ini_config.write(new_ini_config)
	base_ini_config.close()
	new_ini_config.close()
	os.system('./noc-sim')


for x in range (8, 33):
	threads = 1024/(x*x)
	packets = 1000*threads
#ReRAM settings
	base_ini_config = open("config_new.ini", 'r')
	new_ini_config = open("config.ini", 'w')
	ini_config = ConfigParser.RawConfigParser()
	ini_config.optionxform = str
	ini_config.readfp(base_ini_config)
	ini_config.set('sim_settings','trace_file', "filteredTrace.txt")
	ini_config.set('sim_settings','dram_mode', 0)
	ini_config.set('noc_structure','x_max', x)
	ini_config.set('noc_structure','y_max', x)
	#x = ini_config.get('noc_structure','x_max')
  	#y = ini_config.get('noc_structure','y_max')
	banks = 8192/(x*x)
	ini_config.set('noc_structure','banks', banks)
	ini_config.set('noc_structure','packets', packets)
	ini_config.set('noc_structure','fetch_width', 1)
	ini_config.set('noc_structure','phits',1)

	ini_config.set('noc_timing','read_lat', 500)
	ini_config.set('noc_timing','write_lat', 1000)

	ini_config.set('core_settings','threads_per_core', threads)


	ini_config.write(new_ini_config)
	base_ini_config.close()
	new_ini_config.close()
	os.system('./noc-sim')
#command = "./noc_sim"
#args = shlex.split('noc_sim')
#p = subprocess.Popen(args)
