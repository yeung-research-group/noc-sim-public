#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_reram::L2cache_reram(int x, int y) : L2cache_bank(x, y)
{
    //make bank schedule
    bank_schedule = std::priority_queue<reram_task>();

    cycle_complete = 0;
    cur_req = nullptr;

    //make reram banks and bank info
    n_reram_banks = (conf->reram_settings.banks) / conf->l2_settings.banks;
    reram_banks = new bank_reram[n_reram_banks];

    for (int i = 0; i < n_reram_banks; i++)
    {
        reram_banks[i] = { false, 0, nullptr };
    }

}

void L2cache_reram::cycle(long long cur_cycle)
{

    //route out and only proceed if output is cleared
    route_out(cur_cycle);
    if (output_queue.size() < conf->buffer_size && cycle_complete <= cur_cycle)  //TODO: better size limit?
    {
        handle_request(cur_cycle);
    }

    //route and transfer packets from NOC
    if (request_rob.size() < conf->l2_settings.rob_entries)
    {
        route_in(cur_cycle);
    }
}

void L2cache_reram::handle_request(long long cur_cycle)
{
    if (cur_req != nullptr)
    {
        if (cur_req->next_stage == rr_r)  //receiving ReRAM reply
        {
            //send next reram request
           /* mem_request* next_req;
            if(read_service_priority == 0){
                next_req = find_wb_request(cur_req->bank);
                if (next_req != nullptr)
                {
                    reram_send_req(cur_cycle, next_req);
                }
                read_service_priority = 8;
            }
            else{
            next_req = find_next_request(cur_req->subbank, cur_req->bank);
            if (next_req != nullptr)
            {
                if(next_req->type == read_block)
                    next_req->bank = cur_req->bank;
                reram_send_req(cur_cycle, next_req);
                next_req->mshr->sectors_recv[SECTOR_ID(next_req->bank)] = sent_req;
                read_service_priority--;
            }
            else  //handle write backs
            {
                //check for any write backs to send to ReRAM
                next_req = find_wb_request(cur_req->bank);
                if (next_req != nullptr)
                {
                    reram_send_req(cur_cycle, next_req);
                }
            }
            }*/
            mem_request* next_req = find_next_request(cur_req->subbank, cur_req->bank);
            if (next_req != nullptr)
            {
                if(next_req->type == read_block)
                    next_req->bank = cur_req->bank;
                reram_send_req(cur_cycle, next_req);
                next_req->mshr->sectors_recv[SECTOR_ID(next_req->bank)] = sent_req;
            }
            else  //handle write backs
            {
                //check for any write backs to send to ReRAM
                next_req = find_wb_request(cur_req->bank);
                if (next_req != nullptr)
                {
                    reram_send_req(cur_cycle, next_req);
                }
            }

            if (cur_req->type == read_block)
            {
                //for blocks, we made extra requests for each reram bank accessed
                //we need to merge them with the original request, then delete
                mem_request* hold = cur_req;
                if(cur_req->mshr->done)
                {
                    std::stringstream errMsg;
                    errMsg << "Error: (" << y_coord << ", " << x_coord << "): MSHR already completed.";
                    throw std::invalid_argument(errMsg.str());
                }
                cur_req = cur_req->read_sub->req;
                cur_req->valid_data = true;

                for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                    cur_req->valid[i] |= hold->valid[i];

                delete hold;
            }

            //check if mshr complete
            if (cur_req->type == write_back)
            {
                //done with that request
                delete cur_req;
            }
            else if (check_mshr_complete(cur_req))
            {
                //cache fill
                L2_cache_line* evicted = cache_array->tag_update(*cur_req);

                //handle write back buffer
                if (evicted != nullptr)
                {
                    writeback_req(evicted);
                }

                //send back any read replies
                mshr_entry* mshr = cur_req->mshr;
                unsigned subbank = cur_req->subbank;
                //cur_req = new mem_request(*cur_req);  //so request doesn't get deleted below
                while (mshr->read_subentries > 0)
                {
                    output_queue.push(*mshr->read_sub[--mshr->read_subentries].req);
                    delete mshr->read_sub[mshr->read_subentries].req;
                }

                //done with the read subentries, delete them
                delete_mshr(mshr, subbank);


                //delete cur_req;
            }
        }
        else if (cur_req->type == read_block || cur_req->type == read_dw)
        {
            //done with the read
            if (cur_req->valid_data)
            {
                output_queue.push(*cur_req);
                delete cur_req;
            }
            else
            {
                //send reram requests
                if (cur_req->type == read_dw)
                {
                    //Check if bank can accept request and send if it can
                    if (!reram_banks[cur_req->bank].busy && cur_req->mshr->sectors_recv[SECTOR_ID(cur_req->bank)] == pending_req)
                    {
                        reram_send_req(cur_cycle, cur_req);
                        cur_req->mshr->sectors_recv[SECTOR_ID(cur_req->bank)] = sent_req;
                    }
                }
                else if (cur_req->type == read_block)
                {

                    //Check if bank can accept request and send if it can
                    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                    {
                        if (cur_req->mshr->sectors_recv[i] == pending_req &&
                            !reram_banks[cur_req->bank + i].busy)
                        {
                            mem_request* next_req = new mem_request(*cur_req);
                            next_req->next_stage = rr_s;
                            next_req->bank += i;
                            reram_send_req(cur_cycle, next_req);
                            cur_req->mshr->sectors_recv[SECTOR_ID(next_req->bank)] = sent_req;
                        }
                    }
                }
            }
        }
        else  //write request
        {
            //update cache tags if its not in the MSHR file or in the cache already
            if (cur_req->valid_data)
            {
                L2_cache_line* evicted = cache_array->tag_update(*cur_req);

                if (evicted != nullptr)
                {
                    writeback_req(evicted);
                }
            }

            stats->set_from_L2(cur_req->packet_id, cur_cycle, 1, x_coord, y_coord);
            delete cur_req;
        }
    }

    //get next request
    if (!bank_schedule.empty() && bank_schedule.top().cycle <= cur_cycle)
    {
        reram_banks[bank_schedule.top().bank].busy = false;
        cur_req = reram_banks[bank_schedule.top().bank].cur_op;
        cur_req->valid[SECTOR_ID(cur_req->bank)] = true;
        cur_req->next_stage = rr_r;
        cur_req->valid_data = true;
        
        bank_schedule.pop();
    }
    else if (!request_rob.empty())
    {
        cur_req = get_next_request();
    }
    else
    {
        cur_req = nullptr;
    }

    //find time to complete request
    if (cur_req != nullptr)
    {
        if (cur_req->next_stage == rr_r)
        {
            cycle_complete = cur_cycle + 4;
            
            if (cur_req->type == read_dw || cur_req->type == read_block)
            {
                stats->set_from_bank(cur_req->packet_id, cur_cycle, x_coord, y_coord);
                cycle_complete += 1;
                update_mshr(cur_req);
                if (check_mshr_complete(cur_req))
                {
                    cycle_complete += 4 + cur_req->mshr->read_subentries;

                    //cache fill
                    tag_res tag = cache_array->tag_look_up(cur_req->addr);

                    if (tag.evict_dirty)
                        cycle_complete += 2;
                }
            }
        }
        else
        {
            cycle_complete = cur_cycle + 4;

            //check if hit/miss
            bool valid = tag_lookup(cur_req);
            bool dirty = cur_req->dirty_data;
            cur_req->valid_data = valid;

            if (!valid)
            {
                //do mshr stuff
                mshr_entry* hit = check_mshrs(cur_req);
                if (cur_req->type == write_dw || cur_req->type == write_block)
                {
                    if (!hit)
                    {
                        if (dirty)
                            cycle_complete += 3;
                        else
                            cycle_complete += 1;

                        cur_req->valid_data = true;
                    }
                    else
                    {
                        //write data into MSHR
                        //cycle_complete += 2;
                        write_mshr(cur_req);
                    }
                }
                else  //read
                {
                    cycle_complete += 1;
                    if (!hit)
                    {
                        //read without hit, make new mshr
                        cur_req->mshr = handle_mshr_miss(cur_req);
                        //create Read Subentry
                        add_read_subentry(cur_req);
                    }
                    else  //hit
                    {
                        valid = check_read_valid(cur_req);
                        if (valid)
                        {
                            cur_req->valid_data = true;
                        }
                        else
                        {
                            //create Read Subentry
                            add_read_subentry(cur_req);
                        }
                    }
                }
            }
        }
    }
}

void L2cache_reram::reram_send_req(long long cur_cycle, mem_request* req)
{
    //transmit to rr
    if (!reram_banks[req->bank].busy)
    {
        long long done;
        reram_banks[req->bank].busy = true;
        reram_banks[req->bank].uses++;
        reram_banks[req->bank].cur_op = req;

        stats->bank_profile_incr(x_coord, y_coord, req->bank);

        if (req->type != write_back)
        {
            done = cur_cycle + (conf->reram_settings.read_lat / conf->cycle_time);

            //schedule task -- just so we aren't looping too much; hardware would look at latency count down
            reram_task task = { done, req->bank, true };
            bank_schedule.push(task);

            //tell stats what we've done
            stats->set_to_bank(req->packet_id, cur_cycle, x_coord, y_coord);
            stats->tile_ram_stats[IND(x_coord, y_coord)].reram_read++;

            req->next_stage = rr;
            req->cur_stage = rr;
        }
        else
        {
            done = cur_cycle + (conf->reram_settings.write_lat / conf->cycle_time);

            //schedule task -- just so we aren't looping too much; hardware would look at latency count down
            reram_task task = { done, req->bank, false };
            bank_schedule.push(task);

            //tell stats what we've done
            stats->tile_ram_stats[IND(x_coord, y_coord)].reram_write++;

            req->next_stage = rr;
        }

#if BANK_UTILIZATION_TRACKING
        stats->tile_ram_stats[IND(x_coord, y_coord)].busy_bank_cycles += (done - cur_cycle);
        if (done > (cur_cycle + (EPOCH_PERIOD - cur_cycle % EPOCH_PERIOD)))
            stats->tile_ram_stats[IND(x_coord, y_coord)].extra_cycles_from_next_epoch += done - (cur_cycle + (EPOCH_PERIOD - cur_cycle % EPOCH_PERIOD));
#endif

    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: (" << y_coord << ", " << x_coord << "): Cannot handle request; ReRAM bank already in use.";
        throw std::invalid_argument(errMsg.str());
    }
}

/*
void L2cache_reram::check_incoming_reram(long long cur_cycle)
{
    if (!bank_schedule.empty() && bank_schedule.top().cycle <= cur_cycle)
    {
        //if (mshr_active_latch || tag_active_latch)
        //{
        //    stall++;
        //}
        //else
        //{
            if (bank_schedule.top().read)
            {
                //if (move_req(reram_banks[bank_schedule.top().bank].cur_op, rr_r, false))
                //{
                    stats->set_from_bank(decode_latch->packet_id, cur_cycle);
                    decode_latch->valid[SECTOR_ID(decode_latch->bank)] = true;
                    decode_latch->valid_data = true;

                    reram_banks[bank_schedule.top().bank].busy = false;
                    bank_schedule.pop();
                //    stall += 4;
               // }
            }
            else  //write is done - frees bank, need to check for next item
            {
                // (move_req(reram_banks[bank_schedule.top().bank].cur_op, mshr_r, false))
                //{
                    mshr_latch->valid_data = true;
                    reram_banks[bank_schedule.top().bank].busy = false;
                    bank_schedule.pop();
                //
            }
        }
    }
}
*/

void L2cache_reram::writeback_req(L2_cache_line * evicted)
{
    wb_entry wb;
    //bool vec_write = true;

    //no vector wb because we have to do each one individually from ReRAM perspective
    for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
    {
        if (evicted->dirty[i])
        {
            //wb tag is just addr without sector, so add back to get word address
            wb = { (evicted->tag) << conf->l2_settings.index_mask_offset | ((uint64)i << conf->l2_settings.sector_mask_offset) , 1 };
            stats->tile_l2_stats[IND(x_coord, y_coord)].wb_from_l2++;

            //if the bank isn't busy, schedule the write back
            if (!reram_banks[BANK_ID(wb.addr)].busy)
            {
                mem_request wb_req = mem_request(wb.addr, BANK_ID(wb.addr), SUBBANK_ID(wb.addr), wb.data_flits);
                request_rob.push_front(wb_req);
            }
            else
            {
                wb_queue.push_back(wb);
            }
        }
        // else
        //{
        //    vec_write = false;
        //}
    }

    //TODO: remove as this is double counting--just for debug purposes
    //if(vec_write)
    //    stats->vec_wb_from_l2++;

    delete evicted;
}

mem_request* L2cache_reram::find_next_request(unsigned subbank, unsigned bank)
{
    list<mshr_entry>* mshr_list;
    int sector = SECTOR_ID(bank);

    if (conf->l2_settings.banked_mshr)
    {
        mshr_list = &mshrs[subbank];
    }
    else
    {
        mshr_list = mshrs;
    }

    //from start of the list back, search for mshr with pending entry for correct bank/sector
    for (std::list<mshr_entry>::iterator mshr = mshr_list->begin(); mshr != mshr_list->end(); ++mshr)
    {
        if (mshr->subbank == subbank)
        {
            if (mshr->sectors_recv[sector] == pending_req)
            {
                for (unsigned i = 0; i < mshr->read_subentries; i++)
                {
                    if (mshr->read_sub[i].vector)
                    {
                        mem_request* new_vec = new mem_request(*mshr->read_sub[i].req);
                        //new_vec->bank = bank;
                        return new_vec;
                    }
                    else if (mshr->read_sub[i].sector == sector)
                    {
                        return mshr->read_sub[i].req;
                    }
                }
            }
        }
    }

    return nullptr;
}


mem_request* L2cache_reram::find_wb_request(unsigned bank)
{
    //std::list<wb_entry> wb_queue;
    /*for (std::list<wb_entry>::iterator wb = wb_queue.begin(); wb != wb_queue.end(); ++wb)
    {
        if (bank == (BANK_ID(wb->addr)))
        {
            mem_request* wb_req = new mem_request(wb->addr, bank, SUBBANK_ID(wb->addr), wb->data_flits);
            wb_queue.erase(wb);

            return wb_req;
        }
    }*/

    std::list<wb_entry>::iterator wb = std::find_if(wb_queue.begin(), wb_queue.end(),
        [&](wb_entry const& wb) { return bank == (BANK_ID(wb.addr));  });

    if (wb != wb_queue.end())
    {
        mem_request* wb_req = new mem_request(wb->addr, bank, SUBBANK_ID(wb->addr), wb->data_flits);
        wb_queue.erase(wb);

        return wb_req;
    }

    return nullptr;
}

mem_request* L2cache_reram::get_next_request()
{
    return next_request();
}

//sort such that smallest cycle has largest value then
// break any ties by address of bank
bool operator<(const reram_task& a, const reram_task& b)
{
    if (a.cycle == b.cycle)
        return a.bank < b.bank;

    return a.cycle > b.cycle;
}


