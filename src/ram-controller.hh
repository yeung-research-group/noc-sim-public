#pragma once

/*
  ram-controller.hh

  Candace Walden
  01 September 2019

  RAM Controller: Base class for either DRAM or RRAM controller
 */

#if BANK_CONFLICT_DETECTION
struct conflict_data {
    char valid;
    uint64 packet_id;
    long long cycle_issued;
};
#endif

typedef struct ram_request {
    //ram_request() {}
    ram_request(unsigned id)
      : type(),
        packet_id(id),
        addr(0),
        bank(0),
        sector(0),
        valid(),
        dw_count(0),
        vector_req(nullptr) {};
    ram_request(request_type req_type, unsigned b)
        : type(req_type),
        packet_id(0),
        addr(0),
        bank(b),
        sector(0),
        valid(),
        dw_count(0),
        vector_req(nullptr) {};
    ram_request(const ram_request* req);
    request_type type;
    unsigned packet_id;
    uint64 addr;
    unsigned bank;
    unsigned sector;
    bool valid[8];
    unsigned dw_count;
    ram_request* vector_req;
} ram_request;

class ram_controller
{
protected:
    int x_coord;
    int y_coord;

    //Input/output
    buffer* input;
    buffer* output;
    ram_request* input_req;
    ram_request* output_req;
    noc_state noc_read_state;
    noc_state noc_write_state;

    //work queues
    std::queue<ram_request> output_requests;

    //Settings
    Conf_ReRAM* conf;
    Stats* stats;
    ram_char* settings;


#if BANK_CONFLICT_DETECTION
    struct conflict_data* bank_accesses;
    void bank_conflict_calc(long long cur_cycle, unsigned bank);
#endif

    virtual void route_in(long long cur_cycle) = 0;

    void route_out(long long cur_cycle);
    uint64 get_delta_flit(int packet_id);

public:
    ram_controller(int x, int y);
    tile_type active_type;

    buffer* get_input_buffer() { return (input); };
    void set_output_buffer(buffer* out) { output = out; };
    virtual void cycle(long long cur_cycle) = 0;
    virtual void collect_stats() = 0;
    virtual void PrintEpochStats() = 0;
    virtual void PrintFinalStats() = 0;
};
