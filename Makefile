DEBUG ?= 0
# type make DEBUG=1 to enable debug build

PROF ?= 0

CC = g++-6
#CC = g++-7.2

ODIR=obj
DRAM_LIB_DIR=ext/dramsim3
INI_DIR=ext/inih/src
CPTL_DIR=ext/cptl
DRAM_DIR=ext/dramsim3/src
SRC_DIR=src

INC=-I$(DRAM_DIR) -I$(INI_DIR) -I$(CPTL_DIR) -I$(SRC_DIR)
LIB=-L$(DRAM_LIB_DIR)

ifeq ($(DEBUG), 1)
	CXXFLAGS = -Wall -g -ggdb -gdwarf-2
else
	CXXFLAGS = -O2
endif

ifeq ($(PROF), 1)
	CXXFLAGS := $(CXXFLAGS) -pg
endif

CXXFLAGS := $(CXXFLAGS) -pthread -std=c++11 -static-libstdc++ -no-pie $(INC)

INI_SRCS=INIReader.cc ini.cc
NOC_SIM_SRCS=router.cc buffer.cc stats.cc rram-bank.cc ram-controller.cc traffic-gen.cc \
		config.cc traffic-queue.cc traffic-gen-thread.cc mem-trace.cc instruction.cc L2cache.cc \
		L2cache-bank.cc L2cache-pipeline.cc L2cache-standalone.cc L2cache-standalone-pipeline.cc \
		L2cache-reram.cc L2cache-reram-pipeline.cc L2cache-het.cc L2cache-het-pipeline.cc \
		L2cache-array.cc cacheL1.cc prefetcher.cc rram-controller.cc dram-controller.cc
EXE_SRCS=rram-noc-sim.cc
EXE_NAME=noc-sim

NS_OBJECTS = $(addsuffix .o, $(basename $(NOC_SIM_SRCS)))
DRAM_LIB = $(DRAM_LIB_DIR)/libdramsim3.a
INI_OBJECTS = $(addsuffix .o, $(basename $(INI_SRCS)))
EXE_OBJS = $(addsuffix .o, $(basename $(EXE_SRCS)))
EXE_OBJS := $(EXE_OBJS) $(NS_OBJECTS) $(INI_OBJECTS)
OBJS = $(patsubst %,$(ODIR)/%,$(EXE_OBJS)) $(DRAM_LIB)

DEPS = $(addsuffix .hh, $(basename $(NOC_SIM_SRCS)))
DEPS := $(DEPS) rram-noc-sim.hh noc-sim.hh $(DRAM_DIR)/dramsim3.h

VPATH=$(SRC_DIR) $(INI_DIR) $(DRAM_LIB_DIR)

all: $(EXE_NAME)

$(EXE_NAME): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(ODIR)/%.o : %.cc $(DEPS)
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(DRAM_LIB) :
	$(MAKE) -C $(DRAM_LIB_DIR) 

.PHONY: clean

clean:
	-rm -f $(OBJS) $(EXE_NAME)



#all:  noc-sim
#
#
#noc-sim:  rram-noc-sim.o router.o buffer.o stats.o rram-controller.o rram-bank.o traffic-queue.o traffic-gen.o INIReader.o ini.o config.o traffic-gen-thread.o mem-trace.o instruction.o L2cache.o L2cache-bank.o L2cache-pipeline.o L2cache-standalone.o L2cache-standalone-pipeline.o L2cache-reram.o L2cache-reram-pipeline.o L2cache-het.o L2cache-het-pipeline.o L2cache-array.o cacheL1.o prefetcher.o
#	$(CC) $(CFLAGS) -o noc-sim rram-noc-sim.o router.o buffer.o stats.o rram-bank.o rram-controller.o traffic-gen.o INIReader.o ini.o config.o traffic-queue.o traffic-gen-thread.o mem-trace.o instruction.o L2cache.o L2cache-bank.o L2cache-pipeline.o L2cache-standalone.o L2cache-standalone-pipeline.o L2cache-reram.o L2cache-reram-pipeline.o L2cache-het.o L2cache-het-pipeline.o L2cache-array.o cacheL1.o prefetcher.o
#
#rram-noc-sim.o:  rram-noc-sim.cc noc-sim.hh config.hh buffer.hh stats.hh router.hh rram-noc-sim.hh rram-controller.hh traffic-gen.hh mem-trace.hh L2cache.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c rram-noc-sim.cc
#
#config.o:  config.cc ext/inih/src/INIReader.h ext/inih/src/ini.h config.hh stats.hh
#	$(CC) $(CFLAGS) -c config.cc
#
#INIReader.o: ext/inih/src/INIReader.cpp ext/inih/src/INIReader.h ext/inih/src/ini.h
#	$(CC) $(CFLAGS) -c ext/inih/src/INIReader.cpp
#
#ini.o: ext/inih/src/ini.c ext/inih/src/ini.h
#	$(CC) $(CFLAGS) -c ext/inih/src/ini.c
#
#stats.o: stats.cc noc-sim.hh config.hh stats.hh
#	$(CC) $(CFLAGS) -c stats.cc
#
#mem-trace.o: mem-trace.cc noc-sim.hh config.hh stats.hh rram-noc-sim.hh traffic-gen.hh traffic-queue.hh traffic-gen-thread.hh mem-trace.hh instruction.hh
#	$(CC) $(CFLAGS) -c mem-trace.cc
#
#instruction.o: instruction.cc noc-sim.hh config.hh stats.hh instruction.hh
#	$(CC) $(CFLAGS) -c instruction.cc
#
#traffic-gen.o: traffic-gen.cc noc-sim.hh config.hh stats.hh traffic-gen.hh traffic-queue.hh traffic-gen-thread.hh mem-trace.hh instruction.hh cacheL1.hh prefetcher.hh
#	$(CC) $(CFLAGS) -c traffic-gen.cc
#
#traffic-queue.o: traffic-queue.cc noc-sim.hh config.hh stats.hh traffic-gen.hh traffic-queue.hh traffic-gen-thread.hh mem-trace.hh instruction.hh
#	$(CC) $(CFLAGS) -c traffic-queue.cc
#
#traffic-gen-thread.o: traffic-gen-thread.cc noc-sim.hh config.hh stats.hh traffic-gen.hh traffic-queue.hh traffic-gen-thread.hh mem-trace.hh instruction.hh
#	$(CC) $(CFLAGS) -c traffic-gen-thread.cc
#
#cacheL1.o: cacheL1.cc noc-sim.hh config.hh buffer.hh stats.hh router.hh traffic-gen.hh L2cache-array.hh L2cache.hh L2cache-bank.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c cacheL1.cc
#
#router.o:  router.cc noc-sim.hh config.hh buffer.hh stats.hh router.hh rram-controller.hh L2cache.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c router.cc
#
#buffer.o:  buffer.cc noc-sim.hh config.hh buffer.hh stats.hh
#	$(CC) $(CFLAGS) -c buffer.cc
#
#rram-bank.o: rram-bank.cc noc-sim.hh config.hh stats.hh rram-controller.hh rram-bank.hh L2cache.hh L2cache-bank.hh
#	$(CC) $(CFLAGS) -c rram-bank.cc
#
#rram-controller.o: rram-controller.cc noc-sim.hh config.hh buffer.hh stats.hh rram-controller.hh rram-bank.hh L2cache.hh L2cache-bank.hh
#	$(CC) $(CFLAGS) -c rram-controller.cc
#
#L2cache.o: L2cache.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache.hh L2cache-bank.hh L2cache-pipeline.hh L2cache-standalone.hh L2cache-standalone-pipeline.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache.cc
#
#L2cache-bank.o: L2cache-bank.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-pipeline.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-bank.cc
#
#L2cache-pipeline.o: L2cache-pipeline.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-pipeline.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-pipeline.cc
#
#L2cache-standalone.o: L2cache-standalone.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-standalone.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-standalone.cc
#
#L2cache-standalone-pipeline.o: L2cache-standalone-pipeline.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-pipeline.hh L2cache-standalone.hh L2cache-standalone-pipeline.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-standalone-pipeline.cc
#
#L2cache-reram.o: L2cache-reram.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-reram.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-reram.cc
#
#L2cache-reram-pipeline.o: L2cache-reram-pipeline.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-pipeline.hh L2cache-reram.hh L2cache-reram-pipeline.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-reram-pipeline.cc
#
#L2cache-het.o: L2cache-het.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-het.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-het.cc
#
#L2cache-het-pipeline.o: L2cache-het-pipeline.cc noc-sim.hh config.hh buffer.hh stats.hh L2cache-bank.hh L2cache-pipeline.hh L2cache-standalone.hh L2cache-het.hh L2cache-het-pipeline.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-het-pipeline.cc
#
#L2cache-array.o: L2cache-array.cc noc-sim.hh config.hh stats.hh L2cache-bank.hh cacheL1.hh
#	$(CC) $(CFLAGS) -c L2cache-array.cc
#
#prefetcher.o: prefetcher.cc config.hh stats.hh traffic-gen.hh prefetcher.hh cacheL2.hh cacheL1.hh 
#	$(CC) $(CFLAGS) -c prefetcher.cc
#
#clean:
#	rm noc-sim *.o
