#pragma once
/*
 rram-bank.hh

 Candace Walden
 20 August 2017

 RRAM subarray simulates latency for RRAM subarrays inside RRAM banks.
*/

//MODES
enum memory_mode {idle_mode, read_mode, write_mode, cpu_mode, cache_wb_mode, cpu_init_mode, close_page_mode};

typedef struct schedule_item schedule_item;
typedef struct ram_request ram_request;

class rram_bank
{
private:
  int x_coord;
  int y_coord;
  uint64 row;
  long long cycle_complete;

  std::priority_queue<schedule_item> *bank_schedule;

  Conf_ReRAM *conf;
  Stats *stats;
  ram_char *settings;

  void read_request(long long cur_cycle);
  void write_request(long long cur_cycle);

public:
  rram_bank(int x, int y, int bank, ram_char *settings, std::priority_queue<schedule_item> *scheduler);
  int b_coord;
  bool busy;
  ram_request* cur_req;

  void send_request(long long cur_cycle, ram_request* req);
  void complete_request(long long cur_cycle);
  void bank_close_page(long long cur_cycle);
}; 

struct schedule_item {
  long long cycle;
  rram_bank *bank;
};
bool operator<(const schedule_item& a, const schedule_item& b);