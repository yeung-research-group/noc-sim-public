#pragma once
/*
  traffic-gen.hh

  Donald Yeung
  26 July 2017

  Traffic-gen class injects messsages into the NOC, either using
  randomly generated accesses, or from traces.
 */

class traffic_generator {
private:
  //Core/sim information
  int x_coord;
  int y_coord;
  int x_extent;
  int y_extent;
  int num_threads;
  int trace_finished;
  int number_packets_to_inject;

  //Operation information
  int rw;
  int sg; // set in case of scatter or gather instructions
  int pref;
  //uint64 pref_addr[8];
  instruction* ins;
  int simd_width;
  int vector;
  int miss_retry;

  int dram_vec;
  //int dram_pref_count;


  //Threading
  int cur_thread;
  int context_switch_counter;
  int get_available_thread(); // returns avaialable thread's ID or -1 if none of them are available.

  //Cache
  cacheL1 *cache;
  buffer *cache_in;

  //Prefetcher
  prefetcher *Pprefetcher;

  //Global objects
  Stats *stats;
  Conf_ReRAM *conf;

public:
  traffic_gen_thread ** my_threads;

  //Set up
  traffic_generator(int x, int y, int npackets);
  ~traffic_generator();
  void set_buffer_link(buffer *my_buffer) { cache->set_buffer_link(my_buffer); };
  buffer *get_input_buffer() { return (cache->get_input_buffer()); };
  void set_npackets(int npackets);

  //Operation
  void add_instruction(instruction* ins, int threadID);
  void cycle(long long cur_cycle);
  int core_finished_trace(){ return trace_finished; };

  bool active;

};
