
/*
  config.cc

  Candace Walden
  22 August 2017

  Config for all parameters
*/

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include "noc-sim.hh"
#ifdef _MSC_VER  // if visual studio
#include "../ext/inih/src/INIReader.h"
#else
#include "INIReader.h"
#endif


Conf_ReRAM* Conf_ReRAM::conf = NULL;

Conf_ReRAM* Conf_ReRAM::get(const string& config_file_name)
{
    if (!conf)
        conf = new Conf_ReRAM(config_file_name);
    return conf;
}

Conf_ReRAM* Conf_ReRAM::get()
{
    return conf;
}

Conf_ReRAM::Conf_ReRAM(const std::string& config_file_name)
{
    string dram_tile_s;

    INIReader reader(config_file_name);

    //Sim settings
    trace = reader.GetBoolean("sim_settings", "trace", 1);
    exec = reader.GetBoolean("sim_settings", "exec_mode", 1);
    packets = reader.GetInteger("sim_settings", "packets", 0);

    //0: reram-cpu, 1: dram, 2: l2_cpu, 3: l2-reram, 4: dram-l2-reram
    ram_mode = (simulation_coupling)reader.GetInteger("sim_settings", "ram_mode", 0);

    //0: tile/channel first, banks first, random, local
    addr_mode = (addr_modes)reader.GetInteger("sim_settings", "addr_mode", 0);

    trace_file = reader.Get("sim_settings", "trace_file", "trace.txt");
    out_file = reader.Get("sim_settings", "out_file", "stats.txt");
    sim_title = reader.Get("sim_settings", "sim_title", "base");
    bucket_size = reader.GetInteger("sim_settings", "bucket_size", 10);

    threads = reader.GetInteger("sim_settings", "threads", 1025);
    thread_zero_empty = reader.GetBoolean("sim_settings", "thread_zero_empty", 1);

    //NOC Structure
    x_max = reader.GetInteger("noc_structure", "x_max", 16);
    y_max = reader.GetInteger("noc_structure", "y_max", 16);
    phits = reader.GetInteger("noc_structure", "phits", 1);
    dram_tile_s = reader.Get("noc_structure", "dram_tiles", "NONE");
    buffer_size = reader.GetInteger("noc_structure", "buffer_size", 20);
    //0: stripe, 1: half block
    ram_distrib ram_tile_loc = (ram_distrib)reader.GetInteger("noc_structure", "ram_distrib", 0);
    integrate_cache_rram = reader.GetBoolean("noc_structure", "integrated_cache_reram", true);
    if (ram_mode == dram || ram_mode == l2_cpu)  //can't combine in these cases
        integrate_cache_rram = false;

    if (ram_mode == reram_cpu || ram_mode == dram)
    {
        cpu_tiles = x_max * y_max;
    }
    else
    {
        cpu_tiles = (x_max * y_max) / 2;
    }


    //ReRAM Characteristics
    reram_settings = ram_char();
    reram_settings.dram = false;
    reram_settings.banks = reader.GetInteger("reram_settings", "banks", 128);
    reram_settings.queue_depth = reader.GetInteger("reram_settings", "queue_depth", 2 * reram_settings.banks);
    reram_settings.read_lat = reader.GetInteger("reram_settings", "read_lat", 200);
    reram_settings.write_lat = reader.GetInteger("reram_settings", "write_lat", 400);
    reram_settings.row_open_latency = reader.GetInteger("reram_settings", "row_open_latency", 0);
    reram_settings.row_close_latency = reader.GetInteger("reram_settings", "row_close_latency", 0);
    reram_settings.ram_page_policy = (page_policy)reader.GetInteger("reram_settings", "page_policy", 0);  //page policy 0: perfect, 1: closed, 2: open
    reram_settings.fetch_width = reader.GetInteger("reram_settings", "fetch_width", 1);
    reram_settings.read_energy = reader.GetReal("reram_settings", "read_energy_per_bit", 2.4) * 64 * reram_settings.fetch_width;
    reram_settings.write_energy = reader.GetReal("reram_settings", "write_energy_per_bit", 4.8) * 64 * reram_settings.fetch_width;
    reram_settings.refresh_power = reader.GetReal("reram_settings", "refresh_power", 0);

    reram_settings.vector_size = 8 / (reram_settings.fetch_width);  //vector request dws / fetch width dws
    if (reram_settings.vector_size < 1) reram_settings.vector_size = 1;

    reram_settings.fw_mask = ~(((uint64)reram_settings.fetch_width * 8) - 1);

    
    //Timing
    cycle_time = reader.GetReal("noc_timing", "cycle_time", 1);

    //DRAM Characteristics
    if (ram_mode == dram || ram_mode == het_l2_reram)
    {
        dram_settings = ram_char();
        dram_settings.dram = true;
        string dram_config_file = reader.Get("dram_settings", "config_file", "ext/dramsim3/configs/HMC2_8GB_4Lx16.ini");
        string dram_output_dir = reader.Get("dram_settings", "output_dir", "");

        dram_conf = new dramsim3::Config(dram_config_file, dram_output_dir);
        dram_timing = new dramsim3::Timing(*dram_conf);
        dram_settings.banks = dram_conf->banks;
        dram_settings.fetch_width = dram_conf->request_size_bytes / 8;  //NoC fetch width is in terms of 8 bytes
        num_dram_tiles = (ram_mode == dram || ram_mode == het_l2_reram) ? dram_conf->channels : 0;

        dram_settings.vector_size = 8 / (dram_settings.fetch_width);  //vector request dws / fetch width dws
        if (dram_settings.vector_size < 1) dram_settings.vector_size = 1;

        dram_settings.fw_mask = ~(((uint64)dram_settings.fetch_width * 8) - 1);

        
        cpu_mem_clk_ratio = round(dram_conf->tCK / cycle_time);
        if (cpu_mem_clk_ratio < 1)
        {
            std::stringstream errMsg;
            errMsg << "Error: NoCSim cycle must be less than or equal to DRAMSim cycle.\n";
            throw std::invalid_argument(errMsg.str());
        }
    }
    else
    {
        dram_settings = ram_char();
        dram_settings.dram = true;

        dram_conf = nullptr;
        dram_timing = nullptr;
        dram_settings.banks = 0;
        dram_settings.fetch_width = 0;  //NoC fetch width is in terms of 8 bytes
        num_dram_tiles = 0;

        dram_settings.vector_size = 0;  //vector request dws / fetch width dws

        dram_settings.fw_mask = -1;
        cpu_mem_clk_ratio = 1;
    }


    //Core Settings
    max_simd_width = reader.GetInteger("core_settings", "SIMD_WIDTH", 16);
    context_switch_delay = reader.GetInteger("core_settings", "context_switch_delay", 5);
    vector_cache_block_fetch = reader.GetInteger("core_settings", "vector_cache_block_fetch", 1);
    scalar_cache_block_fetch = reader.GetInteger("core_settings", "scalar_cache_block_fetch", 1);
    sg_cache_block_fetch = reader.GetInteger("core_settings", "sg_cache_block_fetch", 0);
    prefetch = reader.GetInteger("core_settings", "prefetch", 1);
    prefetch_depth = reader.GetInteger("core_settings", "prefetch_depth", 10);
    prefetch_streams_per_thread = reader.GetInteger("core_settings", "prefetch_streams_per_thread", 10);

    //Power
    transport_energy = reader.GetReal("power", "transport_energy_per_bit", 0.1) * 64;

    //Cache
    l1_settings = cache_char();
    l2_settings = cache_char();

    //if you have DRAM with a fetch size greater than 8, you need a 128B line, otherwise, 64B
    int block_size = ((ram_mode == het_l2_reram || ram_mode == dram) && dram_settings.fetch_width > 8) ? 128 : 64;

    l1_settings.cache_lat = reader.GetInteger("l1_cache", "cache_lat", 1);
    l1_settings.size = reader.GetInteger("l1_cache", "size", 32) * 1024;
    l1_settings.block_size = reader.GetInteger("l1_cache", "block_size", block_size);
    l1_settings.assoc = reader.GetInteger("l1_cache", "assoc", 4);
    l1_settings.banks = reader.GetInteger("l1_cache", "banks", 1);
    l1_settings.rob_entries = reader.GetInteger("l1_cache", "rob_entries", 1);
    l1_settings.unified_mshr_entries = reader.GetInteger("l1_cache", "unified_mshr_entries", 32);
    l1_settings.banked_mshr_entries = reader.GetInteger("l1_cache", "banked_mshr_entries", 16);
    l1_settings.read_subentries = reader.GetInteger("l1_cache", "read_subentries", 16);
    l1_settings.banked_mshr = reader.GetBoolean("l1_cache", "banked_mshr", 0);
    l1_settings.pipelined = reader.GetBoolean("l1_cache", "pipelined", 0);
    l1_settings.cache_writeback = reader.GetBoolean("l1_cache", "cache_writeback", 1);
    l1_settings.cache_writealloc = reader.GetBoolean("l1_cache", "cache_writealloc", 1);

    l2_settings.cache_lat = reader.GetInteger("l2_cache", "cache_lat", 5);
    l2_settings.size = reader.GetInteger("l2_cache", "size", 256) * 1024;
    l2_settings.block_size = reader.GetInteger("l2_cache", "block_size", block_size);
    l2_settings.assoc = reader.GetInteger("l2_cache", "assoc", 8);
    l2_settings.banks = reader.GetInteger("l2_cache", "banks", 1);
    l2_settings.rob_entries = reader.GetInteger("l2_cache", "rob_entries", 8);
    l2_settings.unified_mshr_entries = reader.GetInteger("l2_cache", "unified_mshr_entries", 32);
    l2_settings.banked_mshr_entries = reader.GetInteger("l2_cache", "banked_mshr_entries", 16);
    l2_settings.read_subentries = reader.GetInteger("l2_cache", "read_subentries", 16);
    l2_settings.banked_mshr = reader.GetBoolean("l2_cache", "banked_mshr", 0);
    l2_settings.pipelined = reader.GetBoolean("l2_cache", "pipelined", 1);
    //TODO: make these settings do something?
    l2_settings.cache_writeback = reader.GetBoolean("l2_cache", "cache_writeback", 1);
    l2_settings.cache_writealloc = reader.GetBoolean("l2_cache", "cache_writealloc", 1);

    set_up_l1cache();
    set_up_l2cache();

    //ceiling of threads / cores
    max_threads_per_core = threads / cpu_tiles + (threads % cpu_tiles != 0);

    cpu_tiles_list = std::vector<int>();
    dram_tiles_list = std::vector<int>();
    reram_tiles_list = std::vector<int>();

    map_ram_tiles(dram_tile_s, ram_tile_loc);
    map_threads();

    //set_up_dram();
    int chunk_size = reader.GetInteger("reram_settings", "chunk_size", 256) * 1024;
    set_up_reram(chunk_size);

    set_local_tiles(ram_tile_loc);

    mpki = reader.GetReal("synthetic_bench", "mpki", 1.0);
    micro_bench_simd_width = reader.GetInteger("synthetic_bench", "micro_bench_simd_width", 1);
    spatial_reuse = reader.GetInteger("synthetic_bench", "spatial_reuse", 0);
    ins_per_mem_access = reader.GetInteger("synthetic_bench", "ins_per_mem_access", 100);
}

void Conf_ReRAM::set_up_reram(int chunk_size)
{
    /* * * * * * * * * *
    * ReRAM ADDRESSING
    * assumes
    * 8b fetch & 64b blocks
    * this means the upper bank bits and lower bank bits may be split
    * |   index   | upper bank | tile id | chunk | cache bank | lower bank | offset |
    * |   index   | tile id | chunk | upper bank | cache bank | lower bank | offset |
    * |63         |                                           |5          3|2      0|
    * But we're doing DW addresses, so drop offset
    * |63         |                                           |2          0|      -3|
    * * * * * * * * * */
    unsigned sectors = l2_settings.n_sectors;
    unsigned max_offset = 0;

    n_sector_bits = LOG2(sectors);

    if (ram_mode == dram)  //use tile id to calculate L2 position
    {
        //take tile bits right after offset -- lower bank is usually 0
        //|   index   |   tile id   | chunk | cache bank | sector | offset (fw) | offset (8B) |
        //|63         |                                  |        |            0|           -3|

        sector_offset = LOG2(dram_settings.fetch_width);
        sector_bits = ((uint64)l2_settings.n_sectors - 1) << sector_offset;

        cache_bank_offset = LOG2(dram_settings.vector_size) + sector_offset;
        cache_bank_bits = (uint64(l2_settings.banks) - 1) << cache_bank_offset;

        chunk_size = chunk_size / dram_settings.fetch_width / 8 / l2_settings.banks;
        if (chunk_size < 1) chunk_size = 1; //fixes if chunk size smaller than already allocated chunks

        chunk_offset = cache_bank_offset + LOG2(l2_settings.banks);
        chunk_bits = (uint64(chunk_size) - 1) << chunk_offset;

        tile_id_offset = chunk_offset + LOG2(chunk_size);
        tile_id_bits = (uint64(cpu_tiles) - 1) << tile_id_offset;

        max_offset = tile_id_offset + LOG2(cpu_tiles);

        upper_bank_offset = 0;
        upper_bank_bits = 0;

        //TODO: make L2 tiles map to DRAM channels
    }
    else
    {
        sector_offset = 0;
        sector_bits = ((uint64)sectors - 1) << sector_offset;
        cache_bank_offset = n_sector_bits;
        cache_bank_bits = (uint64(l2_settings.banks) - 1) << n_sector_bits;

        if (addr_mode == tile_first)
        {
            //|    index    | upper bank | tile id | chunk | cache bank | sector | offset |
            //|63           |                                           |2      0|      -3|

            chunk_size = chunk_size / 8 / sectors / l2_settings.banks;
            if (chunk_size < 1) chunk_size = 1; //fixes if chunk size smaller than already allocated chunks

            chunk_offset = cache_bank_offset + LOG2(l2_settings.banks);
            chunk_bits = (uint64(chunk_size) - 1) << chunk_offset;

            tile_id_offset = chunk_offset + LOG2(chunk_size);
            tile_id_bits = (uint64(num_reram_tiles) - 1) << tile_id_offset;

            upper_bank_offset = LOG2(num_reram_tiles) + tile_id_offset;
            upper_bank_bits = (uint64(reram_settings.banks / l2_settings.banks / sectors) - 1) << upper_bank_offset;

            max_offset = upper_bank_offset + LOG2(reram_settings.banks / l2_settings.banks / sectors);
        }
        else //bank_first
        {
            //|    index    | tile id | chunk | upper bank | cache bank | sector | offset |
            //|63           |                                           |2      0|      -3|

            upper_bank_offset = cache_bank_offset + LOG2(l2_settings.banks);
            upper_bank_bits = (uint64(reram_settings.banks / l2_settings.banks / sectors) - 1) << upper_bank_offset;

            chunk_size = chunk_size / 8 / reram_settings.banks;
            if (chunk_size < 1) chunk_size = 1; //fixes if chunk size smaller than already allocated chunks

            chunk_offset = LOG2(reram_settings.banks / l2_settings.banks / sectors) + upper_bank_offset;
            chunk_bits = (uint64(chunk_size) - 1) << chunk_offset;

            tile_id_offset = chunk_offset + LOG2(chunk_size);
            tile_id_bits = (uint64(num_reram_tiles) - 1) << tile_id_offset;

            max_offset = tile_id_offset + LOG2(num_reram_tiles);
        }
    }

    if (ram_mode != dram)  //use tile id to calculate L2 position
    {
        /* |      | upper z bank |    | upper y bank | all the other stuff |
         * |63    |              |    |              |max_offset          0|
         */
        upper_bank_offset_y = max_offset;
        upper_bank_bits_y = (uint64(reram_settings.banks / l2_settings.banks / sectors) - 1) << upper_bank_offset_y;
        if (chunk_bits > 0)
            upper_bank_offset_z = chunk_offset;
        else
            upper_bank_offset_z = max_offset + 10 + LOG2(reram_settings.banks / sectors);
        upper_bank_bits_z = (uint64(reram_settings.banks / l2_settings.banks / sectors) - 1) << upper_bank_offset_z;
    }
    else
    {
        upper_bank_offset_y = 0;
        upper_bank_bits_y = 0;
        upper_bank_offset_z = 0;
        upper_bank_bits_z = 0;
    }

    cache_bank_offset_y = upper_bank_offset_y + LOG2(reram_settings.banks / l2_settings.banks / sectors);
    cache_bank_bits_y = (uint64(l2_settings.banks - 1)) << cache_bank_offset_y;
    cache_bank_offset_z = upper_bank_offset_z + LOG2(reram_settings.banks / l2_settings.banks / sectors);
    cache_bank_bits_z = (uint64(l2_settings.banks - 1)) << cache_bank_offset_z;


    /* |  MSHR Tag | cache bank |  sector  | offset |
     * |63         |            |5        3|2      0|
     * But we're doing DW addresses, so drop offset
     * |63         |            |2        0|      -3|
     */
    mshr_tag_offset = cache_bank_offset + LOG2(l2_settings.banks);

    row_bits = 0;
}

void Conf_ReRAM::set_up_l1cache()
{
    l1_settings.lines = l1_settings.size / l1_settings.block_size;
    l1_settings.words_per_block = l1_settings.block_size / WORD_SIZE;

    if (ram_mode == dram) //pure dram system will sector based on dram
        l1_settings.sector_size = dram_settings.fetch_width * 8; //sector size (bytes)
    else //any system with reram will sector based on reram
        l1_settings.sector_size = reram_settings.fetch_width * 8; //sector size (bytes)

    l1_settings.n_sets = l1_settings.lines / l1_settings.assoc;
    l1_settings.n_sectors = l1_settings.block_size / l1_settings.sector_size;  //sectors per line
    // if fetch_width more than cache block size then an issue corrected below
    if (l1_settings.n_sectors < 1)
        l1_settings.n_sectors = 1;

    l1_settings.index_mask_offset = LOG2(l1_settings.block_size);
    l1_settings.index_mask = (uint64_t)(l1_settings.n_sets - 1) << l1_settings.index_mask_offset;

    l1_settings.sector_mask_offset = LOG2(l1_settings.sector_size);
    l1_settings.sector_mask = (uint64_t)(l1_settings.n_sectors - 1) << l1_settings.sector_mask_offset;

    l1_settings.reram_fetch_shift = LOG2(reram_settings.fetch_width);
    l1_settings.dram_fetch_shift = LOG2(dram_settings.fetch_width);

    if (ram_mode == dram) //pure dram system will shift based on dram
        l1_settings.main_mem_fetch_shift = l1_settings.dram_fetch_shift;
    else  //any system with reram will shift based on reram
        l1_settings.main_mem_fetch_shift = l1_settings.reram_fetch_shift;
}

void Conf_ReRAM::set_up_l2cache()
{
    l2_settings.lines = l2_settings.size / l2_settings.block_size;
    l2_settings.words_per_block = l2_settings.block_size / WORD_SIZE;

    if (ram_mode == dram)
        l2_settings.sector_size = dram_settings.fetch_width * 8; //sector size (bytes)
    else
        l2_settings.sector_size = reram_settings.fetch_width * 8; //sector size (bytes)

    l2_settings.n_sets = l2_settings.lines / l2_settings.assoc;
    l2_settings.n_sectors = l2_settings.block_size / l2_settings.sector_size;  //sectors per line
    if (l2_settings.n_sectors < 1)
        l2_settings.n_sectors = 1;

    unsigned n_reram_banks = (reram_settings.banks) / l2_settings.banks;
    l2_settings.n_subbanks = n_reram_banks / l2_settings.n_sectors;

    // L2 does word addresses (never ask for byte at L2 level + need empty tail indicator):
    // |   tag       |        index       |  sector  | offset |
    // |60               index_mask_offset|         0|      -3|

    //DW Address
    l2_settings.index_mask_offset = LOG2(l2_settings.n_sectors);
    l2_settings.index_mask = (uint64_t)(l2_settings.n_sets - 1) << l2_settings.index_mask_offset;

    l2_settings.sector_mask_offset = 0;
    l2_settings.sector_mask = (uint64_t)(l2_settings.n_sectors - 1) << l2_settings.sector_mask_offset;

    l2_settings.offset_mask = (uint64_t)(l2_settings.block_size - 1) >> WORD_SIZE_OFFSET;

    l2_settings.reram_fetch_shift = LOG2(reram_settings.fetch_width);
    l2_settings.dram_fetch_shift = LOG2(dram_settings.fetch_width);

    if (ram_mode == dram) //pure dram system will shift based on dram
        l2_settings.main_mem_fetch_shift = l2_settings.dram_fetch_shift;
    else  //any system with reram will shift based on reram
        l2_settings.main_mem_fetch_shift = l2_settings.reram_fetch_shift;
}

void Conf_ReRAM::map_ram_tiles(const string& dram_tile_s, ram_distrib ram_tile_loc)
{
    ram_tiles = new tile_type[x_max * y_max];

    num_reram_tiles = 0;

    for (int i = 0; i < x_max * y_max; i++)
    {
        ram_tiles[i] = compute;
    }

    switch (ram_mode)
    {
    case dram:          map_dram_tiles(dram_tile_s);
        //all cpu tiles
        for (int i = 0; i < (x_max * y_max); i++)
            cpu_tiles_list.push_back(i);
        break;
    case reram_cpu:     map_full_reram_tiles();
        break;
    case l2_cpu:
    case l2_reram:      map_part_reram_tiles(ram_tile_loc);
        break;
    case het_l2_reram:  map_dram_tiles(dram_tile_s);
        map_part_reram_tiles(ram_tile_loc);
        break;
    }

}

void Conf_ReRAM::map_full_reram_tiles()
{
    for (int i = 0; i < x_max * y_max; i++)
    {
        if (ram_tiles[i] != dram_tile)
        {
            ram_tiles[i] = reram_tile;
            reram_tiles_list.push_back(i);
            num_reram_tiles++;
        }
    }

    //all cpu tiles
    for (int i = 0; i < (x_max * y_max); i++)
        cpu_tiles_list.push_back(i);
}

void Conf_ReRAM::map_dram_tiles(const std::string& dram_tile_s)
{

    if (dram_tile_s != "NONE") //then expecting a comma separated values
    {
        int x, y;
        char separator;
        int value;
        std::stringstream ss(dram_tile_s);
        ss >> value;
        x = value / y_max;
        y = value % y_max;
        ram_tiles[CIND(x, y)] = dram_tile;
        dram_tiles_list.push_back(value);
        num_dram_tiles++;
        while (ss >> separator >> value && separator == ',')
        {
            x = value / y_max;
            y = value % y_max;
            ram_tiles[CIND(x, y)] = dram_tile;
            dram_tiles_list.push_back(value);
            num_dram_tiles++;
        }

        if (num_dram_tiles != dram_conf->channels)
        {
            std::stringstream errMsg;
            errMsg << "Error: Mismatch between DRAMSim config and NoCSim Config. " << dram_conf->channels << " only " << num_dram_tiles << " tiles specified.\n";
            throw std::invalid_argument(errMsg.str());
        }
    }
    else if (num_dram_tiles == 64 && x_max == 16 && y_max == 16)
    {
        //edges
        for (int i = 1; i < 15; i++)
        {
            ram_tiles[CIND(0, i)] = dram_tile;
            ram_tiles[CIND(i, 0)] = dram_tile;
            ram_tiles[CIND(i, 15)] = dram_tile;
            ram_tiles[CIND(15, i)] = dram_tile;
            dram_tiles_list.push_back((0 * y_max) + i);
            dram_tiles_list.push_back((i * y_max) + 0);
            dram_tiles_list.push_back((i * y_max) + 15);
            dram_tiles_list.push_back((15 * y_max) + i);
        }
        
        //4 corners
        ram_tiles[CIND(0, 0)] = dram_tile;
        ram_tiles[CIND(15, 0)] = dram_tile;
        ram_tiles[CIND(0, 15)] = dram_tile;
        ram_tiles[CIND(15, 15)] = dram_tile;
        dram_tiles_list.push_back(0);
        dram_tiles_list.push_back((15 * y_max) + 0);
        dram_tiles_list.push_back((0 * y_max) + 15);
        dram_tiles_list.push_back((15 * y_max) + 15);

        //4 extra??
        ram_tiles[CIND(5, 8)] = dram_tile;
        ram_tiles[CIND(7, 5)] = dram_tile;
        ram_tiles[CIND(8, 10)] = dram_tile;
        ram_tiles[CIND(10, 7)] = dram_tile;
        dram_tiles_list.push_back((5 * y_max) + 8);
        dram_tiles_list.push_back((7 * y_max) + 5);
        dram_tiles_list.push_back((8 * y_max) + 10);
        dram_tiles_list.push_back((10 * y_max) + 7);
    }
    else if (num_dram_tiles == 32 && x_max == 16 && y_max == 16)
    {
        ram_tiles[CIND(0, 4)] = dram_tile;
        ram_tiles[CIND(0, 5)] = dram_tile;
        ram_tiles[CIND(0, 6)] = dram_tile;
        ram_tiles[CIND(0, 7)] = dram_tile;
        ram_tiles[CIND(0, 8)] = dram_tile;
        ram_tiles[CIND(0, 9)] = dram_tile;
        ram_tiles[CIND(0, 10)] = dram_tile;
        ram_tiles[CIND(0, 11)] = dram_tile;
        dram_tiles_list.push_back((0 * y_max) + 4);
        dram_tiles_list.push_back((0 * y_max) + 5);
        dram_tiles_list.push_back((0 * y_max) + 6);
        dram_tiles_list.push_back((0 * y_max) + 7);
        dram_tiles_list.push_back((0 * y_max) + 8);
        dram_tiles_list.push_back((0 * y_max) + 9);
        dram_tiles_list.push_back((0 * y_max) + 10);
        dram_tiles_list.push_back((0 * y_max) + 11);

        ram_tiles[CIND(4, 0)] = dram_tile;
        ram_tiles[CIND(5, 0)] = dram_tile;
        ram_tiles[CIND(6, 0)] = dram_tile;
        ram_tiles[CIND(7, 0)] = dram_tile;
        ram_tiles[CIND(8, 0)] = dram_tile;
        ram_tiles[CIND(9, 0)] = dram_tile;
        ram_tiles[CIND(10, 0)] = dram_tile;
        ram_tiles[CIND(11, 0)] = dram_tile;
        dram_tiles_list.push_back((4 * y_max) + 0);
        dram_tiles_list.push_back((5 * y_max) + 0);
        dram_tiles_list.push_back((6 * y_max) + 0);
        dram_tiles_list.push_back((7 * y_max) + 0);
        dram_tiles_list.push_back((8 * y_max) + 0);
        dram_tiles_list.push_back((9 * y_max) + 0);
        dram_tiles_list.push_back((10 * y_max) + 0);
        dram_tiles_list.push_back((11 * y_max) + 0);

        ram_tiles[CIND(4, 15)] = dram_tile;
        ram_tiles[CIND(5, 15)] = dram_tile;
        ram_tiles[CIND(6, 15)] = dram_tile;
        ram_tiles[CIND(7, 15)] = dram_tile;
        ram_tiles[CIND(8, 15)] = dram_tile;
        ram_tiles[CIND(9, 15)] = dram_tile;
        ram_tiles[CIND(10, 15)] = dram_tile;
        ram_tiles[CIND(11, 15)] = dram_tile;
        dram_tiles_list.push_back((4 * y_max) + 15);
        dram_tiles_list.push_back((5 * y_max) + 15);
        dram_tiles_list.push_back((6 * y_max) + 15);
        dram_tiles_list.push_back((7 * y_max) + 15);
        dram_tiles_list.push_back((8 * y_max) + 15);
        dram_tiles_list.push_back((9 * y_max) + 15);
        dram_tiles_list.push_back((10 * y_max) + 15);
        dram_tiles_list.push_back((11 * y_max) + 15);

        ram_tiles[CIND(15, 4)] = dram_tile;
        ram_tiles[CIND(15, 5)] = dram_tile;
        ram_tiles[CIND(15, 6)] = dram_tile;
        ram_tiles[CIND(15, 7)] = dram_tile;
        ram_tiles[CIND(15, 8)] = dram_tile;
        ram_tiles[CIND(15, 9)] = dram_tile;
        ram_tiles[CIND(15, 10)] = dram_tile;
        ram_tiles[CIND(15, 11)] = dram_tile;
        dram_tiles_list.push_back((15 * y_max) + 4);
        dram_tiles_list.push_back((15 * y_max) + 5);
        dram_tiles_list.push_back((15 * y_max) + 6);
        dram_tiles_list.push_back((15 * y_max) + 7);
        dram_tiles_list.push_back((15 * y_max) + 8);
        dram_tiles_list.push_back((15 * y_max) + 9);
        dram_tiles_list.push_back((15 * y_max) + 10);
        dram_tiles_list.push_back((15 * y_max) + 11);
    }
    else if (num_dram_tiles == 16 && x_max == 16 && y_max == 16)
    {
        ram_tiles[CIND(0, 6)] = dram_tile;
        ram_tiles[CIND(0, 7)] = dram_tile;
        ram_tiles[CIND(0, 8)] = dram_tile;
        ram_tiles[CIND(0, 9)] = dram_tile;
        dram_tiles_list.push_back((0 * y_max) + 6);
        dram_tiles_list.push_back((0 * y_max) + 7);
        dram_tiles_list.push_back((0 * y_max) + 8);
        dram_tiles_list.push_back((0 * y_max) + 9);

        ram_tiles[CIND(6, 0)] = dram_tile;
        ram_tiles[CIND(7, 0)] = dram_tile;
        ram_tiles[CIND(8, 0)] = dram_tile;
        ram_tiles[CIND(9, 0)] = dram_tile;
        dram_tiles_list.push_back((6 * y_max) + 0);
        dram_tiles_list.push_back((7 * y_max) + 0);
        dram_tiles_list.push_back((8 * y_max) + 0);
        dram_tiles_list.push_back((9 * y_max) + 0);

        ram_tiles[CIND(6, 15)] = dram_tile;
        ram_tiles[CIND(7, 15)] = dram_tile;
        ram_tiles[CIND(8, 15)] = dram_tile;
        ram_tiles[CIND(9, 15)] = dram_tile;
        dram_tiles_list.push_back((6 * y_max) + 15);
        dram_tiles_list.push_back((7 * y_max) + 15);
        dram_tiles_list.push_back((8 * y_max) + 15);
        dram_tiles_list.push_back((9 * y_max) + 15);

        ram_tiles[CIND(15, 8)] = dram_tile;
        ram_tiles[CIND(15, 9)] = dram_tile;
        ram_tiles[CIND(15, 6)] = dram_tile;
        ram_tiles[CIND(15, 7)] = dram_tile;
        dram_tiles_list.push_back((15 * y_max) + 6);
        dram_tiles_list.push_back((15 * y_max) + 7);
        dram_tiles_list.push_back((15 * y_max) + 8);
        dram_tiles_list.push_back((15 * y_max) + 9);
    }
    else if (num_dram_tiles == 8 && x_max == 16 && y_max == 16)
    {
        ram_tiles[CIND(0, 7)] = dram_tile;
        ram_tiles[CIND(0, 8)] = dram_tile;
        dram_tiles_list.push_back((0 * y_max) + 7);
        dram_tiles_list.push_back((0 * y_max) + 8);

        ram_tiles[CIND(7, 0)] = dram_tile;
        ram_tiles[CIND(8, 0)] = dram_tile;
        dram_tiles_list.push_back((7 * y_max) + 0);
        dram_tiles_list.push_back((8 * y_max) + 0);

        ram_tiles[CIND(7, 15)] = dram_tile;
        ram_tiles[CIND(8, 15)] = dram_tile;
        dram_tiles_list.push_back((7 * y_max) + 15);
        dram_tiles_list.push_back((8 * y_max) + 15);

        ram_tiles[CIND(15, 7)] = dram_tile;
        ram_tiles[CIND(15, 8)] = dram_tile;
        dram_tiles_list.push_back((15 * y_max) + 7);
        dram_tiles_list.push_back((15 * y_max) + 8);
    }
    else if (num_dram_tiles == 16 && x_max == 8 && y_max == 8)
    {
        ram_tiles[CIND(0, 2)] = dram_tile;
        ram_tiles[CIND(0, 3)] = dram_tile;
        ram_tiles[CIND(0, 4)] = dram_tile;
        ram_tiles[CIND(0, 5)] = dram_tile;
        dram_tiles_list.push_back((0 * y_max) + 2);
        dram_tiles_list.push_back((0 * y_max) + 3);
        dram_tiles_list.push_back((0 * y_max) + 4);
        dram_tiles_list.push_back((0 * y_max) + 5);

        ram_tiles[CIND(2, 0)] = dram_tile;
        ram_tiles[CIND(3, 0)] = dram_tile;
        ram_tiles[CIND(4, 0)] = dram_tile;
        ram_tiles[CIND(5, 0)] = dram_tile;
        dram_tiles_list.push_back((2 * y_max) + 0);
        dram_tiles_list.push_back((3 * y_max) + 0);
        dram_tiles_list.push_back((4 * y_max) + 0);
        dram_tiles_list.push_back((5 * y_max) + 0);

        ram_tiles[CIND(2, 7)] = dram_tile;
        ram_tiles[CIND(3, 7)] = dram_tile;
        ram_tiles[CIND(4, 7)] = dram_tile;
        ram_tiles[CIND(5, 7)] = dram_tile;
        dram_tiles_list.push_back((2 * y_max) + 7);
        dram_tiles_list.push_back((3 * y_max) + 7);
        dram_tiles_list.push_back((4 * y_max) + 7);
        dram_tiles_list.push_back((5 * y_max) + 7);

        ram_tiles[CIND(7, 2)] = dram_tile;
        ram_tiles[CIND(7, 3)] = dram_tile;
        ram_tiles[CIND(7, 4)] = dram_tile;
        ram_tiles[CIND(7, 5)] = dram_tile;
        dram_tiles_list.push_back((7 * y_max) + 2);
        dram_tiles_list.push_back((7 * y_max) + 3);
        dram_tiles_list.push_back((7 * y_max) + 4);
        dram_tiles_list.push_back((7 * y_max) + 5);
    }
    else if (num_dram_tiles == 8 && x_max == 8 && y_max == 8)
    {
        ram_tiles[CIND(0, 3)] = dram_tile;
        ram_tiles[CIND(0, 5)] = dram_tile;
        dram_tiles_list.push_back((0 * y_max) + 3);
        dram_tiles_list.push_back((0 * y_max) + 5);

        ram_tiles[CIND(3, 0)] = dram_tile;
        ram_tiles[CIND(5, 0)] = dram_tile;
        dram_tiles_list.push_back((3 * y_max) + 0);
        dram_tiles_list.push_back((5 * y_max) + 0);

        ram_tiles[CIND(2, 7)] = dram_tile;
        ram_tiles[CIND(4, 7)] = dram_tile;
        dram_tiles_list.push_back((2 * y_max) + 7);
        dram_tiles_list.push_back((4 * y_max) + 7);

        ram_tiles[CIND(7, 2)] = dram_tile;
        ram_tiles[CIND(7, 4)] = dram_tile;
        dram_tiles_list.push_back((7 * y_max) + 2);
        dram_tiles_list.push_back((7 * y_max) + 4);
    }
    else if (num_dram_tiles == 4)
    {
        //the four corners
        ram_tiles[CIND(0, 0)] = dram_tile;
        ram_tiles[CIND(0, y_max - 1)] = dram_tile;
        ram_tiles[CIND(x_max - 1, 0)] = dram_tile;
        ram_tiles[CIND(x_max - 1, y_max - 1)] = dram_tile;

        dram_tiles_list.push_back(0);
        dram_tiles_list.push_back(y_max - 1);
        dram_tiles_list.push_back((x_max - 1) * y_max);
        dram_tiles_list.push_back(((x_max - 1) * y_max) + y_max - 1);
        num_dram_tiles = 4;
    }
    else
    {
        std::stringstream errMsg;
        errMsg << "Error: " << dram_conf->channels << " DRAM channels is an unknown configuration.\n";
        throw std::invalid_argument(errMsg.str());
    }
}

void Conf_ReRAM::map_part_reram_tiles(ram_distrib ram_tile_loc)
{
    if (ram_tile_loc == stripe) //every other row
    {
        for (int x = 0; x < x_max; x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                if (ram_tiles[CIND(x, y)] != dram_tile)
                {
                    if (x % 2 == 0)
                    {
                        //RAM tile
                        ram_tiles[CIND(x, y)] = reram_tile;
                        reram_tiles_list.push_back((x * y_max) + y);
                        num_reram_tiles++;
                    }
                    else
                    {
                        //CPU tile
                        ram_tiles[CIND(x, y)] = compute;
                        cpu_tiles_list.push_back((x * y_max) + y);
                    }
                }
            }
        }
    }
    else //all on one side
    {
        for (int x = 0; x < x_max; x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                if (ram_tiles[CIND(x, y)] != dram_tile)
                {
                    if ((x >= (x_max / 2)) == 0)
                    {
                        //RAM tile
                        ram_tiles[CIND(x, y)] = reram_tile;
                        reram_tiles_list.push_back((x * y_max) + y);
                        num_reram_tiles++;
                    }
                    else
                    {
                        //CPU tile
                        ram_tiles[CIND(x, y)] = compute;
                        cpu_tiles_list.push_back((x * y_max) + y);
                    }
                }
            }
        }
    }
}


void Conf_ReRAM::map_threads()
{
    thread_tile_map = new coord[threads];
    tile_thread_map = new int** [max_threads_per_core];
    num_threads = new int[x_max * y_max];

    for (int t = 0; t < max_threads_per_core; t++)
    {
        tile_thread_map[t] = new int* [x_max];
        for (int x = 0; x < x_max; x++)
        {
            tile_thread_map[t][x] = new int[y_max];
            for (int y = 0; y < y_max; y++)
            {
                tile_thread_map[t][x][y] = -1;
            }
        }
    }

    for (int i = 0; i < x_max * y_max; i++)
    {
        num_threads[i] = 0;
    }

    int threadID = 0;
    //certain tiles are not CPUs
    if (ram_mode == l2_reram || ram_mode == het_l2_reram)
    {
        for (int x = 0; x < x_max; x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                for (int t = 0; t < max_threads_per_core; t++)
                {
                    if (ram_tiles[CIND(x, y)] != compute)  //no core on ram tiles
                        continue;

                    thread_tile_map[threadID] = { x, y, t };
                    tile_thread_map[t][x][y] = threadID;
                    num_threads[CIND(x, y)]++;
                    threadID++;
                    if (threadID >= threads)
                        return;
                }
            }
        }
    }
    //twice as many tiles as threads
    else if(x_max * y_max >= 2 * threads)
    {
        for (int x = 0; x < x_max; x++)
        {
            for (int y = 0; y < y_max; y+=2)
            {
                for (int t = 0; t < max_threads_per_core; t++)
                {
                    thread_tile_map[threadID] = { x, y, t };
                    tile_thread_map[t][x][y] = threadID;
                    num_threads[CIND(x, y)]++;
                    threadID++;
                    if (threadID >= threads)
                        return;
                }
            }
        }
    }
    else
    {
        for (int x = 0; x < x_max; x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                for (int t = 0; t < max_threads_per_core; t++)
                {
                    thread_tile_map[threadID] = { x, y, t };
                    tile_thread_map[t][x][y] = threadID;
                    num_threads[CIND(x, y)]++;
                    threadID++;
                    if (threadID >= threads)
                        return;
                }
            }
        }
    }
}

bool Conf_ReRAM::is_ram_tile(int x, int y, tile_type type)
{
    return ram_tiles[CIND(x, y)] == type;
}

bool Conf_ReRAM::is_ram_tile(int x, int y)
{
    return ram_tiles[CIND(x, y)] != compute;
}

tile_type Conf_ReRAM::get_tile_type(int x, int y)
{
    return ram_tiles[CIND(x, y)];
}

void Conf_ReRAM::nth_tile_type(int n, tile_type type, int* x, int* y)
{
    int tile_id;
    if (type == reram_tile)
    {
        tile_id = reram_tiles_list[n];
    }
    else if (type == dram_tile)
    {
        tile_id = dram_tiles_list[n];
    }
    else
    {
        tile_id = cpu_tiles_list[n];
    }

    *x = tile_id / y_max;
    *y = tile_id % y_max;
}

void Conf_ReRAM::threads_tile(int threadID, int* x, int* y, int* t)
{
    *x = thread_tile_map[threadID].x;
    *y = thread_tile_map[threadID].y;
    *t = thread_tile_map[threadID].t;
}

int Conf_ReRAM::tiles_thread(int x, int y, int t)
{
    return tile_thread_map[t][x][y];
}

void Conf_ReRAM::local_tile(int tile_x, int tile_y, tile_type type, int* x, int* y)
{
    if (type == reram_tile)
    {
        *x = local_reram_tile_map[CIND(tile_x, tile_y)].x;
        *y = local_reram_tile_map[CIND(tile_x, tile_y)].y;
    }
    else if (type == dram_tile)
    {
        *x = local_dram_tile_map[CIND(tile_x, tile_y)].x;
        *y = local_dram_tile_map[CIND(tile_x, tile_y)].y;
    }
}

void Conf_ReRAM::set_local_tiles(ram_distrib ram_tile_loc)
{
    if (ram_mode == het_l2_reram) //heterogeneous memory system
    {
        set_local_dram_tiles();
        set_local_reram_tiles(ram_tile_loc);
    }
    else
    {
        set_local_reram_tiles(ram_tile_loc);
    }
}


void Conf_ReRAM::set_local_dram_tiles()
{
    //ceiling of cpu tiles / controllers
    int cpu_per_contr = cpu_tiles / num_dram_tiles + (cpu_tiles % num_dram_tiles != 0);
    int assigned = 0;
    int layer = 0;
    int* num_assigned = new int[num_dram_tiles];
    int d_x, d_y, t_x, t_y;
    int* dist = new int[num_dram_tiles];

    //initialize local tile map
    local_dram_tile_map = new coord[x_max * y_max];
    int tile = 0;
    for (int i = 0; i < x_max * y_max; i++)
    {
        local_dram_tile_map[i] = { -1, -1, tile };
        tile++;
    }

    //initialize dram controller variables
    for (int i = 0; i < num_dram_tiles; i++)
    {
        num_assigned[i] = 0;
        dist[i] = 0;
    }

    //go through each layer of tiles surrouning a controller assigning them until each tile
    //has been assigned and each controller doesn't exceed the maximum
    while (assigned < cpu_tiles)
    {
        int init = (layer % 2 == 0) ? 0 : num_dram_tiles - 1;
        int incr = (layer % 2 == 0) ? 1 : -1;

        for (int i = init; i < num_dram_tiles && i >= 0; i += incr)
        {
            if (num_assigned[i] >= cpu_per_contr) continue;

            d_x = dram_tiles_list[i] / x_max;
            d_y = dram_tiles_list[i] % x_max;

            //pos, pos layer
            for (int delta = 0; delta <= layer; delta++)
            {
                t_x = d_x + layer - delta;
                t_y = d_y + delta;

                //only going to get further off
                if (t_y >= y_max) break;
                //might possibly come back on board
                if (t_x >= x_max) continue;

                //not yet assigned
                if (local_dram_tile_map[CIND(t_x, t_y)].x == -1)
                {
                    local_dram_tile_map[CIND(t_x, t_y)].x = d_x;
                    local_dram_tile_map[CIND(t_x, t_y)].y = d_y;
                    num_assigned[i]++;
                    assigned++;
                    dist[i] += delta;
                    if (num_assigned[i] >= cpu_per_contr) break;
                }
            }
            if (num_assigned[i] >= cpu_per_contr) continue;

            //pos, neg layer
            for (int delta = 0; delta <= layer; delta++)
            {
                t_x = d_x + layer - delta;
                t_y = d_y - delta;

                //only going to get further off
                if (t_y < 0) break;
                //might possibly come back on board
                if (t_x >= x_max) continue;

                //not yet assigned
                if (local_dram_tile_map[CIND(t_x, t_y)].x == -1)
                {
                    local_dram_tile_map[CIND(t_x, t_y)].x = d_x;
                    local_dram_tile_map[CIND(t_x, t_y)].y = d_y;
                    num_assigned[i]++;
                    assigned++;
                    dist[i] += delta;
                    if (num_assigned[i] >= cpu_per_contr) break;
                }
            }
            if (num_assigned[i] >= cpu_per_contr) continue;

            //neg, pos layer
            for (int delta = 0; delta <= layer; delta++)
            {
                t_x = d_x - layer + delta;
                t_y = d_y + delta;

                //only going to get further off
                if (t_y >= y_max) break;
                //might possibly come back on board
                if (t_x < 0) continue;

                //not yet assigned
                if (local_dram_tile_map[CIND(t_x, t_y)].x == -1)
                {
                    local_dram_tile_map[CIND(t_x, t_y)].x = d_x;
                    local_dram_tile_map[CIND(t_x, t_y)].y = d_y;
                    num_assigned[i]++;
                    assigned++;
                    dist[i] += delta;
                    if (num_assigned[i] >= cpu_per_contr) break;
                }
            }
            if (num_assigned[i] >= cpu_per_contr) continue;

            //neg neg layer
            for (int delta = 0; delta <= layer; delta++)
            {
                t_x = d_x - layer + delta;
                t_y = d_y - delta;

                //only going to get further off
                if (t_y < 0) break;
                //might possibly come back on board
                if (t_x < 0) continue;

                //not yet assigned
                if (local_dram_tile_map[CIND(t_x, t_y)].x == -1)
                {
                    local_dram_tile_map[CIND(t_x, t_y)].x = d_x;
                    local_dram_tile_map[CIND(t_x, t_y)].y = d_y;
                    num_assigned[i]++;
                    assigned++;
                    dist[i] += delta;
                    if (num_assigned[i] >= cpu_per_contr) break;
                }
            }
            if (num_assigned[i] >= cpu_per_contr) continue;
        }

        layer++;
    }


#if CONFIG_OUT

    printf("x\t");
    for (int y = 0; y < y_max; y++)
    {
        printf("%d\t", y);
    }
    printf("\n");
    for (int x = 0; x < x_max; x++)
    {
        printf("%d\t", x);
        for (int y = 0; y < y_max; y++)
        {
            printf("%d,%d\t", local_tile_map[x][y].x, local_tile_map[x][y].y);
        }
        printf("\n");
    }

    for (int i = 0; i < num_dram_tiles; i++)
    {
        d_x = dram_tiles[i] / x_max;
        d_y = dram_tiles[i] % x_max;

        printf("(%d,%d) assigned: %d total dist: %d\n", d_x, d_y, num_assigned[i], dist[i]);
        dist[i] = dist[i] / num_assigned[i];
    }

#endif

    return;
}

void Conf_ReRAM::set_local_reram_tiles(ram_distrib ram_tile_loc)
{
    //initialize local tile map
    local_reram_tile_map = new coord[x_max * y_max];
    int tile = 0;

    if (ram_mode == reram_cpu)
    {
        for (int x = 0; x < x_max; x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                local_reram_tile_map[CIND(x, y)] = { x, y, tile };
                tile++;
            }
        }
    }
    else if (ram_tile_loc == stripe)
    {
        for (int x = 0; x < x_max; x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                local_reram_tile_map[CIND(x, y)] = { x + (x % 2), y, tile };
                tile++;
            }
        }
    }
    else //half and half
    {
        for (int x = 0; x < (x_max / 2); x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                local_reram_tile_map[CIND(x, y)] = { x + (x_max / 2), y, tile };
                tile++;
            }
        }
        for (int x = (x_max / 2); x < x_max / 2; x++)
        {
            for (int y = 0; y < y_max; y++)
            {
                local_reram_tile_map[CIND(x, y)] = { x, y, tile };
                tile++;
            }
        }
    }
}
