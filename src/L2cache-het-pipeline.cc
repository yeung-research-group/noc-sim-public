#include "noc-sim.hh"

#ifdef _MSC_VER   //if visual studio
#define random rand
#endif

L2cache_het_pipeline::L2cache_het_pipeline(int x, int y) : L2cache_bank(x, y), L2cache_pipeline(x, y), L2cache_het(x,y), L2cache_standalone(x, y)
{
    dram_last = true;
}

void L2cache_het_pipeline::set_dram_out_buffer(buffer* out)
{
    dram_out = out;
}


void L2cache_het_pipeline::set_mem_out_buffer(buffer* out)
{
    mem_out = out;
}

void L2cache_het_pipeline::cycle(long long cur_cycle)
{

    //route out
    route_out(cur_cycle);
    send_ram_request(cur_cycle);
    send_dram_request(cur_cycle);

    //proceed if outputs are cleared
    if (output_queue.size() < conf->buffer_size && ram_req_queue.size() < conf->buffer_size)  //TODO: better size limit?
    {
        move_pipeline(cur_cycle);
        send_request();
    }

    //route in from memory
    if ((dram_req_queue.empty() || dram_last) && ram_return_req.size() < conf->l2_settings.rob_entries)
    {
        //if successfully read ReRAM reply, DRAM next
        dram_last = !read_ram_reply(cur_cycle);
    }
    else if (request_rob.size() < conf->l2_settings.rob_entries)
    {
        //if successfully read DRAM reply, ReRAM next
        dram_last = read_dram_reply(cur_cycle);
    }
    else
    {
        //prevent deadlock if one queue is full
        dram_last = !dram_last;
    }

    //route in from NOC
    if (request_rob.size() < conf->l2_settings.rob_entries)
        route_in(cur_cycle);
}

void L2cache_het_pipeline::writeback_req(L2_cache_line* evicted)
{
    uint64 addr = (evicted->tag) << conf->l2_settings.index_mask_offset;

    if (addr & DRAM_ADDRESS_MASK)
    {
        L2cache_het::writeback_req(evicted);
    }
    else
    {
        L2cache_standalone::writeback_req(evicted);
    }
}

void L2cache_het_pipeline::reram_send_exec(long long cur_cycle)
{
    std::stringstream errMsg;
    errMsg << "Error: (" << y_coord << ", " << x_coord << "): Invalid request type standalone cache.";
    throw std::invalid_argument(errMsg.str());
}


void L2cache_het_pipeline::mshr_update_exec(long long cur_cycle)
{

    if (mshr_stage->valid_data)  //return request from ReRAM
    {
        //write data into MSHR
        update_mshr(mshr_stage);

        //is all requested MSHR data received?
        if (check_mshr_complete(mshr_stage))
        {
            //send back read replies
            move_req(new mem_request(*mshr_stage), mshr_o, false);

            //allows deleting of requests as output without endagering the held request
            mshr_stage = new mem_request(*mshr_stage);
            mshr_stage->type = cache_fill;
            mshr_stage->next_stage = d;

            request_rob.push_front(*mshr_stage);
        }

        //all done with this stage
        delete mshr_stage;
    }
    else  //new request
    {
        first_mshr_update(cur_cycle);
    }
}

void L2cache_het_pipeline::mshr_reram_exec(long long cur_cycle)
{
    if (mshr_stage->type == read_dw)
    {
        //send to reram output queue if hasn't already
        if (mshr_stage->mshr->sectors_recv[SECTOR_ID(mshr_stage->bank)] == pending_req)
        {
            if (mshr_stage->addr & DRAM_ADDRESS_MASK)
            {
                dram_req_queue.push(*mshr_stage);
                //TODO: handle smaller than cache block DRAM fetch sizes
                for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
                {
                    if (mshr_stage->mshr->sectors_recv[i] == pending_req)
                        mshr_stage->mshr->sectors_recv[i] = sent_req;
                }
            }
            else
            {
                ram_req_queue.push(*mshr_stage);
            }
        }
    }
    else
    {
        bool pending = false;
        for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
        {
            pending |= (mshr_stage->mshr->sectors_recv[i] == pending_req);
        }

        if(pending)
        {
            if (mshr_stage->addr & DRAM_ADDRESS_MASK)
                dram_req_queue.push(*mshr_stage);
            else
                ram_req_queue.push(*mshr_stage);
            for (unsigned i = 0; i < conf->l2_settings.n_sectors; i++)
            {
                if(mshr_stage->mshr->sectors_recv[i] == pending_req)
                    mshr_stage->mshr->sectors_recv[i] = sent_req;
            }
        }
    }
}
