#pragma once

#include <list>

class L2cache_standalone : virtual public L2cache_bank
{
public:
    L2cache_standalone(int x, int y);
    void cycle(long long cur_cycle) override;

    buffer* get_mem_in_buffer() override { return (mem_in); };
    void set_mem_out_buffer(buffer* out) override;
    buffer* get_dram_in_buffer() override { return nullptr; };
    void set_dram_out_buffer(buffer* out) override { return; };


protected:
    //Network
    buffer* mem_in;
    buffer* mem_out;

    //RAM network state
    mem_request* ram_req;
    mem_request* out_ram_req;
    wb_entry* out_wb;
    noc_state ram_read_state;
    noc_state ram_write_state;

    mem_request* cur_req;
    long long cycle_complete;
    void handle_request(long long cur_cycle);

    //RAM request queue
    std::queue<mem_request> ram_req_queue;
    std::queue<mem_request> ram_return_req;

    mem_request* next_ram_request();

    void writeback_req(L2_cache_line* evicted) override;

    bool send_ram_request(long long cur_cycle);
    bool read_ram_reply(long long cur_cycle);
    mem_request* get_next_request() override;
};


