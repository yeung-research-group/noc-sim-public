#include <stdio.h>
#include <stdlib.h>
#include "noc-sim.hh"  



instruction::instruction(const instruction& ins)
{
    type = ins.type;
    mem_locs = ins.mem_locs;
    PC = ins.PC;
    vector_mask = ins.vector_mask;

    if(mem_locs > 0)
    {
        address = new uint64[mem_locs];
        for(int i = 0; i < mem_locs; i++)
        {
            address[i] = ins.address[i];
        }
    }
    else
    {
        address = nullptr;
    }
}


instruction::instruction(ins_type type)
{
  this->type = type;
  mem_locs = 0;
  address = nullptr;
  PC = 0;
  vector_mask = 0;
}

instruction::instruction(ins_type type, uint64 address_in)
{
  address = new uint64 [1];
  this->type = type;
  mem_locs = 1;
  PC = 0;
  address[0] = address_in;
  vector_mask = 0;
}

instruction::instruction(ins_type type, uint64 address_in[], int addresses)
{
  address = new uint64 [addresses];
  this->type = type;
  mem_locs = addresses;
  for (int i=0; i < addresses; i++)
  {
    address[i] = address_in[i];
  }
  PC = 0;
  vector_mask = 0;
}

instruction::instruction(ins_type type, uint64 address_in, uint64 pc)
{
  address = new uint64[1];
  this->type = type;
  mem_locs = 1;
  address[0] = address_in;
  PC = pc;
  vector_mask = 0;
}

instruction::instruction(ins_type type, uint64 address_in[], int addresses, uint64 pc,
        int vector_mask)
{
  address = new uint64[addresses];
  this->type = type;
  mem_locs = addresses;
  for (int i=0; i < addresses; i++)
  {
    address[i] = address_in[i];
  }
  this->PC = pc;
  this->vector_mask = (Conf_ReRAM::get()->max_simd_width == 8) ? 255 : 65535;
  if(type == vector_read) //DS:mask for others too
    this->vector_mask = vector_mask;
}

instruction::~instruction()
{
  if(address != nullptr)
  {
    delete [] address;
    address=nullptr;
  }
}
